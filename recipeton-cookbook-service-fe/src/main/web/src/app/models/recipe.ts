/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

class RecipeSection {
  name: string;
  ingredients: string[];
  instructions: string[];
}

class Nutrition {
  calories: string;
  carbohydrateContent: string;
  fatContent: string;
  proteinContent: string;
  fibreContent: string;
  cholesterolContent: string;
}

export declare class Recipe {
  name: string;

  totalTime: string;
  cookTime: string;
  prepTime: string;

  recipeYield: string;

  recipeCategory: string[];
  recipeSections: RecipeSection;

  hints: string[];
  utensils: string[];
  nutrition: Nutrition;

  keywords: string[];
  collections: string[];
  devices: string[];

  locale: string;
  source: string;
  author: string;
  difficulty: string;
  rating: string;

  documentVersion: string;
}
