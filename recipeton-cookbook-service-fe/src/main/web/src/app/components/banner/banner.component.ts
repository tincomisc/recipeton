/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { GROW_DOWN_ANIMATION } from './animations';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  animations: [GROW_DOWN_ANIMATION],
})
export class BannerComponent {
  // Emits one value when the user picks an action
  private _clicks?: Subject<number>;
  private active?: Observable<number>;
  private pending: {
    message: string;
    actions: string[];
    ret: Subject<number>;
  }[] = [];

  // Text message to display
  private _message?: string;

  public get message(): string | undefined {
    return this._message;
  }

  // List of button labels to show
  private _actions?: string[];

  public get actions(): string[] | undefined {
    return this._actions;
  }

  // True if the panel is opened
  public get opened(): boolean {
    return !!this._clicks;
  }

  actionClicked(idx: number): void {
    if (!this._clicks) {
      console.log(
        'Developer Error: banner action clicked but observable available!'
      );
      return;
    }

    // Click subject can only ever emit one value
    this._clicks.next(idx);
    this._clicks.complete();
    this._clicks.unsubscribe();
    this._clicks = undefined;
  }

  // Open this banner with a message and at least one action
  open(message: string, actions: string[]): Observable<number> {
    if (this._clicks) {
      throw 'Tried to open banner when outlet was already opened.';
    }

    if (actions.length === 0) {
      throw 'Tried to open banner without any action buttons.';
    }

    this._message = message;
    this._actions = actions;
    this._clicks = new Subject();
    return this._clicks.asObservable();
  }

  // Should improve with color and maybe auto-hide
  public push(message: string, actions: string[]): Observable<number> {
    if (!this.active) {
      return this.doOpen(message, actions);
    } else {
      const ret: Subject<number> = new Subject();
      this.pending.push({ message, actions, ret });
      return ret.asObservable();
    }
  }

  private doOpen(message: string, actions: string[]): Observable<number> {
    this.active = this.open(message, actions);
    this.active.subscribe((next) => {
      this.active = undefined;
      const args = this.pending.shift();
      if (args) {
        setTimeout(
          () => this.doOpen(args.message, args.actions).subscribe(args.ret),
          500
        );
      }
    });
    return this.active;
  }
}
