/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {
  animate,
  animateChild,
  group,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

// I suspect Material2 provides all this somewhere but I don't know where
let transformDuration = 200; // Desktop (default)
const transformTiming = `${transformDuration}ms cubic-bezier(0.4, 0.0, 0.2, 1)`;
const fadeInTiming = '150ms 75ms cubic-bezier(0.4, 0.0, 0.2, 1)';
const fadeOutTiming = '150ms cubic-bezier(0.4, 0.0, 0.2, 1)';

// Start at nothing and grow in height until reaching normal size
export const GROW_DOWN_ANIMATION = trigger('growDown', [
  transition(':enter', [
    // Start at 0 height, all children transparent
    style({ height: '0' }),
    query('*', [style({ opacity: '0' })], { optional: true }),
    group([
      // Start growing height, fadeInTiming includes brief delay before opacity kicks in
      animate(transformTiming, style({ height: '*' })),
      query('*', [animate(fadeInTiming, style({ opacity: '*' }))], {
        optional: true,
      }),
    ]),
    // Last, animate children
    query('@constrainWidth, @listCount', [animateChild()], { optional: true }),
  ]),
  transition(':leave', [
    style({ height: '*' }),
    query('*', [style({ opacity: '*' })], { optional: true }),
    group([
      query('@constrainWidth, @listCount', [animateChild()], {
        optional: true,
      }),
      animate(transformTiming, style({ height: '0' })),
      query('*', [animate(fadeOutTiming, style({ opacity: '0' }))], {
        optional: true,
      }),
    ]),
  ]),
]);
