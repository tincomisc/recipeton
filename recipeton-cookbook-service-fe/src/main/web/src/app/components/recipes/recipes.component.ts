/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

import chefHat from '@iconify-icons/mdi/chef-hat';
import { StorageMap } from '@ngx-pwa/local-storage';
import { TranslateService } from '@ngx-translate/core';
import { TableVirtualScrollDataSource } from 'ng-table-virtual-scroll';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AppConfig } from '../../app.config';
import { Cookbook } from '../../models/cookbook';
import { RecipeSummary } from '../../models/recipeSummary';
import { User, UserService } from '../../services/user.service';
import { CookbookService } from '../../services/cookbook.service';
import { BannerComponent } from '../banner/banner.component';
import { LoginMenuitemComponent } from '../login/login-menuitem.component';

const RECIPES_STATE = 'recipes_state';

interface PageState {
  scrollTop: number;
  searchMode: boolean;
  searchKeywordsActive: string[];
  searchModeKeywordsActive: string[];
  searchCookbookId: string;
  query: any;
}

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class RecipesComponent
  implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked
{
  displayedColumns: string[] = ['recipe', 'actions'];

  homeIcon = chefHat;

  cookbooks?: Cookbook[];
  publishedCookbook: Cookbook;

  recipeSummaries?: RecipeSummary[];
  recipeKeywords: string[];
  searchCookbook: Cookbook;

  searchMode: boolean;

  searchResults?: RecipeSummary[];

  // error: Error;
  busy: boolean;

  @ViewChild('queryInput', { static: true }) queryInput: ElementRef;
  @ViewChild('scrollViewport', { static: true })
  scrollViewport: CdkVirtualScrollViewport;
  @ViewChild('banner', { static: true }) banner: BannerComponent;
  @ViewChild('login', { static: true }) login: LoginMenuitemComponent;

  dataSource: MatTableDataSource<RecipeSummary>;
  user: User;

  private searchKeywordsActive = new Set<string>();
  private searchText: string;
  private searchModeKeywordsActive: Set<string>;

  private selectedRecipeSummary: RecipeSummary;

  private restoredScrollTop: number;
  private restoredCookbookId: string;

  constructor(
    private cookbookService: CookbookService,
    private userService: UserService,
    private router: Router,
    private matSnackBar: MatSnackBar,
    private translateService: TranslateService,
    private dialog: MatDialog,
    private storage: StorageMap
  ) {}

  doPublish(cookbookId: string) {
    this.busy = true;
    this.cookbookService.postPublishRequest({ id: cookbookId }).subscribe(
      (d) => {
        console.log(d);
        this.busy = false;
        this.translateService.get('Cookbook published').subscribe((t) => {
          this.matSnackBar.open(t, 'OK', {
            duration: AppConfig.snackBarDuration,
          });
        });
      },
      (e) => {
        this.busy = false;
        this.onError(e);
      }
    );
  }

  fetchCookbooks() {
    console.debug('fetchCookbooks');
    this.busy = true;
    this.cookbookService.getAll().subscribe(
      (cs) => {
        let expectedStorageCookbookIds = cs.map(
          (c) => AppConfig.cookbookStoragePrefix + c.id
        );

        this.storage.keys().subscribe((key) => {
          if (key.startsWith(AppConfig.cookbookStoragePrefix)) {
            if (expectedStorageCookbookIds.indexOf(key) == -1) {
              console.debug(`removing obsolete book key=${key}`);
              this.storage.delete(key).subscribe(() => {});
            }
          }
        });

        cs.forEach((c) => {
          this.storage
            .get(AppConfig.cookbookStoragePrefix + c.id)
            .subscribe((rs) => {
              if (rs === undefined) {
                console.debug('fetchCookbookContent ', c.id);
                this.cookbookService.getContents(c.id).subscribe(
                  (d) => {
                    d.contents.forEach((rs) => {
                      let terms = CookbookService.toTerms(rs.name);
                      rs.cookbookId = c.id;
                      rs.terms = terms;
                    });
                    c.contents = d.contents.sort((a, b) =>
                      a.name.localeCompare(b.name)
                    );
                    this.onDataReceived();
                    this.storage
                      .set(AppConfig.cookbookStoragePrefix + c.id, d)
                      .subscribe(() => {});
                  },
                  (e) => {
                    this.onError(e);
                  }
                );
              } else {
                // @ts-ignore
                c.contents = rs.contents;
                this.onDataReceived();
              }
            });
        });

        this.cookbooks = cs;
        if (this.restoredCookbookId != undefined) {
          this.searchCookbook = this.cookbooks.find(
            (c) => c.id == this.restoredCookbookId
          );
          this.restoredCookbookId = undefined;
        }
        this.onDataReceived();
      },
      (e) => {
        this.onError(e);
        this.busy = false;
      }
    );
  }

  ngAfterViewChecked(): void {
    if (this.restoredScrollTop !== undefined) {
      let currentScrollTop = this.scrollViewport.measureScrollOffset();
      if (currentScrollTop == this.restoredScrollTop) {
        // console.log("set!");
        this.restoredScrollTop = undefined;
        return;
      }
      this.scrollViewport.scrollToOffset(this.restoredScrollTop);
    }
  }

  isSearchTermsTitle() {
    return (
      this.searchCookbook !== undefined || this.getSearchKeywords().size > 0
    );
  }

  getSearchKeywords() {
    return this.searchMode
      ? this.searchModeKeywordsActive
      : this.searchKeywordsActive;
  }

  getThumbnailUrl(recipeSummary: RecipeSummary) {
    return this.cookbookService.getThumbnailUrl(
      recipeSummary.cookbookId,
      recipeSummary.id
    );
  }

  isCookbookPublished(cookbook: Cookbook) {
    return cookbook == this.publishedCookbook;
  }

  isKeywordSelected(keyword: string) {
    return this.getSearchKeywords().has(keyword);
  }

  isPublishDisabled() {
    return this.busy || this.selectedRecipeSummary == undefined;
  }

  isPublished(recipeSummary: RecipeSummary) {
    return (
      this.publishedCookbook !== undefined &&
      recipeSummary.cookbookId == this.publishedCookbook.id
    );
  }

  isSelected(recipeSummary: RecipeSummary) {
    return this.selectedRecipeSummary == recipeSummary;
  }

  ngAfterViewInit() {
    this.storage.get(RECIPES_STATE).subscribe((d) => {
      if (d !== undefined) {
        let pageState = d as PageState;
        console.debug('restore ', pageState);
        this.searchMode = pageState.searchMode;

        this.searchKeywordsActive = new Set<string>(
          pageState.searchKeywordsActive
        );
        if (pageState.searchModeKeywordsActive != undefined) {
          this.searchModeKeywordsActive = new Set<string>(
            pageState.searchModeKeywordsActive
          );
        }
        // INCOGNITO MODE in FF Now does not unserialize sets!!
        // this.searchKeywordsActive = pageState.searchKeywordsActive;
        // this.searchModeKeywordsActive = pageState.searchModeKeywordsActive;

        if (pageState.query !== undefined) {
          this.queryInput.nativeElement.value = pageState.query;
        }
        this.restoredScrollTop = pageState.scrollTop;
        this.restoredCookbookId = pageState.searchCookbookId;

        this.storage.delete(RECIPES_STATE).subscribe(() => {});
      }

      this.fetchCookbooks();
    });

    fromEvent(this.queryInput.nativeElement, 'keyup')
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe(() => {
        this.doSearch();
      });
  }

  onError(e: any) {
    console.log('onError', e);
    if (e['status'] == 403 || e['status'] == 401) {
      this.banner
        .push(this.translateService.instant('error_credentials'), [
          'LOGIN',
          'IGNORE',
        ])
        .subscribe((o) => {
          if (o == 0) {
            this.login.doLogin();
          }
        });

      return;
    }
    this.banner.push(e.message ? e.message : e, ['OK']).subscribe((o) => {
      // Just dismiss
      // console.log("onBannerClicked", o);
    });
  }

  ngOnDestroy(): void {
    this.persistState();
  }

  onThumbnailClicked(recipeSummary: RecipeSummary) {
    this.router.navigate([
      `${AppConfig.routes.recipeton}/cookbook/${recipeSummary.cookbookId}/recipe/${recipeSummary.id}`,
    ]);
  }

  ngOnInit(): void {
    this.busy = true;
    this.dataSource = new TableVirtualScrollDataSource<RecipeSummary>();
  }

  onCookbookClicked(cookbook: Cookbook) {
    if (this.searchCookbook !== cookbook) {
      this.searchCookbook = cookbook;
    } else {
      this.searchCookbook = undefined;
    }
    this.doSearch();
    // this.router.navigate([`${AppConfig.routes.recipeton}/cookbook/${cookbook.id}`]);
  }

  onKeywordClicked(keyword: string) {
    let searchKeywords = this.getSearchKeywords();
    if (searchKeywords.has(keyword)) {
      searchKeywords.delete(keyword);
    } else {
      searchKeywords.add(keyword);
      searchKeywords.add(keyword);
    }
    this.doSearch();
  }

  onPublishClicked() {
    if (this.selectedRecipeSummary === undefined) {
      return;
    }

    this.doPublish(this.selectedRecipeSummary.cookbookId);
  }

  onRowClicked(recipeSummary: RecipeSummary) {
    if (this.selectedRecipeSummary == recipeSummary) {
      this.router.navigate([
        `${AppConfig.routes.recipeton}/cookbook/${recipeSummary.cookbookId}/recipe/${recipeSummary.id}`,
      ]);
    } else {
      this.setSelected(recipeSummary);
    }
  }

  onSearchButtonClicked() {
    if (this.searchMode) {
      // clear
      this.doSearchClear();
    } else {
      this.doSearchPrepare();
    }
  }

  private doSearch() {
    if (this.recipeSummaries === undefined) {
      console.log('No data to search');
      return;
    }

    let searchText = this.getSearchTermsFieldValue();
    console.debug('computeSearch', searchText, this.getSearchKeywords());

    let searchTextEmpty = searchText === undefined || searchText.length < 3;
    let searchKeywordsEmpty = this.getSearchKeywords().size == 0;
    let searchCookbookEmpty = this.searchCookbook === undefined;

    let searchCriteriaEmpty =
      searchTextEmpty && searchKeywordsEmpty && searchCookbookEmpty;
    if (searchCriteriaEmpty) {
      if (this.searchResults !== undefined) {
        console.debug('Search terms clearing');
        this.searchText = undefined;
        this.searchResults = undefined;
        this.updateDataSource();
        console.debug('Search terms cleared');
      }
      return;
    }

    let searchTokens;
    if (!searchTextEmpty) {
      searchTokens = CookbookService.toTerms(searchText);
      this.searchText = searchTokens.join(' ');
    }

    let searchKeywords;
    if (!searchKeywordsEmpty) {
      searchKeywords = Array.from(this.getSearchKeywords());
    }

    let searchCookbookId;
    if (!searchCookbookEmpty) {
      searchCookbookId = this.searchCookbook.id;
    }

    this.busy = true;
    let searchResults = this.recipeSummaries.filter((r) => {
      // Can optimize speed. Terms are sorted
      if (this.searchCookbook !== undefined) {
        if (r.cookbookId !== searchCookbookId) {
          return false;
        }
      }
      if (searchTokens !== undefined) {
        let matches = searchTokens.filter(
          (st) => r.terms.find((t) => t.startsWith(st)) !== undefined
        );
        let match = matches.length == searchTokens.length;
        if (!match) {
          return false;
        }
      }
      if (searchKeywords !== undefined) {
        let matches = searchKeywords.filter(
          (c) => r.keywords.find((k) => k == c) !== undefined
        );
        let match = matches.length == searchKeywords.length;
        if (!match) {
          return false;
        }
      }
      return true;
    });

    this.selectedRecipeSummary = undefined;
    this.searchResults = searchResults;
    this.updateDataSource();
    this.busy = false;
    return true;
  }

  private doSearchClear() {
    this.queryInput.nativeElement.value = '';
    if (this.searchModeKeywordsActive.size < this.searchKeywordsActive.size) {
      this.searchKeywordsActive = this.searchModeKeywordsActive;
    }
    this.searchModeKeywordsActive = undefined;
    this.searchMode = false;
    this.doSearch();
  }

  private doSearchPrepare() {
    this.searchMode = true;
    this.searchModeKeywordsActive = new Set(this.searchKeywordsActive);
    this.queryInput.nativeElement.focus();
  }

  private getSearchTermsFieldValue() {
    return this.queryInput.nativeElement.value
      ? this.queryInput.nativeElement.value
      : undefined;
  }

  private onDataReceived() {
    if (this.cookbooks == undefined) {
      return;
    }
    let undefinedCount = this.cookbooks
      .map((rc) => rc.contents)
      .filter((t) => t === undefined).length;
    if (undefinedCount == 0) {
      this.onDataReceivedComplete();
    }
  }

  private onDataReceivedComplete() {
    console.log('onDataReceivedComplete');

    let recipes = [];
    this.cookbooks.forEach((c) => {
      if (c.published) {
        this.publishedCookbook = c;
      }
      recipes = recipes.concat(c.contents);
    });

    let aux = new Set<string>();
    recipes.forEach((r) => {
      r.keywords.forEach((k) => {
        aux.add(k);
      });
    });
    this.recipeKeywords = Array.from(aux).sort();

    let sorted = recipes.sort((a, b) => a.name.localeCompare(b.name));
    this.recipeSummaries = sorted;

    this.busy = false;
    this.doSearch();
  }

  private setSelected(recipeSummary: RecipeSummary) {
    this.selectedRecipeSummary = recipeSummary;
  }

  private persistState() {
    let state: PageState = {
      scrollTop: this.scrollViewport.measureScrollOffset(),
      searchMode: this.searchMode,
      // INCOGNITO MODE in FF Now does not unserialize sets!!
      // searchKeywordsActive: this.searchKeywordsActive,
      // searchModeKeywordsActive: this.searchModeKeywordsActive,
      searchKeywordsActive: Array.from(this.searchKeywordsActive),
      searchModeKeywordsActive:
        this.searchModeKeywordsActive == undefined
          ? undefined
          : Array.from(this.searchModeKeywordsActive),
      searchCookbookId:
        this.searchCookbook !== undefined ? this.searchCookbook.id : undefined,
      // "searchText": this.searchText,
      query: this.getSearchTermsFieldValue(),
    };
    console.log('persisting ', state);
    this.storage.set(RECIPES_STATE, state).subscribe(() => {
      console.log('persisted');
    });
  }

  private updateDataSource() {
    console.log('updateDataSource');
    if (this.searchResults !== undefined) {
      this.dataSource.data = this.searchResults;
      // this.scrollViewport.scrollTo({
      //   behavior: 'smooth',
      //   top: 6000
      // });
    } else {
      this.dataSource.data = [];
    }
  }
}
