/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { Component, Inject, ViewChild } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface Credentials {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
})
export class LoginDialogComponent {
  @ViewChild('form', { static: true }) formElement;
  @ViewChild('formSubmit', { static: true }) formSubmitElement;
  loginForm: UntypedFormGroup;

  constructor(
    private dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private formBuilder: UntypedFormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      username: new UntypedFormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      password: new UntypedFormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
    });
  }

  onFormSubmitted(): void {
    console.log('onFormSubmitted');
    this.validateAndClose();
  }

  onOkClicked(): void {
    console.log('onOkClicked');
    this.validateAndClose();
  }

  private validateAndClose() {
    if (!this.loginForm.valid) {
      console.log('invalidForm');
      return;
    }
    const formValue = this.loginForm.value;
    this.dialogRef.close(formValue);
  }
}
