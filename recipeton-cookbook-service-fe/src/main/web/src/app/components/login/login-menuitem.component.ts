/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppConfig } from '../../app.config';
import { User, UserService } from '../../services/user.service';
import { LoginDialogComponent } from './login-dialog.component';
import { delay, startWith, tap } from 'rxjs/operators';

@Component({
  selector: 'app-login-menuitem',
  templateUrl: './login-menuitem.component.html',
})
export class LoginMenuitemComponent implements AfterViewInit {
  user: User;
  @Output() loginError = new EventEmitter<string>();

  constructor(
    private userService: UserService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  doLogin() {
    this.openDialog().subscribe(
      (u) => {
        console.log('login result ', u);
        if (u) {
          this.user = u;
        }
      },
      (e) => {
        console.log('login error ', e);
        this.loginError.emit(e);
      }
    );
  }

  ngAfterViewInit(): void {
    this.fetchUser();
  }

  onLoginClicked(): void {
    this.doLogin();
  }

  onLogoutClicked(): void {
    console.log('logout clicked');
    this.userService.logout().subscribe(
      () => {
        console.log('logout success');
        this.router
          .navigateByUrl(AppConfig.routes.recipeton)
          .then(() => this.fetchUser());
      },
      (e) => {
        console.log('logout error', e);
        this.userService.clearAuthenticatedUser();
        this.router
          .navigateByUrl(AppConfig.routes.recipeton)
          .then(() => this.fetchUser());
      }
    );
  }

  private fetchUser() {
    this.userService
      .getAuthenticatedUser()
      .pipe(
        // Construction to avoid changed after check
        startWith(this.user),
        delay(0),
        tap((u) => (this.user = u))
      )
      .subscribe();
  }

  private openDialog(): Observable<User> {
    return new Observable<User>((o) => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {};
      const dialogRef = this.dialog.open(LoginDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(
        (dr) => {
          if (dr) {
            this.userService.login(dr.username, dr.password).subscribe(
              (u) => {
                o.next(u);
              },
              (e) => {
                o.error(e);
              }
            );
          } else {
            o.next(undefined);
          }
        },
        (e) => {
          o.error(e);
        }
      );
    });
  }
}
