/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfig } from '../app.config';

export interface User {
  name: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private initialized = false;
  private authenticatedUser: User;
  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = AppConfig.endpoints.userApi;
  }

  getAuthenticatedUser(): Observable<User> {
    if (!this.initialized) {
      this.initialized = true;
      return this.getUserMe({});
    }
    return new Observable((o) => {
      o.next(this.authenticatedUser);
    });
  }

  login(username, password): Observable<User> {
    console.log('login', username);
    let options = {
      headers: new HttpHeaders({
        authorization: 'Basic ' + btoa(username + ':' + password),
      }),
    };
    return this.getUserMe(options);
  }

  logout() {
    return this.http.post('logout', {}).pipe(
      tap(() => {
        console.log('logout response');
        this.clearAuthenticatedUser();
      })
    );
  }

  clearAuthenticatedUser() {
    this.authenticatedUser = undefined;
  }

  private getUserMe(options: {}): Observable<User> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/me`, options).pipe(
      tap((r) => {
        if (r != null && r['name']) {
          this.authenticatedUser = <User>r;
        } else {
          this.clearAuthenticatedUser();
        }
      })
    );
  }
}
