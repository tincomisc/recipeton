package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "tmControlTime")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class TmControlTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "tmControl_id", nullable = false)
    private Long tmControlId;

    @OneToOne
    @JoinColumn(name = "tmControl_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private TmControl tmControl;

    @ManyToOne
    @JoinColumn(name = "tmControlTimeType_id", nullable = false)
    @ToString.Include
    private TmControlTimeType tmControlTimeType;

    @OneToMany(mappedBy = "tmControlTime", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private List<TmControlTimeRange> tmControlTimeRanges;

    @PrePersist
    public void prePersist() {
        toStream(tmControlTimeRanges).filter(e -> e.getTmControlTime() == null).forEach(e -> e.setTmControlTime(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TmControlTime other = (TmControlTime) o;

        return tmControlId != null && tmControlId.equals(other.tmControlId);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }


}
