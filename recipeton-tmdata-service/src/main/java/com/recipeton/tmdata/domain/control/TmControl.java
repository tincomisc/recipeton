package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.control.lang.TmControlLang;
import com.recipeton.tmdata.domain.recipe.RecipeStepTmControl;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "tmControl")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class TmControl implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tmControlTemperatureType_id", nullable = false)
    private TmControlTemperatureType tmControlTemperatureType;

    @Column(name = "position", nullable = false)
    private Long position;

    @OneToOne(mappedBy = "tmControl", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @ToString.Include
    private TmControlTime tmControlTime;

    @OneToOne(mappedBy = "tmControl", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @ToString.Include
    private TmControlSpeed tmControlSpeed;

    @OneToOne(mappedBy = "tmControl", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @ToString.Include
    private TmControlProgram tmControlProgram;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "tmControl", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Include
    private Collection<TmControlLang> langs;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "tmControl", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<RecipeStepTmControl> recipeStepTmControls = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getTmControl() == null).forEach(n -> n.setTmControl(this));
        toStream(recipeStepTmControls).filter(n -> n.getTmControl() == null).forEach(n -> n.setTmControl(this));

        if (tmControlProgram != null) {
            tmControlProgram.setTmControl(this);
        }
        if (tmControlSpeed != null) {
            tmControlSpeed.setTmControl(this);
        }
        if (tmControlTime != null) {
            tmControlTime.setTmControl(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TmControl other = (TmControl) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

}
