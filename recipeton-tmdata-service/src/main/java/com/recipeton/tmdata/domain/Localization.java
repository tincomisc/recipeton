package com.recipeton.tmdata.domain;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.data.util.Pair;

import java.io.ByteArrayOutputStream;
import java.text.Normalizer;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.util.Pair.of;


public interface Localization<T> {

    // Could Increase speed by using sparse.
    Map<Character, byte[]> SORT_KEY_MAP = Stream.of(
            of('\\', "0bbdbfef"),
            of(' ', "04"),
            of(')', "0abdbfef"),
            of('(', "0abdbfef"),
            of('-', "0646"),
            of(',', "07"),
            of('/', "0bbdbfef3d370a6b0a670bbdbfef"),
            of('.', "09"),
            of('1', "14"),
            of('0', "12"),
            of('3', "18"),
            of('2', "16"),
            of('5', "1c"),
            of('4', "1a"),
            of('7', "20"),
            of('6', "1e"),
            of('9', "24"),
            of('8', "22"),
            of('<', "0a67"),
            of('>', "0a6b"),
            of('a', "27"),
            of('b', "29"),
            of('c', "2b"),
            of('d', "2d"),
            of('e', "2f"),
            of('f', "31"),
            of('g', "33"),
            of('h', "35"),
            of('i', "37"),
            of('j', "39"),
            of('k', "3b"),
            of('l', "3d"),
            of('n', "41"),
            of('m', "3f"),
            of('o', "43"),
            of('p', "45"),
            of('q', "47"),
            of('r', "49"),
            of('s', "4b"),
            of('t', "4d"),
            of('u', "4f"),
            of('v', "51"),
            of('w', "53"),
            of('x', "55"),
            of('y', "57"),
            of('z', "59")
    ).collect(Collectors.toMap(Pair::getFirst, characterStringPair -> {
        try {
            String hexString = characterStringPair.getSecond();
            return Hex.decodeHex(hexString.toCharArray());
        } catch (DecoderException e) {
            throw new IllegalArgumentException("Invalid mapping", e);
        }
    }));

    static byte[] toSortKey(String text) {
        if (text == null) {
            return null;
        }
        String normalizedText = Normalizer.normalize(text, Normalizer.Form.NFC);

        ByteArrayOutputStream bo = new ByteArrayOutputStream(); // close no effect
        normalizedText.chars().forEach(c -> {
                    int lowerCase = Character.toLowerCase(c);
                    byte[] bytes = SORT_KEY_MAP.get((char) lowerCase);
                    if (bytes != null) {
                        bo.write(bytes, 0, bytes.length);
                    }
                }
        );
        return bo.toByteArray();
    }

    Locale getLocale();

    String getText();

    void prePersist();

    T setSortKey(String sortKey);

    default String toSortKey() {
        byte[] bytes = toSortKey(getText());
        return bytes == null ? null : new String(bytes);
    }
}
