package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "recipeLink")
public class RecipeLink implements Serializable {

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "targetRecipe_id")
    @MapsId("targetRecipeId")
    private Recipe targetRecipe;

    @ManyToOne
    @JoinColumn(name = "sourceRecipe_id")
    @MapsId("sourceRecipeId")
    private Recipe sourceRecipe;

    @ManyToOne
    @JoinColumn(name = "recipeLinkType_id")
    @MapsId("recipeLinkTypeId")
    private RecipeLinkType recipeLinkType;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeLink other = (RecipeLink) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    @Data
    public static class ID implements Serializable {
        @Column(name = "targetRecipe_id", nullable = false)
        private Long targetRecipeId;

        @Column(name = "sourceRecipe_id", nullable = false)
        private Long sourceRecipeId;

        @Column(name = "recipeLinkType_id", nullable = false)
        private Long recipeLinkTypeId;
    }

}
