package com.recipeton.tmdata.domain.maintenance;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "maintenanceRecipeHash")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MaintenanceRecipeHash implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "maintenanceCloudSyncHash_id", nullable = false)
    private Long maintenancecloudsynchashId;

    @Column(name = "recipe_id", nullable = false)
    private Long recipeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MaintenanceRecipeHash other = (MaintenanceRecipeHash) o;

        return maintenancecloudsynchashId != null && maintenancecloudsynchashId.equals(other.maintenancecloudsynchashId);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

}
