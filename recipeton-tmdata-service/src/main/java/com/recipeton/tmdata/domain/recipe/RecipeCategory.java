package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.lang.RecipeCategoryLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeCategoryLangAttribute;
import com.recipeton.util.jpa.IdentityFallbackGenerator;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "recipeCategory")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeCategory implements Localized, Serializable {

    public static final String DEFAULT_MATERIAL = "CATEGORY_default.png";

    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name = IdentityFallbackGenerator.NAME, strategy = IdentityFallbackGenerator.ID)
    @GeneratedValue(generator = IdentityFallbackGenerator.NAME)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uid", nullable = false)
    @ToString.Include
    private String uid;

    @Column(name = "position", nullable = false)
    @ToString.Include
    private Long position;

    @Column(name = "isPrimary", nullable = false)
    private boolean primary;

    @Column(name = "lft", nullable = false)
    private Long lft;

    @Column(name = "rgt", nullable = false)
    private Long rgt;

    @OneToMany(mappedBy = "recipeCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<RecipeCategoryLang> langs;

    @OneToMany(mappedBy = "recipeCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<RecipeCategoryMaterialObject> recipeCategoryMaterialObjects;

    @ManyToMany(mappedBy = "recipeCategories")
    @EqualsAndHashCode.Exclude
    private Collection<Recipe> recipes;

    @PrePersist
    public void prePersist() {
        toStream(recipeCategoryMaterialObjects).filter(n -> n.getRecipeCategory() == null).forEach(n -> n.setRecipeCategory(this));
        toStream(langs).filter(n -> n.getRecipeCategory() == null).forEach(n -> n.setRecipeCategory(this));
    }

    public String toMainLang(Locale defaultLocale) {
        return getLangs().stream()
                .filter(l -> l.getAttribute().getId().equals(RecipeCategoryLangAttribute.Enum.NAME.getId()))
                .min((o1, o2) -> {
                    Long l1 = o1.getLocale().getId();
                    if (l1.equals(defaultLocale.getId())) {
                        l1 = -1L;
                    }
                    Long l2 = o2.getLocale().getId();
                    if (l2.equals(defaultLocale.getId())) {
                        l2 = -1L;
                    }
                    return (int) (l1 - l2);
                })
                .map(RecipeCategoryLang::getText)
                .get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeCategory other = (RecipeCategory) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

}
