package com.recipeton.tmdata.domain.material;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.misc.Company;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "material")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class Material implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "materialType_id", nullable = false)
    private MaterialType materialtype;

    @Column(name = "mdbId")
    @ToString.Include
    private Long mdbId;  // For recipe png! Set to recipe id??

    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "material_materialStorageLocation", joinColumns = @JoinColumn(name = "material_id"), inverseJoinColumns = @JoinColumn(name = "materialStorageLocation_id"))
    @ToString.Include
    private Collection<MaterialStorageLocation> materialStorageLocations;

    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "material_company", joinColumns = @JoinColumn(name = "material_id"), inverseJoinColumns = @JoinColumn(name = "company_id"))
    private Collection<Company> companies;

    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "material_materialFlag", joinColumns = @JoinColumn(name = "material_id"), inverseJoinColumns = @JoinColumn(name = "materialFlag_id"))
    @ToString.Include
    private Collection<MaterialFlag> materialFlags;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Material other = (Material) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

}
