package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Entity
@Table(name = "guidedCreatedIngredientStep")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedCreatedIngredientStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "guidedStep_id", nullable = false)
    private Long guidedStepId;

    @OneToOne
    @JoinColumn(name = "guidedStep_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private GuidedStep guidedStep;

    @OneToMany(mappedBy = "guidedCreatedIngredientStep", cascade = CascadeType.ALL, orphanRemoval = true)
    @EqualsAndHashCode.Exclude
    private Collection<GuidedCreatedIngredientAmountRange> guidedCreatedIngredientAmountRanges;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "guidedCreatedIngredientNotation_id", nullable = false)
    private GuidedCreatedIngredientNotation guidedCreatedIngredientNotation;

    @ManyToOne // No cascade, these are shared
    @JoinColumn(name = "guidedCreatedIngredientStepText_id", nullable = false)
    @ToString.Include
    private GuidedCreatedIngredientStepText guidedCreatedIngredientStepText;

    @ManyToOne
    @JoinColumn(name = "guidedIngredientStepWeighingAttribute_id", nullable = false)
    @ToString.Include
    private GuidedIngredientStepWeighingAttribute guidedIngredientStepWeighingAttribute;

    @ManyToOne
    @JoinColumn(name = "unitNotation_id")
    private UnitNotation unitNotation;

    @PrePersist
    public void prePersist() {
        toStream(guidedCreatedIngredientAmountRanges).filter(e -> e.getGuidedCreatedIngredientStep() == null).forEach(e -> e.setGuidedCreatedIngredientStep(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GuidedCreatedIngredientStep other = (GuidedCreatedIngredientStep) o;

        return guidedStepId != null && guidedStepId.equals(other.guidedStepId);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }
}
