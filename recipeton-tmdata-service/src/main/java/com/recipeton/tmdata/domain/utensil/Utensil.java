package com.recipeton.tmdata.domain.utensil;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.Localized;
import com.recipeton.tmdata.domain.utensil.lang.UtensilLang;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "utensil")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class Utensil implements Localized, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "uid", nullable = false)
    @ToString.Include
    private String uid;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "utensilType_id", nullable = false)
    @ToString.Include
    private UtensilType utensilType;

    @Column(name = "version", nullable = false)
    private BigDecimal version;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "utensil", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Include
    private Collection<UtensilLang> langs;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "utensil", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("position")
    @ToString.Include
    private List<UtensilMaterialObject> utensilMaterialObjects;

    @PrePersist
    public void prePersist() {
        toStream(langs).filter(n -> n.getUtensil() == null).forEach(n -> n.setUtensil(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Utensil other = (Utensil) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

}
