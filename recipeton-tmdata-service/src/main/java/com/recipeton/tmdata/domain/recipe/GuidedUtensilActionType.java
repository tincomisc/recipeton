package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag;
import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "guidedUtensilActionType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class GuidedUtensilActionType implements Serializable {

    public static final List<GuidedUtensilActionType> DEFAULT = Stream.of(GuidedUtensilActionType.Enum.values()).map(e -> new GuidedUtensilActionType(e.id, e.text)).collect(Collectors.toList());
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    @ToString.Include
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GuidedUtensilActionType other = (GuidedUtensilActionType) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public enum Enum {
        SIMMERING_BASKET_PUT(1L, "insert simmering basket", RecipeToolComposedActionTag.SIMMERING_BASKET_PUT_INSIDE, "utensil_action_insert_simmering_basket.png"),
        SIMMERING_BASKET_REMOVE(2L, "remove simmering basket", RecipeToolComposedActionTag.SIMMERING_BASKET_REMOVE, "utensil_action_remove_simmering_basket.png"),
        STEAMER_TRAY_PUT(3L, "insert varoma tray", RecipeToolComposedActionTag.STEAMER_TRAY_PUT, "utensil_action_insert_varoma_tray.png"),
        STEAMER_TRAY_REMOVE(4L, "remove varoma tray", RecipeToolComposedActionTag.STEAMER_TRAY_REMOVE, "utensil_action_remove_varoma_tray.png"),
        STEAMER_PUT(5L, "place varoma into position", RecipeToolComposedActionTag.STEAMER_PUT, "utensil_action_place_varoma_into_position.png"),
        STEAMER_REMOVE(6L, "set varoma aside", RecipeToolComposedActionTag.STEAMER_REMOVE, "utensil_action_set_varoma_aside.png"),
        SIMMERING_BASKET_AS_LID_PUT(7L, "place simmering basket instead of measuring cup onto lid", RecipeToolComposedActionTag.SIMMERING_BASKET_AS_LID_PUT, "utensil_action_place_simmering_basket_instead_of_measuring_cup_onto_lid.png"),
        SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT(8L, "remove simmering basket insert measuring cup", RecipeToolComposedActionTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, "utensil_action_remove_simmering_basket_insert_measuring_cup.png"),
        MIXING_BOWL_REMOVE(9L, "remove mixing bowl", RecipeToolComposedActionTag.MIXING_BOWL_REMOVE, "utensil_action_remove_mixing_bowl.png"),
        MIXING_BOWL_PUT(10L, "place mixing bowl into position", RecipeToolComposedActionTag.MIXING_BOWL_PUT, "utensil_action_place_mixing_bowl_into_position.png"),
        TRANSFER_TO_BOWL(11L, "transfer into a bowl and set aside", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, "utensil_action_transfer_into_a_bowl_and_set_aside.png"),
        MIXING_BOWL_EMPTY(12L, "empty mixing bowl", RecipeToolComposedActionTag.MIXING_BOWL_EMPTY, "utensil_action_empty_mixing_bowl.png"),
        BUTTERFLY_PUT(13L, "insert butterfly whisk", RecipeToolComposedActionTag.WHISK_PUT, "utensil_action_insert_butterfly_whisk.png"),
        BUTTERFLY_REMOVE(14L, "remove butterfly whisk", RecipeToolComposedActionTag.WHISK_REMOVE, "utensil_action_remove_butterfly_whisk.png"),
        MIXING_BOWL_CLEAN(15L, "clean mixing bowl", RecipeToolComposedActionTag.MIXING_BOWL_CLEAN, "utensil_action_clean_mixing_bowl.png"),

        MEASURING_CUP_NOTUSE(16L, "without measuring cup", RecipeToolComposedActionTag.MEASURING_CUP_NOTUSE, "utensil_action_without_measuring_cup.png"),
        MEASURING_CUP_REMOVE(17L, "remove measuring cup", RecipeToolComposedActionTag.MEASURING_CUP_REMOVE, "utensil_action_remove_measuring_cup.png"),
        MEASURING_CUP_PUT(18L, "insert measuring cup", RecipeToolComposedActionTag.MEASURING_CUP_PUT, "utensil_action_insert_measuring_cup.png"),

        SPATULA_MIX(19L, "mix with aid of spatula", RecipeToolComposedActionTag.SPATULA_MIX, "utensil_action_mix_with_aid_of_spatula.png"),
        SPATULA_SCRAP_SIDE(20L, "scrape down sides of mixing bowl with spatula", RecipeToolComposedActionTag.SPATULA_SCRAP_SIDE, "utensil_action_scrape_down_sides_of_mixing_bowl_with_spatula.png"),
        MEASURING_CUP_HOLD_VIBRATION(21L, "hold measuring cup to prevent it from vibrating", RecipeToolComposedActionTag.MEASURING_CUP_HOLD_VIBRATION, "utensil_action_hold_measuring_cup_to_prevent_it_from_vibrating.png"),
        OVEN_PREHEAT(22L, "preheat oven", RecipeToolComposedActionTag.OVEN_OPERATION, "utensil_action_preheat_oven.png"),
        REFRIGERATE(23L, "refrigerate", RecipeToolComposedActionTag.REFRIGERATE, "utensil_action_refrigerate.png"),
        FREEZE(24L, "freeze", RecipeToolComposedActionTag.FREEZE, "utensil_action_freeze.png"),
        STEAMER(25L, "varoma", RecipeToolComposedActionTag.STEAMER_CLOSE, "utensil_action_varoma.png"),
        STEAMER_DISH_REMOVE(26L, "take varoma dish", RecipeToolComposedActionTag.STEAMER_DISH_REMOVE, "utensil_action_take_varoma_dish.png"),
        SIMMERING_BASKET_AS_LID_REMOVE(27L, "remove simmering basket from mixing bowl lid", RecipeToolComposedActionTag.SIMMERING_BASKET_AS_LID_REMOVE, "utensil_action_remove_simmering_basket_from_mixing_bowl_lid.png"),
        SUMMERING_BASKET(28L, "simmering basket", RecipeToolComposedActionTag.SIMMERING_BASKET_OTHER, "utensil_action_simmering_basket.png"),
        BUTTERFLY(29L, "butterfly whisk", RecipeToolComposedActionTag.WHISK, "utensil_action_butterfly_whisk.png"),
        MEASURING_CUP(30L, "measuring cup", RecipeToolComposedActionTag.MEASURING_CUP, "utensil_action_measuring_cup.png"),
        SPATULA(31L, "spatula", RecipeToolComposedActionTag.SPATULA, "utensil_action_spatula.png"),
        SPATULA_MIX_WELL(32L, "mix well with spatula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, "utensil_action_mix_well_with_spatula.png"),
        MIXING_BOWL(33L, "mixing bowl", RecipeToolComposedActionTag.MIXING_BOWL, "utensil_action_mixing_bowl.png"),
        THERMOMIX_PARTS(34L, "thermomix parts", RecipeToolComposedActionTag.OTHER_OPERATION, "utensil_action_thermomix_parts.png"),
        USEFUL_ITEMS(35L, "useful items", RecipeToolComposedActionTag.USEFUL_ITEMS, "utensil_action_useful_items.png"),
        KITCHEN_EQUIPMENT(36L, "kitchen equipment", RecipeToolComposedActionTag.KITCHEN_EQUIPMENT, "utensil_action_kitchen_equipment.png"),
        STEAMER_DISH_PUT(37L, "place varoma dish into position", RecipeToolComposedActionTag.STEAMER_DISH_PUT, "utensil_action_place_varoma_dish_into_position.png");

        private final long id;
        private final String text;
        private final RecipeToolComposedActionTag tag;
        private final String defaultIcon;

        Enum(Long id, String text, RecipeToolComposedActionTag tag, String defaultIcon) {
            this.id = id;
            this.text = text;
            this.tag = tag;
            this.defaultIcon = defaultIcon;
        }

        public static Enum by(RecipeToolComposedActionTag tag) {
            if (tag == null) {
                return null;
            }
            return Stream.of(values()).filter(v -> v.tag.equals(tag)).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid action found with value=" + tag + "."));
        }

        public String getDefaultIcon() {
            return defaultIcon;
        }

        public Long getId() {
            return id;
        }

        public String getText() {
            return text;
        }
    }


}
