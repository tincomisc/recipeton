package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "guidedUtensilStep")
@ToString(onlyExplicitlyIncluded = true)
public class GuidedUtensilStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "guidedStep_id", nullable = false)
    private Long guidedStepId;

    @OneToOne
    @JoinColumn(name = "guidedStep_id")
    @MapsId
    @EqualsAndHashCode.Exclude
    private GuidedStep guidedStep;

    @ManyToOne
    @JoinColumn(name = "guidedUtensilActionType_id", nullable = false)
    @ToString.Include
    private GuidedUtensilActionType guidedUtensilActionType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "guidedUtensilActionText_id", nullable = false)
    @ToString.Include
    private GuidedUtensilActionText guidedUtensilActionText;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GuidedUtensilStep other = (GuidedUtensilStep) o;

        return guidedStepId != null && guidedStepId.equals(other.guidedStepId);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }
}
