package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.material.MaterialObject;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "guidedStep_materialObject")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class GuidedStepMaterialObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @MapsId("guidedStepId")
    @JoinColumn(name = "guidedStep_id")
    private GuidedStep guidedStep;

    @ManyToOne
    @MapsId("guidedStepMaterialObjectTypeId")
    @JoinColumn(name = "guidedStepMaterialObjectType_id")
    @ToString.Include
    private GuidedStepMaterialObjectType guidedStepMaterialObjectType;

    @ManyToOne // Better create and link (cascade = CascadeType.ALL)
    @MapsId("materialObjectId")
    @JoinColumn(name = "materialObject_id")
    @ToString.Include
    private MaterialObject materialObject;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GuidedStepMaterialObject other = (GuidedStepMaterialObject) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    @Data
    public static class ID implements Serializable {
        @Column(name = "guidedStep_id", nullable = false)
        private Long guidedStepId;

        @Column(name = "guidedStepMaterialObjectType_id", nullable = false)
        private Long guidedStepMaterialObjectTypeId;

        @Column(name = "materialObject_id", nullable = false)
        private Long materialObjectId;
    }

}
