package com.recipeton.tmdata.domain.project;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.material.MaterialObject;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "projectEntry_materialObject")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProjectEntryMaterialObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "projectEntry_id")
    @MapsId("projectEntryId")
    private ProjectEntry projectEntry;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "materialObject_id")
    @MapsId("materialObjectId")
    private MaterialObject materialObject;

    @Column(name = "position", nullable = false)
    private Long position;

    @Column(name = "isPrimary", nullable = false)
    private String primary;

    @Data
    public static class ID implements Serializable {
        @Column(name = "projectEntry_id", nullable = false)
        private Long projectEntryId;

        @Column(name = "materialObject_id", nullable = false)
        private Long materialObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectEntryMaterialObject other = (ProjectEntryMaterialObject) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }
}
