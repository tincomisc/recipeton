package com.recipeton.tmdata.domain.unit.lang;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.Localization;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "unitNotationLang")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class UnitNotationLang implements Localization<UnitNotationLang>, Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private ID id = new ID();

    @ManyToOne
    @JoinColumn(name = "unitNotation_id")
    @MapsId("unitNotationId")
    private UnitNotation unitNotation;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "unitNotationLangAttribute_id")
    @MapsId("unitNotationLangAttributeId")
    @ToString.Include
    private UnitNotationLangAttribute attribute;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "locale_id")
    @MapsId("localeId")
    @ToString.Include
    private Locale locale;

    @Column(name = "text", nullable = false)
    @ToString.Include
    private String text;

    @Column(name = "sortKey", nullable = false)
    private String sortKey;

    @Override
    @PrePersist
    public void prePersist() {
        setSortKey(toSortKey());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnitNotationLang other = (UnitNotationLang) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    @Data
    @Embeddable
    public static class ID implements Serializable {
        private static final long serialVersionUID = 1L;

        @Column(name = "unitNotation_id", nullable = false)
        private Long unitNotationId;

        @Column(name = "unitNotationLangAttribute_id", nullable = false)
        private Long unitNotationLangAttributeId;

        @Column(name = "locale_id", nullable = false)
        private Long localeId;
    }

}
