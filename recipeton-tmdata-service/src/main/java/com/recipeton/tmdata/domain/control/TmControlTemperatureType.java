package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "tmControlTemperatureType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class TmControlTemperatureType implements Serializable {

    public static final List<TmControlTemperatureType> DEFAULT = Stream.of(TmControlTemperatureType.Enum.values()).map(e -> new TmControlTemperatureType(e.id, e.value, e.text)).collect(Collectors.toList());

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    private Long value;

    @Column(name = "description", nullable = false)
    @ToString.Include
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TmControlTemperatureType other = (TmControlTemperatureType) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public enum Enum {
        T0(0L, 0L, "none"),
        T30(2L, 30L, "30°C"),
        T35(4L, 35L, "35°C"),
        T37(5L, 37L, "37°C"),
        T40(6L, 40L, "40°C"),
        T45(7L, 45L, "45°C"),
        T50(8L, 50L, "50°C"),
        T55(9L, 55L, "55°C"),
        T60(10L, 60L, "60°C"),
        T65(11L, 65L, "65°C"),
        T70(12L, 70L, "70°C"),
        T72(13L, 72L, "72°C"),
        T75(14L, 75L, "75°C"),
        T80(15L, 80L, "80°C"),
        T85(16L, 85L, "85°C"),
        T90(17L, 90L, "90°C"),
        T95(18L, 95L, "95°C"),
        T98(19L, 98L, "98°C"),
        T100(20L, 100L, "100°C"),
        T105(21L, 105L, "105°C"),
        T110(22L, 110L, "110°C"),
        T115(23L, 115L, "115°C"),
        T120(24L, 120L, "120°C"),
        TVAROMA(25L, 121L, "VAROMA"),
        //
        T42(142L, 42L, "42°C"),
        T44(144L, 44L, "44°C"),
        T46(146L, 46L, "46°C"),
        T48(148L, 48L, "48°C"),
        T52(152L, 52L, "52°C"),
        T54(154L, 54L, "54°C"),
        T56(156L, 56L, "56°C"),
        T58(158L, 58L, "58°C"),
        T62(162L, 62L, "62°C"),
        T64(164L, 64L, "64°C"),
        T66(166L, 66L, "66°C"),
        T68(168L, 68L, "68°C"),
        T82(182L, 82L, "82°C"),
        T84(184L, 84L, "84°C"),
        T86(186L, 86L, "86°C"),
        T88(188L, 88L, "88°C");

        private final long id;
        private final long value;
        private final String text;

        Enum(long id, long value, String text) {
            this.id = id;
            this.value = value;
            this.text = text;
        }

        public static Enum byValue(Long value) {
            if (value == null) {
                return T0;
            }
            // Could do closest...
            return Stream.of(values()).filter(v -> v.value == value).findFirst().orElseThrow(() -> new IllegalArgumentException("No valid temperature found with value=" + value + "."));
        }

        public long getId() {
            return id;
        }
    }

}
