package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.ingredient.IngredientNotation;
import com.recipeton.tmdata.domain.ingredient.IngredientPreparation;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.recipeton.shared.util.StreamUtil.toStream;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "recipeIngredient")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeIngredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "recipe_id", nullable = false)
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "ingredientNotation_id", nullable = false)
    @ToString.Include
    private IngredientNotation ingredientNotation;

    @ManyToOne
    @JoinColumn(name = "ingredientPreparation_id")
    private IngredientPreparation ingredientPreparation;

    @OneToMany(mappedBy = "recipeIngredient", cascade = ALL, orphanRemoval = true)
    private Collection<RecipeIngredientRange> recipeIngredientRange;

    @Column(name = "isCommaSeparated", nullable = false)
    private boolean commaSeparated;

    @ManyToOne
    @JoinColumn(name = "recipeGroup_id")
    @ToString.Include
    private RecipeGroup recipeGroup;

    @Column(name = "position", nullable = false)
    private Long position;

    @Column(name = "priority", nullable = false)
    private Long priority;

    /**
     * Ingredients that can be added at will dont need preparation or unit.
     */
    @ManyToOne
    @JoinColumn(name = "unitNotation_id")
    @ToString.Include
    private UnitNotation unitNotation;

    @Column(name = "isOptional", nullable = false)
    private boolean optional;

    public static Optional<RecipeIngredient> find(RecipeIngredient recipeIngredient, List<RecipeIngredient> recipeIngredients, Locale locale) {
        return recipeIngredients.stream().filter(cri ->
                cri.getRecipeGroup().equals(recipeIngredient.getRecipeGroup())
                        && Objects.equals(cri.getUnitNotation(), recipeIngredient.getUnitNotation())
                        && StringUtils.equals(cri.getIngredientNotationTextByLocale(locale), recipeIngredient.getIngredientNotationTextByLocale(locale))
                        && StringUtils.equals(cri.getIngredientPreparationTextByLocale(locale), recipeIngredient.getIngredientPreparationTextByLocale(locale))
        ).findFirst();
    }

    private String getIngredientNotationTextByLocale(Locale locale) {
        return this.ingredientNotation == null ? null : this.ingredientNotation.getTextByLocale(locale);
    }

    private String getIngredientPreparationTextByLocale(Locale locale) {
        return this.ingredientPreparation == null ? null : this.ingredientPreparation.getTextByLocale(locale);
    }

    @PrePersist
    public void prePersist() {
        toStream(recipeIngredientRange).filter(n -> n.getRecipeIngredient() == null).forEach(n -> n.setRecipeIngredient(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecipeIngredient other = (RecipeIngredient) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }
}
