package com.recipeton.tmdata.domain.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "guidedStepType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class GuidedStepType implements Serializable {

    public static final List<GuidedStepType> DEFAULT = Stream.of(GuidedStepType.Enum.values()).map(e -> new GuidedStepType(e.id, e.text)).collect(Collectors.toList());


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ToString.Include
    @Column(name = "value", nullable = false)
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GuidedStepType other = (GuidedStepType) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public enum Enum {
        INGREDIENT(1L, "ingredient step"),
        TM_SETTING(2L, "tm setting step"),
        UTENSIL(3L, "utensil step"),
        CREATED_INGREDIENT(4L, "created ingredient step"),
        FREETEXT(5L, "freetext step");

        private final long id;
        private final String text;

        Enum(Long id, String text) {
            this.id = id;
            this.text = text;
        }

        public Long getId() {
            return id;
        }


    }

}
