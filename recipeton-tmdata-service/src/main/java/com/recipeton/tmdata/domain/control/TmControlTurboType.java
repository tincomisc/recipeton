package com.recipeton.tmdata.domain.control;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "tmControlTurboType")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class TmControlTurboType implements Serializable {

    public static final List<TmControlTurboType> DEFAULT = Stream.of(TmControlTurboType.Enum.values()).map(e -> new TmControlTurboType(e.id, e.text)).collect(Collectors.toList());
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "value", nullable = false)
    @ToString.Include
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TmControlTurboType other = (TmControlTurboType) o;

        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return ApplicationConfiguration.ENTITY_HC;
    }

    public enum Enum {
        HALF_SECOND(2L, Duration.ofMillis(500), "500"),
        ONE_SECOND(3L, Duration.ofSeconds(1), "1000"),
        TWO_SECONDS(4L, Duration.ofSeconds(2), "2000"); // Can I add more???

        private final long id;
        private final String text;
        private final Duration duration;

        Enum(long id, Duration duration, String text) {
            this.id = id;
            this.text = text;
            this.duration = duration;
        }

        public static Enum by(Duration duration) {
            if (duration == null) {
                return null;
            }
            // Could do closest...
            return Stream.of(values()).filter(v -> v.duration.equals(duration)).findFirst().orElseThrow(() -> new IllegalArgumentException("No found for duration=" + duration + "."));
        }

        public long getId() {
            return id;
        }
    }
}
