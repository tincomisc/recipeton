package com.recipeton.tmdata.service.material;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.material.Material;
import com.recipeton.tmdata.domain.material.MaterialFlag;
import com.recipeton.tmdata.domain.material.MaterialObject;
import com.recipeton.tmdata.domain.material.MaterialStorageLocation;
import com.recipeton.tmdata.domain.recipe.GuidedUtensilActionType;
import com.recipeton.tmdata.service.DataInitializer;

import java.io.IOException;
import java.nio.file.Path;

public interface MaterialService extends DataInitializer {
    Material createMaterial(Path filePath, Long mdbId, MaterialFlag.Enum flags);

    Path getOrCreateMaterialLargeResource(String fileName, boolean verifyExists) throws IOException;

    Path getOrCreateMaterialResource(String fileName, ApplicationConfiguration.ImageDimensions imageDimensions, boolean verifyExists) throws IOException;

    Path getOrCreateMaterialSmallResource(String fileName, boolean verifyExists) throws IOException;

    MaterialObject getPersisted(GuidedUtensilActionType.Enum type);

    MaterialStorageLocation getPersistedMaterialStorageLocation(String source);
}
