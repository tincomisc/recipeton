package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.ingredient.IngredientNotation;
import com.recipeton.tmdata.domain.ingredient.IngredientPreparation;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.recipe.RecipeGroup;
import com.recipeton.tmdata.domain.recipe.RecipeIngredient;
import com.recipeton.tmdata.domain.recipe.RecipeIngredientRange;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.service.ingredient.IngredientService;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.unit.UnitService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeIngredientService {

    private final IngredientService ingredientService;
    private final RangeTypeRepository rangeTypeRepository;
    private final UnitService unitService;

    // NOTE: Find out how to declare alternative ingredient. Possibly position and priority
    public RecipeIngredient createRecipeIngredient(String uid, String ingredientName, String preparationText, BigDecimal magnitudeFrom, BigDecimal magnitudeTo, String measurementUnit, Locale locale, RecipeGroup recipeGroup, boolean optional) {
        Pair<IngredientNotation, IngredientPreparation> pair = ingredientService.getPersisted(uid, ingredientName, preparationText, locale);

        UnitNotation unitNotation = null;
        if (StringUtils.isNotBlank(measurementUnit)) {
            unitNotation = unitService.getPersisted(measurementUnit, locale, UnitNotationPriority.Enum.RECIPE_SPECIFIC, UnitType.Enum.OTHERS);
        }

        Collection<RecipeIngredientRange> ranges = Stream.of(
                (magnitudeFrom == null) ? null : new RecipeIngredientRange().setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM)).setIngredientAmountValue(magnitudeFrom),
                (magnitudeTo == null) ? null : new RecipeIngredientRange().setRangeType(rangeTypeRepository.getOne(RangeType.Enum.TO)).setIngredientAmountValue(magnitudeTo)
        ).filter(Objects::nonNull).collect(Collectors.toList());

        return new RecipeIngredient()
                .setPosition(1L) // I think that these two are to declare alternative ingredient. Verify!!
                .setPriority(0L) // I think that these two are to declare alternative ingredient. Verify!!
                .setRecipeGroup(recipeGroup)
                .setIngredientNotation(pair.getLeft())
                .setIngredientPreparation(pair.getRight())
                .setCommaSeparated(pair.getLeft().getLangs().size() > 1) // Show preparation??
                .setRecipeIngredientRange(ranges)
                .setUnitNotation(unitNotation)
                .setOptional(optional);
    }
}
