package com.recipeton.tmdata.service.material;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.material.MaterialObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.Optional;

import static org.hibernate.jpa.QueryHints.HINT_CACHEABLE;

public interface MaterialObjectRepository extends JpaRepository<MaterialObject, Long>, JpaSpecificationExecutor<MaterialObject> {

    // Change for HQL. Issues with inner join in SQLITE

    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    @Query(value = "select mO.* from materialObject mO join material on material.id = mO.material_id join material_materialStorageLocation mmSL on material.id = mmSL.material_id join materialStorageLocation mSL on mmSL.materialStorageLocation_id = mSL.id where mSL.source=:source", nativeQuery = true)
    Optional<MaterialObject> findFirstByMaterialStorageLocationSource(@Param("source") String source);
}
