package com.recipeton.tmdata.service.cookstick;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.cookstick.CookstickMaterialObject;
import com.recipeton.tmdata.domain.cookstick.CookstickMaterialObjectType;
import com.recipeton.tmdata.domain.cookstick.lang.CookstickExtLang;
import com.recipeton.tmdata.domain.cookstick.lang.CookstickExtLangAttribute;
import com.recipeton.tmdata.domain.material.MaterialFlag;
import com.recipeton.tmdata.domain.material.MaterialObject;
import com.recipeton.tmdata.domain.material.MaterialObjectFlag;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.cookstick.lang.CookstickExtLangAttributeRepository;
import com.recipeton.tmdata.service.cookstick.lang.CookstickExtLangRepository;
import com.recipeton.tmdata.service.material.MaterialObjectFlagRepository;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.misc.LocaleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.util.ListUtil.toListMutable;


@Slf4j
@Service
@AllArgsConstructor
public class CookstickServiceImpl implements CookstickService {

    private final ApplicationConfiguration applicationConfiguration;
    private final CookstickExtLangAttributeRepository cookstickExtLangAttributeRepository;
    private final CookstickExtLangRepository cookstickExtLangRepository;
    private final CookstickMaterialObjectRepository cookstickMaterialObjectRepository;
    private final CookstickMaterialObjectTypeRepository cookstickMaterialObjectTypeRepository;
    private final LocaleService localeService;
    private final MaterialObjectFlagRepository materialObjectFlagRepository;
    private final MaterialService materialService;

    private CookstickMaterialObject createCookstickMaterialObject(CookstickMaterialObjectType.Enum objectType, Locale locale, Path filePath) {

        return new CookstickMaterialObject()
                .setLocale(locale)
                .setCookstickMaterialObjectType(cookstickMaterialObjectTypeRepository.getOne(objectType))
                .setMaterialObject(
                        new MaterialObject()
                                .setMaterialObjectFlags(toListMutable(
                                        materialObjectFlagRepository.getOne(MaterialObjectFlag.Enum.NONE)
                                ))
                                .setMaterial(materialService.createMaterial(filePath, null, MaterialFlag.Enum.USER_DEFINED))
                );
    }

    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        DataInitializer.getPersisted(cookstickExtLangAttributeRepository, CookstickExtLangAttribute.DEFAULT);
        DataInitializer.getPersisted(cookstickMaterialObjectTypeRepository, CookstickMaterialObjectType.DEFAULT);

        if (cookstickExtLangRepository.count() == 0) {
            for (Locale l : localeService.getLocalesByDefault()) {
                getOrCreateCookstickExtLang(l);
                getOrCreateCookstickMaterialObjects(l);
            }
        }
    }

    private CookstickExtLang getOrCreateCookstickExtLang(Locale locale) {
        CookstickExtLangAttribute attribute = cookstickExtLangAttributeRepository.getOne(CookstickExtLangAttribute.Enum.NAME);
        return cookstickExtLangRepository.findById(new CookstickExtLang.ID(attribute.getId(), locale.getId()))
                .orElseGet(() -> {
                    CookstickExtLang entity = new CookstickExtLang()
                            .setAttribute(attribute)
                            .setLocale(locale)
                            .setText(applicationConfiguration.getBookName());
                    log.debug("initialize entity={}", entity);
                    return cookstickExtLangRepository.save(entity);
                });

    }

    private List<CookstickMaterialObject> getOrCreateCookstickMaterialObjects(Locale locale) throws IOException {
        List<CookstickMaterialObject> cookstickMaterialObjects = cookstickMaterialObjectRepository.findAllByLocale(locale);
        if (!cookstickMaterialObjects.isEmpty()) {
            return cookstickMaterialObjects;
        }

        boolean verifyExists = applicationConfiguration.isFailOnBookAssetUnresolved();
        Path bookMaterialIcon = materialService.getOrCreateMaterialSmallResource(applicationConfiguration.getBookIconFileName(), verifyExists);
        CookstickMaterialObject cookstickMaterialObjectIcon = createCookstickMaterialObject(CookstickMaterialObjectType.Enum.MENU_ICON, locale, bookMaterialIcon);

        Path bookMaterialSplash = materialService.getOrCreateMaterialLargeResource(applicationConfiguration.getBookSplashFileName(), verifyExists);
        CookstickMaterialObject cookstickMaterialObjectSplash = createCookstickMaterialObject(CookstickMaterialObjectType.Enum.SPLASH_SCREEN, locale, bookMaterialSplash);

        return cookstickMaterialObjectRepository.saveAll(Stream.of(
                cookstickMaterialObjectIcon, cookstickMaterialObjectSplash
        ).peek(c -> log.debug("initialize entity={}", c)).collect(Collectors.toList()));
    }
}
