package com.recipeton.tmdata.service.ingredient;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.ingredient.IngredientNotation;
import com.recipeton.tmdata.domain.ingredient.IngredientPreparation;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.service.DataInitializer;
import org.apache.commons.lang3.tuple.Pair;

public interface IngredientService extends DataInitializer {

    Pair<IngredientNotation, IngredientPreparation> getPersisted(String uid, String notationText, String preparationText, Locale locale);
}
