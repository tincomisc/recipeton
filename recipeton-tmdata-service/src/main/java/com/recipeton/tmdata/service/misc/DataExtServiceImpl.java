package com.recipeton.tmdata.service.misc;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.misc.DataExtType;
import com.recipeton.tmdata.service.DataInitializer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class DataExtServiceImpl implements DataExtService {

    private final ApplicationConfiguration applicationConfiguration;
    private final DataExtRepository dataExtRepository;
    private final DataExtTypeRepository dataExtTypeRepository;

    @Override
    @Transactional
    public void doDataInitialize() {
        DataInitializer.getPersisted(dataExtTypeRepository, DataExtType.DEFAULT);
        dataExtRepository.getPersisted(applicationConfiguration.getDatabaseVersion(), applicationConfiguration.getDatabaseExportDateTime(), applicationConfiguration.getDatabaseRelease());
    }
}
