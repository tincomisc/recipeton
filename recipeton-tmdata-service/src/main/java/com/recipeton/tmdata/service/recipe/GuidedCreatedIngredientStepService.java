package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.GuidedCreatedIngredientNotationLang;
import com.recipeton.tmdata.domain.recipe.lang.GuidedCreatedIngredientStepTextLang;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.recipe.lang.GuidedCreatedIngredientStepTextLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedCreatedIngredientStepService {

    private final GuidedCreatedIngredientStepTextAttributeRepository guidedCreatedIngredientStepTextAttributeRepository;
    private final GuidedCreatedIngredientStepTextLangRepository guidedCreatedIngredientStepTextLangRepository;
    private final GuidedCreatedIngredientStepTextRepository guidedCreatedIngredientStepTextRepository;
    private final GuidedIngredientStepWeighingAttributeRepository guidedIngredientStepWeighingAttributeRepository;
    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final RangeTypeRepository rangeTypeRepository;

    public GuidedCreatedIngredientStepText createGuidedCreatedIngredientStepText(Map<Locale, String> localization, GuidedCreatedIngredientStepTextAttribute.Enum attribute, boolean predefined) {
        return new GuidedCreatedIngredientStepText()
                .setPredefined(predefined)
                .setAttribute(guidedCreatedIngredientStepTextAttributeRepository.getOne(attribute))
                .setLangs(localization.entrySet().stream()
                        .map(e -> new GuidedCreatedIngredientStepTextLang()
                                .setText(e.getValue())
                                .setLocale(e.getKey())
                        ).collect(Collectors.toList())
                );
    }

    public GuidedStep createGuidedStepCreatedIngredient(String ingredientText, BigDecimal magnitude, GuidedIngredientStepWeighingAttribute.Enum weighingAttribute, Locale locale, GuidedCreatedIngredientStepTextAttribute.Enum guidedCreatedIngredientStepTextAttribute) {
        return new GuidedStep()
                .setPosition(-1L)
                .setGuidedCreatedIngredientStep(new GuidedCreatedIngredientStep()
                        .setGuidedCreatedIngredientAmountRanges(magnitude == null ? null : of(
                                new GuidedCreatedIngredientAmountRange()
                                        .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                        .setAmount(magnitude)
                        ))
                        .setGuidedIngredientStepWeighingAttribute(guidedIngredientStepWeighingAttributeRepository.getOne(weighingAttribute))
                        .setGuidedCreatedIngredientStepText(guidedCreatedIngredientStepTextRepository.getOneByAttributeAndPredefined(guidedCreatedIngredientStepTextAttributeRepository.getOne(guidedCreatedIngredientStepTextAttribute), true))
                        .setGuidedCreatedIngredientNotation(new GuidedCreatedIngredientNotation()
                                .setLangs(ingredientText == null ? null : of(
                                        new GuidedCreatedIngredientNotationLang()
                                                .setLocale(locale)
                                                .setText(ingredientText)
                                ))))
                .setGuidedStepType(guidedStepTypeRepository.getOne(GuidedStepType.Enum.CREATED_INGREDIENT));
    }

    public GuidedCreatedIngredientStepText getPersistedGuidedCreatedIngredientStepText(String text, Locale locale, GuidedCreatedIngredientStepTextAttribute.Enum attribute, boolean predefined) {
        return guidedCreatedIngredientStepTextLangRepository.findFirstByTextAndLocale(text, locale)
                .map(GuidedCreatedIngredientStepTextLang::getGuidedCreatedIngredientStepText)
                .orElseGet(() -> guidedCreatedIngredientStepTextRepository.save(createGuidedCreatedIngredientStepText(Map.of(locale, text), attribute, predefined)));
    }
}
