package com.recipeton.tmdata.service.unit;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.MeasurementUnitDefinition;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.service.DataInitializer;

public interface UnitService extends DataInitializer {

    UnitNotation getPersisted(MeasurementUnitDefinition measurementUnitDefinition, Locale locale, UnitNotationPriority.Enum priority, UnitType.Enum unitType);

    UnitNotation getPersisted(String text, Locale locale, UnitNotationPriority.Enum priority, UnitType.Enum unitType);
}
