package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeCustomizationConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeAnalyzer;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.service.RecipeMeasurementAnalyzer;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.shared.util.TextUtil;
import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.domain.control.TmControlRotationDirectionType;
import com.recipeton.tmdata.domain.control.TmControlSpeedType;
import com.recipeton.tmdata.domain.control.TmControlTemperatureType;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.TmVersion;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValue;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeLangAttribute;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.domain.utensil.UtensilType;
import com.recipeton.tmdata.service.ImageService;
import com.recipeton.tmdata.service.misc.LocaleService;
import com.recipeton.tmdata.service.misc.TmVersionRepository;
import com.recipeton.tmdata.service.nutrition.RecipeNutritionService;
import com.recipeton.tmdata.service.recipe.lang.RecipeLangAttributeRepository;
import com.recipeton.tmdata.service.unit.UnitService;
import com.recipeton.tmdata.service.utensil.UtensilService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_KCAL;
import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.RECIPE_CATEGORY_NAME_EXTRA_PREFIX;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.shared.util.TextUtil.EOL_PATTERN;
import static com.recipeton.shared.util.TextUtil.replaceAll;
import static com.recipeton.shared.util.ThrowingHelper.uFunction;
import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeImportService {

    public static final double CAL_TO_KJ = 4.184; // Move to unit
    public static final double KJ_TO_CAL = 0.239;
    public static final long YEAR = 2015L;

    private final ApplicationConfiguration applicationConfiguration;
    private final GuidedFreetextStepService guidedFreetextStepService;
    private final GuidedIngredientStepService guidedIngredientStepService;
    private final GuidedCreatedIngredientStepService guidedCreatedIngredientStepService;
    private final GuidedTmStepService guidedTmStepService;
    private final GuidedUtensilStepService guidedUtensilStepService;
    private final ImageService imageService;
    private final List<RecipeAnalyzer> recipeAnalyzers;
    private final LocaleService localeService;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeDifficultyRepository recipeDifficultyRepository;
    private final RecipeIngredientService recipeIngredientService;
    private final RecipeLangAttributeRepository recipeLangAttributeRepository;
    private final RecipeNutritionService recipeNutritionService;
    private final RecipePriceRepository recipePriceRepository;
    private final RecipeRepository recipeRepository;
    private final RecipeService recipeService;
    private final TmVersionRepository tmVersionRepository;
    private final UnitService unitService;
    private final UtensilService utensilService;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;

    private GuidedStep createGuidedStepIngredient(RecipeIngredientTerm recipeTerm, RecipeGroup recipeGroup, RecipeInstructionAnalysis ria, String explanationText, Locale locale, Map<RecipeIngredientAnalysis, RecipeIngredient> recipeIngredients) { // NOPMD Pure BS
        if (recipeTerm.isDetachedMatch()) {
            if (ria.isTagPresent(DO_ADD)) {
                log.debug("create recipeSectionSubStep createdIngredientAdd term={}", recipeTerm);
                return guidedCreatedIngredientStepService.createGuidedStepCreatedIngredient(recipeTerm.getIngredient().getText(),
                        null, GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE,
                        locale, GuidedCreatedIngredientStepTextAttribute.Enum.ADD);
            }
            if (ria.isTagPresent(DO_PLACE)) {
                log.debug("create recipeSectionSubStep createdIngredientPlace term={}", recipeTerm);
                return guidedCreatedIngredientStepService.createGuidedStepCreatedIngredient(recipeTerm.getIngredient().getText(),
                        null, GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE,
                        locale, GuidedCreatedIngredientStepTextAttribute.Enum.PLACE);
            }
            return null;
        }

        if (ria.isTagPresent(FOCUS_MAIN)) {
            GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeTerm.getIngredient().isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE_AND_MIXING_BOWL : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;

            if (ria.isTagPresent(DO_ADD)) { // recipeIngredientAnalysis.getAlternative() Find out how to declare alternative!
                log.debug("create recipeSectionSubStep ingredientAdd term={}", recipeTerm);
                GuidedIngredientStepText persistedGuidedIngredientStepText = guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.ADD);

                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), persistedGuidedIngredientStepText, weighingAttribute);
            }

            if (ria.isTagPresent(DO_PLACE)) {
                log.debug("create recipeSectionSubStep ingredientPlace term={}", recipeTerm);
                GuidedIngredientStepText persistedGuidedIngredientStepText = guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.PLACE);

                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), persistedGuidedIngredientStepText, weighingAttribute);
            }

            if (ria.isTagPresent(DO_WEIGH)) {
                log.debug("create recipeSectionSubStep ingredientWeighInMain term={}", recipeTerm);
                GuidedIngredientStepText persistedGuidedIngredientStepText = guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, false, explanationText, locale);

                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), persistedGuidedIngredientStepText, weighingAttribute);
            }
        }

        if (ria.isTagPresent(FOCUS_ON_TOP)) {
            GuidedIngredientStepText guidedIngredientStepText = guidedIngredientStepService.getPersistedGuidedIngredientStepText(GuidedIngredientStepTextAttribute.Enum.WEIGHT_OUTSIDE, false, explanationText, locale);
            GuidedIngredientStepWeighingAttribute.Enum weighingAttribute = recipeTerm.getIngredient().isWeighted() ? GuidedIngredientStepWeighingAttribute.Enum.SHOW_SCALE : GuidedIngredientStepWeighingAttribute.Enum.DO_NOT_SHOW_SCALE;

            if (ria.isTagPresent(DO_ADD)) {
                log.debug("create recipeSectionSubStep ingredientAddOnTop term={}", recipeTerm);

                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), guidedIngredientStepText, weighingAttribute);
            }

            if (ria.isTagPresent(DO_PLACE)) {
                log.debug("create recipeSectionSubStep ingredientPlaceOnTop term={}", recipeTerm);
                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), guidedIngredientStepText, weighingAttribute);
            }

            if (ria.isTagPresent(DO_WEIGH)) {
                log.debug("create recipeSectionSubStep ingredientWeighOnTop term={}", recipeTerm);
                RecipeIngredient recipeIngredient = recipeIngredients.computeIfAbsent(recipeTerm.getIngredient(), r -> createRecipeIngredient(r, recipeGroup, locale));
                return guidedIngredientStepService.createGuidedStepIngredient(recipeIngredient, recipeTerm.getMagnitudeFromValue(), recipeTerm.getMagnitudeToValue(), guidedIngredientStepText, weighingAttribute);
            }
        }

        return null;
    }

    private GuidedStep createGuidedStepRecipeCommand(RecipeCommandTerm recipeCommandTerm, String explanationText, Locale locale) {
        String executingText = applicationConfiguration.isImportStepCommandExecuteTextFull() ? explanationText : StringUtils.substringBefore(explanationText, recipeCommandTerm.getText()).trim();
        if (recipeCommandTerm.getProgram() == null) {
            log.debug("create recipeSectionSubStep commandStandard term={}", recipeCommandTerm);
            return guidedTmStepService.createGuidedStepTmControl(recipeCommandTerm.getDuration(),
                    TmControlTemperatureType.Enum.byValue(recipeCommandTerm.getTemperature()),
                    TmControlSpeedType.Enum.byValue(recipeCommandTerm.getSpeed()),
                    TmControlRotationDirectionType.Enum.by(recipeCommandTerm.getRotation()),
                    explanationText, executingText, locale);
        } else {
            switch (recipeCommandTerm.getProgram()) {
                case TURBO:
                    log.debug("create recipeSectionSubStep commandTurbo term={}", recipeCommandTerm);
                    return guidedTmStepService.createGuidedStepTmTurbo(recipeCommandTerm.getDuration(), recipeCommandTerm.getRepetitionFrom(), explanationText, executingText, locale);
                case DOUGH:
                    log.debug("create recipeSectionSubStep commandDough term={}", recipeCommandTerm);
                    return guidedTmStepService.createGuidedStepTmDough(recipeCommandTerm.getDuration(), explanationText, executingText, locale);
                case SOUS_VIDE:
                case FERMENT:
                case HEAT_ONLY:
                default:
                    log.warn("create recipeSectionSubStep command not managed. Ignoring. term={}", recipeCommandTerm);
                    break;
            }
        }
        return null;
    }

    private GuidedStep createGuidedStepUtensil(RecipeToolComposedActionTerm recipeTerm, String explanationText, Locale locale) {
        log.debug("create recipeSectionSubStep utensil term={}", recipeTerm);
        return guidedUtensilStepService.createGuidedStepUtensil(GuidedUtensilActionType.Enum.by(recipeTerm.getTag()), explanationText, false, locale);
    }

    private List<RecipeCategory> createRecipeCategories(RecipeDefinition recipeDefinition, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        Map<String, String> categoryUidNameMap = new HashMap<>();

        toStream(recipeDefinition.getRecipeCategory())
                .map(proposedName -> recipeAnalyzer.getRecipeCategoryAnalyzer().getRecipeCategoryDefinition(proposedName, true))
                .filter(Objects::nonNull)
                .forEach(p -> {
                    String uid = Optional.of(p.getUid())
                            .filter(RecipeCategoryDefinition::isRecipeCategoryUidPrincipal)
                            .orElseGet(() -> RecipeCategoryDefinition.createRecipeCategoryUidByPrincipal(p.getUid()));
                    if (StringUtils.isNotBlank(uid) && !categoryUidNameMap.containsKey(uid)) {
                        categoryUidNameMap.put(uid, p.getName());
                    }
                });

        if (applicationConfiguration.isImportRecipeNamePrefixAsPrimaryRecipeCategories()) {
            String title = recipeAnalyzer.analyzeTitle(recipeDefinition);
            String firstChars = title.substring(0, 2).trim();
            categoryUidNameMap.put("pr_" + firstChars, RECIPE_CATEGORY_NAME_EXTRA_PREFIX + firstChars);
        }

        if (applicationConfiguration.isImportRecipeKeywordsAsPrimaryRecipeCategories()) {
            toStream(recipeDefinition.getKeywords())
                    .map(proposedName -> recipeAnalyzer.getRecipeCategoryAnalyzer().getRecipeCategoryDefinition(proposedName, false))
                    .filter(Objects::nonNull)
                    .forEach(p -> {
                        if (StringUtils.isNotBlank(p.getUid()) && !categoryUidNameMap.containsKey(p.getUid())) {
                            categoryUidNameMap.put(p.getUid(), p.getName());
                        }
                    });
        }

        return categoryUidNameMap.entrySet().stream()
                .map(uFunction(e -> recipeCategoryService.getPersisted(e.getKey(), e.getValue(), locale, "CATEGORY_" + e.getKey() + ".png", true)))
                .distinct()
                .collect(Collectors.toList());
    }

    private RecipeIngredient createRecipeIngredient(RecipeIngredientAnalysis recipeIngredientAnalysis, RecipeGroup recipeGroup, Locale locale) {
        boolean normalized = applicationConfiguration.isImportMeasurementUnitNormalized();
        return recipeIngredientService.createRecipeIngredient(
                recipeIngredientAnalysis.getUid(),
                recipeIngredientAnalysis.getIngredientNotation(),
                recipeIngredientAnalysis.getPreparation(),
                recipeIngredientAnalysis.getMagnitudeFromValue(),
                recipeIngredientAnalysis.getMagnitudeToValue(),
                recipeIngredientAnalysis.getUnitText(normalized), locale, recipeGroup, recipeIngredientAnalysis.isOptional());
    }

    private List<RecipeLang> createRecipeLangs(String uid, RecipeDefinition recipeDefinition, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        String title = recipeAnalyzer.analyzeTitle(recipeDefinition);
        String hints = recipeDefinition.getHints() == null ? null : recipeDefinition.getHints().stream()
                .map(s -> StringUtils.replace(s, "\n", "<br>"))
                .collect(Collectors.joining("<br><br>"));

        RecipeMessage recipeMessage = toBackgroundReport(uid, recipeDefinition, recipeAnalyzer);
        return Stream.of(
                new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.TITLE))
                        .setText(title)
                        .setLocale(locale),
                hints == null ? null : new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.TIP))
                        .setText(hints)
                        .setLocale(locale),
                recipeMessage == null ? null : new RecipeLang()
                        .setAttribute(recipeLangAttributeRepository.getOne(RecipeLangAttribute.Enum.BACKGROUND))
                        .setText(recipeMessage.getBody())
                        .setLocale(locale)
        ).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public RecipeNutritionalValue createRecipeNutritionalValue(RecipeDefinition recipeDefinition, UnitNotation servingUnitNotation, RecipeMeasurementAnalyzer recipeMeasurementAnalyzer, Locale locale) { // NOPMD
        RecipeDefinition.Nutrition nutrition = recipeDefinition.getNutrition();
        boolean nutritionFound = nutrition != null;
        if (!nutritionFound) {
            return recipeNutritionService.createRecipeNutritionalValue(servingUnitNotation);
        }

        RecipeMeasurement caloriesMeasurement = recipeMeasurementAnalyzer.analyzeRecipeMeasurement(nutrition.getCalories());
        BigDecimal calories;
        BigDecimal caloriesKcal;
        if (caloriesMeasurement == null || caloriesMeasurement.getMagnitudeValue() == null) {
            calories = null;
            caloriesKcal = null;
        } else {
            if (UNIT_KCAL.equals(caloriesMeasurement.getUnit())) {
                caloriesKcal = caloriesMeasurement.getMagnitudeValue();
                calories = new BigDecimal((int) (caloriesMeasurement.getMagnitudeValue().intValue() * CAL_TO_KJ));
            } else {
                calories = caloriesMeasurement.getMagnitudeValue();
                caloriesKcal = new BigDecimal((int) (caloriesMeasurement.getMagnitudeValue().intValue() * KJ_TO_CAL));
            }
        }

        return recipeNutritionService.createRecipeNutritionalValue(
                calories,
                recipeMeasurementAnalyzer.analyzeRecipeMeasurementValue(nutrition.getProteinContent()),
                recipeMeasurementAnalyzer.analyzeRecipeMeasurementValue(nutrition.getCarbohydrateContent()),
                recipeMeasurementAnalyzer.analyzeRecipeMeasurementValue(nutrition.getFatContent()),
                recipeMeasurementAnalyzer.analyzeRecipeMeasurementValue(nutrition.getCholesterolContent()),
                recipeMeasurementAnalyzer.analyzeRecipeMeasurementValue(nutrition.getFibreContent()),
                caloriesKcal,
                servingUnitNotation, locale);
    }

    private List<RecipeTmVersion> createRecipeTmVersions() {
        return of(
                new RecipeTmVersion().setTmVersion(tmVersionRepository.getOne(TmVersion.Enum.APP))
//                tmVersionRepository.getOne(TmVersion.Enum.TM31),
        );
    }

    private List<RecipeUtensil> createRecipeUtensils(List<String> toolTexts, RecipeAnalyzer recipeAnalyzer, Locale locale) {
        if (toolTexts == null) {
            return null;
        }
        return toolTexts.stream()
                .map(s -> recipeAnalyzer.getRecipeToolAnalyzer().analyzeRecipeTool(s))
                .filter(Objects::nonNull)
                .map(u -> new RecipeUtensil()
                        .setPosition(1L)
                        .setUtensil(utensilService.getPersisted(u.getUid(), u.getText(), null, UtensilType.Enum.KITCHEN_EQUIPMENT, locale))
                )
                .distinct()
                .collect(Collectors.toList());
    }

    @Transactional
    public Pair<ImportRecipeResult, Long> doImportRecipe(Path recipePath, Path picturePath) throws IOException {
        log.info("Importing recipe path={}, picture={}", recipePath, picturePath);
        RecipeDefinition recipeDefinition = recipeDefinitionStorageService.read(recipePath);

        if (applicationConfiguration.getImportDeviceNameFilter() != null && !recipeDefinition.getDevices().contains(applicationConfiguration.getImportDeviceNameFilter())) {
            log.info("Skipping. Not supported device.path={}", recipePath);
            return Pair.of(ImportRecipeResult.SKIP, null);
        }

        String uid = FilenameUtils.getBaseName(recipePath.getFileName().toString());
        recipeRepository.findFirstByUid(uid).ifPresent(
                r -> {
                    log.info("Found existing recipe with same uid. Replacing uid={}, path={}", uid, recipePath);
                    recipeRepository.delete(r);
                }
        );

        RecipeAnalyzer recipeAnalyzer = recipeAnalyzers.stream()
                .filter(ra -> ra.isHandlerOf(recipeDefinition))
                .findFirst()
                .orElseThrow(() -> new RecipeImportUncheckedException("Could not find valid analyzer for recipe={} in analyzers={},", recipeDefinition, recipeAnalyzers));
        RecipeMeasurementAnalyzer recipeMeasurementAnalyzer = recipeAnalyzer.getRecipeMeasurementAnalyzer();

        Locale locale = localeService.getLocaleByLabel(recipeAnalyzer.analyzeLocale(recipeDefinition));

        RecipeMeasurement recipeYield = recipeMeasurementAnalyzer.analyzeRecipeMeasurement(recipeDefinition.getRecipeYield());
        UnitNotation servingUnitNotation = toRecipeServingUnitNotation(recipeYield, locale);

        long mdbId = System.nanoTime(); // whatever for now

        log.info("Importing recipe name={}, configId={}", recipeDefinition.getName(), recipeAnalyzer.getConfiguration().getId());

        Recipe recipe = new Recipe()
                .setUid(uid)
                .setOriginalTitle(StringUtils.trim(recipeDefinition.getName()))
                .setRecipeCategories(createRecipeCategories(recipeDefinition, recipeAnalyzer, locale))
                .setLangs(createRecipeLangs(uid, recipeDefinition, recipeAnalyzer, locale))
                .setRecipeMaterialObject(recipeService.createRecipeMaterialObjects(picturePath, mdbId))
                .setRecipeIngredients(new ArrayList<>())
                .setRecipeSteps(new ArrayList<>())
                .setRecipePrice(recipePriceRepository.getOne(RecipePrice.Enum.by(recipeAnalyzer.analyzeRecipePrice(recipeDefinition))))
                .setRecipeDifficulty(recipeDifficultyRepository.getOne(RecipeDifficulty.Enum.by(recipeAnalyzer.analyzeRecipeDifficulty(recipeDefinition))))
                .setRecipeUtensils(createRecipeUtensils(recipeDefinition.getUtensils(), recipeAnalyzer, locale))
                .setRecipeTime(Stream.of(
                        recipeService.createRecipeTime(RecipeTimeType.Enum.TOTAL, recipeDefinition.getTotalTime(), null, locale),
                        recipeService.createRecipeTime(RecipeTimeType.Enum.BAKE, recipeDefinition.getCookTime(), null, locale),
                        recipeService.createRecipeTime(RecipeTimeType.Enum.PREP, recipeDefinition.getPrepTime(), null, locale)
                ).filter(Objects::nonNull).collect(Collectors.toSet()))
                .setRecipeTmVersions(createRecipeTmVersions())
                .setServingQuantity(Optional.ofNullable(recipeYield.getMagnitudeValue()).orElse(BigDecimal.ONE))
                .setServingUnitNotation(servingUnitNotation)
                .setRecipeNutritionalValue(createRecipeNutritionalValue(recipeDefinition, servingUnitNotation, recipeMeasurementAnalyzer, locale))
                .setRecipeGroups(new ArrayList<>())
                .setVersion(BigDecimal.ZERO) //
                .setYear(YEAR) // NOTE: Use analyzer to extract
                .setDeleted(false);

        recipe.setRecipeSteps(recipeDefinition.getRecipeSections().stream()
                .flatMap(rs -> toRecipeSteps(rs, recipe, recipeAnalyzer, locale).stream())
                .collect(Collectors.toList()));

        log.debug("Importing recipe. prepare picture. uid={}", uid);
        if (Files.exists(picturePath)) {
            ApplicationConfiguration.ImageDimensions targetImageDimensions = applicationConfiguration.getBookImageSmallDimensions();
            imageService.resize(picturePath, applicationConfiguration.getBookAssetsPath().resolve(picturePath.getFileName()), "jpg", targetImageDimensions.getWidth(), targetImageDimensions.getHeight());
        }
        log.debug("Importing recipe. Saving. uid={}", uid);
        return Pair.of(ImportRecipeResult.SUCCESS, recipeService.prepareAndSave(recipe).getId());
    }

    public RecipeMessage toBackgroundReport(String uid, RecipeDefinition recipeDefinition, RecipeAnalyzer recipeAnalyzer) {
        RecipeCustomizationConfiguration configuration = recipeAnalyzer.getConfiguration().getRecipeCustomizationConfiguration();
        StringBuilder backGround = new StringBuilder("<i>").append(configuration.getBackgroundReportOriginalTitleLabel()).append("</i><br>")
                .append(recipeDefinition.getName()).append("<br>");
        if (recipeDefinition.getCollections() != null) {
            backGround.append("<br>").append("<i>").append(configuration.getBackgroundReportCollectionsLabel()).append("</i><br>");
            for (String c : recipeDefinition.getCollections()) {
                backGround.append("• ").append(c).append("<br>");
            }
        }
        backGround.append("<br><br>")
                .append("<i>Ref</i> ").append(uid).append("<br><br>")
                .append("<i>").append(configuration.getBackgroundReportSourceLabel()).append("</i><br>")
                .append(recipeDefinition.getSource())
                .append("<br>");
        if (StringUtils.isNotBlank(recipeDefinition.getAuthor())) {
            backGround
                    .append("<br><i>").append(configuration.getBackgroundReportAuthorLabel()).append("</i><br>")
                    .append(replaceAll(recipeDefinition.getAuthor(), EOL_PATTERN, "<br>"));
        }
        return new RecipeMessage("", backGround.toString());
    }

    public String toRecipeAdditionStepUnclearMessage(String stepMessage, RecipeAnalyzer recipeAnalyzer) {
        RecipeCustomizationConfiguration configuration = recipeAnalyzer.getConfiguration().getRecipeCustomizationConfiguration();
        return configuration.getRecipeAdditionUnclearStepSubtitle() + "<br>" + stepMessage;
    }

    public RecipeMessage toRecipeIngredientUnmatchedReport(List<RecipeIngredientAnalysis> ingredientsByUnmatched, RecipeAnalyzer recipeAnalyzer) {
        RecipeCustomizationConfiguration configuration = recipeAnalyzer.getConfiguration().getRecipeCustomizationConfiguration();
        return new RecipeMessage(configuration.getRecipeUnclearTitle(),
                new StringBuilder()
                        .append(configuration.getRecipeUnclearBody()).append("<br>")
                        .append(ingredientsByUnmatched.stream()
                                .map(RecipeIngredientAnalysis::getIngredientNotation)
                                .map(s -> " • " + s)
                                .collect(Collectors.joining("<br>"))).toString()
        );
    }

    public UnitNotation toRecipeServingUnitNotation(RecipeMeasurement recipeMeasurement, Locale locale) {
        return unitService.getPersisted(recipeMeasurement.getUnit(), locale, UnitNotationPriority.Enum.RECIPE_SPECIFIC, UnitType.Enum.PORTION);
    }

    private List<RecipeStep> toRecipeSteps(RecipeDefinition.RecipeSection recipeSection, Recipe recipe, RecipeAnalyzer recipeAnalyzer, Locale locale) {  // NOPMD Clear view
        log.debug("Importing recipeSection name={}", recipeSection.getName());

        RecipeInstructionAnalyzer recipeInstructionAnalyzer = recipeAnalyzer.getRecipeInstructionAnalyzer();
        RecipeIngredientAnalyzer recipeIngredientAnalyzer = recipeAnalyzer.getRecipeIngredientAnalyzer();

        RecipeGroup recipeGroup = RecipeGroup.of(recipeAnalyzer.getRecipeLangAnalyzer().normalizeText(recipeSection.getName()), locale); // Each section is a group
        recipe.getRecipeGroups().add(recipeGroup);

        List<RecipeIngredientAnalysis> recipeIngredientAnalyses = recipeIngredientAnalyzer.analyze(recipeSection.getIngredients());
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = recipeInstructionAnalyzer.analyze(recipeSection.getInstructions(), recipeIngredientAnalyses, applicationConfiguration.isImportAnalysisStrictSuper());

        List<RecipeStep> recipeSteps = new ArrayList<>();

        List<RecipeIngredientAnalysis> ingredientsByUnmatched = recipeIngredientAnalyzer.findIngredientsByUnmatched(recipeIngredientAnalyses, recipeInstructionAnalyses);
        boolean ingredientsUnmatchedPresent = !ingredientsByUnmatched.isEmpty();
        if (ingredientsUnmatchedPresent) {
            if (applicationConfiguration.isImportAnalysisStrict()) {
                throw new RecipeImportUncheckedException("Could not find essential ingredient in analysis. missingIngredients={}", ingredientsByUnmatched);
            }
            log.warn("Could not find essential ingredient in analysis. missingIngredients={}", ingredientsByUnmatched);
            try {
                RecipeCategoryDefinition zzzProblem = recipeAnalyzer.getRecipeCategoryAnalyzer().getRecipeCategoryDefinition("Zzz Problems", true);
                if (recipe.getRecipeCategories().stream().noneMatch(rc -> zzzProblem.getUid().equals(rc.getUid()))) {
                    recipe.getRecipeCategories().add(recipeCategoryService.getPersisted(zzzProblem.getUid(), zzzProblem.getName(), locale, applicationConfiguration.getBookRecipeCategoryDefaultIconFileName(), true));
                }
            } catch (IOException e) {
                throw new RecipeImportUncheckedException("Adding warn category failed!", e);
            }

            RecipeMessage report = toRecipeIngredientUnmatchedReport(ingredientsByUnmatched, recipeAnalyzer);
            recipeSteps.add(recipeService.createRecipeStep(report.getTitle(), recipeGroup, locale,
                    of(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, report.getBody(), locale))));
        }

        Map<RecipeIngredientAnalysis, RecipeIngredient> recipeIngredients = new HashMap<>();
        Set<RecipeIngredientAnalysis> unusedRecipeIngredientAnalyses = new HashSet<>(recipeIngredientAnalyses);
        for (RecipeInstructionAnalysis ria : recipeInstructionAnalyses) {
            log.debug("Importing recipeSectionStep ria={}", ria);
            if (ria.isTagsAnyPresent(QUALIFIES_PREVIOUS)) {
                log.debug("Importing recipeSectionStep QUALIFIES_PREV. Expect to handle by near step.");
                continue;
            }

            String recipeStepText = ria.getText();
            String guidedStepExplanationText = recipeInstructionAnalyzer.getTextAsHumanReadable(ria);
            List<GuidedStep> guidedSteps = new ArrayList<>(); // NOPMD
            // REPLACE with tag from analysis. TAG added if discrepancy detected between expected ingredients (count/length) and added ingredients in TAGS_DO_ADDITION + LOCATION_MAIN
//            if ((ingredientsUnmatchedPresent || ria.streamRecipeIngredientTerms().anyMatch(t -> !t.isConclusive())) && ria.isTagPresent(IS_INGREDIENT_ADDITION)) {
            if (ria.isTagPresent(IS_INGREDIENT_POTENTIAL_MISMATCH) && applicationConfiguration.isImportWarnPotentialMismatch()) {
                log.debug("Importing addition step with some non conclusive ingredients. Adding explanation step just in case something is forgotten. ria={}", ria);
                // CHANGE. Generate special text trying to guess what is matched and what is not and remove matched or <u></u> underline not found ingredients
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, toRecipeAdditionStepUnclearMessage(guidedStepExplanationText, recipeAnalyzer), locale));
            }

            if (ria.isTagPresentInNext(QUALIFIES_PREVIOUS) && !ria.isTagPresentInNext(SEQUENCE_PARALLEL)) {
                RecipeInstructionAnalysis nextRia = ria.getNext();
                recipeStepText = TextUtil.capitalizeFirst(nextRia.getText()) + ". " + recipeStepText; // NOPMD
                nextRia.streamRecipeToolComposedActionTerms().forEach(t -> {
                    log.debug("create recipeSectionSubStep utensilAction from NEXT. ria={}", t);
                    guidedSteps.add(createGuidedStepUtensil(t, nextRia.getText(), locale));
                });
            }

            for (RecipeTerm recipeTerm : recipeInstructionAnalyzer.getTermsByConclusiveAndSortedByTypeAndOffset(ria)) {

                if (recipeTerm instanceof RecipeIngredientTerm) {
                    RecipeIngredientTerm recipeIngredientTerm = (RecipeIngredientTerm) recipeTerm;
                    GuidedStep guidedStep = createGuidedStepIngredient(recipeIngredientTerm, recipeGroup, ria, guidedStepExplanationText, locale, recipeIngredients);
                    if (guidedStep != null) {
                        guidedSteps.add(guidedStep);
                        unusedRecipeIngredientAnalyses.remove(recipeIngredientTerm.getIngredient());
                    }
                    continue;
                }

                if (recipeTerm instanceof RecipeCommandTerm) {
                    RecipeCommandTerm recipeCommandTerm = (RecipeCommandTerm) recipeTerm;
                    GuidedStep guidedStep = createGuidedStepRecipeCommand(recipeCommandTerm, guidedStepExplanationText, locale);
                    if (guidedStep != null) {
                        if (ria.isTagPresent(DO_COMMAND_MAIN_PREPARATION_DEFAULT)) {
                            log.debug("create recipeSectionSubStep commandStandardPreparation (before command)");
                            guidedSteps.add(guidedUtensilStepService.createGuidedStepUtensil(GuidedUtensilActionType.Enum.MEASURING_CUP_PUT, recipeAnalyzer.getRecipeCustomizationConfiguration().getMeasuringLidAndCupBody(), true, locale));
                        }
                        guidedSteps.add(guidedStep);
                    }
                    continue;
                }

                if (recipeTerm instanceof RecipeToolComposedActionTerm) {
                    guidedSteps.add(createGuidedStepUtensil((RecipeToolComposedActionTerm) recipeTerm, guidedStepExplanationText, locale));
                }
            }

            if (ria.isTagPresentInNext(QUALIFIES_PREVIOUS) && ria.isTagPresentInNext(SEQUENCE_PARALLEL)) {
                log.debug("create recipeSectionSubStep textParallel from NEXT. ria={}", ria);
                RecipeInstructionAnalysis nextRia = ria.getNext();
                recipeStepText = recipeStepText + ". " + nextRia.getText(); // NOPMD
                String text = recipeAnalyzer.getRecipeCustomizationConfiguration().getTextAsParallel(recipeInstructionAnalyzer.getTextAsHumanReadable(nextRia));
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.PARALLEL, text, locale));
            }

            if (guidedSteps.isEmpty() && ria.isTagPresent(DO_SERVE)) {
                log.debug("create recipeSectionSubStep textServing");
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.SERVING, guidedStepExplanationText, locale));
            }

            if (guidedSteps.isEmpty() && ria.isTagPresent(SEQUENCE_PARALLEL)) {
                log.warn("create recipeSectionSubStep textParallel without previous command. ria={}", ria);
                String text = recipeAnalyzer.getRecipeCustomizationConfiguration().getTextAsParallel(guidedStepExplanationText);
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.PARALLEL, text, locale));
            }

            if (guidedSteps.isEmpty()) {
                log.debug("create recipeSectionSubStep textOther");
                guidedSteps.add(guidedFreetextStepService.createGuidedStepFreetext(GuidedFreetextType.Enum.DEFAULT, guidedStepExplanationText, locale));
            }

            recipeSteps.add(recipeService.createRecipeStep(recipeStepText, recipeGroup, locale, guidedSteps));
        }

        for (RecipeIngredientAnalysis unusedRecipeIngredientAnalysis : unusedRecipeIngredientAnalyses) {
            RecipeIngredient recipeIngredient = createRecipeIngredient(unusedRecipeIngredientAnalysis, recipeGroup, locale);
            recipe.getRecipeIngredients().add(recipeIngredient);
        }

        return recipeSteps;
    }

    public enum ImportRecipeResult {
        SUCCESS, SKIP
    }


}
