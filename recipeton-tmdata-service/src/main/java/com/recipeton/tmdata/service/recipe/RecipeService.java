package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.TmControl;
import com.recipeton.tmdata.domain.material.MaterialFlag;
import com.recipeton.tmdata.domain.material.MaterialObject;
import com.recipeton.tmdata.domain.material.MaterialObjectFlag;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.RecipeStepLang;
import com.recipeton.tmdata.domain.recipe.lang.RecipeStepLangAttribute;
import com.recipeton.tmdata.domain.recipe.lang.RecipeTimeLang;
import com.recipeton.tmdata.service.material.MaterialObjectFlagRepository;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.recipe.lang.RecipeStepLangAttributeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static com.recipeton.shared.util.ListUtil.toListMutable;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.tmdata.domain.misc.TmVersion.isApp;
import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeService {

    private final RecipeTimeTypeRepository recipeTimeTypeRepository;
    private final RangeTypeRepository rangeTypeRepository;
    private final RecipeRepository recipeRepository;
    private final RecipeStepLangAttributeRepository recipeStepLangAttributeRepository;
    private final MaterialService materialService;
    private final MaterialObjectFlagRepository materialObjectFlagRepository;
    private final RecipeStepRepository recipeStepRepository;

    public List<RecipeMaterialObject> createRecipeMaterialObjects(Path picturePath, long mdbId) {
        return of(
                new RecipeMaterialObject()
                        .setPosition(1L)
                        .setPrimary(true)
                        .setMaterialObject(
                                new MaterialObject()
                                        .setMaterialObjectFlags(toListMutable(materialObjectFlagRepository.getOne(MaterialObjectFlag.Enum.RESULT)))
                                        .setMaterial(materialService.createMaterial(picturePath.getFileName(), mdbId, MaterialFlag.Enum.PROFESSIONAL))
                        )
        );
    }

    public RecipeStep createRecipeStep(String content, RecipeGroup recipeGroup, Locale locale, GuidedStep... guidedSteps) {
        return createRecipeStep(content, recipeGroup, locale, of(guidedSteps));
    }

    public RecipeStep createRecipeStep(String content, RecipeGroup recipeGroup, Locale locale, List<GuidedStep> guidedSteps) {
        return new RecipeStep()
                .setRecipeGroup(recipeGroup)
                .setLangs(of(
                        new RecipeStepLang()
                                .setText(content)
                                .setAttribute(recipeStepLangAttributeRepository.getOne(RecipeStepLangAttribute.Enum.CONTENT))
                                .setLocale(locale)
                ))
                //.setRecipeStepMaterialObjects( ) // Not essential.
                .setGuidedSteps(guidedSteps)
                .setRecipeStepUtensils(new ArrayList<>());
    }

    public RecipeTime createRecipeTime(RecipeTimeType.Enum type, Duration duration, String text, Locale locale) {
        if (duration == null) {
            return null;
        }
        return new RecipeTime()
                .setRecipeTimeType(recipeTimeTypeRepository.getOne(type))
                .setLangs(text == null ? null : of(
                        new RecipeTimeLang()
                                .setText(text)
                                .setLocale(locale)
                ))
                .setRecipeTimeRanges(of(
                        new RecipeTimeRange()
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM))
                                .setSecondsValue(duration.getSeconds())
                ));
    }


    public void prepare(Recipe recipe) { // NOPMD Complex
        if (recipe.getRecipeSteps() == null) {
            throw new IllegalArgumentException("Trying to save recipe without steps uid=" + recipe.getUid());
        }
        long recipeStepPosition = 1;

        AtomicLong guidedStepPosition = new AtomicLong(1L);
        for (RecipeStep recipeStep : recipe.getRecipeSteps()) {
            if (recipeStep.getRecipe() == null) {
                recipeStep.setRecipe(recipe);
            }
            recipeStep.setPosition(recipeStepPosition)
                    .setDisplayNo(recipeStepPosition);
            recipeStepPosition++;
            if (recipeStep.getGuidedSteps() == null) {
                continue;
            }

            Optional<RecipeTmVersion> tmVersion = toStream(recipe.getRecipeTmVersions())
                    .filter(rt -> isApp(rt.getTmVersion()))
                    .findFirst();
            long recipeStepIngredientPosition = 1;

            List<RecipeStepRecipeIngredient> existingRecipeStepRecipeIngredients = new ArrayList<>(); // NOPMD

            for (GuidedStep guidedStep : recipeStep.getGuidedSteps()) {
                if (guidedStep.getRecipeStep() == null) {
                    guidedStep.setRecipeStep(recipeStep);
                }
                guidedStep.setPosition(guidedStepPosition.getAndIncrement());

                if (guidedStep.getGuidedIngredientStep() != null) {
                    for (GuidedIngredientStepIngredient g : guidedStep.getGuidedIngredientStep().getGuidedIngredientStepIngredients()) {
                        RecipeStepRecipeIngredient recipeStepRecipeIngredient = g.getRecipeStepRecipeIngredient();
                        recipeStepRecipeIngredient.setRecipeStep(recipeStep);
                        RecipeStepRecipeIngredient duplicatedRecipeIngredient = existingRecipeStepRecipeIngredients.stream()
                                .filter(r -> r.getRecipeIngredient().equals(recipeStepRecipeIngredient.getRecipeIngredient()))
                                .findFirst().orElse(null);
                        if (duplicatedRecipeIngredient != null) { // Unlikely situation.
                            log.warn("Duplicated ingredients in same step. Investigate recipe. uid={}", recipe.getUid());
                            g.setRecipeStepRecipeIngredient(duplicatedRecipeIngredient);
                            continue;
                        }
                        existingRecipeStepRecipeIngredients.add(recipeStepRecipeIngredient);
                        recipeStepRecipeIngredient.setPosition(recipeStepIngredientPosition);
                        recipeStepIngredientPosition++;

                        RecipeIngredient proposedRecipeIngredient = recipeStepRecipeIngredient.getRecipeIngredient();
                        recipe.getRecipeIngredients().add(proposedRecipeIngredient);

//                        RecipeIngredient proposedRecipeIngredient = recipeStepRecipeIngredient.getRecipeIngredient();
//
//                        Optional<RecipeIngredient> maybeExistingRecipeIngredient = RecipeIngredient.find(proposedRecipeIngredient, recipe.getRecipeIngredients(), locale);
//                        if (maybeExistingRecipeIngredient.isPresent()) {
//                            recipeStepRecipeIngredient.setRecipeIngredient(maybeExistingRecipeIngredient.get());
//                        } else {
//                            recipe.getRecipeIngredients().add(proposedRecipeIngredient);
//                        }
                    }
                    continue;
                }

                if (guidedStep.getGuidedTmSettingStep() != null) {
                    GuidedTmSettingStep tmSettingStep = guidedStep.getGuidedTmSettingStep();
                    tmSettingStep.setRecipeStep(recipeStep);

                    TmControl tmControl = tmSettingStep.getTmControl();

                    tmControl.getRecipeStepTmControls().add(new RecipeStepTmControl() // NOPMD instantiate
                            .setRecipeTmVersion(tmVersion.orElseThrow(() -> new IllegalStateException("Trying to save recipe with tmControls and tmVersion  below APP"))) // NOPMD BS
                            .setRecipeStep(recipeStep)
                            .setTmControl(tmControl));
                }
            }
        }
    }

    @Transactional
    public Recipe prepareAndSave(Recipe recipe) { // NOPMD complex
        prepare(recipe);

        return save(recipe);
    }

    private Recipe save(Recipe recipe) {
        Recipe saved = recipeRepository.save(recipe);
        recipeStepRepository.saveAll(saved.getRecipeSteps()); // Not needed??
        return saved;
    }


}
