package com.recipeton.tmdata.service.nutrition;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.MeasurementUnitDefinition;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.misc.RangeType;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValue;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValueData;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValuePortionRange;
import com.recipeton.tmdata.domain.nutrition.RecipeNutritionalValueType;
import com.recipeton.tmdata.domain.unit.UnitNotation;
import com.recipeton.tmdata.domain.unit.UnitNotationPriority;
import com.recipeton.tmdata.domain.unit.UnitType;
import com.recipeton.tmdata.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.service.unit.UnitService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class RecipeNutritionService {

    private final UnitService unitService;
    private final RangeTypeRepository rangeTypeRepository;
    private final RecipeNutritionalValueTypeRepository recipeNutritionalValueTypeRepository;

    public RecipeNutritionalValue createRecipeNutritionalValue(BigDecimal calories, BigDecimal protein, BigDecimal carbohydrates, BigDecimal fat, BigDecimal cholesterol, BigDecimal fibre, BigDecimal caloriesKcal, UnitNotation servingUnitNotation, Locale locale) { // NOPMD
        List<RecipeNutritionalValueData> recipeNutritionalValueDatas = Stream.of(
                createRecipeNutritionalValueData(calories, locale, RecipeNutritionalValueType.Enum.CALORIC_VALUE, MeasurementUnitDefinition.UNIT_KJ),
                createRecipeNutritionalValueData(protein, locale, RecipeNutritionalValueType.Enum.PROTEIN, MeasurementUnitDefinition.UNIT_GRAM),
                createRecipeNutritionalValueData(carbohydrates, locale, RecipeNutritionalValueType.Enum.CARBS, MeasurementUnitDefinition.UNIT_GRAM),
                createRecipeNutritionalValueData(fat, locale, RecipeNutritionalValueType.Enum.FAT, MeasurementUnitDefinition.UNIT_GRAM),
                createRecipeNutritionalValueData(cholesterol, locale, RecipeNutritionalValueType.Enum.CHOLESTEROL, MeasurementUnitDefinition.UNIT_MGRAM),
                createRecipeNutritionalValueData(fibre, locale, RecipeNutritionalValueType.Enum.FIBRE, MeasurementUnitDefinition.UNIT_GRAM),
                createRecipeNutritionalValueData(caloriesKcal, locale, RecipeNutritionalValueType.Enum.CALORIC_KCAL, MeasurementUnitDefinition.UNIT_KCAL)
        ).filter(v -> v.getNutritionalValue() != null && v.getNutritionalValue().compareTo(BigDecimal.ZERO) >= 0)
                .collect(Collectors.toList());
        return createRecipeNutritionalValue(servingUnitNotation, recipeNutritionalValueDatas);
//                                .setLangs() // Optional (they are comments)
    }

    public RecipeNutritionalValue createRecipeNutritionalValue(UnitNotation servingUnitNotation) {
        return createRecipeNutritionalValue(servingUnitNotation, Collections.emptyList());
    }

    public RecipeNutritionalValue createRecipeNutritionalValue(UnitNotation servingUnitNotation, List<RecipeNutritionalValueData> recipeNutritionalValueDatas) {
        return new RecipeNutritionalValue() // Move this to service that creates this object
                .setRecipeNutritionalValueDatas(recipeNutritionalValueDatas)
                .setRecipeNutritionalValuePortionRanges(of(
                        new RecipeNutritionalValuePortionRange() // Only one here always?
                                .setPortionAmountValue(BigDecimal.ONE)
                                .setRangeType(rangeTypeRepository.getOne(RangeType.Enum.FROM.getId())) // Always FROM
                ))
                .setServingUnitNotation(servingUnitNotation);
    }

    public RecipeNutritionalValueData createRecipeNutritionalValueData(BigDecimal value, Locale locale, RecipeNutritionalValueType.Enum carbs, MeasurementUnitDefinition measurementUnitDefinition) {
        return new RecipeNutritionalValueData()
                .setNutritionalValue(value)
                .setRecipeNutritionalValueType(recipeNutritionalValueTypeRepository.getOne(carbs.getId()))
                .setUnitNotation(unitService.getPersisted(measurementUnitDefinition, locale, UnitNotationPriority.Enum.PRIMARY, UnitType.Enum.PORTION));
    }
}
