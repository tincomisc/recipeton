package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.export.RecipeSummary;
import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.Recipe;
import com.recipeton.tmdata.service.JsonResourceService;
import com.recipeton.tmdata.service.misc.LocaleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.RECIPE_CATEGORY_NAME_EXTRA_PREFIX;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeExportService {

    private final LocaleService localeService;
    private final RecipeRepository recipeRepository;
    private final JsonResourceService jsonResourceService;

    @Transactional
    public List<String> getRecipeNames() {
        Locale defaultLocale = localeService.getLocaleByDefault();

        List<Recipe> recipes = recipeRepository.findAll();
        return recipes.stream().map(r -> r.getLangText(defaultLocale)).collect(Collectors.toList());
    }

    @Transactional
    public List<RecipeSummary> getRecipeSummaries() {
        Locale defaultLocale = localeService.getLocaleByDefault();
        List<Recipe> recipes = recipeRepository.findAll();
        return recipes.stream()
                .map(r -> {
                    String mainLang = r.getLangText(defaultLocale);
                    List<String> keywords = r.getRecipeCategories().stream()
                            .map(rc -> rc.toMainLang(defaultLocale))
                            .filter(k -> !k.startsWith(RECIPE_CATEGORY_NAME_EXTRA_PREFIX))
                            .collect(Collectors.toList());
                    return RecipeSummary.builder()
                            .uid(r.getUid())
                            .title(mainLang)
                            .keywords(keywords)
                            .build();
                }).collect(Collectors.toList());
    }

    @Transactional
    public void writeFileNames(Path path) throws IOException {
        String text = getRecipeNames().stream().sorted().collect(Collectors.joining("\n"));
        Files.deleteIfExists(path);
        Files.write(path, text.getBytes(Charset.defaultCharset()), StandardOpenOption.CREATE);
    }

    @Transactional
    public void writeRecipeSummaries(Path path) throws IOException {
        Files.deleteIfExists(path);
        jsonResourceService.write(path, getRecipeSummaries());
    }

}

