package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.recipe.*;
import com.recipeton.tmdata.domain.recipe.lang.GuidedUtensilActionTextLang;
import com.recipeton.tmdata.service.material.MaterialService;
import com.recipeton.tmdata.service.recipe.lang.GuidedUtensilActionTextLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

import static java.util.List.of;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedUtensilStepService {

    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final GuidedStepMaterialObjectTypeRepository guidedStepMaterialObjectTypeRepository;
    private final GuidedUtensilActionTypeRepository guidedUtensilActionTypeRepository;
    private final GuidedUtensilActionTextLangRepository guidedUtensilActionTextLangRepository;
    private final MaterialService materialService;

    public GuidedStep createGuidedStepUtensil(GuidedUtensilActionType.Enum actionType, String text, boolean predefined, Locale locale) {
        return new GuidedStep()
                .setPosition(-1L)
                .setGuidedStepType(guidedStepTypeRepository.getOne(GuidedStepType.Enum.UTENSIL))
                .setGuidedUtensilStep(new GuidedUtensilStep()
                        .setGuidedUtensilActionText(getPersistedGuidedUtensilActionText(text, locale, predefined))
                        .setGuidedUtensilActionType(guidedUtensilActionTypeRepository.getOne(actionType)))
                .setGuidedStepMaterialObject(of(
                        new GuidedStepMaterialObject()
                                .setGuidedStepMaterialObjectType(guidedStepMaterialObjectTypeRepository.getOne(GuidedStepMaterialObjectType.Enum.ICON))
                                .setMaterialObject(materialService.getPersisted(actionType))

                ));
    }

    private GuidedUtensilActionText createGuidedUtensilActionText(Map<Locale, String> localization, boolean predefined) {
        return new GuidedUtensilActionText()
                .setPredefined(predefined)
                .setLangs(localization.entrySet().stream()
                        .map(e -> new GuidedUtensilActionTextLang()
                                .setText(e.getValue())
                                .setLocale(e.getKey())
                        ).collect(Collectors.toList())
                );
    }

    private GuidedUtensilActionText getPersistedGuidedUtensilActionText(String text, Locale locale, boolean predefined) {
        if (predefined) {
            GuidedUtensilActionText guidedUtensilActionText = guidedUtensilActionTextLangRepository.findFirstByTextAndLocale(text, locale).map(GuidedUtensilActionTextLang::getGuidedUtensilActionText).orElse(null);
            if (guidedUtensilActionText != null) {
                return guidedUtensilActionText;
            }
        }
        return createGuidedUtensilActionText(Map.of(locale, text), predefined);
    }

}
