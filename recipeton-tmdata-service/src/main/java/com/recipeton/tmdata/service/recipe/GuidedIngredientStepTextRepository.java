package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.recipe.GuidedIngredientStepText;
import com.recipeton.tmdata.domain.recipe.GuidedIngredientStepTextAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.QueryHints;

import javax.persistence.QueryHint;

import static org.hibernate.jpa.QueryHints.HINT_CACHEABLE;

public interface GuidedIngredientStepTextRepository extends JpaRepository<GuidedIngredientStepText, Long>, JpaSpecificationExecutor<GuidedIngredientStepText> {

    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    GuidedIngredientStepText getOneByAttributeAndPredefined(GuidedIngredientStepTextAttribute attribute, boolean predefined);

}
