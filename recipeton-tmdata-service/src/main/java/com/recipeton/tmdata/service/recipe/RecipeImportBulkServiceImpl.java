package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeCategoryDefinition;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import com.recipeton.tmdata.config.ApplicationConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeImportBulkServiceImpl implements RecipeImportBulkService {

    private static final long IMPORT_DELAY = 1000L;

    private final ApplicationConfiguration applicationConfiguration;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;
    private final RecipeImportService recipeImportService;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeService recipeService;
    private final RecipeExportService recipeExportService;

    private final RecipeRepository recipeRepository;

    private final AtomicBoolean importWatching = new AtomicBoolean();
    private final AtomicBoolean importRequested = new AtomicBoolean();

    private ExecutorService executor;
    private WatchService watchService;

    static void copyBookSharedAssetsIfNotPresent(Path sourceDirectory, Path targetDirectory) throws IOException {
        Files.walk(sourceDirectory).forEach(s -> {
            try {
                Path t = targetDirectory.resolve(sourceDirectory.relativize(s));
                if (Files.isDirectory(s)) {
                    if (!Files.exists(t)) {
                        Files.createDirectory(t);
                    }
                    return;
                }
                if (!Files.exists(t)) {
                    Files.copy(s, t);
                }
            } catch (IOException e) {
                throw new IllegalStateException("Copy failed ", e);
            }
        });
    }

    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        Files.createDirectories(applicationConfiguration.getBookAssetsSharedPath());
        Files.createDirectories(applicationConfiguration.getBookAssetsPath());
        Files.createDirectories(applicationConfiguration.getBookRecipesImportPath());
        Files.createDirectories(applicationConfiguration.getBookRecipesImportCompletePath());

        copyBookSharedAssetsIfNotPresent(applicationConfiguration.getBookAssetsSharedPath(), applicationConfiguration.getBookExportPath());
    }

    @Override
    public void doImport() throws IOException {
        if (recipeRepository.count() == 0) {
            Path bookResourceImportDonePath = applicationConfiguration.getBookRecipesImportCompletePath();
            log.info("doImport. Recipes empty. Check Recipes path first path={}", bookResourceImportDonePath);
            doImportDirectory(bookResourceImportDonePath);
        }

        doImportDirectory(applicationConfiguration.getBookRecipesImportPath());
    }

    private void doImportDirectory(Path directoryPath) throws IOException {
        List<Path> paths = Files.walk(directoryPath, 1)
                .filter(recipeDefinitionStorageService::isRecipe)
                .collect(Collectors.toList());
        log.info("doImport found recipes. count={}, path={}", paths.size(), directoryPath);
        for (Path path : paths) {
            doImportFile(path);
        }
        log.info("doImport completed. count={}, path={}", paths.size(), directoryPath);

        if (!paths.isEmpty()) {
            recipeCategoryService.removeRecipeCategoriesByRecipeCountLessThan(applicationConfiguration.getImportRecipeCategoriesRecipeCountMin(), rc -> RecipeCategoryDefinition.isRecipeCategoryUidExtra(rc.getUid()));
            recipeCategoryService.removeRecipeCategoriesByRecipeCountGreaterThan(applicationConfiguration.getImportRecipeCategoriesRecipeCountMax(), rc -> RecipeCategoryDefinition.isRecipeCategoryUidExtra(rc.getUid()));
            recipeCategoryService.updateRecipeCategoryPositions();

            recipeExportService.writeFileNames(applicationConfiguration.getBookRootPath().resolve(applicationConfiguration.getBookName() + ".lst"));
            recipeExportService.writeRecipeSummaries(applicationConfiguration.getBookRootPath().resolve(applicationConfiguration.getBookName() + ".json"));
        }
    }

    private void doImportFile(Path recipePath) throws IOException {
        log.debug("Importing. path={}", recipePath);
        Path picturePath = recipePath.resolveSibling(FilenameUtils.getBaseName(recipePath.getFileName().toString()) + ".jpg");
        try {
            if (!Files.exists(picturePath)) {
                log.warn("Import missing picture. path={}", recipePath);
            }

            Pair<RecipeImportService.ImportRecipeResult, Long> pair = recipeImportService.doImportRecipe(recipePath, picturePath);
            switch (pair.getKey()) {
                case SUCCESS:
                    moveToTarget(recipePath, picturePath, applicationConfiguration.getBookRecipesImportCompletePath());
                    break;
                case SKIP:
                    moveToTarget(recipePath, picturePath, applicationConfiguration.getBookRecipesImportSkipPath());
                    break;
                default:
                    break;
            }
//        } catch (IOException | DataIntegrityViolationException  e) {
        } catch (Exception e) { // NOPMD Finding out types...
            log.error("Import failed. Transfer file to error. path={}", recipePath, e);
            try {
                Path errorPath = applicationConfiguration.getBookRecipesImportErrorPath();
                moveToTarget(recipePath, picturePath, errorPath);
                try (PrintWriter pw = new PrintWriter(errorPath.resolve(recipePath.getFileName() + ".err").toFile())) {
                    pw.println("Error trying to import " + recipePath);
                    pw.println("Reason: " + e.getMessage());
                    pw.println("-------------------------------------------------------------------------------------");
                    e.printStackTrace(pw);
                }
            } catch (IOException e2) {
                log.error("Import failed. Failed moving to error.", e2);
                throw e2;
            }
        }
    }

    private Path move(Path source, Path target) throws IOException {
        if (source.equals(target)) {
            return target;
        } else {
            Path parent = target.getParent();
            if (parent != null) {
                Files.createDirectories(parent);
            }

            try {
                return Files.move(source, target, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE);
            } catch (AtomicMoveNotSupportedException var4) {
                return Files.move(source, target, StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }

    @Override
    public void moveToTarget(Path recipePath, Path picturePath, Path targetPath) throws IOException {
        Files.createDirectories(targetPath);
        if (Files.exists(recipePath)) {
            move(recipePath, targetPath.resolve(recipePath.getFileName()));
        }
        if (Files.exists(picturePath)) {
            move(picturePath, targetPath.resolve(picturePath.getFileName()));
        }
    }

    @Override
    @PostConstruct
    public void postConstruct() {
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    @PreDestroy
    public void preDestroy() throws IOException {
        if (executor != null) {
            executor.shutdown();
        }

        if (watchService != null) {
            watchService.close();
        }

    }

    @Override
    public void requestDoImport() {
        if (importRequested.compareAndSet(false, true)) {
            log.info("Import request submitted");
            executor.submit(() -> {
                try {
                    Thread.sleep(IMPORT_DELAY); // wait a bit for file copy completion (also picture)
                    importRequested.set(false);

                    doImport();

                    if (importRequested.get()) {
                        log.info("Import finished but new import was requested");
                        importRequested.set(false);
                        requestDoImport();
                    }
                } catch (IOException | InterruptedException e) {
                    log.error("Import failed", e);

                }
            });
        }
    }

    @Override
    public void startDirectoryWatch() throws IOException {
        if (importWatching.compareAndSet(false, true)) {
            log.info("Watching directories");
            watchService = FileSystems.getDefault().newWatchService();

            Path bookRecipesImportPath = applicationConfiguration.getBookRecipesImportPath();
            bookRecipesImportPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);

            Path bookRecipesPath = applicationConfiguration.getBookRecipesImportCompletePath();
            bookRecipesPath.register(watchService, StandardWatchEventKinds.ENTRY_DELETE);

            try {
                WatchKey key;
                while ((key = watchService.take()) != null) {
                    boolean importChanged = false;
                    Set<Path> dataFilesRemoved = new HashSet<>();  // NOPMD
                    Path dir = (Path) key.watchable();
                    for (WatchEvent<?> event : key.pollEvents()) {
                        Path filePath = dir.resolve(String.valueOf(event.context()));
                        log.debug("Event kind={}, file={}", event.kind(), filePath);
                        if (filePath.startsWith(bookRecipesImportPath)) {
                            importChanged = true;
                        } else if (filePath.startsWith(bookRecipesPath)) {
                            dataFilesRemoved.add(filePath);
                        }
                    }
                    key.reset();

                    if (importChanged) {
                        requestDoImport();
                    }
                    if (!dataFilesRemoved.isEmpty()) {
                        dataFilesRemoved.forEach(p -> {
                            String recipeId = FilenameUtils.getBaseName(p.getFileName().toString());
                            log.info("Data changed. Removing id={}, paths={}", recipeId, p);
                            recipeRepository.findFirstByUid(recipeId).ifPresent(recipeRepository::delete);

                        });
                    }
                }

            } catch (InterruptedException e) {
                log.error("Interrupted file watch", e);
            }

            log.info("Start first import");
            doImport();
        } else {
            log.warn("Aleady watching directories. Ignoring request.");
        }
    }

}
