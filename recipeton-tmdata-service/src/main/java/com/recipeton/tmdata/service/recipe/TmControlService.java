package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.control.*;
import com.recipeton.tmdata.service.DataInitializer;

import java.time.Duration;

public interface TmControlService extends DataInitializer {
    TmControl createTmControl(Duration from, Duration to, TmControlTemperatureType.Enum temperature, TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction);

    TmControl createTmControlByDough(Duration from, Duration to, TmControlTemperatureType.Enum temperatureType);

    TmControl createTmControlBySoftBlending(Duration duration, TmControlSpeedType.Enum startSpeed, TmControlSpeedType.Enum viaSpeed, TmControlSpeedType.Enum finalSpeed, TmControlTemperatureType.Enum temperatureType);

    TmControl createTmControlByTurbo(TmControlTurboType.Enum turboType, int impulseCount, TmControlTemperatureType.Enum temperatureType);

    TmControlSpeed createTmControlSpeed(TmControlSpeedType.Enum speed, TmControlRotationDirectionType.Enum direction);

    TmControlSpeed createTmControlSpeed(TmControlSpeedType.Enum speedFrom, TmControlSpeedType.Enum speedTo, TmControlRotationDirectionType.Enum direction);

    TmControlTime createTmControlTimeByFixedTime(Duration duration);

    TmControlTime createTmControlTimeByRange(Duration from, Duration to);

    TmControlTime createTmControlTimeByUserDefined();

    TmControlTime createTmControlTimeByUserManually();
}
