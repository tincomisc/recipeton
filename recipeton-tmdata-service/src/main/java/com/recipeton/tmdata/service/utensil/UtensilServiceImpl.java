package com.recipeton.tmdata.service.utensil;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.domain.misc.Locale;
import com.recipeton.tmdata.domain.utensil.Utensil;
import com.recipeton.tmdata.domain.utensil.UtensilType;
import com.recipeton.tmdata.domain.utensil.lang.UtensilLang;
import com.recipeton.tmdata.domain.utensil.lang.UtensilLangAttribute;
import com.recipeton.tmdata.service.DataInitializer;
import com.recipeton.tmdata.service.utensil.lang.UtensilLangAttributeRepository;
import com.recipeton.tmdata.service.utensil.lang.UtensilLangRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
public class UtensilServiceImpl implements UtensilService {

    private final UtensilRepository utensilRepository;
    private final UtensilLangRepository utensilLangRepository;
    private final UtensilLangAttributeRepository utensilLangAttributeRepository;
    private final UtensilTypeRepository utensilTypeRepository;

    @Override
    @Transactional
    public void doDataInitialize() {
        DataInitializer.getPersisted(utensilLangAttributeRepository, UtensilLangAttribute.DEFAULT);
        DataInitializer.getPersisted(utensilTypeRepository, UtensilType.DEFAULT);

        // Not needed. Recheck specifically and remove
//        getPersisted("tm", "Thermomix", null, UtensilType.Enum.THERMOMIX, localeService.getLocaleByDefault());
    }

    @Override
    @Transactional
    public Utensil getPersisted(String uid, String name, String description, UtensilType.Enum type, Locale locale) {
        Utensil utensil = utensilRepository.findByUid(uid).orElse(null);
        if (utensil != null) {
            return utensil;
        }

        UtensilLangAttribute nameAttribute = utensilLangAttributeRepository.getOne(UtensilLangAttribute.Enum.NAME);
        List<UtensilLang> utensilLangs = utensilLangRepository.findByTextAndLocaleAndAttribute(name, locale, nameAttribute);
        if (!utensilLangs.isEmpty()) {
            return utensilLangs.get(0).getUtensil();
        }

        utensil = new Utensil()
                .setUid(uid)
                .setUtensilType(utensilTypeRepository.getOne(type))
                .setLangs(Stream.of(
                        new UtensilLang()
                                .setAttribute(nameAttribute)
                                .setText(name)
                                .setLocale(locale),
                        description == null ? null : new UtensilLang()
                                .setAttribute(utensilLangAttributeRepository.getOne(UtensilLangAttribute.Enum.DESCRIPTION))
                                .setText(description)
                                .setLocale(locale)
                        )
                                .filter(Objects::nonNull)
                                .collect(Collectors.toList())
                )
                .setVersion(BigDecimal.ONE);
        log.debug("saving utensil={}", utensil);
        return utensilRepository.save(utensil);
    }


//    CREATE MaterialObjects for guided MaterialObect...
}

