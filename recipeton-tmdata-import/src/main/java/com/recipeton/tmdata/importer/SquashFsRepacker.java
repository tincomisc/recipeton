package com.recipeton.tmdata.importer;

/*-
 * #%L
 * recipeton-tmdata-import
 * %%
 * Copyright (C) 2021 - 2022 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import de.tisoft.jsquashfs.parser.Squashfs;
import io.kaitai.struct.ByteBufferKaitaiStream;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

@Slf4j
public class SquashFsRepacker {

    public static final int SECTOR_SIZE = 512;

    private final byte[] key;
    private final File sourceFile;
    private final File baseDirectory;

    public SquashFsRepacker(String keyString, File baseDirectory, File sourceFile) {
        key = fromHexString(keyString);
        this.baseDirectory = baseDirectory;
        this.sourceFile = sourceFile;
    }

    public byte[] decrypt(int sectorId, byte[] encryptedBytes, byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        int ivSize = 16;

        // Extract IV.
        byte[] iv = new byte[ivSize];
        Arrays.fill(iv, (byte) 0);
        ByteBuffer.wrap(iv).order(ByteOrder.LITTLE_ENDIAN).putInt(sectorId);

        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Extract encrypted part.
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Decrypt.
        Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/NoPadding");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

        return cipherDecrypt.doFinal(encryptedBytes);
    }

    public void doRepack() throws IOException, NoSuchAlgorithmException {
        log.debug("repacking");

        baseDirectory.mkdirs();

        if (isBlockDevice()) {
            // it's a block device, get the serial from the corresponding usb device
            Files.copy(Path.of("/sys/class/block", sourceFile.getName()).toRealPath().resolve("../../../../../../serial").toRealPath(),
                // What is???? ../../../../../../
                baseDirectory.toPath().resolve("serial").toAbsolutePath(), StandardCopyOption.REPLACE_EXISTING);
        }

        RandomAccessFile randomAccessFile = new RandomAccessFile(sourceFile, "r");
        RandomAccessFile iso = new RandomAccessFile(new File(baseDirectory, baseDirectory.getName() + ".iso"), "rw");

        byte[] buffer = new byte[8192];
        while (randomAccessFile.getFilePointer() < randomAccessFile.getChannel().size()) {
            int read = randomAccessFile.read(buffer);
            iso.write(buffer, 0, read);
        }
        randomAccessFile.seek(0);

        RandomAccessSource source = new RandomAccessSource(randomAccessFile, key);

        byte[] b = new byte[(int) source.length()];
        source.read(b);

        Squashfs squashfs = new Squashfs(new ByteBufferKaitaiStream(b));

        Squashfs.InodeHeader rootInode = squashfs.superblock().rootInodeRef().inodeTable().inodeHeader();

        Map<Long, Squashfs.InodeHeader> inodes = squashfs.inodeTable().inodes().inodeHeader()
            .stream()
            .collect(toMap(Squashfs.InodeHeader::inodeNumber, Function.identity()));

        extract(inodes, rootInode, new File(baseDirectory, "export"));

        // make sure th md5 files are created so that update_images.sh will skip these directories
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        try (FileInputStream is = new FileInputStream(new File(baseDirectory, "export/ext.sdb"))) {
            int nread;
            while ((nread = is.read(buffer)) != -1) {
                messageDigest.update(buffer, 0, nread);
            }
        }
        String sb = toHexBytes(messageDigest.digest());
        System.out.println("db.md5 " + sb);
        Files.write(new File(baseDirectory, "db.md5").toPath(), sb.getBytes(StandardCharsets.UTF_8));

        String ls = Arrays.stream(new File(baseDirectory, "export/material/photo/150x150").listFiles()).map(File::getName)
            .sorted(Comparator.comparing(String::toLowerCase))
            .collect(joining("\n")) + "\n";

        messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(ls.getBytes(StandardCharsets.UTF_8));
        sb = toHexBytes(messageDigest.digest());
        System.out.println("photos.md5 " + sb);
        Files.write(new File(baseDirectory, "photos.md5").toPath(), sb.getBytes(StandardCharsets.UTF_8));
    }

    public void extract(Map<Long, Squashfs.InodeHeader> inodes, Squashfs.InodeHeader inodeHeader, File file) {
        System.out.println(file);
        if (inodeHeader.type() == Squashfs.InodeType.BASIC_DIRECTORY) {
            file.mkdirs();
            Squashfs.InodeHeaderBasicDirectory directory = (Squashfs.InodeHeaderBasicDirectory) inodeHeader.header();
            directory.dir().directoryHeader().stream()
                .map(Squashfs.DirectoryHeader::directoryEntry)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .forEach(directoryEntry -> {
                    File entryFile = new File(file, directoryEntry.name());
                    extract(inodes, inodes.get((long) directoryEntry._parent().inodeNumber() + directoryEntry.inodeOffset()), entryFile);
                });
        } else if (inodeHeader.type() == Squashfs.InodeType.EXTENDED_DIRECTORY) {
            file.mkdirs();
            Squashfs.InodeHeaderExtendedDirectory directory = (Squashfs.InodeHeaderExtendedDirectory) inodeHeader.header();
            directory.dir().directoryHeader().stream()
                .map(Squashfs.DirectoryHeader::directoryEntry)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .forEach(directoryEntry -> {
                    extract(inodes, inodes.get((long) directoryEntry._parent().inodeNumber() + directoryEntry.inodeOffset()), new File(file, directoryEntry.name()));
                });
        } else if (inodeHeader.type() == Squashfs.InodeType.BASIC_FILE) {
            Squashfs.InodeHeaderBasicFile basicFile = (Squashfs.InodeHeaderBasicFile) inodeHeader.header();
            try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
                raf.setLength(basicFile.fileSize());
                byte[] data = new byte[(int) basicFile.fileSize()];
                for (Squashfs.DataBlock block : basicFile.blocks()) {
                    raf.write(block.data().data());
                }
                if (basicFile.fragIndex() != 0xFFFFFFFFL) {
                    raf.write(basicFile.fragment().block().data().data(), (int) basicFile.blockOffset(), (int) (data
                        .length - raf.getFilePointer()));
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            throw new IllegalArgumentException("Unsupported inode type " + inodeHeader.type());
        }
    }

    public byte[] fromHexString(String s) {
        int len = s.length();
        if (len % 2 != 0) {
            throw new IllegalArgumentException("The string must have an even length: " + s);
        }
        byte[] data = new byte[len / 2];
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) ((Character.digit(s.charAt(i * 2), 16) << 4) + Character.digit(s.charAt(i * 2 + 1), 16));
        }
        return data;
    }

    public boolean isBlockDevice() {
        return sourceFile.getParentFile().toString().equals("/dev");
    }

    public String toHexBytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    class RandomAccessSource {
        private final RandomAccessFile randomAccessFile;
        private final byte[] key;
        int currentSectorId;
        byte[] currentSector = new byte[SECTOR_SIZE];
        int currentPosInSector;

        public RandomAccessSource(RandomAccessFile randomAccessFile, byte[] key) throws IOException {
            this.randomAccessFile = randomAccessFile;
            this.key = key;
            readSector(currentSectorId);
        }

        public long getFilePointer() throws IOException {
            return currentSectorId * SECTOR_SIZE + currentPosInSector;
        }

        public long length() throws IOException {
            return randomAccessFile.getChannel().size();
        }

        public int read() throws IOException {
            byte[] b = new byte[1];
            if (read(b) == -1) {
                return -1;
            } else {
                return b[0];
            }
        }

        public int read(byte[] buffer, int off, int len) throws IOException {
            int count = 0;
            while (count < len) {
                int length = Math.min(len, SECTOR_SIZE - currentPosInSector);
                System.arraycopy(currentSector, currentPosInSector, buffer, off + count, length);
                count += length;
                currentPosInSector += length;
                if (count < len && (currentSectorId + 1) * SECTOR_SIZE < length()) {
                    // need to read next sector
                    readSector(currentSectorId + 1);
                } else {
                    break;
                }
            }
            return count;
        }

        public int read(byte[] bytes) throws IOException {
            return read(bytes, 0, bytes.length);
        }

        private void readSector(int newSectorId) throws IOException {
            currentSectorId = newSectorId;
            currentPosInSector = 0;
            randomAccessFile.seek((long) currentSectorId * SECTOR_SIZE);
            randomAccessFile.read(currentSector);
            try {
                currentSector = decrypt(currentSectorId, currentSector, key);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
                throw new IOException(e);
            }
        }

        public void seek(long l) throws IOException {
            currentSectorId = (int) l / SECTOR_SIZE;
            readSector(currentSectorId);
            currentPosInSector = (int) l % SECTOR_SIZE;
        }
    }


}
