package com.recipeton.scrapper.util.selenium;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface SeleniumTrait {

    Duration DEFAULT_TIMEOUT = Duration.ofSeconds(30);
    Duration WAIT_MIN = toDuration("test.web.wait.min", 0);
    Duration WAIT_IMPLICIT = toDuration("test.web.wait.implicit", 5);
    String ATTRIBUTE_SEMANTIC_QUALIFIER = "data-semq";

    private static Duration toDuration(String property, int defaultSeconds) {
        return Duration.ofSeconds(Optional.ofNullable(System.getProperty(property, null)).map(Integer::valueOf).orElse(defaultSeconds));
    }

    default Function<WebDriver, Boolean> expectInvisibilityOfElementsLocated(By... selectors) {
        return w -> Stream.of(selectors).allMatch(s -> ExpectedConditions.invisibilityOfElementLocated(s).apply(w));
    }

    WebDriver getDriver();

    default String getInnerHtml(WebElement webElement) {
        return webElement.getAttribute("innerHTML");
    }

    default String getInputValue(WebElement element) {
        return element.getAttribute("value");
    }

    default String getSemanticQualifier(WebElement webElement) {
        return webElement.getAttribute(ATTRIBUTE_SEMANTIC_QUALIFIER);
    }

    default List<String> getSemanticQualifiers(List<WebElement> webElements) {
        return webElements.stream().map(this::getSemanticQualifier).collect(Collectors.toList());
    }

    default boolean isDisplayed(WebElement webElement) {
        try {
            return webElement.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    default LogEntries popBrowserLogs() {
        return getDriver().manage().logs().get(LogType.BROWSER);
    }

    default void resetImplicitWait() {
        setImplicitlyWait(WAIT_IMPLICIT);
    }

    default void scrollAndClick(WebElement element) {
        scrollTo(element);
        element.click();
    }

    default void scrollTo(int x, int y) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("window.scrollBy(" + x + "," + y + ")", "");
    }

    default void scrollTo(WebElement element) {
        Point location = element.getLocation();
        int x = location.x;
        int y = location.y;
        scrollTo(x, y);
    }

    default void setImplicitlyWait(Duration timeOut) {
        try {
            RemoteWebDriver remoteWebDriver = (RemoteWebDriver) getDriver(); // IllegalAccessException
            remoteWebDriver.manage().timeouts().implicitlyWait(timeOut);
        } catch (Exception e) { // NOPMD
            throw new RuntimeException(e); // NOPMD
        }
    }

    default void setValueByQualifier(WebElement webElement, String semanticQualifier) {
        if (webElement.getTagName().equalsIgnoreCase("select")) {
            Select select = new Select(webElement);
            select.selectByIndex(getSemanticQualifiers(select.getOptions()).indexOf(semanticQualifier));
            return;
        }
        throw new IllegalArgumentException("Unsupported webElement type " + webElement + ". Only SELECT is currently supported.");
    }

    default By toSelector(WebElement webElement) {
        String s = webElement.toString();
        if (s.contains("By.cssSelector:")) {
            return By.cssSelector(StringUtils.substringBetween(s, "By.cssSelector:", "'").trim());
        }
        if (s.contains("css selector:")) {
            return By.cssSelector(StringUtils.substringBetween(s, "css selector:", "]").trim());
        }

        throw new RuntimeException("Could not extract selector from element. Maybe improve toSelector method. Element toString:" + webElement); // NOPMD
    }

    default String waitForAttribute(WebElement webElement, String attribute, Duration timeOut) {
        new WebDriverWait(getDriver(), timeOut).until(w -> ExpectedConditions.attributeToBeNotEmpty(webElement, attribute).apply(w));
        return webElement.getAttribute(attribute);
    }

    default void waitForCondition(ExpectedCondition<?> expectedCondition) {
        waitForCondition(expectedCondition, DEFAULT_TIMEOUT);
    }

    default boolean waitForCondition(Function<? super WebDriver, ?> expectedCondition, Duration timeOut) {
        try {
            setImplicitlyWait(WAIT_MIN);
            new WebDriverWait(getDriver(), timeOut).until(expectedCondition::apply);
            return true;
        } finally {
            resetImplicitWait();
        }
    }

    default WebElement waitForElementText(WebElement element, String text) throws Error {
        waitForElementText(element, text, DEFAULT_TIMEOUT);
        return element;
    }

    default WebElement waitForElementText(WebElement element, String text, Duration timeOut) throws Error {
        new WebDriverWait(getDriver(), timeOut).until(w -> ExpectedConditions.textToBePresentInElement(element, text).apply(w));
        return element;
    }

    default boolean waitForInvisibility(WebElement... webElements) {
        return waitForInvisibility(WAIT_IMPLICIT, Stream.of(webElements).map(this::toSelector).toArray(By[]::new));
    }

    default boolean waitForInvisibility(String... cssSelectors) {
        return waitForInvisibility(WAIT_IMPLICIT, cssSelectors);
    }

    default boolean waitForInvisibility(By... selectors) {
        return waitForInvisibility(WAIT_IMPLICIT, selectors);
    }

    default boolean waitForInvisibility(Duration timeOut, String... cssSelectors) {
        return waitForInvisibility(timeOut, Stream.of(cssSelectors).map(By::cssSelector).toArray(By[]::new));
    }

    default boolean waitForInvisibility(Duration timeOut, By... selectors) {
        return waitForCondition(expectInvisibilityOfElementsLocated(selectors), timeOut);
    }

    default List<WebElement> waitForInvisibility(List<WebElement> elements) {
        new WebDriverWait(getDriver(), DEFAULT_TIMEOUT).until(w -> ExpectedConditions.invisibilityOfAllElements(elements).apply(w));
        return elements;
    }

    default String waitForSemanticQualifier(WebElement webElement) {
        return waitForSemanticQualifier(webElement, DEFAULT_TIMEOUT);
    }

    default String waitForSemanticQualifier(WebElement webElement, Duration timeOut) throws Error {
        return waitForAttribute(webElement, ATTRIBUTE_SEMANTIC_QUALIFIER, timeOut);
    }

    default void waitForVisibility(List<WebElement> elements, String semanticId) {
        waitForCondition(w -> elements.stream().filter(e -> getSemanticQualifier(e).equals(semanticId)).findFirst().map(e -> true).orElse(null), DEFAULT_TIMEOUT);
    }

    default List<WebElement> waitForVisibility(List<WebElement> elements) {
        new WebDriverWait(getDriver(), DEFAULT_TIMEOUT).until(w -> ExpectedConditions.visibilityOfAllElements(elements).apply(w));
        return elements;
    }

    default WebElement waitForVisibility(WebElement element) throws Error {
        waitForVisibility(element, DEFAULT_TIMEOUT);
        return element;
    }

    default WebElement waitForVisibility(By locator) throws Error {
        waitForCondition(w -> ExpectedConditions.visibilityOfElementLocated(locator).apply(w), DEFAULT_TIMEOUT);
        return getDriver().findElement(locator);
    }

    default WebElement waitForVisibility(WebElement element, Duration timeout) throws Error {
        new WebDriverWait(getDriver(), timeout).until(w -> ExpectedConditions.visibilityOf(element).apply(w));
        return element;
    }
}
