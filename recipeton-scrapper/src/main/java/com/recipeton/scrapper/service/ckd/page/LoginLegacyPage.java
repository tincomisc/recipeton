package com.recipeton.scrapper.service.ckd.page;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.config.ApplicationConfiguration;
import com.recipeton.scrapper.util.selenium.AbstractPage;
import com.recipeton.scrapper.util.selenium.SeleniumPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@SeleniumPage
public class LoginLegacyPage extends AbstractPage {
    @FindBy(id = "j_login_form_id")
    private WebElement component;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "j_submit_id")
    private WebElement submit;

    public LoginLegacyPage(WebDriver driver) {
        super(driver);
    }

    public void doLogin(ApplicationConfiguration.Credentials credentials) {
        email.sendKeys(credentials.getName());
        password.sendKeys(credentials.getPassword());
        submit.click();
    }

    public void waitForVisibility() {
        waitForVisibility(component);
    }
}
