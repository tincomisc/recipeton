package com.recipeton.scrapper.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.google.common.collect.ImmutableMap;
import com.recipeton.scrapper.config.SeleniumConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.Response;
import org.openqa.selenium.remote.service.DriverService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Service
public class SeleniumWebDriverService {

    public static final int OFFLINE_THROUGHPUT = 1024 * 1024;
    private final SeleniumConfiguration seleniumConfiguration;

    private final List<WebDriver> webDrivers = new CopyOnWriteArrayList<>();

    private ChromeDriverService chromeDriverService;

    public SeleniumWebDriverService(SeleniumConfiguration seleniumConfiguration) {
        this.seleniumConfiguration = seleniumConfiguration;
    }

    public void closeWebDriver(WebDriver webDriver) {
        try {
            //        webDriver.close();
            webDriver.quit();
        } catch (Exception e) { // NOPMD
            log.warn("Error quitting driver ", e);
        }
    }

    public void closeWebDrivers() {
        webDrivers.stream().filter(Objects::nonNull).forEach(this::closeWebDriver);

        Optional.ofNullable(chromeDriverService).filter(DriverService::isRunning).ifPresent(DriverService::stop);
    }

    private WebDriver createChromeDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();

        if (seleniumConfiguration.getBrowser().isHeadless()) {
            chromeOptions.addArguments("headless");
        }
        chromeOptions.addArguments("start-maximized");
        WebDriverManager webDriverManager = WebDriverManager.chromedriver().capabilities(chromeOptions);

        return seleniumConfiguration.getChromeDriverPaths().stream()
                .map(File::new)
                .filter(File::exists)
                .findFirst()
                .filter(f -> f.getAbsolutePath().startsWith("/snap"))
                .map(
                        f -> {
                            webDriverManager.setup();
                            chromeDriverService =
                                    new ChromeDriverService.Builder() {
                                        @Override
                                        protected File findDefaultExecutable() {
                                            return new File(f.getAbsolutePath()) {
                                                @Override
                                                public String getCanonicalPath() {
                                                    return this.getAbsolutePath();
                                                }
                                            };
                                        }
                                    }.build();

                            return (WebDriver) new ChromeDriver(chromeDriverService, chromeOptions);
                        })
                .orElseGet(webDriverManager::create);
    }

    private WebDriver createChromeDriverInDocker() {
        return WebDriverManager.chromedriver().browserInDocker().dockerExtraHosts("host.docker.internal:host-gateway").create();
    }

    private WebDriver createEdgeDriver() {
        WebDriverManager webDriverManager = WebDriverManager.edgedriver();
        webDriverManager.setup();
        return new EdgeDriver();
    }

    private WebDriver createFirefoxDriver() {
        WebDriverManager webDriverManager = WebDriverManager.firefoxdriver();
        webDriverManager.setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("marionette", true);

        if (seleniumConfiguration.getBrowser().isHeadless()) {
            firefoxOptions.addArguments("-headless");
        }
        return new FirefoxDriver(firefoxOptions);
    }

    private WebDriver createFirefoxDriverInDocker() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("marionette", true);
        WebDriverManager webDriverManager = WebDriverManager.firefoxdriver().capabilities(firefoxOptions).browserInDocker();
        // webDriverManager.dockerNetwork("host");
        // Potentially
        // webDriverManager.enableRecording();
        // webDriverManager.enableVnc();
        // getDockerNoVncUrl():
        return webDriverManager.create();
    }

    private WebDriver createHtmlUnitDriver() {
        return new HtmlUnitDriver(BrowserVersion.FIREFOX, true);
    }

    private WebDriver createOperaDriver() {
        WebDriverManager webDriverManager = WebDriverManager.operadriver();
        webDriverManager.setup();
        return new EdgeDriver();
    }

    private WebDriver createSafariDriver() {
        WebDriverManager webDriverManager = WebDriverManager.safaridriver();
        webDriverManager.setup();
        return new EdgeDriver();
    }

    public WebDriver createWebDriver() {
        WebDriver webDriver;
        switch (seleniumConfiguration.getDriver()) {
            case CHROME:
                webDriver = createChromeDriver();
                break;
            case CHROME_DOCKER:
                webDriver = createChromeDriverInDocker();
                break;
            case EDGE:
                webDriver = createEdgeDriver();
                break;
            case FIREFOX:
                webDriver = createFirefoxDriver();
                break;
            case FIREFOX_DOCKER:
                webDriver = createFirefoxDriverInDocker();
                break;
            case OPERA:
                webDriver = createOperaDriver();
                break;
            case SAFARI:
                webDriver = createSafariDriver();
                break;
            case HTMLUNIT:
            default:
                webDriver = createHtmlUnitDriver();
                break;
        }
        webDrivers.add(webDriver);
        return webDriver;
    }

    @PreDestroy
    public void preDestroy() {
        closeWebDrivers();
    }

    public void release(WebDriver webDriver, ApplicationContext applicationContext) {
        if (seleniumConfiguration.isCloseDriverAfterEachTest()) {
            webDriver.close();
        }
        if (seleniumConfiguration.isQuitDriverAfterEachTest()) {
            try {
                webDriver.quit();
            } catch (Exception e) { // NOPMD
                // Do nothing
            }
        }
        // Not test ;)
        // if (seleniumConfiguration.isNewDriverEachTest()) {
        // applicationContext.getBean(SeleniumFunctionalTestScope.class).reset();
        // }

    }

    public void saveScreenshot(String fileName, File screenshot) throws IOException {
        Path targetPath = seleniumConfiguration.getScreenshotPath().resolve(fileName);
        Path parent = targetPath.getParent();
        if (parent != null) {
            Files.createDirectories(parent);
        }
        try {
            Files.copy(screenshot.toPath(), targetPath);
        } catch (FileSystemException e) {
            log.error("Could not save screenshot at path={}", targetPath, e);
        }
    }

    public void setOffline(WebDriver webDriver) throws IOException {
        if (!(webDriver instanceof ChromeDriver)) {
            log.warn("setOffline ignored. Not Chromedriver");
            return;
        }
        ChromeDriver chromeDriver = (ChromeDriver) webDriver;
        CommandExecutor executor = chromeDriver.getCommandExecutor();
        Response response =
                executor.execute(
                        new Command(
                                chromeDriver.getSessionId(),
                                "setNetworkConditions",
                                ImmutableMap.of("network_conditions", ImmutableMap.of("offline", true, "latency", 0, "download_throughput", OFFLINE_THROUGHPUT, "upload_throughput", OFFLINE_THROUGHPUT))));

        log.debug("setOffline executed {}", response);
    }

    public void setUpBrowser(WebDriver webDriver) {
        Dimension dimension = new Dimension(seleniumConfiguration.getBrowser().getWidth(), seleniumConfiguration.getBrowser().getHeight()); // (400,800);
        webDriver.manage().window().setSize(dimension);
        if (seleniumConfiguration.getImplicitWait() != null) {
            webDriver.manage().timeouts().implicitlyWait(seleniumConfiguration.getImplicitWait());
        }
        if (seleniumConfiguration.getPageLoadTimeout() != null) {
            webDriver.manage().timeouts().pageLoadTimeout(seleniumConfiguration.getPageLoadTimeout());
        }
    }
}
