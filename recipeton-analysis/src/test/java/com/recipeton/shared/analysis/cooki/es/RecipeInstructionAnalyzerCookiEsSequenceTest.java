package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEsSequenceTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final List<RecipeIngredientAnalysis> empty = createRecipeIngredientAnalyses(configuration);
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeContainsAbbreviatureWithDotThenReturnsStringNotSplittingInAbbreviatureDot() {
        assertThat(analyzer.analyze(of("A aprox. b"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("A aprox. b");
        assertThat(analyzer.analyze(of("A aprox. b. Cc"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("A aprox. b", "Cc");
    }

    @Test
    void testAnalyzeContainsCommaBeginningSentenceThenRemoveComma() {
        assertThat(analyzer.analyze(of(", mezcle b"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Mezcle b");
    }

    @Test
    void testAnalyzeContainsFixedSplittingSentenceConnectorThenSplits() {
        assertThat(analyzer.analyze(of("Aaa y, mientras tanto, bbb"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Aaa", "Mientras tanto, bbb");
    }

    @Test
    void testAnalyzeContainsSplittingSentenceConnectorThenSplits() {
        Stream.of("Aaa y mezcle b", "Aaa, mezcle b", "Aaa, y mezcle b", "Aaa, después mezcle b", "Aaa, luego mezcle b")
                .forEach(s -> assertThat(analyzer.analyze(of(s), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Aaa", "Mezcle b"));
    }

    @Test
    void testAnalyzeGivenAddAccessoryInsideMainCompartmentAndWeighIngredientInAccessoryThenReturnsWeighInMainLocation() {
        assertThat(analyzer.analyze(of("Introduzca el cestillo en el vaso", "Pese el arroz en el cestillo"), empty))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, TOOL_INTERNAL, SIMMERING_BASKET_PUT_INSIDE, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_WEIGH, FOCUS_MAIN, TOOL_INTERNAL));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInMainCompartmentThenWeighIsOnTop() {
        assertThat(analyzer.analyze(of("Introduzca en el vaso el agua, la levadura prensada fresca, el azúcar y la harina de fuerza."), createRecipeIngredientAnalyses(configuration, "agua", "levadura", "azucar", "harina")))
                //        assertThat(analyzer.analyze(of("Ponga en el vaso el agua, la levadura prensada fresca, el azúcar y la harina de fuerza."), createRecipeIngredientAnalyses(configuration, "agua", "levadura", "azucar", "harina")))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                        });
    }

    @Test
    void testAnalyzeGivenAddAccessoryOnTopOfMainCompartmentAndWeighIngredientThenWeighIsOnTop() {
        assertThat(analyzer.analyze(of("Ponga el cestillo sobre la tapa del vaso y pese el arroz"), empty))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_ON_TOP, TOOL_INTERNAL, TOOL_MAIN, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_WEIGH, FOCUS_ON_TOP));
                        });
    }

    @Test
    void testAnalyzeGivenAddAccessoryToMainCompartmentAndAddIngredientThenReturnsIngredientAddedInMainLocation() {
        assertThat(analyzer.analyze(of("Introduzca el cestillo en el vaso. Agregue el arroz"), createRecipeIngredientAnalyses(configuration, "arroz")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, TOOL_INTERNAL, SIMMERING_BASKET_PUT_INSIDE, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeGivenAddAccessoryToMainCompartmentAndWeightIngredientThenReturnsWeighInMainLocation() {
        assertThat(analyzer.analyze(of("Introduzca el cestillo en el vaso y pese el arroz"), createRecipeIngredientAnalyses(configuration, "arroz")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, TOOL_INTERNAL, SIMMERING_BASKET_PUT_INSIDE, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_WEIGH, FOCUS_MAIN));
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceInMainLocationInstructionThenAddInstructionHasInMainLocationTag() {

        assertThat(analyzer.analyze(of("Ponga el tomate en el vaso", "Agregue la patata"), createRecipeIngredientAnalyses(configuration, "tomate", "patata")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceInMainLocationInstructionWhenAddInstructionIsInToDifferentCompartmentThenAddInstructionHasNotInMainLocationTag() {
        assertThat(analyzer.analyze(of("Ponga en el vaso el tomate", "añada la patata a un bol"), createRecipeIngredientAnalyses(configuration, "tomate", "patata")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, TOOL_EXTERNAL, FOCUS_EXTERNAL));
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceInMainLocationInstructionWhenPlaceSteamerThenAddInstructionHasNotInMainLocationTag() {
        assertThat(analyzer.analyze(of("Ponga en el vaso el tomate", "Coloque el varoma en su posición", "añada la patata"), createRecipeIngredientAnalyses(configuration, "tomate", "patata")))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_ON_TOP, STEAMER_PUT, TOOL_INTERNAL));
                            assertThat(l).element(2).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_ON_TOP, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterPlaceNotInMainLocationInstructionThenAddInstructionNotHasInMainLocationTag() {

        assertThat(analyzer.analyze(of("Ponga el tomate en la bandeja", "Agregue la patata"), createRecipeIngredientAnalyses(configuration, "tomate", "patata"))).allSatisfy(ria -> assertThat(ria.getTags()).doesNotContain(FOCUS_MAIN));
    }

    @Test
    void testAnalyzeGivenAddInstructionAfterWeighOnTopThenAddInstructionHasOnTop() {
        assertThat(analyzer.analyze(of("Coloque un tazón sobre la tapa del vaso, pese la manzana "), createRecipeIngredientAnalyses(configuration, "manzana")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).contains(DO_PLACE, FOCUS_ON_TOP));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).contains(FOCUS_ON_TOP, DO_WEIGH));
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionToMainCompartmentThenInstructionHasInMainLocationTagAndNotInExternalCompartment() {
        List<String> recipeInstructions = of("Ponga en el vaso la cebolla, los dientes de ajo y el aceite");

        assertThat(analyzer.analyze(recipeInstructions, createRecipeIngredientAnalyses(configuration, "cebolla", "ajo", "aceite")))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenAddInstructionToMainCompartmentThenInstructionAndIngredientInAnotherNonPlaceInstructionThenIngredientInPlaceInstruction() {
        List<String> recipeInstructions =
                of("Ponga en el vaso 180 g de agua, la levadura, las hojas de espinaca, la harina y la sal. Mezcle <nobr>15 seg/vel 6</nobr>.", "Con las manos engrasadas, retire la masa del vaso sobre la encimera espolvoreada con harina.");

        assertThat(analyzer.analyze(recipeInstructions, createRecipeIngredientAnalyses(configuration, "harina de fuerza", "agua", "levadura", "hojas de espinaca", "sal")))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).contains(DO_PLACE, FOCUS_MAIN, IS_INGREDIENT_ADDITION, TOOL_MAIN, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).contains(DO_COMMAND_MAIN_PREPARATION_DEFAULT));
                            assertThat(l).element(2).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TRANSFER_FROM_BOWL, TOOL_MAIN, DO_REMOVE, IS_ITEM_FROM_TO_TRANSFER));
                        });
    }

    @Test
    void testAnalyzeGivenAddWeighOnAccessoryAndAddThenAddsOnAccessory() {
        assertThat(analyzer.analyze(of("Ponga sobre la tapa del vaso una bandeja de aluminio.", "Pese en la bandeja el arroz y espolvoree con sal"), empty))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_ON_TOP, TOOL_MAIN, TOOL_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_WEIGH, FOCUS_ON_TOP, FOCUS_EXTERNAL, TOOL_EXTERNAL));
                            assertThat(l).element(2).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_EXTERNAL));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInExternalAfterCommandThenPlaceInExternal() {
        assertThat(analyzer.analyze(of("Cocine [1 min]", "Vierta el glaseado en varios boles"), empty))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER, FOCUS_EXTERNAL, TRANSFER_FROM_BOWL));
                        });
    }

    @Test
    void testAnalyzeGivenCleanAccessoryAndAddIngredientThenAddsLocationMain() {
        assertThat(analyzer.analyze(of("Sin lavar el vaso", "Ponga el arroz"), createRecipeIngredientAnalyses(configuration, "arroz")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_MAIN, FOCUS_MAIN));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsCommandWithParallelPlaceStepThenReturnsInstructions() {

        assertThat(analyzer.analyze(of("Rehogue [10 min/120°C/reverse/vel 1]", "Mientras tanto, ponga los ramilletes de brócoli en la vaporera"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Rehogue [10 min/120°C/reverse/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mientras tanto, ponga los ramilletes de brócoli en la vaporera");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(SEQUENCE_PARALLEL, DO_PLACE, TOOL_INTERNAL, QUALIFIES_PREVIOUS, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsSeparatedByModifierThenSplitsCorrectly() {

        assertThat(analyzer.analyze(of("Incorpore al vaso el caramelo líquido y la sal y, sin poner el cubilete, programe [2 min]"), createRecipeIngredientAnalyses(configuration, "caramelo líquido", "sal")))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Incorpore al vaso el caramelo líquido y la sal");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Sin poner el cubilete");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_INTERNAL, MEASURING_CUP_NOTUSE);
                                            });
                            assertThat(l)
                                    .element(2)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Programe [2 min]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenCommandInstructionPreceededByRemoveCupThenNoCupPlace() {

        assertThat(analyzer.analyze(of("Vierta la masa en el vaso. Coloque el cestillo sobre la tapa en lugar del cubilete. Caliente <nobr>5 min/120°C/vel 1</nobr>."), createRecipeIngredientAnalyses(configuration, "masa")))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Vierta la masa en el vaso");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(2)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Caliente [5 min/120°C/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenCommandInstructionWithSteamerInPlaceThenNoCupPlace() {
        assertThat(analyzer.analyze(of("Vierta la masa en el vaso. Coloque el Varoma en posición. Coloque los platanos y tape. Caliente <nobr>5 min/120°C/vel 1</nobr>."), createRecipeIngredientAnalyses(configuration, "masa")))
                .hasSize(4)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Vierta la masa en el vaso");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(3)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Caliente [5 min/120°C/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithAddAndToMainCompartmentAndNoIngredientThenTagCreatedIngredient() {

        assertThat(analyzer.analyze(of("Vierta la masa en el vaso y caliente <nobr>5 min/120°C/vel 1</nobr>."), createRecipeIngredientAnalyses(configuration, "masa")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Vierta la masa en el vaso");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Caliente [5 min/120°C/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsAddToMainCompartmentAndPourOnExternalAndReferenceIngredientThenIngredientIsNotAdded() {
        assertThat(analyzer.analyze(of("Ponga el tomate en el vaso. Vierta en un plato y riegue con aceite"), createRecipeIngredientAnalyses(configuration, "tomate", "aceite")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TRANSFER_FROM_BOWL, DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsCommandWithParallelNonActionExplanationThenReturnsInstruction() {

        assertThat(analyzer.analyze(of("Caliente <nobr>5 min/120°C/vel 1</nobr> y, mientras tanto, mire por la ventana"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Caliente [5 min/120°C/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mientras tanto, mire por la ventana");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(SEQUENCE_PARALLEL, QUALIFIES_PREVIOUS);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInMainAndTransferThenTransferIsNotLocationMain() {

        assertThat(analyzer.analyze(of("Ponga el arroz en el vaso. Vierta el contenido del vaso en la paella."), createRecipeIngredientAnalyses(configuration, "arroz")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Ponga el arroz en el vaso");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Vierta el contenido del vaso en la paella");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TRANSFER_FROM_BOWL, FOCUS_EXTERNAL, TOOL_MAIN, TOOL_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsWithParallelExecutionSepparatedByCommasAndParallelIsCommandThenReturnsInstructionsAndParallelInstructionTagged() {
        // This does not make any sense in practical terms. As a matter of fact I think that it should return error or remove parallel
        assertThat(analyzer.analyze(of("Caliente <nobr>5 min/120°C/vel 1</nobr>. Mientras tanto, pique <nobr>5 min/vel 8</nobr>"), createRecipeIngredientAnalyses(configuration, "")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Caliente [5 min/120°C/vel 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mientras tanto, pique [5 min/vel 8]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(SEQUENCE_PARALLEL, DO_COMMAND_MAIN, QUALIFIES_PREVIOUS, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInstructionInExternalAndAddInstructionThenAddsIsInExternal() {
        assertThat(analyzer.analyze(of("Ponga las patatas en una fuente para servir", "Añada aceite"), empty))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_EXTERNAL));
                        });
    }

    @Test
    void testAnalyzeGivenRefOnTopInstructionThenInstructionHasOnTopTag() {

        assertThat(analyzer.analyze(of("Ponga un bol sobre la tapa del vaso"), empty)).singleElement().satisfies(ria -> assertThat(ria.getTags()).contains(FOCUS_ON_TOP));
    }

    @Test
    void testAnalyzeGivenServeInstructionWithIngredientsThenIngredientsAreDecoration() {

        assertThat(analyzer.analyze(of("Sirva muy frío, espolvoreado con los dados de jamón serrano y el huevo duro picado."), empty)).allSatisfy(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE));
    }

    @Test
    void testAnalyzeGivenTransferMainCompartmentInstructionThenAddsTags() {

        assertThat(analyzer.analyze(of("Vierta el contenido del vaso sobre la bandeja"), empty))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TRANSFER_FROM_BOWL, TOOL_EXTERNAL, TOOL_MAIN, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }
}
