package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeIngredientTerm;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionTag;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.MIXING_BOWL_PUT;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.TOOL_MAIN;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEsIngredientTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenAddInstructionWithIngredientWithoutUnitThenTermContainsMagnitude() {

        List<RecipeIngredientAnalysis> recipeIngredientAnalyses = createRecipeIngredientAnalyses(configuration, "5 platanos");
        assertThat(analyzer.analyze(of("Ponga 2 platanos en el vaso"), recipeIngredientAnalyses))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).contains(DO_PLACE);
                            List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                            assertThat(recipeIngredientTerms)
                                    .singleElement()
                                    .satisfies(
                                            t -> {
                                                assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("platanos");
                                                assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(2));
                                            });
                        });

        assertThat(analyzer.analyze(of("Ponga un platano en el vaso"), recipeIngredientAnalyses))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).contains(DO_PLACE);
                            List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                            assertThat(recipeIngredientTerms)
                                    .singleElement()
                                    .satisfies(
                                            t -> {
                                                assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("platanos");
                                                assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(1));
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionWithMagnitudeAndExtraStopWordThenTermContainsMagnitude() {
        List<RecipeIngredientAnalysis> recipeIngredientAnalyses = createRecipeIngredientAnalyses(configuration, "5 cucharaditas de sal");

        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("Ponga 3 cucharaditas de la sal en el vaso"), recipeIngredientAnalyses))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).contains(DO_PLACE);
                            List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                            assertThat(recipeIngredientTerms)
                                    .singleElement()
                                    .satisfies(
                                            t -> {
                                                assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("sal");
                                                assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(3));
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionWithMagnitudeRangeThenTermContainsMagnitudeRange() {

        RecipeIngredientAnalysis recipeIngredientAnalysis = createRecipeIngredientAnalysis("5 cucharaditas de sal", configuration);

        assertThat(analyzer.analyze(of("Ponga 2 - 3 cucharaditas de sal en el vaso"), of(recipeIngredientAnalysis)))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).contains(DO_PLACE);
                            List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                            assertThat(recipeIngredientTerms)
                                    .singleElement()
                                    .satisfies(
                                            t -> {
                                                assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("sal");
                                                assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(2));
                                                assertThat(t.getMagnitudeToValue()).isEqualTo(new BigDecimal(3));
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenAddInstructionWithMagnitudeThenTermContainsMagnitude() {
        List<RecipeIngredientAnalysis> recipeIngredientAnalyses = createRecipeIngredientAnalyses(configuration, "5 cucharaditas de sal");

        assertThat(analyzer.analyze(of("Ponga 2 cucharaditas de sal en el vaso"), recipeIngredientAnalyses))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).contains(DO_PLACE);
                            List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                            assertThat(recipeIngredientTerms)
                                    .singleElement()
                                    .satisfies(
                                            t -> {
                                                assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("sal");
                                                assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(2));
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientHasLessWordsThenIngredientIsDetected() {

        assertThat(
                        analyzer.analyze(
                                of("Ponga la lata de confit de pato en una olla con agua y caliente al baño maría durante 10-15 minutos " + "para que la grasa que recubre los muslos de pato se funda", "Ponga la mermelada en el vaso"),
                                createRecipeIngredientAnalyses(configuration, "mermelada de higos")))
                .element(2)
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .allSatisfy(
                                                t -> {
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("mermelada de higos");
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientHasManyLessWordsThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of("Ponga platano, patata, coche, sal, un platillo volante y otras multiples cosas en el vaso"), createRecipeIngredientAnalyses(configuration, "sal de roca ignea del monte fuji")))
                .singleElement()
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .singleElement()
                                        .isInstanceOfSatisfying(RecipeIngredientTerm.class, t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("sal de roca ignea del monte fuji")));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientInSingularIrregularThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of("Ponga una nuez en el vaso"), createRecipeIngredientAnalyses(configuration, "nueces")))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                            assertThat(ria.getRecipeIngredientTerms()).singleElement().satisfies(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("nueces"));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientMostSignificativeWorkIsNotFirstThenIngredientIsDetected() {

        assertThat(analyzer.analyze(of("Ponga los tomates, la cayena, el azucar en el vaso"), createRecipeIngredientAnalyses(configuration, "pimienta de cayena")))
                .singleElement()
                .satisfies(
                        ria -> {
                            assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_INGREDIENT_POTENTIAL_MISMATCH, IS_ITEM_FROM_TO_TRANSFER);
                            assertThat(ria.getRecipeIngredientTerms()).anySatisfy(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("pimienta de cayena"));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithTrademarkRegisteredIngredientThenTrademarkRegisteredIngredientIsFound() {

        assertThat(analyzer.analyze(of("Mire los Chupa Chups®"), createRecipeIngredientAnalyses(configuration, "Chupa Chups®")))
                .singleElement()
                .satisfies(ria -> assertThat(extractRecipeIngredientTermNotations(ria)).containsOnly("Chupa Chups®"));
    }

    @Test
    void testAnalyzeGivenInstructionWithoutIngredientAndIngredientHasManyWordsThenIngredientIsNotDetected() {

        assertThat(
                        analyzer.analyze(
                                of("platano patata coche un monito y otras multiples cosas", "dummy step", "Ponga dummy step en el vaso1", "Ponga dummy step en el vaso2"),
                                createRecipeIngredientAnalyses(configuration, "sal de roca ignea del monte fuji")))
                .hasSize(4)
                .element(0)
                .satisfies(ria -> assertThat(ria.getTerms()).isEmpty());
    }

    @Test
    void testAnalyzeGivenMultipleAddInstructionWithSameIngredientAndUnitThenIngredientAddedToAllInstructions() {

        List<RecipeInstructionAnalysis> recipeInstructionAnalyses =
                analyzer.analyze(
                        of(
                                "Ponga en el vaso 2 cucharaditas de arroz y cueza <nobr>3 min/120°C/vel 1</nobr>.",
                                "Introduzca el cestillo en el vaso y pese el arroz",
                                "extraiga el cestillo, ponga el arroz en una fuente",
                                "Vacíe el vaso.",
                                "Ponga en el vaso 3 cucharaditas de arroz"),
                        createRecipeIngredientAnalyses(configuration, "5 cucharaditas de arroz"));
        assertThat(recipeInstructionAnalyses)
                .hasSize(8)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("arroz");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(2));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                            assertThat(l)
                                    .element(7)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("arroz");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(5));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(3));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenMultipleAddInstructionWithSimilarIngredientsThenAddsIngredientToCorrectStep() {

        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = analyzer.analyze(of("Ponga en el vaso el huevo, el pan", "Añada al vaso las yemas"), createRecipeIngredientAnalyses(configuration, "1 huevo", "1 yemas de huevo"));

        assertThat(recipeInstructionAnalyses)
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_INGREDIENT_POTENTIAL_MISMATCH, IS_ITEM_FROM_TO_TRANSFER);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .anySatisfy(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("huevo");
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("yemas de huevo");
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                        });
    }

    //    @Test
    //    void testAnalyzeGivenMultiplePlaceInstructionWithNoIndentificableIngredientAndNoIngredientsMatchedThenInstructionIngredientTermsAreNotAdded() {
    //        assertThat(analyzer.analyze(of("Ponga los ingredientes de la masa en el vaso", "Ponga la cebolla en el vaso", "otra instruccion"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
    //                .allSatisfy(ria -> assertThat(extractRecipeIngredientTermNotations(ria)).isEmpty());
    //    }

    @Test
    void testAnalyzeGivenPlaceAllInstructionAndCommandThenAddsAllIngredientsToThatInstruction() {

        assertThat(analyzer.analyze(of("Ponga todos los ingredientes en el vaso y triture <nobr>vel 5</nobr>"), createRecipeIngredientAnalyses(configuration, "plátanos", "arándanos")))
                .hasSize(2)
                .element(0)
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .hasSize(2)
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("plátanos");
                                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                                })
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("arándanos");
                                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                                }));
    }

    @Test
    void testAnalyzeGivenPlaceAllInstructionThenAddsAllIngredientsToThatInstruction() {

        assertThat(analyzer.analyze(of("Ponga todos los ingredientes de la masa en el vaso"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
                .singleElement()
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .hasSize(2)
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("patata");
                                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                                })
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getTarget()).isEqualTo(RecipeIngredientTerm.Target.PROCESS);
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("tomate");
                                                    assertThat(t.getMatch().getScore()).isEqualTo(SCORE_MAX);
                                                }));
    }

    @Test
    void testAnalyzeGivenPlaceInMainLocationAddInMainLocationPlaceElsewhereThenOnlyInMainLocationInstructionsHaveInMainLocationTagAndOnlyInMainLocationAreConclusive() {

        assertThat(
                        analyzer.analyze(
                                of(
                                        "Ponga en el vaso las hierbas especiales",
                                        "Pique <nobr>5 seg/vel 7</nobr>",
                                        "Con la espátula, baje los ingredientes hacia el fondo del vaso",
                                        "Añada el aceite y la pimienta",
                                        "Pique <nobr>5 seg/vel 7</nobr>",
                                        "Vierta el aceite en un bol o jarra pequeña y reserve"),
                                createRecipeIngredientAnalyses(configuration, "hierbas especiales", "aceite de oliva", "pimienta")))
                .hasSize(7)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).contains(DO_PLACE, RecipeInstructionTag.FOCUS_MAIN);
                                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("hierbas especiales");
                                                assertThat(extractRecipeIngredientTermsByIngredientNotation("hierbas especiales", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                                            });
                            assertThat(l)
                                    .element(3)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).contains(RecipeInstructionTag.DO_ADD, RecipeInstructionTag.FOCUS_MAIN);
                                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("aceite de oliva", "pimienta");
                                                assertThat(extractRecipeIngredientTermsByIngredientNotation("aceite de oliva", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                                                assertThat(extractRecipeIngredientTermsByIngredientNotation("pimienta", ria)).anySatisfy(t -> assertThat(t.isConclusive()).isTrue());
                                            });
                            assertThat(l)
                                    .element(5)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).doesNotContain(RecipeInstructionTag.FOCUS_MAIN);
                                                assertThat(extractRecipeIngredientTermNotations(ria)).contains("aceite de oliva");
                                                assertThat(ria.getRecipeIngredientTerms()).allSatisfy(t -> assertThat(t.isConclusive()).isFalse());
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenPlaceMainCompartmentAndThenAddIngredientInstructionsThenAddIngredientIsInMainLocation() {
        assertThat(analyzer.analyze(of("Coloque el vaso en su posicion. Agregue tomate"), createRecipeIngredientAnalyses(configuration, "tomate")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).contains(DO_PLACE, MIXING_BOWL_PUT));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeGivenSinglePlaceInstructionWithNoIndentificableIngredientAndNoIngredientsMatchedThenInstructionGetsAllIngredientTerms() {

        assertThat(analyzer.analyze(of("Ponga los ingredientes de la masa en el vaso", "otra instruccion"), createRecipeIngredientAnalyses(configuration, "patata", "tomate")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(extractRecipeIngredientTermNotations(ria)).containsExactlyInAnyOrder("patata", "tomate"));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTerms()).isEmpty());
                        });
    }
}
