package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.service.RecipeCategoryAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeCategoryAnalyzerCookiFrTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeCategoryAnalyzer analyzer = createRecipeCategoryAnalyzer(new RecipeAnalyzerCookiFrConfiguration());

    @Test
    public void testGetRecipeCategoryDefinitionGivenFruitThenReturnsFruit() {
        assertThat(analyzer.getRecipeCategoryDefinition("Fruit", false).getName()).isEqualTo("#Fruit");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenOeufVariantsThenReturnsOeufs() {
        assertThat(analyzer.getRecipeCategoryDefinition("Contient des oeufs", false).getName()).isEqualTo("#Contient des oeufs");
        assertThat(analyzer.getRecipeCategoryDefinition("Contient des œufs", false).getName()).isEqualTo("#Contient des oeufs");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenPoissonsVariantsThenReturnsPoissonsEtFruitsDeMer() {
        assertThat(analyzer.getRecipeCategoryDefinition("Poissons et fruits de mer", false).getName()).isEqualTo("Poissons et fruits de mer");
        assertThat(analyzer.getRecipeCategoryDefinition("Poissons", false).getName()).isEqualTo("Poissons et fruits de mer");
        assertThat(analyzer.getRecipeCategoryDefinition("Fruits de mer", false).getName()).isEqualTo("Poissons et fruits de mer");
    }
}
