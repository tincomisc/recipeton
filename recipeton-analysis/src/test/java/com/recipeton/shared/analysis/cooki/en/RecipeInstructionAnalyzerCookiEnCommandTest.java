package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RecipeInstructionAnalyzerCookiEnCommandTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();

    @Test
    public void testAcceptGivenInstructionWithDurationHalfSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("0.5 sec", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofMillis(500)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHoursThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 h", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofHours(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationMinutesThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 min", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofMinutes(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 sec", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofSeconds(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithNoRecognizedContentThenTermsEmpty() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(List.of(createWrappedCommand("otherjunk", configuration)), createRecipeIngredientAnalyses(configuration)))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getCommandTerms()).isEmpty());
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedSlowThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("speed \uE002", configuration).satisfies(t -> assertThat(t.getSpeed()).isEqualTo(new BigDecimal("0.3")));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("speed 1", configuration).satisfies(t -> assertThat(t.getSpeed()).isEqualTo(new BigDecimal("1")));
    }
}
