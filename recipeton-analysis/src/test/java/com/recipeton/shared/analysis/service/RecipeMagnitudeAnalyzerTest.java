package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeMagnitudeAnalyzerTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeMagnitudeAnalyzer analyzer = createRecipeMagnitudeAnalyzer(createRecipeAnalyzerConfiguration());

    @Test
    public void testGetMagnitudeGivenNotNumericThenReturnsNull() {
        assertThat(analyzer.getMagnitude("aasdasdas")).isEmpty();
    }

    @Test
    public void testGetMagnitudeGivenNumericDecimalThenReturnsNumber() {
        assertThat(analyzer.getMagnitude("123.4")).hasValue(new BigDecimal("123.4"));
    }

    @Test
    public void testGetMagnitudeGivenNumericThenReturnsNumber() {
        assertThat(analyzer.getMagnitude("123")).hasValue(new BigDecimal("123"));
    }

    @Test
    public void testGetMagnitudeGivenNumericWithVulgarFractionThenReturnsNumber() {
        assertThat(analyzer.getMagnitude("123½")).hasValue(new BigDecimal("123.5"));
        assertThat(analyzer.getMagnitude("123¼")).hasValue(new BigDecimal("123.25"));
    }

    @Test
    public void testGetMagnitudeGivenVulgarFractionThenReturnsNumber() {
        assertThat(analyzer.getMagnitude("½")).hasValue(new BigDecimal("0.5"));
        assertThat(analyzer.getMagnitude("¼")).hasValue(new BigDecimal("0.25"));
    }
}
