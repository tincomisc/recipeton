package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static com.recipeton.shared.analysis.cooki.es.RecipeAnalyzerCookiEsTest.COOKI_ES_TOOLS_EXTERNAL;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.TRANSFER_FROM_BOWL;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.TOOL_EXTERNAL;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.TOOL_INTERNAL;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEsTagTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithAddExpressionThenAddTagIsAdded() {
        assertThatAnalyzeSingleResult(of("Añada el agua"), configuration).satisfies(ria2 -> assertThat(ria2.getTags()).containsExactlyInAnyOrder(DO_ADD));
        assertThatAnalyzeSingleResult(of("Incorpore el agua"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(DO_ADD));
        assertThatAnalyzeSingleResult(of("Agregue el agua"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD));
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceExpressionThenPlaceTagIsAdded() {
        assertThatAnalyzeSingleResult(of("Vierta el agua"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_UNDEFINED));
        assertThatAnalyzeSingleResult(of("Coloque el pollo"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_UNDEFINED));
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceInExternalExpressionThenPlaceInExternalTagIsAdded() {
        assertThatAnalyzeSingleResult(of("Ponga la lata de confit de pato en una olla con agua"), configuration)
                .satisfies(ria4 -> assertThat(ria4.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
        assertThatAnalyzeSingleResult(of("Coloque la lata de confit de pato en una olla con agua"), configuration)
                .satisfies(ria3 -> assertThat(ria3.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
        assertThatAnalyzeSingleResult(of("Ponga en un bol"), configuration).satisfies(ria2 -> assertThat(ria2.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
        assertThatAnalyzeSingleResult(of("Ponga en una sartén"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
        assertThatAnalyzeSingleResult(of("Coloque en una charola"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionWithRefrigerateExpressionThenAddsTags() {
        assertThatAnalyzeSingleResult(of("Ponga la cosa en el refrigerador durante una hora"), configuration)
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_EXTERNAL, TOOL_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionWithServingExpressionThenServingTagIsAdded() {
        assertThatAnalyzeSingleResult(of("Y disfrute"), configuration).satisfies(ria4 -> assertThat(ria4.getTags()).containsExactlyInAnyOrder(DO_SERVE));
        assertThatAnalyzeSingleResult(of("Disfrute del plato"), configuration).satisfies(ria3 -> assertThat(ria3.getTags()).containsExactlyInAnyOrder(DO_SERVE, TOOL_EXTERNAL));
        assertThatAnalyzeSingleResult(of("Sírvalo caliente"), configuration).satisfies(ria2 -> assertThat(ria2.getTags()).containsExactlyInAnyOrder(DO_SERVE));
        assertThatAnalyzeSingleResult(of("Sirva immediatamente"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(DO_SERVE));
        assertThatAnalyzeSingleResult(of("Antes de servir ponga velas"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE, DO_PLACE, FOCUS_UNDEFINED));
        //        assertThatAnalyzeSingleResultContainsTagsExactly("Antes de servir ponga velas", configuration, SERVING, PLACE);
    }

    @Test
    void testAnalyzeGivenInstructionWithTransferInExternalThenPlaceAndInExternalAndTransferTagsAreAdded() {
        assertThatAnalyzeSingleResult(of("Vierta en una sopera"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, TRANSFER_FROM_BOWL, IS_ITEM_FROM_TO_TRANSFER));
        assertThatAnalyzeSingleResult(of("Vierta en un tazón"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, TRANSFER_FROM_BOWL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionWithWeightExpressionThenWeightTagIsAdded() {
        assertThatAnalyzeSingleResult(of("Pese el arroz en el cestillo y luego..."), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_WEIGH, TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenReferenceExternalLocationInstructionThenAddsTags() {
        Stream.of(COOKI_ES_TOOLS_EXTERNAL)
                .flatMap(
                        s ->
                                Stream.of(
                                        "en un " + s,
                                        " en un " + s,
                                        "en unos " + s,
                                        " en unos " + s,
                                        "en una " + s,
                                        " en una " + s,
                                        "en el " + s,
                                        " en el " + s,
                                        "en la " + s,
                                        " en la " + s,
                                        "a un " + s,
                                        " a un " + s,
                                        "a la " + s,
                                        " a la " + s,
                                        "a el " + s,
                                        " a el " + s))
                .forEach(
                        s -> {
                            assertThatAnalyzeSingleResult(of(s), configuration).satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(TOOL_EXTERNAL, FOCUS_EXTERNAL));
                        });
    }

    @Test
    void testAnalyzeGivenReferenceExternalToolInstructionThenAddsTags() {
        Stream.of(COOKI_ES_TOOLS_EXTERNAL).forEach(s -> assertThatAnalyzeSingleResult(of(s), configuration).satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(TOOL_EXTERNAL)));
    }
}
