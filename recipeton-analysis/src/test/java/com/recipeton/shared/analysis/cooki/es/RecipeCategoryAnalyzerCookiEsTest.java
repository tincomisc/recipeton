package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeCategoryDefinition;
import com.recipeton.shared.analysis.service.RecipeCategoryAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeCategoryAnalyzerCookiEsTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeCategoryAnalyzer analyzer = createRecipeCategoryAnalyzer(new RecipeAnalyzerCookiEsConfiguration());

    @Test
    public void testFormatCategoryUidGivenEndsWithPluralThenReturnsWithoutEnding() {
        assertThat(analyzer.formatCategoryUid("Sin Grasas")).isEqualTo("singrasa");
        assertThat(analyzer.formatCategoryUid("Sin Grasa")).isEqualTo("singrasa");
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithVerbalThenReturnsWithoutEnding() {
        assertThat(analyzer.formatCategoryUid("cocinar")).isEqualTo("cosina");
        assertThat(analyzer.formatCategoryUid("cocinado")).isEqualTo("cosina");
    }

    @Test
    public void testFormatCategoryUidGivenHasWithDemonymInMiddleOfTheWordThenDoesNotRemove() {
        assertThat(analyzer.formatCategoryUid("AnoAnoX")).isEqualTo("anoanoc");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenCountriesDemonymThenReturnsSameCategory() {
        assertThat(analyzer.getRecipeCategoryDefinition("Africa", false).getName()).isEqualTo("#Africa");
        assertThat(analyzer.getRecipeCategoryDefinition("Africano", false).getName()).isEqualTo("#Africa");

        assertThat(analyzer.getRecipeCategoryDefinition("Japon", false).getName()).isEqualTo("#Japon");
        assertThat(analyzer.getRecipeCategoryDefinition("Japones", false).getName()).isEqualTo("#Japon");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenExistingCategoryThenReturnsCategory() {
        assertThat(analyzer.getRecipeCategoryDefinition("Plato principal - carnes y pollo", false)).isEqualTo(new RecipeCategoryDefinition("0_040mainmeat", "Carnes y aves"));
        assertThat(analyzer.getRecipeCategoryDefinition("Plato principal", false)).isEqualTo(new RecipeCategoryDefinition("0_070mainother", "Otros principales"));
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenOtherCasesThenRemovesTheseCases() {
        assertThat(analyzer.getRecipeCategoryDefinition("Cocinando para grupos", false).getName()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryDefinition("Cocinado para grupos", false).getName()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryDefinition("Cocinado al Vapor", false).getName()).isEqualTo("#Vapor");
        assertThat(analyzer.getRecipeCategoryDefinition("Cocinar para grupos", false).getName()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryDefinition("Cocina para grupos", false).getName()).isEqualTo("#Grupos");

        assertThat(analyzer.getRecipeCategoryDefinition("Para grupos", false).getName()).isEqualTo("#Grupos");
        assertThat(analyzer.getRecipeCategoryDefinition("Para la pareja", false).getName()).isEqualTo("#Pareja");
        assertThat(analyzer.getRecipeCategoryDefinition("Para el amigo", false).getName()).isEqualTo("#Amigo");

        assertThat(analyzer.getRecipeCategoryDefinition("En pareja", false).getName()).isEqualTo("#Pareja");
        assertThat(analyzer.getRecipeCategoryDefinition("En 3 pasos", false).getName()).isEqualTo("#3 pasos");

        assertThat(analyzer.getRecipeCategoryDefinition("Al Vapor", false).getName()).isEqualTo("#Vapor");

        assertThat(analyzer.getRecipeCategoryDefinition("De Diario", false).getName()).isEqualTo("#Diario");

        assertThat(analyzer.getRecipeCategoryDefinition("Del Chef", false).getName()).isEqualTo("#Chef");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenRawFoodSimilarVariantThenReturnsRawFood() {
        assertThat(analyzer.getRecipeCategoryDefinition("Raw food", false).getName()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryDefinition("Raw Foed", false).getName()).isEqualTo("#Raw food");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenRawFoodVariantThenReturnsRawFood() {
        assertThat(analyzer.getRecipeCategoryDefinition("Raw food (en crudo)", false).getName()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryDefinition("Raw food", false).getName()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryDefinition("Raw Food", false).getName()).isEqualTo("#Raw food");
        assertThat(analyzer.getRecipeCategoryDefinition("rAW FOOD", false).getName()).isEqualTo("#Raw food");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenSimilarToExistingCategoryThenReturnsCategory() {
        assertThat(analyzer.getRecipeCategoryDefinition("Bebés", false)).isEqualTo(new RecipeCategoryDefinition("0_170baby", "Alimentación infantil"));
        assertThat(analyzer.getRecipeCategoryDefinition("infantil", false)).isEqualTo(new RecipeCategoryDefinition("0_170baby", "Alimentación infantil"));
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenStartsWithCleanedPrefixThenReturnsWithoutCleanedPrefix() {
        assertThat(analyzer.getRecipeCategoryDefinition("para grupos", false).getName()).isEqualTo("#Grupos");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenStartsWithCocinarThenRemovesCocinar() {
        assertThat(analyzer.getRecipeCategoryDefinition("Cocina patatas", false).getName()).isEqualTo("#Patatas");
        assertThat(analyzer.getRecipeCategoryDefinition("Cocinar patatas", false).getName()).isEqualTo("#Patatas");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenStartsWithContieneThenRemovesContiene() {
        assertThat(analyzer.getRecipeCategoryDefinition("Contiene patatas", false).getName()).isEqualTo("#Patatas");
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenAvesThenReturnsCarnesAves() {
        Stream.of("Aves")
                .forEach(
                        s -> {
                            String name = analyzer.getRecipeCategoryDefinition(s, false).getName();
                            assertThat(name).isEqualTo("Carnes y aves");
                        });
    }

    @Test
    public void testGetRecipeCategoryDefinitionGivenPrimaveraThenReturnsPrimavera() {
        Stream.of("Primavera")
                .forEach(
                        s -> {
                            String name = analyzer.getRecipeCategoryDefinition(s, false).getName();
                            assertThat(name).isEqualTo("#Primavera");
                        });
    }
}
