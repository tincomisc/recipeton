package com.recipeton.shared.analysis.test.support;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.*;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.*;
import com.recipeton.shared.analysis.service.filter.IngredientRecipeInstructionAnalysisFilter;
import com.recipeton.shared.analysis.service.filter.RecipeInstructionAnalysisFilterFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_GRAM;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_PIECE;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static java.util.regex.Pattern.compile;

public interface RecipetonAnalyzerTestFixtureTrait {

    default RecipeMeasurementUnitAnalysis createMeasurementUnitAnalysis(MeasurementUnitDefinition unitDefinition) {
        return createMeasurementUnitAnalysis(unitDefinition, false);
    }

    default RecipeMeasurementUnitAnalysis createMeasurementUnitAnalysis(String unitName, MeasurementUnitDefinition unitDefinition) {
        return new RecipeMeasurementUnitAnalysis(unitName, unitDefinition, false);
    }

    default RecipeMeasurementUnitAnalysis createMeasurementUnitAnalysis(MeasurementUnitDefinition unitDefinition, boolean contextual) {
        return new RecipeMeasurementUnitAnalysis(unitDefinition.getName(), unitDefinition, contextual);
    }

    default RecipeAnalyzerConfiguration createRecipeAnalyzerConfiguration() {
        return new RecipeAnalyzerConfiguration(
                "test",
                null,
                new RecipeLangAnalyzerConfiguration(Set.of()),
                new RecipeCategoryAnalyzerConfiguration(),
                new RecipeCommandAnalyzerConfiguration(),
                new RecipeMagnitudeAnalyzerConfiguration(Map.of()),
                new RecipeMeasurementAnalyzerConfiguration(UNIT_PIECE, UNIT_GRAM)
                        .setMeasurementUnitDefinitions(UNIT_GRAM, UNIT_PIECE, MeasurementUnitDefinition.UNIT_PINCH, MeasurementUnitDefinition.UNIT_TSP, MeasurementUnitDefinition.UNIT_TBSP),
                new RecipeInstructionAnalyzerConfiguration(new RecipeInstructionSplitter.Replacement[] {}, Collections.emptyMap(), compile(", ")),
                new RecipeIngredientAnalyzerConfiguration("or "),
                new RecipeToolAnalyzerConfiguration(Map.of(), Map.of()),
                new RecipeCustomizationConfiguration());
    }

    default RecipeCategoryAnalyzer createRecipeCategoryAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeCategoryAnalyzer(createRecipeLangAnalyzer(configuration), configuration.getRecipeCategoryAnalyzerConfiguration());
    }

    private RecipeCommandAnalyzer createRecipeCommandAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeCommandAnalyzer(configuration.getRecipeCommandAnalyzerConfiguration());
    }

    default List<RecipeIngredientAnalysis> createRecipeIngredientAnalyses(RecipeAnalyzerConfiguration configuration, String... ingredients) {
        return toStream(ingredients).filter(StringUtils::isNotBlank).map(s -> createRecipeIngredientAnalysis(s, configuration)).collect(Collectors.toList());
    }

    default RecipeIngredientAnalysis createRecipeIngredientAnalysis(String textSource, RecipeAnalyzerConfiguration configuration) {
        return createRecipeIngredientAnalyzer(configuration).analyze(textSource);
    }

    default RecipeIngredientAnalysis createRecipeIngredientAnalysis(String textSource, String notation, RecipeAnalyzerConfiguration configuration) {
        return createRecipeIngredientAnalysis(textSource, configuration);
    }

    default RecipeIngredientAnalyzer createRecipeIngredientAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return createRecipeIngredientAnalyzer(createRecipeMeasurementUnitAnalyzer(configuration), configuration);
    }

    default RecipeIngredientAnalyzer createRecipeIngredientAnalyzer() {
        return createRecipeIngredientAnalyzer(createRecipeAnalyzerConfiguration());
    }

    default RecipeIngredientAnalyzer createRecipeIngredientAnalyzer(RecipeMeasurementAnalyzer recipeMeasurementAnalyzer, RecipeAnalyzerConfiguration configuration) {
        return new RecipeIngredientAnalyzer(createRecipeLangAnalyzer(configuration), createRecipeMagnitudeAnalyzer(configuration), recipeMeasurementAnalyzer, configuration.getRecipeIngredientAnalyzerConfiguration());
    }

    default IngredientRecipeInstructionAnalysisFilter createRecipeIngredientRecipeInstructionAnalysisFilter(String text, RecipeAnalyzerConfiguration configuration) {
        RecipeIngredientAnalysis recipeIngredientAnalysis = createRecipeIngredientAnalysis(text, configuration);
        return new IngredientRecipeInstructionAnalysisFilter(
                recipeIngredientAnalysis,
                createRecipeMagnitudeAnalyzer(configuration),
                createRecipeMeasurementUnitAnalyzer(configuration),
                createRecipeInstructionIngredientAnalyzer(configuration),
                createRecipeLangAnalyzer(configuration).getTokens(recipeIngredientAnalysis.getIngredientNotation()));
    }

    default RecipeInstructionAnalysis createRecipeInstructionAnalysis(String text, RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionAnalysis(createRecipeLangAnalyzer(configuration).getTokenization(text));
    }

    default RecipeInstructionAnalysisFilterFactory createRecipeInstructionAnalysisFilterFactory(RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionAnalysisFilterFactory(
                createRecipeMagnitudeAnalyzer(configuration),
                createRecipeMeasurementUnitAnalyzer(configuration),
                createRecipeCommandAnalyzer(configuration),
                createRecipeToolAnalyzer(configuration),
                createRecipeInstructionIngredientAnalyzer(configuration),
                createRecipeLangAnalyzer(configuration),
                configuration.getRecipeInstructionAnalyzerConfiguration());
    }

    default RecipeInstructionAnalysis createRecipeInstructionAnalysisWrapped(String instructionTextWithoutCommandTags, RecipeAnalyzerConfiguration configuration) {
        Pair<String, String> pair = configuration.getRecipeCommandAnalyzerConfiguration().getCommandDelimiters().get(0);
        return createRecipeInstructionAnalysis(pair.getLeft() + instructionTextWithoutCommandTags + pair.getRight(), configuration);
    }

    default RecipeInstructionAnalyzer createRecipeInstructionAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionAnalyzer(
                createRecipeLangAnalyzer(configuration), createRecipeMeasurementUnitAnalyzer(configuration), createRecipeInstructionSplitter(configuration), createRecipeInstructionAnalysisFilterFactory(configuration), configuration);
    }

    default RecipeInstructionIngredientAnalyzer createRecipeInstructionIngredientAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionIngredientAnalyzer(createRecipeToolAnalyzer(configuration), configuration.getRecipeInstructionAnalyzerConfiguration(), configuration.getRecipeIngredientAnalyzerConfiguration());
    }

    default RecipeInstructionSplitter createRecipeInstructionSplitter(RecipeAnalyzerConfiguration configuration) {
        return new RecipeInstructionSplitter(true, configuration.getRecipeInstructionAnalyzerConfiguration().getRecipeInstructionSplitReplacements());
    }

    default RecipeInstructionSplitter.Replacement[] createRecipeInstructionSplitterReplacements(RecipeInstructionSplitter.Replacement replacement) {
        return new RecipeInstructionSplitter.Replacement[] {replacement};
    }

    default RecipeLangAnalyzer createRecipeLangAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeLangAnalyzer(configuration.getRecipeLangAnalyzerConfiguration());
    }

    default RecipeMagnitudeAnalyzer createRecipeMagnitudeAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeMagnitudeAnalyzer(configuration.getRecipeMagnitudeAnalyzerConfiguration());
    }

    default RecipeMeasurementAnalyzer createRecipeMeasurementAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeMeasurementAnalyzer(createRecipeLangAnalyzer(configuration), configuration.getRecipeMeasurementAnalyzerConfiguration());
    }

    default RecipeMeasurementUnitAnalysis createRecipeMeasurementUnitAnalysis(String text, RecipeAnalyzerConfiguration configuration) {
        return new RecipeMeasurementUnitAnalysis(text, configuration.getRecipeMeasurementAnalyzerConfiguration().findMeasurementUnitDefinition(text).get(), false);
    }

    default RecipeMeasurementAnalyzer createRecipeMeasurementUnitAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return new RecipeMeasurementAnalyzer(createRecipeLangAnalyzer(configuration), configuration.getRecipeMeasurementAnalyzerConfiguration());
    }

    default RecipeAnalyzer createRecipeStandardAnalyzer(RecipeAnalyzerConfiguration configuration) {
        RecipeMeasurementAnalyzer recipeMeasurementAnalyzer = createRecipeMeasurementUnitAnalyzer(configuration);
        RecipeLangAnalyzer recipeLangAnalyzer = createRecipeLangAnalyzer(configuration);
        RecipeCategoryAnalyzer recipeCategoryAnalyzer = new RecipeCategoryAnalyzer(recipeLangAnalyzer, configuration.getRecipeCategoryAnalyzerConfiguration());
        RecipeToolAnalyzer recipeToolAnalyzer = createRecipeToolAnalyzer(recipeLangAnalyzer, configuration);
        return new RecipeAnalyzer(
                recipeMeasurementAnalyzer,
                createRecipeIngredientAnalyzer(recipeMeasurementAnalyzer, configuration),
                createRecipeInstructionAnalyzer(configuration),
                recipeCategoryAnalyzer,
                recipeLangAnalyzer,
                recipeToolAnalyzer,
                configuration);
    }

    default RecipeToolAnalyzer createRecipeToolAnalyzer(RecipeAnalyzerConfiguration configuration) {
        return createRecipeToolAnalyzer(createRecipeLangAnalyzer(configuration), configuration);
    }

    default RecipeToolAnalyzer createRecipeToolAnalyzer(RecipeLangAnalyzer recipeLangAnalyzer, RecipeAnalyzerConfiguration configuration) {
        return new RecipeToolAnalyzer(recipeLangAnalyzer, configuration.getRecipeToolAnalyzerConfiguration());
    }

    default RecipeInstructionTokenization.Position createTokenizationPosition(int offset, int length) {
        return new RecipeInstructionTokenization.Position(offset, length);
    }

    default String createWrappedCommand(String instruction, RecipeAnalyzerConfiguration configuration) {
        Pair<String, String> delimiters = configuration.getRecipeCommandAnalyzerConfiguration().getCommandDelimiters().get(0);
        return delimiters.getLeft() + instruction + delimiters.getRight();
    }
}
