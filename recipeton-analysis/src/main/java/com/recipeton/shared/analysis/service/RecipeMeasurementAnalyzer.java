package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeMeasurementAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.MeasurementUnitDefinition;
import com.recipeton.shared.analysis.domain.RecipeMeasurement;
import com.recipeton.shared.analysis.domain.RecipeMeasurementUnitAnalysis;
import com.recipeton.shared.util.TextUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_PIECE;
import static com.recipeton.shared.util.StreamUtil.toStream;

public class RecipeMeasurementAnalyzer {

    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeMeasurementAnalyzerConfiguration configuration;

    private final Map<String, MeasurementUnitDefinition> measurementUnits;
    private final Map<MeasurementUnitDefinition, Set<String>> intermediateStopWordsByUnitDefinition = new HashMap<>();

    private final String[] translateFrom;
    private final String[] translateTo;

    public RecipeMeasurementAnalyzer(RecipeLangAnalyzer recipeLangAnalyzer, RecipeMeasurementAnalyzerConfiguration configuration) {
        this.recipeLangAnalyzer = recipeLangAnalyzer;
        this.configuration = configuration;
        this.measurementUnits = createMeasurementUnitMap(configuration.getMeasurementUnitDefinitions());

        List<String> aux1 = new ArrayList<>();
        List<String> aux2 = new ArrayList<>();
        this.measurementUnits.entrySet().stream()
                .filter(e -> e.getValue().isNormalizable())
                .sorted(Comparator.comparing(e -> -e.getKey().length()))
                .forEach(
                        e -> {
                            aux1.add(" " + e.getKey() + " ");
                            aux2.add(" " + e.getValue().getName() + " ");
                        });
        translateFrom = aux1.toArray(String[]::new);
        translateTo = aux2.toArray(String[]::new);
    }

    public RecipeMeasurement analyzeRecipeMeasurement(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        String[] parts = text.split(" ");
        RecipeMeasurement recipeMeasurement = new RecipeMeasurement().setOriginal(text);
        boolean hasUnit = parts.length > 1;
        if (hasUnit) {
            String magnitudePart = parts[0];
            String unitPart = parts[1];
            MeasurementUnitDefinition unit = getOrCreateMeasurementUnit(unitPart);
            recipeMeasurement.setMagnitude(magnitudePart).setMagnitudeValue(TextUtil.isNumeric(magnitudePart) ? new BigDecimal(magnitudePart) : null).setUnit(unit);
        } else {
            String magnitudePart = parts[0];
            recipeMeasurement.setMagnitude(magnitudePart).setMagnitudeValue(TextUtil.isNumeric(magnitudePart) ? new BigDecimal(magnitudePart) : null).setUnit(configuration.getUnitPieceDefault());
        }
        return recipeMeasurement;
    }

    public BigDecimal analyzeRecipeMeasurementValue(String text) {
        return Optional.ofNullable(analyzeRecipeMeasurement(text)).map(RecipeMeasurement::getMagnitudeValue).orElse(null);
    }

    public RecipeMeasurementUnitAnalysis createDefault(MeasurementUnitDefinition measurementUnitDefinition) {
        return new RecipeMeasurementUnitAnalysis(measurementUnitDefinition.getName(), measurementUnitDefinition, true);
    }

    private Map<String, MeasurementUnitDefinition> createMeasurementUnitMap(Set<MeasurementUnitDefinition> measurementUnitDefinitions) {
        Map<String, MeasurementUnitDefinition> measurementUnits = new HashMap<>();
        measurementUnitDefinitions.forEach(
                u -> {
                    measurementUnits.put(u.getName().toLowerCase(), u);
                    toStream(u.getVariants()).forEach(v -> measurementUnits.put(v.toLowerCase(), u));
                });
        return measurementUnits;
    }

    public Optional<MeasurementUnitDefinition> findMeasurementUnitByText(String unitText) {
        return Optional.ofNullable(measurementUnits.get(unitText.toLowerCase()));
    }

    public String formatRecipeMeasurementUnits(String text) {
        return StringUtils.replaceEach(text, translateFrom, translateTo);
    }

    public RecipeMeasurementUnitAnalysis getDefaultRecipeMeasurementUnitAnalysis(BigDecimal magnitudeFromValue) {
        if (magnitudeFromValue == null) {
            return null;
        }

        if (magnitudeFromValue.doubleValue() > configuration.getOnUnitUndefinedSetAsWeightedDefaultWhenMagnitudeGreaterThan()) {
            return createDefault(configuration.getUnitWeightedDefault());
        }

        return createDefault(configuration.getUnitPieceDefault());
    }

    public Set<String> getIntermediateStopWordsByUnitDefinition(MeasurementUnitDefinition unitDefinition) {
        return intermediateStopWordsByUnitDefinition.computeIfAbsent(
                unitDefinition, mud -> Stream.concat(configuration.getMeasurementUnitSeparatorTokens().stream(), recipeLangAnalyzer.getStopWords().stream()).filter(s -> !mud.isNotation(s)).collect(Collectors.toSet()));
    }

    public MeasurementUnitDefinition getOrCreateMeasurementUnit(String unitText) {
        return Optional.ofNullable(measurementUnits.get(unitText.toLowerCase())).orElseGet(() -> new MeasurementUnitDefinition(unitText, UNIT_PIECE, false));
    }

    public boolean isUnitSeparator(String token) {
        return configuration.getMeasurementUnitSeparatorTokens().contains(token);
    }
}
