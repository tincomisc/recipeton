package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;

import static com.recipeton.shared.util.MapUtil.toMap;
import static java.util.regex.Pattern.compile;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeLangAnalyzerConfiguration {

    //    public static final Pattern DELIMITERS_PATTERN = compile("<nobr>|<\\/nobr>|\\[|\\]");
    public static final Pattern DELIMITERS_PATTERN = compile("\\[|\\]");

    public static final Pattern DEFAULT_TOKEN_CLEAN_PATTERN = compile("[\\(\\)\\,\\.\\/\\+\\-\\_]");

    public static final String DEFAULT_TOKEN_SPLIT_REGEX = " ";

    public static final Map<String, String> TEXT_NORMALIZE_DEFAULT =
            toMap(
                    "\n", "",
                    // [Zs] Better would have been regex but less flexible
                    "\u00A0", " ", // NO-BREAK SPACE
                    //            "\u0020", " ", // SPACE
                    "\u1680", " ", // OGHAM SPACE MARK
                    "\u2000", " ", // EN QUAD
                    "\u2001", " ", // EM QUAD
                    "\u2002", " ", // EN SPACE
                    "\u2003", " ", // EM SPACE
                    "\u2004", " ", // THREE-PER-EM SPACE
                    "\u2005", " ", // FOUR-PER-EM SPACE
                    "\u2006", " ", // SIX-PER-EM SPACE
                    "\u2007", " ", // FIGURE SPACE
                    "\u2008", " ", // PUNCTUATION SPACE
                    "\u2009", " ", // THIN SPACE
                    "\u200A", " ", // HAIR SPACE
                    "\u202F", " ", // NARROW NO-BREAK SPACE
                    "\u205F", " ", // MEDIUM MATHEMATICAL SPACE
                    "\u3000", " ", // IDEOGRAPHIC SPACE
                    "<br>", "",
                    "<br />", "",
                    "<strong>", "",
                    "</strong>", "",
                    "<i>", "",
                    "</i>", "",
                    // Normalize special sections
                    "<nobr>", "[",
                    "</nobr>", "]");

    public static final Pattern PATTERN_PUNCTUATION_WITHOUT_SPACE_AFTER_AND_NOT_BETWEEN_DELIMITERS = compile("([,.!?;:](?=\\S))(?![^<]*>|[^<>]*<\\/)(?![^\\[]*\\])");
    //    private Pattern punctuationFixPattern = stringPattern("[,.!?;:](?![^<]*>|[^<>]*<\\/)(?![^\\[]*\\])");
    public static final Pattern PATTERN_MULTIPLE_SPACES = compile("\\s+");

    public static final int TOKEN_LENGTH_MIN = 2;

    private final Set<String> stopWords;
    private final Pattern punctuationFixPattern;

    private Map<String, String> textNormalizeReplacements = TEXT_NORMALIZE_DEFAULT;

    private Map<String, String> languageNormalizeReplacements = Collections.emptyMap();

    /** Should do inflection, removal/normalization of useless prefixes or suffixes... */
    private Function<String, String> tokenFormatFunction = s -> s;

    private String tokenSplitRegex = DEFAULT_TOKEN_SPLIT_REGEX;
    private Pattern tokenCleanPattern = DEFAULT_TOKEN_CLEAN_PATTERN;

    public RecipeLangAnalyzerConfiguration(Set<String> stopWords, Pattern punctuationFixPattern) {
        this.stopWords = stopWords;
        this.punctuationFixPattern = punctuationFixPattern;
    }

    public RecipeLangAnalyzerConfiguration(Set<String> stopWords) {
        this(stopWords, PATTERN_PUNCTUATION_WITHOUT_SPACE_AFTER_AND_NOT_BETWEEN_DELIMITERS);
    }
}
