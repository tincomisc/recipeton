package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeCommandProgram;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static java.util.List.of;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeCommandAnalyzerConfiguration {

    public static final List<Pair<String, String>> DEFAULT_COMMAND_DELIMITERS =
            of(
                    //            Pair.of("<nobr>", "</nobr>"),
                    Pair.of("[", "]"));

    public static final Duration HALF_SECOND = Duration.ofMillis(500);

    private List<Pair<String, String>> commandDelimiters = DEFAULT_COMMAND_DELIMITERS;

    private Map<RecipeCommandProgram, List<Predicate<String>>> commandProgramPredicates;
    private List<Predicate<String>> commandRotationReversePredicates;
    private Map<Duration, List<Predicate<String>>> commandDurationPredicates;
    private Pattern commandDurationHoursPattern;
    private Pattern commandDurationMinutesPattern;
    private Pattern commandDurationSecondsPattern;
    private Map<Long, List<Predicate<String>>> commandTemperaturePredicates;
    private Pattern commandTemperaturePattern;
    private Map<BigDecimal, List<Predicate<String>>> commandSpeedPredicates;
    private Pattern commandSpeedPattern;
    private Pattern commandRepetitionFromPattern;
    private Pattern commandRepetitionFromToPattern;
}
