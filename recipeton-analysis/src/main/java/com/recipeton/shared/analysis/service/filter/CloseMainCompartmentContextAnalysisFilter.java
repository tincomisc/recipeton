package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionTag;
import com.recipeton.shared.analysis.domain.RecipeTag;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;

public class CloseMainCompartmentContextAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    private static final RecipeTag[] MEASURING_CUP_PLACEMENT_RELATED =
            new RecipeTag[] {
                MEASURING_CUP_NOTUSE,
                MEASURING_CUP_PUT,
                MEASURING_CUP_HOLD_VIBRATION,
                MEASURING_CUP_REMOVE,
                SIMMERING_BASKET_AS_LID_PUT,
                SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT,
                SIMMERING_BASKET_AS_LID_REMOVE,
                STEAMER_PUT,
                STEAMER_CLOSE,
                STEAMER_TRAY_PUT,
                STEAMER_DISH_PUT
            };

    public CloseMainCompartmentContextAnalysisFilter() {
        super("isMainCompartmentCloseNeeded?");
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) { // NOPMD BS
        if (!recipeInstructionAnalysis.isTagsAnyPresent(DO_COMMAND_MAIN) || recipeInstructionAnalysis.isTagsAnyPresent(SEQUENCE_PARALLEL) || recipeInstructionAnalysis.isTagsAnyPresent(MEASURING_CUP_PLACEMENT_RELATED)) {
            return recipeInstructionAnalysis;
        }

        RecipeInstructionAnalysis prev = recipeInstructionAnalysis.getPrev();
        if (prev != null && prev.isTagsAnyPresent(MEASURING_CUP_PLACEMENT_RELATED) && prev.isTagsNonePresent(QUALIFIES_PREVIOUS)) { // Handled by prev
            return recipeInstructionAnalysis;
        }

        RecipeInstructionAnalysis next = recipeInstructionAnalysis.getNext();
        if (next != null && next.isTagPresent(QUALIFIES_PREVIOUS) && next.isTagsAnyPresent(MEASURING_CUP_PLACEMENT_RELATED)) {
            return recipeInstructionAnalysis;
        }

        RecipeInstructionTag focus = recipeInstructionAnalysis.findFirstMatchInPrev(Integer.MAX_VALUE, TAGS_FOCUS);
        if (FOCUS_MAIN.equals(focus)) {
            return addTermIfNotPresent(recipeInstructionAnalysis, DO_COMMAND_MAIN_PREPARATION_DEFAULT);
        }

        return recipeInstructionAnalysis;
    }
}
