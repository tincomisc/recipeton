package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeCategoryAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeCategoryDefinition;
import lombok.extern.slf4j.Slf4j;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.RECIPE_CATEGORY_NAME_EXTRA_PREFIX;

@Slf4j
public class RecipeCategoryAnalyzer {

    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeCategoryAnalyzerConfiguration configuration;

    private final Map<String, RecipeCategoryDefinition> recipeCategoryDefinitionsByNames;

    public RecipeCategoryAnalyzer(RecipeLangAnalyzer recipeLangAnalyzer, RecipeCategoryAnalyzerConfiguration configuration) {
        this.recipeLangAnalyzer = recipeLangAnalyzer;
        this.configuration = configuration;

        recipeCategoryDefinitionsByNames = new HashMap<>();
        this.configuration
                .getRecipeCategoryNamesByUid()
                .forEach(
                        (uid, names) -> {
                            if (names.size() == 0) {
                                log.warn("Analyzer configured category main variant missing. Recipe category will be ignored until added to config. uid={}", uid);
                                return;
                            }
                            String preferredName = names.get(0); // Do  not modify 1st in the list. Its the literally preferred name
                            names.stream().map(this::formatRecipeCategoryName).forEach(n -> recipeCategoryDefinitionsByNames.put(n, new RecipeCategoryDefinition(uid, preferredName)));
                        });
    }

    public String formatCategoryUid(String proposedName) {
        return recipeLangAnalyzer.toUid(proposedName);
    }

    private String formatRecipeCategoryName(String proposedName) {
        return configuration.getRecipeCategoryNamePreprocessor().apply(proposedName);
    }

    public int getRatioMin(String categoryText, RecipeCategoryDefinition recipeCategoryDefinition) {
        if (recipeCategoryDefinition.isCurated()) {
            return configuration.getRecipeCategoryMatchRatioMinSpecific();
        }

        return categoryText.length() <= configuration.getRecipeCategoryShortSearchSubstringLengthMax() ? configuration.getRecipeCategoryMatchRatioMinSpecific() : configuration.getRecipeCategoryMatchRatioMin();
    }

    public RecipeCategoryDefinition getRecipeCategoryDefinition(String proposedName, boolean mainCategory) {
        String preferredName = formatRecipeCategoryName(proposedName);
        if (StringUtils.isBlank(preferredName)) {
            return null;
        }

        return recipeCategoryDefinitionsByNames.computeIfAbsent(
                preferredName,
                n -> {
                    if (configuration.getRecipeCategoryMatchRatioMin() > 0) {
                        int bestRatio = 0;
                        RecipeCategoryDefinition best = null;
                        for (Map.Entry<String, RecipeCategoryDefinition> entry : recipeCategoryDefinitionsByNames.entrySet()) {
                            String variantName = entry.getKey();
                            //                            log.info("  {} {}", preferredName, entry);
                            if (!isSimilarLength(variantName.length(), preferredName.length())) {
                                continue;
                            }

                            int ratio = FuzzySearch.weightedRatio(n, variantName);
                            int ratioMin = getRatioMin(variantName, entry.getValue());
                            if (ratio > bestRatio && ratio > ratioMin) {
                                best = entry.getValue();
                                bestRatio = ratio;
                            }
                        }
                        if (best != null) {
                            return best;
                        }
                    }
                    String uid = formatCategoryUid(preferredName);
                    String formattedName = mainCategory ? preferredName : RECIPE_CATEGORY_NAME_EXTRA_PREFIX + preferredName;

                    return new RecipeCategoryDefinition(uid, formattedName);
                });
    }

    public boolean isSimilarLength(int length1, int length2) {
        return Math.abs(length1 - length2) <= configuration.getRecipeCategorySearchComparisonMaxLengthDiff();
    }
}
