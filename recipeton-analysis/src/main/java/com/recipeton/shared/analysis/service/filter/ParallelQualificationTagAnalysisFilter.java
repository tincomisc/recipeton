package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;

public class ParallelQualificationTagAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    public ParallelQualificationTagAnalysisFilter() {
        super("isParallelNext?");
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (!recipeInstructionAnalysis.isTagPresent(SEQUENCE_PARALLEL)) {
            return recipeInstructionAnalysis;
        }

        if (recipeInstructionAnalysis.getPrev() != null && recipeInstructionAnalysis.getPrev().isTagPresent(DO_COMMAND_MAIN)) {
            return addTermIfNotPresent(recipeInstructionAnalysis, QUALIFIES_PREVIOUS);
        }

        return recipeInstructionAnalysis;
    }
}
