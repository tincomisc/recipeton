package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionMatcher;
import com.recipeton.shared.analysis.domain.RecipeInstructionTag;
import com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeInstructionAnalyzerConfiguration {

    public static final int INGREDIENT_MATCH_RATIO_MIN = 80;
    public static final int INGREDIENT_MATCH_RATION_MIN_SPECIFIC = 99;
    public static final int INGREDIENT_SHORT_SEARCH_SUBSTRING_LENGTH_MAX = 2;
    public static final int INGREDIENT_SEARCH_COMPARISON_MAX_LENGTH_DIFF = 3;

    private final Replacement[] recipeInstructionSplitReplacements;
    private final Map<RecipeInstructionTag, RecipeInstructionMatcher> recipeInstructionTagMatchers;
    private final Pattern recipeInstructionIngredientSeparatorPattern;
    private Pattern recipeInstructionTransferNexus;

    private Function<String, String> preprocessor = s -> s;

    private int ingredientMatchRatioMin;
    private int ingredientMatchRatioMinSpecific;
    private int ingredientShortSearchSubstringLengthMax;
    private int ingredientSearchComparisonMaxLengthDiff;

    public RecipeInstructionAnalyzerConfiguration(Replacement[] recipeInstructionSplitReplacements, Map<RecipeInstructionTag, RecipeInstructionMatcher> recipeInstructionTagMatchers, Pattern recipeInstructionIngredientSeparatorPattern) {
        this.recipeInstructionSplitReplacements = recipeInstructionSplitReplacements;
        this.recipeInstructionTagMatchers = recipeInstructionTagMatchers;
        this.recipeInstructionIngredientSeparatorPattern = recipeInstructionIngredientSeparatorPattern;

        this.ingredientMatchRatioMin = INGREDIENT_MATCH_RATIO_MIN;
        this.ingredientMatchRatioMinSpecific = INGREDIENT_MATCH_RATION_MIN_SPECIFIC;
        this.ingredientShortSearchSubstringLengthMax = INGREDIENT_SHORT_SEARCH_SUBSTRING_LENGTH_MAX;
        this.ingredientSearchComparisonMaxLengthDiff = INGREDIENT_SEARCH_COMPARISON_MAX_LENGTH_DIFF;
    }

    public RecipeInstructionAnalyzerConfiguration setInstructionPreprocessor(Function<String, String> preprocessor) {
        this.preprocessor = preprocessor;
        return this;
    }
}
