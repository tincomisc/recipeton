package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.ToString;

@ToString(includeFieldNames = false)
public class RecipeToolComposedActionTag implements RecipeTag {

    public static final RecipeToolComposedActionTag OVEN_OPERATION = new RecipeToolComposedActionTag("OVEN_OPERATION");
    public static final RecipeToolComposedActionTag REFRIGERATE = new RecipeToolComposedActionTag("REFRIGERATE");
    public static final RecipeToolComposedActionTag FREEZE = new RecipeToolComposedActionTag("FREEZE");

    public static final RecipeToolComposedActionTag SIMMERING_BASKET_PUT_INSIDE = new RecipeToolComposedActionTag("SIMMERING_BASKET_PUT_INSIDE");
    public static final RecipeToolComposedActionTag SIMMERING_BASKET_REMOVE = new RecipeToolComposedActionTag("SIMMERING_BASKET_REMOVE");
    public static final RecipeToolComposedActionTag SIMMERING_BASKET_OTHER = new RecipeToolComposedActionTag("SIMMERING_BASKET");
    public static final RecipeToolComposedActionTag SIMMERING_BASKET_AS_LID_PUT = new RecipeToolComposedActionTag("SIMMERING_BASKET_AS_LID_PUT");
    public static final RecipeToolComposedActionTag SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT = new RecipeToolComposedActionTag("SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT");
    public static final RecipeToolComposedActionTag SIMMERING_BASKET_AS_LID_REMOVE = new RecipeToolComposedActionTag("SIMMERING_BASKET_AS_LID_REMOVE");

    public static final RecipeToolComposedActionTag MIXING_BOWL = new RecipeToolComposedActionTag("MIXING_BOWL");
    public static final RecipeToolComposedActionTag MIXING_BOWL_REMOVE = new RecipeToolComposedActionTag("MIXING_BOWL_REMOVE");
    public static final RecipeToolComposedActionTag MIXING_BOWL_PUT = new RecipeToolComposedActionTag("MIXING_BOWL_PUT");
    public static final RecipeToolComposedActionTag MIXING_BOWL_EMPTY = new RecipeToolComposedActionTag("MIXING_BOWL_EMPTY");
    public static final RecipeToolComposedActionTag MIXING_BOWL_CLEAN = new RecipeToolComposedActionTag("MIXING_BOWL_CLEAN");

    public static final RecipeToolComposedActionTag TRANSFER_FROM_BOWL = new RecipeToolComposedActionTag("TRANSFER_FROM_BOWL");

    public static final RecipeToolComposedActionTag MEASURING_CUP = new RecipeToolComposedActionTag("MEASURING_CUP");
    public static final RecipeToolComposedActionTag MEASURING_CUP_NOTUSE = new RecipeToolComposedActionTag("MEASURING_CUP_NOTUSE");
    public static final RecipeToolComposedActionTag MEASURING_CUP_REMOVE = new RecipeToolComposedActionTag("MEASURING_CUP_REMOVE");
    public static final RecipeToolComposedActionTag MEASURING_CUP_PUT = new RecipeToolComposedActionTag("MEASURING_CUP_PUT");
    public static final RecipeToolComposedActionTag MEASURING_CUP_HOLD_VIBRATION = new RecipeToolComposedActionTag("MEASURING_CUP_HOLD_VIBRATION");

    public static final RecipeToolComposedActionTag WHISK = new RecipeToolComposedActionTag("WHISK");
    public static final RecipeToolComposedActionTag WHISK_PUT = new RecipeToolComposedActionTag("WHISK_PUT");
    public static final RecipeToolComposedActionTag WHISK_REMOVE = new RecipeToolComposedActionTag("WHISK_REMOVE");

    public static final RecipeToolComposedActionTag SPATULA = new RecipeToolComposedActionTag("SPATULA");
    public static final RecipeToolComposedActionTag SPATULA_SCRAP_SIDE = new RecipeToolComposedActionTag("SPATULA_SCRAP_SIDE");
    public static final RecipeToolComposedActionTag SPATULA_MIX = new RecipeToolComposedActionTag("SPATULA_MIX");
    public static final RecipeToolComposedActionTag SPATULA_MIX_WELL = new RecipeToolComposedActionTag("SPATULA_MIX_WELL");

    public static final RecipeToolComposedActionTag STEAMER_CLOSE = new RecipeToolComposedActionTag("STEAMER_CLOSE");
    public static final RecipeToolComposedActionTag STEAMER_PUT = new RecipeToolComposedActionTag("STEAMER_PUT");
    public static final RecipeToolComposedActionTag STEAMER_REMOVE = new RecipeToolComposedActionTag("STEAMER_REMOVE");
    public static final RecipeToolComposedActionTag STEAMER_TRAY_PUT = new RecipeToolComposedActionTag("STEAMER_TRAY_PUT");
    public static final RecipeToolComposedActionTag STEAMER_TRAY_REMOVE = new RecipeToolComposedActionTag("STEAMER_TRAY_REMOVE");
    public static final RecipeToolComposedActionTag STEAMER_DISH_PUT = new RecipeToolComposedActionTag("STEAMER_DISH_PUT");
    public static final RecipeToolComposedActionTag STEAMER_DISH_REMOVE = new RecipeToolComposedActionTag("STEAMER_DISH_REMOVE");

    public static final RecipeToolComposedActionTag OTHER_OPERATION = new RecipeToolComposedActionTag("OTHER_OPERATION");
    public static final RecipeToolComposedActionTag USEFUL_ITEMS = new RecipeToolComposedActionTag("USEFUL_ITEMS");
    public static final RecipeToolComposedActionTag KITCHEN_EQUIPMENT = new RecipeToolComposedActionTag("KITCHEN_EQUIPMENT");

    public static final RecipeToolComposedActionTag[] TAGS_UTENSIL_PLACEMENT = {
        TRANSFER_FROM_BOWL,
        SIMMERING_BASKET_PUT_INSIDE,
        SIMMERING_BASKET_REMOVE,
        SIMMERING_BASKET_AS_LID_PUT,
        SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT,
        SIMMERING_BASKET_AS_LID_REMOVE,
        MIXING_BOWL_REMOVE,
        MIXING_BOWL_PUT,
        MEASURING_CUP_REMOVE,
        MEASURING_CUP_PUT,
        WHISK_PUT,
        WHISK_REMOVE,
        STEAMER_CLOSE,
        STEAMER_PUT,
        STEAMER_REMOVE,
        STEAMER_TRAY_PUT,
        STEAMER_TRAY_REMOVE,
        STEAMER_DISH_PUT,
        STEAMER_DISH_REMOVE
    };

    private final String value;

    RecipeToolComposedActionTag(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
