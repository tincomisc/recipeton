package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;

@Data
@ToString(onlyExplicitlyIncluded = true)
public class RecipeIngredientTerm implements RecipeMatchedTerm {

    @ToString.Include private final RecipeIngredientAnalysis ingredient;
    private final RecipeInstructionMatch match;
    //    @ToString.Include private final boolean contextualMatch;
    @ToString.Include private final boolean detachedMatch;

    private BigDecimal localMagnitudeFromValue;

    private BigDecimal localMagnitudeToValue;

    // These are set later.
    @ToString.Include @EqualsAndHashCode.Exclude private Double termScore;
    @ToString.Include @EqualsAndHashCode.Exclude private boolean conclusive;
    @EqualsAndHashCode.Exclude private Target target;

    public static RecipeIngredientTerm ofMatchOnAll(RecipeIngredientAnalysis recipeIngredientAnalysis, final String text) {
        RecipeIngredientTerm recipeIngredientTerm = new RecipeIngredientTerm(recipeIngredientAnalysis, new RecipeInstructionMatch(0, text, SCORE_MAX, true, "is_all?"), false);
        recipeIngredientTerm.setConclusive(true);
        return recipeIngredientTerm;
    }

    public BigDecimal getMagnitudeFromValue() {
        return isLocalMagnitudeOverride() ? localMagnitudeFromValue : ingredient.getMagnitudeFromValue();
    }

    public BigDecimal getMagnitudeToValue() {
        return isLocalMagnitudeOverride() ? localMagnitudeToValue : ingredient.getMagnitudeToValue();
    }

    public boolean isLocalMagnitudeOverride() {
        return localMagnitudeFromValue != null;
    }

    public boolean isPartialUse() {
        return isLocalMagnitudeOverride() && !localMagnitudeFromValue.equals(ingredient.getMagnitudeFromValue());
    }

    public enum Target {
        PREPROCESS, // Weigh to use later
        PROCESS, // Main. To process by appliance (add, place in appliance)
        SERVING, // To decorate
        ACCESORY // To prepare some kitchen accessory: oil for the pan
    }
}
