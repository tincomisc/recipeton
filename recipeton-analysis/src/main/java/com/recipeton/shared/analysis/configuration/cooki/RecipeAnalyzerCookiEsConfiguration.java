package com.recipeton.shared.analysis.configuration.cooki;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.*;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.lang.Spanish21TokenConverter;
import com.recipeton.shared.analysis.service.RecipeInstructionSplitter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.configuration.RecipeCategoryAnalyzerConfiguration.FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST;
import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.*;
import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCommandProgram.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatcher;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement.toNext;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class RecipeAnalyzerCookiEsConfiguration extends RecipeAnalyzerConfiguration { // NOPMD Long

    public static final MeasurementUnitDefinition UNIT_PIECE_ES = new MeasurementUnitDefinition("ud", UNIT_PIECE, false, "pieza", "piezas", "trozo", "trozos");
    public static final MeasurementUnitDefinition UNIT_TSP_ES = new MeasurementUnitDefinition("cdta", UNIT_TSP, true, "cdtas", "cdita", "cditas", "cucharadita", "cucharaditas");

    public static final String COOKI_ES_ACTION_DEFAULT = "establezca";

    public static final String[] COOKI_ES_ACTION_REGEXES = {
        COOKI_ES_ACTION_DEFAULT,
        "acreme",
        "amase",
        "bata",
        "caliente",
        "cocine",
        "continue cocinando",
        "continúe cocinando",
        "corte", // causes some problems
        "córtelo",
        "córtela",
        "cueza",
        "deje reducir",
        "deje reposar",
        "derrita",
        "deshebre",
        "disuelva",
        "dore",
        "emulsione",
        "funda",
        //                "deje reposar", // Maybe loses too much context
        "mezcle",
        "mezcla",
        "hierva",
        "inicie",
        "infusione",
        //              "lave", // ... y lave en el cestillo
        "licue",
        "licúe",
        "monte",
        "muela",
        "pique",
        "programe",
        "pulverice",
        "ralle",
        "rállelo",
        "reserve",
        "reduzca",
        "rehogue",
        // "reparta", // Useful in a few cases but bad for many others. Not directly for machine and affects context
        // "remueva", // Needs context to know if needs to split
        "sin colocar",
        "sin programar tiempo",
        "sirva",
        "sofría",
        "tempere",
        "tamice",
        "triturando",
        "triture",
        "trocee",
        "haga puré",
        "saque", // Evaluate
        "termine de mezclar",
        "vacíe",
        "vierta",
        "vuelva a batir",
        "vuelva a mezclar",
        "vuelva a programar",
        //
        "añada",
        "agregue",
        "incorpore",
        "espolvoree",
        "coloque",
        "introduzca",
        //            "lave",
        //            "limpie",
        //            "aclare",
        //            "enjuague",
        //            "extraiga",
        //            "retire",

        "ponga",
        "pese",
        "sitúe",
        "transfiera"
    };

    public static final Set<String> COOKI_ES_PREPARATION_TOKENS =
            Set.of(
                    "en",
                    "para", // aceite para la sarten
                    "adobado",
                    "batido",
                    "ligeramente",
                    "con",
                    "cocida",
                    "cocido",
                    "cocidas",
                    "cocidos",
                    "congelada",
                    "congeladas",
                    "congelado",
                    "congelados",
                    "cortada",
                    "cortadas",
                    "cortado",
                    "cortados",
                    //                "de", // peligroso. casos como pimienta [de cayena], [de oliva] ...
                    "desmenuzada",
                    "desmenuzadas",
                    "desmenuzado",
                    "desmenuzados",
                    "entera",
                    "enteras",
                    "entero",
                    "enteros",
                    "fresco",
                    "fresca",
                    "frescos",
                    "frescas",
                    "molida",
                    "molidas",
                    "molido",
                    "molidos",
                    "pelada",
                    "peladas",
                    "pelado",
                    "pelados",
                    "rallado",
                    "rallada",
                    "recién",
                    "seco",
                    "secos",
                    "seca",
                    "secas",
                    "sin",
                    "troceada",
                    "troceadas",
                    "troceado",
                    "troceados",
                    "virgen");
    private static final Map<RecipeToolDefinition, RecipeInstructionMatcher> RECIPE_TOOL_MATCHERS =
            toMapP(
                    Pair.of(new RecipeToolDefinition("vaso", true, TOOL_MAIN), toRecipeInstructionMatcher("(?i)\\bvaso\\b")),
                    Pair.of(new RecipeToolDefinition("varoma", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:varoma|vaporera|steamer)\\b")),
                    // Plato varoma
                    Pair.of(new RecipeToolDefinition("cestillo", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bcestillo\\b")),
                    Pair.of(new RecipeToolDefinition("espátula", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bespátula\\b")),
                    Pair.of(new RecipeToolDefinition("mariposa", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bmariposa\\b")),
                    Pair.of(new RecipeToolDefinition("cubilete", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bcubilete\\b")),
                    Pair.of(new RecipeToolDefinition("frigorífico", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:frigorífico|nevera|refrigerador)\\b")),
                    Pair.of(new RecipeToolDefinition("congelador", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcongelador\\b")),
                    Pair.of(new RecipeToolDefinition("horno", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bhorno\\b")),
                    Pair.of(new RecipeToolDefinition("base", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bbase[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("bandeja", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bbandeja[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("bol", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bbol(?:es)?\\b")),
                    Pair.of(new RecipeToolDefinition("cazo", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcazo[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("cazuela", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:cazuel(?:a|as|ita|itas)?)\\b")),
                    Pair.of(new RecipeToolDefinition("charola", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcharola[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("copa", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcopa[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("fuente", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bfuente[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("jarra", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:jarr(?:a|as|ita|itas)?)\\b")),
                    Pair.of(new RecipeToolDefinition("molde", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bmolde[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("olla", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bolla[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("plato", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplato[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("paella", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpaella\\b")),
                    Pair.of(new RecipeToolDefinition("platillo", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplatillo[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("recipiente", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\brecipiente[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("refractario", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\brefractario[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("remequín", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:remequín|remequines)\\b")),
                    Pair.of(new RecipeToolDefinition("sartén", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:sartén|sartenes)\\b")),
                    Pair.of(new RecipeToolDefinition("sopera", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bsopera[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("taza", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\btaza[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("tacita", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\btacita[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("tazón", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:tazón|tazones)\\b")));
    private static final String REGEX_TOOL_EXTERNAL_CONTAINER = RecipeToolAnalyzerConfiguration.getCombinedRegex(RECIPE_TOOL_MATCHERS.entrySet().stream().filter(e -> e.getKey().isContainer() && e.getKey().getTag().equals(TOOL_EXTERNAL)));

    private static final String REGEX_ITEM_TRANSFER_NEXUS = "\\b(?:en|sobre|a) (?:el|la|los|las|un|uno|unos|una|unas|varios|varias)\\b";
    private static final String REGEX_FOCUS_EXTERNAL_CONTAINER = REGEX_ITEM_TRANSFER_NEXUS + " (?:" + REGEX_TOOL_EXTERNAL_CONTAINER + ")";

    private static final Spanish21TokenConverter SPANISH_21_TOKEN_CONVERTER = new Spanish21TokenConverter();

    private static final String REGEX_PARALLEL = "mientras tanto";
    private static final Pattern PARALLEL_TEXT_PATTERN = compile("(?i)" + REGEX_PARALLEL + "[,]? ");

    private static final String REGEX_SIN_CUBILETE = "(?i)(?:retire|sin poner|sin colocar).*cubilete";

    public RecipeAnalyzerCookiEsConfiguration() {
        super(
                "COOKI_ES",
                rd -> containsIgnoreCase(rd.getLocale(), "es") && containsIgnoreCase(rd.getSource(), "cooki"),
                createRecipeLangAnalyzerConfiguration(),
                createRecipeCategoryAnalyzerConfiguration(),
                createRecipeCommandAnalyzerConfiguration(),
                createRecipeMagnitudeAnalyzerConfiguration(),
                createRecipeMeasurementAnalyzerConfiguration(),
                createRecipeInstructionAnalyzerConfiguration(),
                createRecipeIngredientAnalyzerConfiguration(),
                createRecipeToolAnalyzerConfiguration(),
                createRecipeCustomizationConfiguration());
    }

    public static Function<String, String> createCookiEsInstructionPreprocessor() {
        return new Function<>() {

            private static final String SEPARATORS_BASE = "[,]? e[,]? |[,]? luego[,]? |[,]? después[,]? |[,]? y luego[,]? |[,]? y[,]? ";

            private static final String REGEX_ACTION_PREPARATIONS = "(?<prep>(?:" + REGEX_SIN_CUBILETE + "|(?:colocando) el cestillo en lugar del cubilete))";

            private final String regexSentenceSeparators = "(?<sep>" + SEPARATORS_BASE + "|(?<!" + REGEX_PARALLEL + "), )";

            private final String regexParallel = "(?<par>" + REGEX_PARALLEL + ")";
            private final Pattern separatorParallelPattern = compile("(?i)" + regexSentenceSeparators + regexParallel);

            private final String regexActions = "(?<act>" + String.join("|", COOKI_ES_ACTION_REGEXES) + ")";

            private final Pattern separatorCommandWithoutActionPattern = compile("(?i)" + "(?<sep>[,]?" + SEPARATORS_BASE + ")" + REGEX_COMMAND);
            private final Pattern beginCommandWithoutActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_COMMAND);

            private final Pattern commandPreparationPattern = compile("(?i)" + REGEX_COMMAND + "[,]? " + REGEX_ACTION_PREPARATIONS);

            private final Pattern separatorPreparationActionPattern = compile("(?i)" + regexSentenceSeparators + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern separatorActionPreparationPattern = compile("(?i)" + regexSentenceSeparators + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");
            private final Pattern beginPreparationActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern beginActionPreparationPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");

            private final Pattern separatorActionPattern = compile("(?i)" + regexSentenceSeparators + regexActions);

            private final Pattern actionsAfterDotPattern = compile("(?i)(?<=\\. )" + regexActions);
            private final Pattern parallelAfterDotPattern = compile("(?i)(?<=\\. )" + regexParallel);
            private final Pattern preparationsAfterDotPattern = compile("(?i)(?<=\\. )" + REGEX_ACTION_PREPARATIONS);

            @Override
            public String apply(String text) {
                String preprocessed = text;

                preprocessed = separatorParallelPattern.matcher(preprocessed).replaceAll(". ${par}");

                preprocessed = separatorCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_ES_ACTION_DEFAULT + " ${cmd}");
                preprocessed = beginCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_ES_ACTION_DEFAULT + " ${cmd}");

                preprocessed = commandPreparationPattern.matcher(preprocessed).replaceAll("${prep} ${cmd}");

                preprocessed = separatorPreparationActionPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = separatorActionPreparationPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = beginPreparationActionPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");
                preprocessed = beginActionPreparationPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");

                preprocessed = separatorActionPattern.matcher(preprocessed).replaceAll(". ${act}");

                preprocessed = actionsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = preparationsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = parallelAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));

                return preprocessed;
            }
        };
    }

    public static RecipeCategoryAnalyzerConfiguration createRecipeCategoryAnalyzerConfiguration() {
        return new RecipeCategoryAnalyzerConfiguration()
                .addRecipeCategoryByPrincipal(UID_BASIC, "Básicos", "Básico")
                .addRecipeCategoryByPrincipal(
                        UID_START,
                        "Entrantes y ensaladas",
                        "Entradas y ensaladas",
                        "Entrante",
                        "Entrantes", // Check results without plurals and remove them if is the same... I think it will be
                        "Entrada",
                        "Entradas",
                        "Ensaladas",
                        "Ensalada")
                .addRecipeCategoryByPrincipal(UID_SOUP, "Sopas", "Sopa")
                .addRecipeCategoryByPrincipal(UID_PASTARICE, "Pastas y arroces", "Pasta y Arroz", "Pasta", "Pastas", "Arroz", "Arroces")
                .addRecipeCategoryByPrincipal(UID_MAINMEAT, "Carnes y aves", "Plato principal - Carnes y aves", "Plato principal - carnes y pollo", "Carne", "Carnes", "Pollo", "Ave", "Aves")
                .addRecipeCategoryByPrincipal(UID_MAINFISH, "Pescados y mariscos", "Plato principal - Pescados y mariscos", "Pescados", "Pescado", "Mariscos", "Marisco")
                .addRecipeCategoryByPrincipal(UID_MAINVEG, "Vegetarianos", "Plato principal - Vegetarianos", "Platos principales vegetarianos", "Vegetarianos", "Vegetariano")
                .addRecipeCategoryByPrincipal(UID_MAINOTHER, "Otros principales", "Otros platos principales", "Plato principal", "Plato prinicipal", "Plato principal - otros")
                .addRecipeCategoryByPrincipal(UID_SIDE, "Acompañamientos", "Guarniciones")
                .addRecipeCategoryByPrincipal(UID_BAKE, "Panadería y bollería", "Pan y bollería", "Masas", "Horneados", "Pan", "Bizcocho", "Bizcochos", "Galleta", "Galletas", "Panaderia", "Bolleria")
                .addRecipeCategoryByPrincipal(
                        UID_BAKE_SAVORY,
                        "Masas saladas",
                        "Masas saladas (quiches, pizzas, empanadas)",
                        "Empanadas, quiches y pizzas",
                        "Horneados salados",
                        "Quiche",
                        "Pizza",
                        "Pizzas",
                        "Pizza y Focaccia",
                        "Pizza y Focaccias",
                        "Focaccia",
                        "Empanada",
                        "Empanadas")
                .addRecipeCategoryByPrincipal(UID_DESSERT, "Postres y dulces", "Postres", "Postre")
                .addRecipeCategoryByPrincipal(UID_BAKE_SWEET, "Repostería", "Horneados dulces")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SWEET, "Salsas y cremas dulces, mermeladas", "Salsas y cremas dulces, mermeladas.", "Salsas, dips y untables - dulces", "Mermeladas", "Mermelada", "Confituras", "Confitura")
                .addRecipeCategoryByPrincipal(UID_DRINK, "Bebidas")
                .addRecipeCategoryByPrincipal(UID_SNACK, "Entrantes y aperitivos", "Botanas y canapés", "Canapés", "Snacks", "Snack", "Fiesta", "Snacks y picoteo", "Refrigerios")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SAVO, "Salsas saladas, dips y patés", "Salsas, dips y untables - salados", "Salsas, dips y untables", "Dips", "Pates", "Pate")
                .addRecipeCategoryByPrincipal(UID_BABY, "Alimentación infantil", "Alimentación para bebés", "Infantil", "Bebés", "Bebé")
                .addRecipeCategoryByPrincipal(UID_BREAK, "Desayuno")
                .addRecipeCategoryByPrincipal(UID_MENU, "Menús", "Menús y más")
                .setRecipeCategoryNamePreprocessor(
                        new Function<>() {
                            private final Pattern cleanupPattern =
                                    compile(
                                            "(?i)"
                                                    + String.join(
                                                            "|",
                                                            "^Al ",
                                                            "^Alimentación ",
                                                            "^Con ",
                                                            "^Cena ",
                                                            "^Calentado ",
                                                            "Caliente",
                                                            "^Cocin(?:a|ar|ado|ando) (?:al|para)?",
                                                            "^Comida(s)? ",
                                                            "^Contiene ",
                                                            "^De ",
                                                            "^Del ",
                                                            "^El ",
                                                            "^En ",
                                                            "^La ",
                                                            "^Para (?:la|el)?",
                                                            "^Platos de ",
                                                            "^Platillo(s)? ",
                                                            "^Receta(s)? ",
                                                            "Sabroso",
                                                            ".*Thermom.*"));

                            @Override
                            public String apply(String s) {
                                return FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }

    public static RecipeCommandAnalyzerConfiguration createRecipeCommandAnalyzerConfiguration() {
        return new RecipeCommandAnalyzerConfiguration()
                .setCommandProgramPredicates(
                        toMap(
                                DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                                FERMENT, stringPredicatePatterns("(?i)fermentar"),
                                HEAT_ONLY, stringPredicatePatterns("(?i)calentar"),
                                SOUS_VIDE, stringPredicatePatterns("(?i)Al vacio"),
                                TURBO, stringPredicatePatterns("(?i)turbo")))
                .setCommandDurationPredicates(toMap(RecipeCommandAnalyzerConfiguration.HALF_SECOND, stringPredicatePatterns("0\\.5 seg")))
                .setCommandDurationHoursPattern(compile("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(compile("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(compile("^([0-9]+) seg"))
                .setCommandTemperaturePredicates(toMap(STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)vapor|steam|varoma")))
                .setCommandTemperaturePattern(compile("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)vel " + TM_SLOW + "|vel slow|vel cuchara|vel lento")))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse|reversa"))
                .setCommandSpeedPattern(compile("vel ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(compile("^([0-9]+) (?:veces|vez)"))
                .setCommandRepetitionFromToPattern(compile("^([0-9]+)-([0-9]+) veces"));
    }

    public static RecipeCustomizationConfiguration createRecipeCustomizationConfiguration() {
        return new RecipeCustomizationConfiguration()
                .setMeasuringLidAndCupBody("Coloque la tapa y el cubilete")
                .setBackgroundReportOriginalTitleLabel("Titulo original")
                .setBackgroundReportAuthorLabel("Autor")
                .setBackgroundReportCollectionsLabel("Colecciones")
                .setBackgroundReportSourceLabel("Origen")
                .setRecipeAdditionUnclearStepSubtitle("<b>Atención!</b> Verifique que...")
                .setRecipeUnclearTitle("<b>ATENCION!</b> Receta dudosa!")
                .setRecipeUnclearBody(
                        "Algunos ingredientes no han sido encontrados en las instrucciones.<br>"
                                + "<b>Verifique manualmente!</b> cuando deba poner ingredientes en la receta.<br>"
                                + "<i>Corrija la receta si es posible!</i><br><br>"
                                + "<b>Ingredientes dudosos:</b>")
                .setRecipeDifficultDefinitionPredicates(
                        toMap(
                                RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)fácil"),
                                RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)medio"),
                                RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)avanzado")))
                .setRecipePriceDefinitionPredicates(toMap(RecipePriceDefinition.LOW, stringPredicatePattern("(?i)económico|barato")))
                .setInstructionParallelFormatFunction(s -> StringUtils.capitalize(replaceAll(s, PARALLEL_TEXT_PATTERN, "")));
    }

    public static RecipeIngredientAnalyzerConfiguration createRecipeIngredientAnalyzerConfiguration() {
        return new RecipeIngredientAnalyzerConfiguration("o bien")
                .setIngredientPreparationTokens(COOKI_ES_PREPARATION_TOKENS)
                .setIngredientOptionalIndicatorTokens(Set.of("(opcional)"))
                .setIngredientForbiddenRegexes(toStream(COOKI_ES_ACTION_REGEXES).collect(Collectors.toSet()));
    }

    public static RecipeInstructionAnalyzerConfiguration createRecipeInstructionAnalyzerConfiguration() {
        return new RecipeInstructionAnalyzerConfiguration(
                        toStream(
                                        toNext(".", ""), // REPLACE ALL with simple regex split implementation "(?<!\\bapprox|\\benv)\\."
                                        toNext(" aprox.", DO_NOT_SPLIT) // Careful. Due to limitationa all need to start from space or punctuation
                                        )
                                .toArray(RecipeInstructionSplitter.Replacement[]::new),
                        toMapP(
                                Pair.of(DO_ADD, toRecipeInstructionMatcher("(?i)añada|agregue|incorpore|espolvoree")),
                                Pair.of(DO_PLACE, toRecipeInstructionMatcher("(?i)coloque|ponga|vierta|introduzca")), // |inserte|transfiera
                                Pair.of(DO_READD, toRecipeInstructionMatcher("(?i)regrese|retorne")),
                                Pair.of(DO_WEIGH, toRecipeInstructionMatcher("(?i)^pese ")),
                                Pair.of(DO_REMOVE, toRecipeInstructionMatcher("(?i)extraiga|retire|saque|vacíe")),
                                Pair.of(DO_CLEAN, toRecipeInstructionMatcher("(?i)lave|limpie|aclare|enjuague")),
                                Pair.of(DO_PREHEAT, toRecipeInstructionMatcher("(?i)\\bprecaliente\\b")),
                                Pair.of(FOCUS_MAIN, toRecipeInstructionMatcher("(?i) en el vaso| al vaso| el vaso (?:el|la|los|las|un|uno|unos|una|unas) |sin lavar el vaso")), // Sometimes Ponga el vaso (without en el vaso)...
                                Pair.of(FOCUS_ON_TOP, toRecipeInstructionMatcher("(?i)sobre la tapa")),
                                Pair.of(FOCUS_EXTERNAL, toRecipeInstructionMatcher("(?i)" + REGEX_FOCUS_EXTERNAL_CONTAINER)),
                                Pair.of(FOCUS_UNDEFINED, toRecipeInstructionMatcher("(?i)^reserve$")),
                                Pair.of(INGREDIENTS_ALL, toRecipeInstructionMatcher("(?i)todos los ingredientes")),
                                Pair.of(INGREDIENTS_ALL_REMAINING, toRecipeInstructionMatcher("(?i)los ingredientes restantes")),
                                Pair.of(IS_USE_RESERVED, toRecipeInstructionMatcher("(?i)reservado")),
                                Pair.of(SEQUENCE_PARALLEL, toRecipeInstructionMatcher("(?i)" + REGEX_PARALLEL)),
                                Pair.of(DO_SERVE, toRecipeInstructionMatcher("(?i)disfrute|sírvalo|sirva|antes de servir|utilice a su gusto"))),
                        compile("[ ]?(?:,|\\by\\b)[ ]?"))
                .setInstructionPreprocessor(createCookiEsInstructionPreprocessor())
                .setRecipeInstructionTransferNexus(compile(REGEX_ITEM_TRANSFER_NEXUS));
    }

    public static RecipeLangAnalyzerConfiguration createRecipeLangAnalyzerConfiguration() {
        return new RecipeLangAnalyzerConfiguration(
                        Set.of(
                                "algún",
                                "alguna",
                                "algunas",
                                "alguno",
                                "algunos",
                                "ambos",
                                "ampleamos",
                                "ante",
                                "antes",
                                "aquel",
                                "aquellas",
                                "aquellos",
                                "aqui",
                                "arriba",
                                "atras",
                                "bajo",
                                "bastante",
                                "bien",
                                "cada",
                                "cierta",
                                "ciertas",
                                "cierto",
                                "ciertos",
                                "como",
                                "con",
                                "conseguimos",
                                "conseguir",
                                "consigo",
                                "consigue",
                                "consiguen",
                                "consigues",
                                "cual",
                                "cuando",
                                "dentro",
                                "desde",
                                "donde",
                                "dos",
                                "el",
                                "ellas",
                                "ellos",
                                "empleais",
                                "emplean",
                                "emplear",
                                "empleas",
                                "empleo",
                                "en",
                                "encima",
                                "entonces",
                                "entre",
                                "era",
                                "eramos",
                                "eran",
                                "eras",
                                "eres",
                                "es",
                                "esta",
                                "estaba",
                                "estado",
                                "estais",
                                "estamos",
                                "estan",
                                "estoy",
                                "fin",
                                "fue",
                                "fueron",
                                "fui",
                                "fuimos",
                                "gueno",
                                "ha",
                                "hace",
                                "haceis",
                                "hacemos",
                                "hacen",
                                "hacer",
                                "haces",
                                "hago",
                                "incluso",
                                "intenta",
                                "intentais",
                                "intentamos",
                                "intentan",
                                "intentar",
                                "intentas",
                                "intento",
                                "ir",
                                "la",
                                "largo",
                                "las",
                                "lo",
                                "los",
                                "mientras",
                                "mio",
                                "modo",
                                "muchos",
                                "muy",
                                "nos",
                                "nosotros",
                                "otro",
                                "para",
                                "pero",
                                "podeis",
                                "podemos",
                                "poder",
                                "podria",
                                "podriais",
                                "podriamos",
                                "podrian",
                                "podrias",
                                "por",
                                "por qué",
                                "porque",
                                "primero",
                                "puede",
                                "pueden",
                                "puedo",
                                "quien",
                                "sabe",
                                "sabeis",
                                "sabemos",
                                "saben",
                                "saber",
                                "sabes",
                                "ser",
                                "si",
                                "siendo",
                                //                "sin",  // Its meaningful in the context of categories. Ingredients already deal with it
                                "sobre",
                                "sois",
                                "solamente",
                                "solo",
                                "somos",
                                "soy",
                                "su",
                                "sus",
                                "también",
                                "teneis",
                                "tenemos",
                                "tener",
                                "tengo",
                                "tiempo",
                                "tiene",
                                "tienen",
                                "todo",
                                "trabaja",
                                "trabajais",
                                "trabajamos",
                                "trabajan",
                                "trabajar",
                                "trabajas",
                                "trabajo",
                                "tras",
                                "tuyo",
                                "ultimo",
                                "un",
                                "una",
                                "unas",
                                "uno",
                                "unos",
                                "usa",
                                "usais",
                                "usamos",
                                "usan",
                                "usar",
                                "usas",
                                "uso",
                                "va",
                                "vais",
                                "valor",
                                "vamos",
                                "van",
                                "vaya",
                                "verdad",
                                "verdadera",
                                "verdadero",
                                "vosotras",
                                "vosotros",
                                "voy",
                                "yo",
                                "él",
                                "ésta",
                                "éstas",
                                "éste",
                                "éstos",
                                "última",
                                "últimas",
                                "último",
                                "últimos",
                                "a",
                                "añadió",
                                "aún",
                                "actualmente",
                                "adelante",
                                "además",
                                "afirmó",
                                "agregó",
                                "ahí",
                                "ahora",
                                "al",
                                "algo",
                                "alrededor",
                                "anterior",
                                "apenas",
                                "aproximadamente",
                                "aquí",
                                "así",
                                "aseguró",
                                "aunque",
                                "ayer",
                                "buen",
                                "buena",
                                "buenas",
                                "bueno",
                                "buenos",
                                "cómo",
                                "casi",
                                "cerca",
                                "cinco",
                                "comentó",
                                "conocer",
                                "consideró",
                                "considera",
                                "contra",
                                "cosas",
                                "creo",
                                "cuales",
                                "cualquier",
                                "cuanto",
                                "cuatro",
                                "cuenta",
                                "da",
                                "dado",
                                "dan",
                                "dar",
                                "de",
                                "debe",
                                "deben",
                                "debido",
                                "decir",
                                "dejó",
                                "del",
                                "demás",
                                "después",
                                "dice",
                                "dicen",
                                "dicho",
                                "dieron",
                                "diferente",
                                "diferentes",
                                "dijeron",
                                "dijo",
                                "dio",
                                "durante",
                                "e",
                                "ejemplo",
                                "ella",
                                "ello",
                                "embargo",
                                "encuentra",
                                "esa",
                                "esas",
                                "ese",
                                "eso",
                                "esos",
                                "está",
                                "están",
                                "estaban",
                                "estar",
                                "estará",
                                "estas",
                                "este",
                                "esto",
                                "estos",
                                "estuvo",
                                "ex",
                                "existe",
                                "existen",
                                "explicó",
                                "expresó",
                                "fuera",
                                "gran",
                                "grandes",
                                "había",
                                "habían",
                                "haber",
                                "habrá",
                                "hacerlo",
                                "hacia",
                                "haciendo",
                                "han",
                                "hasta",
                                "hay",
                                "haya",
                                "he",
                                "hecho",
                                "hemos",
                                "hicieron",
                                "hizo",
                                "hoy",
                                "hubo",
                                "igual",
                                "indicó",
                                "informó",
                                "junto",
                                "lado",
                                "le",
                                "les",
                                "llegó",
                                "lleva",
                                "llevar",
                                "luego",
                                "lugar",
                                "más",
                                "manera",
                                "manifestó",
                                "mayor",
                                "me",
                                "mediante",
                                "mejor",
                                "mencionó",
                                "menos",
                                "mi",
                                "misma",
                                "mismas",
                                "mismo",
                                "mismos",
                                "momento",
                                "mucha",
                                "muchas",
                                "mucho",
                                "nada",
                                "nadie",
                                "ni",
                                "ningún",
                                "ninguna",
                                "ningunas",
                                "ninguno",
                                "ningunos",
                                "no",
                                "nosotras",
                                "nuestra",
                                "nuestras",
                                "nuestro",
                                "nuestros",
                                "nueva",
                                "nuevas",
                                "nuevo",
                                "nuevos",
                                "nunca",
                                "o",
                                "ocho",
                                "otra",
                                "otras",
                                "otros",
                                "parece",
                                "parte",
                                "partir",
                                "pasada",
                                "pasado",
                                "pesar",
                                "poca",
                                "pocas",
                                "poco",
                                "pocos",
                                "podrá",
                                "podrán",
                                "podría",
                                "podrían",
                                "poner",
                                "posible",
                                "próximo",
                                "próximos",
                                "primer",
                                "primera",
                                "primeros",
                                "principalmente",
                                "propia",
                                "propias",
                                "propio",
                                "propios",
                                "pudo",
                                "pueda",
                                "pues",
                                "qué",
                                "que",
                                "quedó",
                                "queremos",
                                "quién",
                                "quienes",
                                "quiere",
                                "realizó",
                                "realizado",
                                "realizar",
                                "respecto",
                                "sí",
                                "sólo",
                                "se",
                                "señaló",
                                "sea",
                                "sean",
                                "según",
                                "segunda",
                                "segundo",
                                "seis",
                                "será",
                                "serán",
                                "sería",
                                "sido",
                                "siempre",
                                "siete",
                                "sigue",
                                "siguiente",
                                "sino",
                                "sola",
                                "solas",
                                "solos",
                                "son",
                                "tal",
                                "tampoco",
                                "tan",
                                "tanto",
                                "tenía",
                                "tendrá",
                                "tendrán",
                                "tenga",
                                "tenido",
                                "tercera",
                                "toda",
                                "todas",
                                "todavía",
                                "todos",
                                "total",
                                "trata",
                                "través",
                                "tres",
                                "tuvo",
                                "usted",
                                "varias",
                                "varios",
                                "veces",
                                "ver",
                                "vez",
                                "y",
                                "ya"))
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "r$", "s$", "(?<=a)ndo$", "(?<=a)do$"));

                            @Override
                            public String apply(String s) {
                                String apply = SPANISH_21_TOKEN_CONVERTER.apply(s);
                                if (apply.length() <= RecipeLangAnalyzerConfiguration.TOKEN_LENGTH_MIN) {
                                    return apply;
                                } else {
                                    return removeAll(apply, cleanupPattern);
                                }
                            }
                        });
    }

    public static RecipeMagnitudeAnalyzerConfiguration createRecipeMagnitudeAnalyzerConfiguration() {
        return new RecipeMagnitudeAnalyzerConfiguration(
                toMap(
                        "un", ONE,
                        "uno", ONE,
                        "una", ONE,
                        "dos", TWO,
                        "tres", THREE,
                        "cuatro", FOUR,
                        "cinco", FIVE,
                        "seis", SIX,
                        "siete", SEVEN,
                        "ocho", EIGHT,
                        "nueve", NINE,
                        "diez", TEN));
    }

    public static RecipeMeasurementAnalyzerConfiguration createRecipeMeasurementAnalyzerConfiguration() {
        return new RecipeMeasurementAnalyzerConfiguration(UNIT_PIECE_ES, UNIT_GRAM)
                .setMeasurementUnitSeparatorTokens(Set.of("de")) // 300 g [de] manzanas
                .setMeasurementUnitDefinitions(
                        UNIT_PIECE_ES,
                        new MeasurementUnitDefinition("diente", UNIT_PIECE_ES, false, "dientes"),
                        new MeasurementUnitDefinition("lata", UNIT_PIECE_ES, false, "latas"),
                        new MeasurementUnitDefinition("ramita", UNIT_PIECE_ES, false, "ramitas"),
                        //
                        new MeasurementUnitDefinition("pellizco", UNIT_PINCH, false, "pellizcos", "punta de cuchillo"),
                        //
                        new MeasurementUnitDefinition("cda", UNIT_TBSP, true, "cdas", "cucharada", "cucharadas"),
                        new MeasurementUnitDefinition("cda (rasa)", UNIT_TBSP, true, "cda rasa", "cdas rasas", "cucharada rasa", "cucharadas rasas"),
                        UNIT_TSP_ES,
                        new MeasurementUnitDefinition("cdta (rasa)", UNIT_TSP_ES, true, "cdta rasa", "cdtas", "cucharadita rasa", "cucharaditas rasas"),
                        //
                        UNIT_GRAM,
                        UNIT_KG,
                        UNIT_LITER,
                        UNIT_MILILITER);
    }

    public static RecipeToolAnalyzerConfiguration createRecipeToolAnalyzerConfiguration() {
        return new RecipeToolAnalyzerConfiguration(
                RECIPE_TOOL_MATCHERS,
                toMapP(
                        // Utensils. Complete!!
                        Pair.of(OVEN_OPERATION, toRecipeInstructionMatcher("(?i)precaliente el horno|^hornee")),
                        Pair.of(REFRIGERATE, toRecipeInstructionMatcher("(?i)(?:(?:reserve|deje reposar).*en.*(?:frigo|frigorífico|nevera|refrigerador)|refrigere)")),
                        Pair.of(FREEZE, toRecipeInstructionMatcher("(?i)(?:(?:reserve|conserve|coloque).*en|lleve.*al).*congelador")),
                        Pair.of(SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatcher("(?i)(?:inserte|introduzca).*cestillo")),
                        Pair.of(SIMMERING_BASKET_REMOVE, toRecipeInstructionMatcher("(?i)(?:extraiga|retire|extraer).*cestillo")),
                        Pair.of(SIMMERING_BASKET_OTHER, toRecipeInstructionMatcher("(?i)(?:escurra|vierta).*cestillo")),
                        Pair.of(SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatcher("(?i)(?:cestillo.*tapa.*cubilete|(?:en lugar).*cubilete.*cestillo)")),
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

                        //                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, defineMe()),
                        //                        Pair.of(RecipeAnalysisTag.MIXING_BOWL_REMOVE, defineMe()),
                        Pair.of(MIXING_BOWL_PUT, toRecipeInstructionMatcher("(?i)coloque el vaso")),
                        Pair.of(MIXING_BOWL_EMPTY, toRecipeInstructionMatcher("(?i)(?:vacíe).*el vaso")),
                        Pair.of(MIXING_BOWL_CLEAN, toRecipeInstructionMatcher("(?i)(?:lave|limpie|aclare|enjuague).*el vaso")),
                        Pair.of(TRANSFER_FROM_BOWL, toRecipeInstructionMatcher("(?i)(?:transfiera|retire|saque|vierta).*(?:del vaso|" + REGEX_FOCUS_EXTERNAL_CONTAINER + ")")),

                        //                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(MEASURING_CUP_NOTUSE, toRecipeInstructionMatcher(REGEX_SIN_CUBILETE)),
                        Pair.of(MEASURING_CUP_PUT, toRecipeInstructionMatcher("(?i)ponga.*cubilete")),
                        Pair.of(MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatcher("(?i)sujete.*cubilete")),

                        //                        Pair.of(RecipeAnalysisTag.WHISK, defineMe()),
                        Pair.of(WHISK_PUT, toRecipeInstructionMatcher("(?i)coloque la mariposa")),
                        Pair.of(WHISK_REMOVE, toRecipeInstructionMatcher("(?i)(?:retire|quite).*mariposa")),

                        //                        Pair.of(RecipeAnalysisTag.SPATULA, defineMe()),
                        Pair.of(SPATULA_MIX, toRecipeInstructionMatcher("(?i)(?:mezcle|remueva|termine de mezclar)(?!(.*bien)).*(?:espátula)")),
                        Pair.of(SPATULA_SCRAP_SIDE, toRecipeInstructionMatcher("(?i)espátula.*baje.*fondo.*vaso.*|baje.*restos.*(vaso|pared).*espátula")),
                        Pair.of(SPATULA_MIX_WELL, toRecipeInstructionMatcher("(?i)(?:mezcle|remueva|termine de mezclar).*(?:bien).*espátula")),
                        Pair.of(STEAMER_CLOSE, toRecipeInstructionMatcher("(?i)Tape.*(?:vaporera|varoma)")),
                        Pair.of(STEAMER_PUT, toRecipeInstructionMatcher("(?i)(?:Coloque|Sitúe).*(?:vaporera|varoma).*posición")),
                        Pair.of(STEAMER_REMOVE, toRecipeInstructionMatcher("(?i)Retire.*(?:vaporera|varoma)"))
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, defineMe())),
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, defineMe())),
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_PUT, defineMe())),
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_DISH_REMOVE, defineMe())),

                        //                        Pair.of(RecipeAnalysisTag.OTHER_OPERATION, defineMe())),

                        //                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe())),
                        //                                Pair.of(KITCHEN_EQUIPMENT, toRecipeInstructionMatcher())
                        ));
    }
}
