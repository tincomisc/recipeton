package com.recipeton.shared.analysis.configuration.cooki;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.*;
import com.recipeton.shared.analysis.domain.MeasurementUnitDefinition;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatcher;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.analysis.domain.RecipeToolDefinition;
import com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeCategoryAnalyzerConfiguration.FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST;
import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.*;
import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCommandProgram.*;
import static com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatcher;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement.toNext;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.List.of;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class RecipeAnalyzerCookiEnConfiguration extends RecipeAnalyzerConfiguration { // NOPMD Long

    public static final MeasurementUnitDefinition UNIT_PIECE_EN = new MeasurementUnitDefinition("piece", UNIT_PIECE, false, "pieces", "unit", "units");

    public static final String COOKI_EN_ACTION_DEFAULT = "set";

    public static final String[] COOKI_EN_ACTION_REGEXES = {
        "cream(?= \\[)", // "cream", // breaks and cream of tartar. Special
        "bake",
        "beat",
        "blend",
        "boil",
        "bring to a boil",
        "bring to the boil",
        "caramelize",
        "chop",
        "churn",
        "combine",
        "cook",
        "crush",
        "dissolve",
        "dry roast",
        "emulsify",
        "grate",
        "grind",
        "heat",
        "infuse",
        "knead",
        "mash",
        "melt",
        "mill",
        "mince",
        "mix",
        "poach",
        "proof",
        "pulse",
        "pulverise",
        "purée",
        "reheat",
        "repeat chopping",
        "roast",
        "roughly chop",
        "sauté",
        COOKI_EN_ACTION_DEFAULT, // set aside, set command
        "sift",
        "simmer",
        "steam",
        "stir",
        "toast",
        "transfer",
        "warm",
        "whip",
        "whisk",
        "zest",
        //
        "place",
        "insert",
        "remove",
        "add",
        "weigh",
        "refrigerate",
        "freeze",
        "pour",
        "clean",
        "secure", // steamer lid
        "line", // and line...
        "placing", // Special. Will need context relation analysis to complete. Generalize PARALLEL case by adding specific tag (QUALIFIES_PREVIOUS + PARALLEL, QUALIFIES_PREVIOUS + TOOL_ADJUST) on regex and with forward lookup in importer

        //                        "insert measuring cup",
        //                        "remove simmering basket"

        // Experiment
    };

    public static final Set<String> COOKI_EN_INGREDIENT_PREPARATION_TOKENS =
            Set.of(
                    "(whipping)",
                    "beaten",
                    "dry-roasted",
                    "canned",
                    "deseeded",
                    "cut",
                    "cutted",
                    "chilled",
                    "diced",
                    "dried",
                    "halved",
                    "quartered",
                    "dry",
                    "large",
                    "small",
                    "drained",
                    "filtered",
                    "squeezed",
                    "fresh",
                    "freshly",
                    "frozen",
                    "ground",
                    "of",
                    "peeled",
                    "pitted",
                    "pouring",
                    "raw",
                    "salted",
                    "skimmed",
                    "sliced",
                    "slivered",
                    "smoked",
                    "smooth",
                    "stripped",
                    "trimmed",
                    "toasted",
                    "thawed",
                    "unhulled",
                    "unsalted",
                    "unsweetened",
                    "whipping",
                    "whole");

    public static final Set<String> COOKI_EN_STOPWORDS =
            Set.of(
                    "able",
                    "about",
                    "above",
                    "abroad",
                    "according",
                    "accordingly",
                    "across",
                    "actually",
                    "adj",
                    "after",
                    "afterwards",
                    "again",
                    "against",
                    "ago",
                    "ahead",
                    "ain't",
                    "all",
                    "allow",
                    "allows",
                    "almost",
                    "alone",
                    "along",
                    "alongside",
                    "already",
                    "also",
                    "although",
                    "always",
                    "am",
                    "amid",
                    "amidst",
                    "among",
                    "amongst",
                    "an",
                    "and",
                    "another",
                    "any",
                    "anybody",
                    "anyhow",
                    "anyone",
                    "anything",
                    "anyway",
                    "anyways",
                    "anywhere",
                    "apart",
                    "appear",
                    "appreciate",
                    "appropriate",
                    "are",
                    "aren't",
                    "around",
                    "as",
                    "a's",
                    "aside",
                    "ask",
                    "asking",
                    "associated",
                    "at",
                    "available",
                    "away",
                    "awfully",
                    "back",
                    "backward",
                    "backwards",
                    "be",
                    "became",
                    "because",
                    "become",
                    "becomes",
                    "becoming",
                    "been",
                    "before",
                    "beforehand",
                    "begin",
                    "behind",
                    "being",
                    "believe",
                    "below",
                    "beside",
                    "besides",
                    "best",
                    "better",
                    "between",
                    "beyond",
                    "both",
                    "brief",
                    "but",
                    "by",
                    "came",
                    "can",
                    "cannot",
                    "cant",
                    "can't",
                    "caption",
                    "cause",
                    "causes",
                    "certain",
                    "certainly",
                    "changes",
                    "clearly",
                    "c'mon",
                    "co",
                    "co.",
                    "com",
                    "come",
                    "comes",
                    "concerning",
                    "consequently",
                    "consider",
                    "considering",
                    "contain",
                    "containing",
                    "contains",
                    "corresponding",
                    "could",
                    "couldn't",
                    "course",
                    "c's",
                    "currently",
                    "dare",
                    "daren't",
                    "definitely",
                    "described",
                    "despite",
                    "did",
                    "didn't",
                    "different",
                    "directly",
                    "do",
                    "does",
                    "doesn't",
                    "doing",
                    "done",
                    "don't",
                    "down",
                    "downwards",
                    "during",
                    "each",
                    "edu",
                    "eg",
                    "eight",
                    "eighty",
                    "either",
                    "else",
                    "elsewhere",
                    "end",
                    "ending",
                    "enough",
                    "entirely",
                    "especially",
                    "et",
                    "etc",
                    "even",
                    "ever",
                    "evermore",
                    "every",
                    "everybody",
                    "everyone",
                    "everything",
                    "everywhere",
                    "ex",
                    "exactly",
                    "example",
                    "except",
                    "fairly",
                    "far",
                    "farther",
                    "few",
                    "fewer",
                    "fifth",
                    "first",
                    "five",
                    "followed",
                    "following",
                    "follows",
                    "for",
                    "forever",
                    "former",
                    "formerly",
                    "forth",
                    "forward",
                    "found",
                    "four",
                    "from",
                    "further",
                    "furthermore",
                    "get",
                    "gets",
                    "getting",
                    "given",
                    "gives",
                    "go",
                    "goes",
                    "going",
                    "gone",
                    "got",
                    "gotten",
                    "greetings",
                    "had",
                    "hadn't",
                    "half",
                    "happens",
                    "hardly",
                    "has",
                    "hasn't",
                    "have",
                    "haven't",
                    "having",
                    "he",
                    "he'd",
                    "he'll",
                    "hello",
                    "help",
                    "hence",
                    "her",
                    "here",
                    "hereafter",
                    "hereby",
                    "herein",
                    "here's",
                    "hereupon",
                    "hers",
                    "herself",
                    "he's",
                    "hi",
                    "him",
                    "himself",
                    "his",
                    "hither",
                    "hopefully",
                    "how",
                    "howbeit",
                    "however",
                    "hundred",
                    "i'd",
                    "ie",
                    "if",
                    "ignored",
                    "i'll",
                    "i'm",
                    "immediate",
                    "in",
                    "inasmuch",
                    "inc",
                    "inc.",
                    "indeed",
                    "indicate",
                    "indicated",
                    "indicates",
                    "inner",
                    "inside",
                    "insofar",
                    "instead",
                    "into",
                    "inward",
                    "is",
                    "isn't",
                    "it",
                    "it'd",
                    "it'll",
                    "its",
                    "it's",
                    "itself",
                    "i've",
                    "just",
                    "k",
                    "keep",
                    "keeps",
                    "kept",
                    "know",
                    "known",
                    "knows",
                    "last",
                    "lately",
                    "later",
                    "latter",
                    "latterly",
                    "least",
                    "less",
                    "lest",
                    "let",
                    "let's",
                    "like",
                    "liked",
                    "likely",
                    "likewise",
                    "little",
                    "look",
                    "looking",
                    "looks",
                    "low",
                    "lower",
                    "ltd",
                    "made",
                    "mainly",
                    "make",
                    "makes",
                    "many",
                    "may",
                    "maybe",
                    "mayn't",
                    "me",
                    "mean",
                    "meantime",
                    "meanwhile",
                    "merely",
                    "might",
                    "mightn't",
                    "mine",
                    "minus",
                    "miss",
                    "more",
                    "moreover",
                    "most",
                    "mostly",
                    "mr",
                    "mrs",
                    "much",
                    "must",
                    "mustn't",
                    "my",
                    "myself",
                    "name",
                    "namely",
                    "nd",
                    "near",
                    "nearly",
                    "necessary",
                    "need",
                    "needn't",
                    "needs",
                    "neither",
                    "never",
                    "neverf",
                    "neverless",
                    "nevertheless",
                    "new",
                    "next",
                    "nine",
                    "ninety",
                    "no",
                    "nobody",
                    "non",
                    "none",
                    "nonetheless",
                    "noone",
                    "no-one",
                    "nor",
                    "normally",
                    "not",
                    "nothing",
                    "notwithstanding",
                    "novel",
                    "now",
                    "nowhere",
                    "obviously",
                    "of",
                    "off",
                    "often",
                    "oh",
                    "ok",
                    "okay",
                    "old",
                    "on",
                    "once",
                    "one",
                    "ones",
                    "one's",
                    "only",
                    "onto",
                    "opposite",
                    "or",
                    "other",
                    "others",
                    "otherwise",
                    "ought",
                    "oughtn't",
                    "our",
                    "ours",
                    "ourselves",
                    "out",
                    "outside",
                    "over",
                    "overall",
                    "own",
                    "particular",
                    "particularly",
                    "past",
                    "per",
                    "perhaps",
                    "placed",
                    "please",
                    "plus",
                    "possible",
                    "presumably",
                    "probably",
                    "provided",
                    "provides",
                    "que",
                    "quite",
                    "qv",
                    "rather",
                    "rd",
                    "re",
                    "really",
                    "reasonably",
                    "recent",
                    "recently",
                    "regarding",
                    "regardless",
                    "regards",
                    "relatively",
                    "respectively",
                    "right",
                    "round",
                    "said",
                    "same",
                    "saw",
                    "say",
                    "saying",
                    "says",
                    "second",
                    "secondly",
                    "see",
                    "seeing",
                    "seem",
                    "seemed",
                    "seeming",
                    "seems",
                    "seen",
                    "self",
                    "selves",
                    "sensible",
                    "sent",
                    "serious",
                    "seriously",
                    "seven",
                    "several",
                    "shall",
                    "shan't",
                    "she",
                    "she'd",
                    "she'll",
                    "she's",
                    "should",
                    "shouldn't",
                    "since",
                    "six",
                    "so",
                    "some",
                    "somebody",
                    "someday",
                    "somehow",
                    "someone",
                    "something",
                    "sometime",
                    "sometimes",
                    "somewhat",
                    "somewhere",
                    "soon",
                    "sorry",
                    "specified",
                    "specify",
                    "specifying",
                    "still",
                    "sub",
                    "such",
                    "sup",
                    "sure",
                    "take",
                    "taken",
                    "taking",
                    "tell",
                    "tends",
                    "th",
                    "than",
                    "thank",
                    "thanks",
                    "thanx",
                    "that",
                    "that'll",
                    "thats",
                    "that's",
                    "that've",
                    "the",
                    "their",
                    "theirs",
                    "them",
                    "themselves",
                    "then",
                    "thence",
                    "there",
                    "thereafter",
                    "thereby",
                    "there'd",
                    "therefore",
                    "therein",
                    "there'll",
                    "there're",
                    "theres",
                    "there's",
                    "thereupon",
                    "there've",
                    "these",
                    "they",
                    "they'd",
                    "they'll",
                    "they're",
                    "they've",
                    "thing",
                    "things",
                    "think",
                    "third",
                    "thirty",
                    "this",
                    "thorough",
                    "thoroughly",
                    "those",
                    "though",
                    "three",
                    "through",
                    "throughout",
                    "thru",
                    "thus",
                    "till",
                    "to",
                    "together",
                    "too",
                    "took",
                    "toward",
                    "towards",
                    "tried",
                    "tries",
                    "truly",
                    "try",
                    "trying",
                    "t's",
                    "twice",
                    "two",
                    "un",
                    "under",
                    "underneath",
                    "undoing",
                    "unfortunately",
                    "unless",
                    "unlike",
                    "unlikely",
                    "until",
                    "unto",
                    "up",
                    "upon",
                    "upwards",
                    "us",
                    "use",
                    "used",
                    "useful",
                    "uses",
                    "using",
                    "usually",
                    "v",
                    "value",
                    "various",
                    "versus",
                    "very",
                    "via",
                    "viz",
                    "vs",
                    "want",
                    "wants",
                    "was",
                    "wasn't",
                    "way",
                    "we",
                    "we'd",
                    "welcome",
                    "well",
                    "we'll",
                    "went",
                    "were",
                    "we're",
                    "weren't",
                    "we've",
                    "what",
                    "whatever",
                    "what'll",
                    "what's",
                    "what've",
                    "when",
                    "whence",
                    "whenever",
                    "where",
                    "whereafter",
                    "whereas",
                    "whereby",
                    "wherein",
                    "where's",
                    "whereupon",
                    "wherever",
                    "whether",
                    "which",
                    "whichever",
                    "while",
                    "whilst",
                    "whither",
                    "who",
                    "who'd",
                    "whoever",
                    "whole",
                    "who'll",
                    "whom",
                    "whomever",
                    "who's",
                    "whose",
                    "why",
                    "will",
                    "willing",
                    "wish",
                    "with",
                    "within",
                    "without",
                    "wonder",
                    "won't",
                    "would",
                    "wouldn't",
                    "yes",
                    "yet",
                    "you",
                    "you'd",
                    "you'll",
                    "your",
                    "you're",
                    "yours",
                    "yourself",
                    "yourselves",
                    "you've",
                    "zero",
                    "a",
                    "how's",
                    "i",
                    "when's",
                    "why's",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "j",
                    "l",
                    "m",
                    "n",
                    "o",
                    "p",
                    "q",
                    "r",
                    "s",
                    "t",
                    "u",
                    "uucp",
                    "w",
                    "x",
                    "y",
                    "z",
                    "I",
                    "www",
                    "amount",
                    "bill",
                    "bottom",
                    "call",
                    "computer",
                    "con",
                    "couldnt",
                    "cry",
                    "de",
                    "describe",
                    "detail",
                    "due",
                    "eleven",
                    "empty",
                    "fifteen",
                    "fifty",
                    "fill",
                    "find",
                    "fire",
                    "forty",
                    "front",
                    "full",
                    "give",
                    "hasnt",
                    "herse",
                    "himse",
                    "interest",
                    "itse”",
                    "mill",
                    "move",
                    "myse”",
                    "part",
                    "put",
                    "show",
                    "side",
                    "sincere",
                    "sixty",
                    "system",
                    "ten",
                    "thick",
                    "thin",
                    "top",
                    "twelve",
                    "twenty",
                    "abst",
                    "accordance",
                    "act",
                    "added",
                    "adopted",
                    "affected",
                    "affecting",
                    "affects",
                    "ah",
                    "announce",
                    "anymore",
                    "apparently",
                    "approximately",
                    "aren",
                    "arent",
                    "arise",
                    "auth",
                    "beginning",
                    "beginnings",
                    "begins",
                    "biol",
                    "briefly",
                    "ca",
                    "date",
                    "ed",
                    "effect",
                    "et-al",
                    "ff",
                    "fix",
                    "gave",
                    "giving",
                    "heres",
                    "hes",
                    "hid",
                    "home",
                    "id",
                    "im",
                    "immediately",
                    "importance",
                    "important",
                    "index",
                    "information",
                    "invention",
                    "itd",
                    "keys",
                    "kg",
                    "km",
                    "largely",
                    "lets",
                    "line",
                    "'ll",
                    "means",
                    "mg",
                    "million",
                    "ml",
                    "mug",
                    "na",
                    "nay",
                    "necessarily",
                    "nos",
                    "noted",
                    "obtain",
                    "obtained",
                    "omitted",
                    "ord",
                    "owing",
                    "page",
                    "pages",
                    "poorly",
                    "possibly",
                    "potentially",
                    "pp",
                    "predominantly",
                    "present",
                    "previously",
                    "primarily",
                    "promptly",
                    "proud",
                    "quickly",
                    "ran",
                    "readily",
                    "ref",
                    "refs",
                    "related",
                    "research",
                    "resulted",
                    "resulting",
                    "results",
                    "run",
                    "sec",
                    "section",
                    "shed",
                    "shes",
                    "showed",
                    "shown",
                    "showns",
                    "shows",
                    "significant",
                    "significantly",
                    "similar",
                    "similarly",
                    "slightly",
                    "somethan",
                    "specifically",
                    "state",
                    "states",
                    "stop",
                    "strongly",
                    "substantially",
                    "successfully",
                    "sufficiently",
                    "suggest",
                    "thered",
                    "thereof",
                    "therere",
                    "thereto",
                    "theyd",
                    "theyre",
                    "thou",
                    "thoughh",
                    "thousand",
                    "throug",
                    "til",
                    "tip",
                    "ts",
                    "ups",
                    "usefully",
                    "usefulness",
                    "'ve",
                    "vol",
                    "vols",
                    "wed",
                    "whats",
                    "wheres",
                    "whim",
                    "whod",
                    "whos",
                    "widely",
                    "words",
                    "world",
                    "youd",
                    "youre");

    private static final Map<RecipeToolDefinition, RecipeInstructionMatcher> RECIPE_TOOL_MATCHERS =
            toMapP(
                    Pair.of(new RecipeToolDefinition("mixing bowl", true, TOOL_MAIN), toRecipeInstructionMatcher("(?i)\\bmixing bowl\\b")),
                    Pair.of(new RecipeToolDefinition("simmering basket", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bsimmering basket\\b")),
                    Pair.of(new RecipeToolDefinition("varoma", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:varoma|steamer)(?! (?:dish|tray))\\b")),
                    Pair.of(new RecipeToolDefinition("varoma dish", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:varoma|steamer) dish\\b")),
                    Pair.of(new RecipeToolDefinition("varoma tray", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:varoma|steamer) tray\\b")),
                    Pair.of(new RecipeToolDefinition("measuring cup", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bmeasuring cup\\b")),
                    Pair.of(new RecipeToolDefinition("butterfly", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bbutterfly\\b")),
                    Pair.of(new RecipeToolDefinition("spatula", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bspatula\\b")),
                    Pair.of(new RecipeToolDefinition("oven", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\boven(?! (?:tray|sheet))\\b")),
                    Pair.of(new RecipeToolDefinition("fridge", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bfridge\\b")),
                    Pair.of(new RecipeToolDefinition("refrigerator", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\brefrigerator\\b")),
                    Pair.of(new RecipeToolDefinition("freezer", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bfreezer\\b")),
                    Pair.of(new RecipeToolDefinition("bowl", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:(?<!mixing )bowl[s]?)\\b")),
                    Pair.of(new RecipeToolDefinition("tablespoon", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:(?:heaped )?tablespoon[s]?)\\b")),
                    //            Pair.of(new RecipeToolDefinition("bowl", true, EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?:(?<!mixing )bowl[s]?)\\b")),
                    Pair.of(new RecipeToolDefinition("casserole", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcasserole[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("colander", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcolander[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("container", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcontainer[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("dish", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?<!(?:varoma|steamer) )dish(?:es)?\\b")),
                    Pair.of(new RecipeToolDefinition("jar", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bjar[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("jug", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bjug[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("paper liner", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpaper liner[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("pan", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpan[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("plate", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?<!grilling |serving )plate[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("plater", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplater[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("ramekin", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bramekin[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("sheet", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?<!oven )sheet[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("steaming rack", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bsteaming rack[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("tin", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\btin[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("tray", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\b(?<!(?:oven|varoma|steamer|baking) )tray[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("baking tray", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bbaking tray[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("oven tray", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\boven tray[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("oven sheet", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\boven sheet[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("surface", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bsurface\\b")),
                    Pair.of(new RecipeToolDefinition("serving plate", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bserving plate[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("grilling plate", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bgrilling plate[s]?\\b")));

    private static final String REGEX_TOOL_EXTERNAL_CONTAINER = RecipeToolAnalyzerConfiguration.getCombinedRegex(RECIPE_TOOL_MATCHERS.entrySet().stream().filter(e -> e.getKey().isContainer() && e.getKey().getTag().equals(TOOL_EXTERNAL)));
    private static final String REGEX_ITEM_TRANSFER_NEXUS = "\\b(?:into |onto |to |in |on )(?:a |an |the |many |one )?";
    //    private static final String REGEX_LOCATION_EXTERNAL_CONTAINER = "\\b(?:into |onto |to |in |on )(?:a |an |the )?(?:large |small |medium )?(?:prepared |reserved )?(?:" + REGEX_TOOL_EXTERNAL_CONTAINER + ")";
    private static final String REGEX_FOCUS_EXTERNAL_CONTAINER = REGEX_ITEM_TRANSFER_NEXUS + ".*(?:" + REGEX_TOOL_EXTERNAL_CONTAINER + ")";

    private static final String REGEX_PARALLEL = "meanwhile";
    private static final Pattern PARALLEL_TEXT_PATTERN = compile("(?i)" + REGEX_PARALLEL + "[,]? ");

    private static final String REGEX_WITHOUT_MEASURING_CUP = "(?:remove|without).*measuring cup";

    public RecipeAnalyzerCookiEnConfiguration() {
        super(
                "COOKI_EN",
                rd -> containsIgnoreCase(rd.getLocale(), "en") && containsIgnoreCase(rd.getSource(), "cooki"),
                createRecipeLangAnalyzerConfiguration(),
                createRecipeCategoryAnalyzerConfiguration(),
                createRecipeCommandAnalyzerConfiguration(),
                createRecipeMagnitudeAnalyzerConfiguration(),
                createRecipeMeasurementAnalyzerConfiguration(),
                createRecipeInstructionAnalyzerConfiguration(),
                createRecipeIngredientAnalyzerConfiguration(),
                createRecipeToolAnalyzerConfiguration(),
                createRecipeCustomizationConfiguration());
    }

    public static Function<String, String> createCookiEnInstructionPreprocessor() {
        return new Function<>() {
            private static final String SEPARATORS_BASE = "[,]? and then[,]? |[,]? then[,]? |[,]? and[,]? ";
            private static final String REGEX_ACTION_MODIFIERS = "(?<mod>(?:without setting a time|with aid of spatula|increasing speed gradually))";
            private static final String REGEX_ACTION_PREPARATIONS =
                    "(?<prep>(?:"
                            + REGEX_WITHOUT_MEASURING_CUP
                            + "|without varoma lid|with measuring cup in place|with simmering basket instead of measuring cup on top of mixing bowl lid|(?:placing|place|with) simmering basket (instead|in place) of measuring cup))";

            private final String regexSentenceSeparators = "(?<sep>" + SEPARATORS_BASE + "|(?<!" + REGEX_PARALLEL + "), )";

            private final String regexActions = "(?<act>" + String.join("|", COOKI_EN_ACTION_REGEXES) + ")";

            private final String regexParallel = "(?<par>" + REGEX_PARALLEL + ")";
            private final Pattern separatorParallelPattern = compile("(?i)" + regexSentenceSeparators + regexParallel);

            private final Pattern separatorCommandWithoutActionPattern = compile("(?i)" + "(?<sep>" + SEPARATORS_BASE + ")" + REGEX_COMMAND);
            private final Pattern beginCommandWithoutActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_COMMAND);

            private final Pattern commandPreparationPattern = compile("(?i)" + REGEX_COMMAND + "[,]? " + REGEX_ACTION_PREPARATIONS);

            private final Pattern separatorPreparationActionPattern = compile("(?i)" + regexSentenceSeparators + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern separatorActionPreparationPattern = compile("(?i)" + regexSentenceSeparators + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");
            private final Pattern beginPreparationActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern beginActionPreparationPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");

            private final Pattern separatorModifierActionPattern = compile("(?i)" + regexSentenceSeparators + REGEX_ACTION_MODIFIERS + "[,]? " + regexActions);
            private final Pattern separatorActionModifierPattern = compile("(?i)" + regexSentenceSeparators + regexActions + "[,]? " + REGEX_ACTION_MODIFIERS + "[,]?");
            private final Pattern beginModifierActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_ACTION_MODIFIERS + "[,]? " + regexActions);
            private final Pattern beginActionModifierPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + regexActions + "[,]? " + REGEX_ACTION_MODIFIERS + "[,]?");

            private final Pattern separatorActionPattern = compile("(?i)" + regexSentenceSeparators + regexActions);

            private final Pattern actionsAfterDotPattern = compile("(?i)(?<=\\. )" + regexActions);
            private final Pattern preparationsAfterDotPattern = compile("(?i)(?<=\\. )" + REGEX_ACTION_PREPARATIONS);
            private final Pattern regexActionModifiersPattern = compile("(?i)" + REGEX_ACTION_MODIFIERS);
            private final Pattern parallelAfterDotPattern = compile("(?i)(?<=\\. )" + regexParallel);

            @Override
            public String apply(String text) {
                String preprocessed = text;

                preprocessed = separatorParallelPattern.matcher(preprocessed).replaceAll(". ${par}");

                preprocessed = separatorCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_EN_ACTION_DEFAULT + " ${cmd}");
                preprocessed = beginCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_EN_ACTION_DEFAULT + " ${cmd}");

                preprocessed = commandPreparationPattern.matcher(preprocessed).replaceAll("${prep} ${cmd}");

                preprocessed = separatorPreparationActionPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = separatorActionPreparationPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = beginPreparationActionPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");
                preprocessed = beginActionPreparationPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");

                preprocessed = separatorModifierActionPattern.matcher(preprocessed).replaceAll(". ${act} ${mod}");
                preprocessed = separatorActionModifierPattern.matcher(preprocessed).replaceAll(". ${act} ${mod}");
                preprocessed = beginModifierActionPattern.matcher(preprocessed).replaceAll("${act} ${mod}");
                preprocessed = beginActionModifierPattern.matcher(preprocessed).replaceAll("${act} ${mod}");

                preprocessed = separatorActionPattern.matcher(preprocessed).replaceAll(". ${act}");

                preprocessed = regexActionModifiersPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.lowerCase(mr.group()));
                preprocessed = actionsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = preparationsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = parallelAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                return preprocessed;
            }
        };
    }

    public static RecipeCategoryAnalyzerConfiguration createRecipeCategoryAnalyzerConfiguration() {
        return new RecipeCategoryAnalyzerConfiguration()
                .addRecipeCategoryByPrincipal(UID_BASIC, "Basics")
                .addRecipeCategoryByPrincipal(UID_START, "Starters & salads", "Starters", "Starters and salads")
                .addRecipeCategoryByPrincipal(UID_SOUP, "Soups")
                .addRecipeCategoryByPrincipal(UID_PASTARICE, "Pasta & rice", "Pasta & rice dishes", "Pasta and rice dishes")
                .addRecipeCategoryByPrincipal(UID_MAINMEAT, "Meat & poultry", "Main dishes - meat and poultry")
                .addRecipeCategoryByPrincipal(UID_MAINFISH, "Fish & seafood", "Main dishes - fish and seafood")
                .addRecipeCategoryByPrincipal(UID_MAINVEG, "Vegetarian", "Main dishes - vegetarian")
                .addRecipeCategoryByPrincipal(UID_MAINOTHER, "Other mains", "Main dishes", "Main dishes - other")
                .addRecipeCategoryByPrincipal(UID_SIDE, "Side dishes")
                .addRecipeCategoryByPrincipal(UID_BAKE, "Breads & rolls", "Breads and rolls", "Baking")
                .addRecipeCategoryByPrincipal(UID_BAKE_SAVORY, "Baking - savory", "Baking - savoury")
                .addRecipeCategoryByPrincipal(UID_DESSERT, "Desserts & sweets", "Desserts and sweets", "Desserts")
                .addRecipeCategoryByPrincipal(UID_BAKE_SWEET, "Baking - sweet")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SWEET, "Sauces - sweet", "Sauces, dips & spreads - sweet", "Sauces, dips and spreads - sweet", "Jam")
                .addRecipeCategoryByPrincipal(UID_DRINK, "Drinks")
                .addRecipeCategoryByPrincipal(UID_SNACK, "Snacks", "Snacks and finger food")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SAVO, "Sauces, dips & spreads", "Sauces, dips and spreads - savory", "Sauces, dips and spreads - savoury")
                .addRecipeCategoryByPrincipal(UID_BABY, "Baby food")
                .addRecipeCategoryByPrincipal(UID_BREAK, "Breakfast")
                .addRecipeCategoryByPrincipal(UID_MENU, "Menus and more", "All-In-One cooking")
                .setRecipeCategoryNamePreprocessor(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "^Cooking for ", "^Cooking ", "notused"));

                            @Override
                            public String apply(String s) {
                                return FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }

    public static RecipeCommandAnalyzerConfiguration createRecipeCommandAnalyzerConfiguration() {
        return new RecipeCommandAnalyzerConfiguration()
                .setCommandProgramPredicates(
                        toMap(
                                DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                                FERMENT, of(),
                                HEAT_ONLY, of(),
                                SOUS_VIDE, of(),
                                TURBO, stringPredicatePatterns("(?i)turbo")))
                .setCommandDurationPredicates(toMap(RecipeCommandAnalyzerConfiguration.HALF_SECOND, stringPredicatePatterns("0\\.5 sec")))
                .setCommandDurationHoursPattern(compile("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(compile("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(compile("^([0-9]+) sec"))
                .setCommandTemperaturePredicates(toMap(STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steam")))
                .setCommandTemperaturePattern(compile("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)speed " + TM_SLOW + "|speed slow")))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse"))
                .setCommandSpeedPattern(compile("speed ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(compile("^([0-9]+) (?:time|times)"))
                .setCommandRepetitionFromToPattern(compile("^([0-9]+)-([0-9]+) times"));
    }

    public static RecipeCustomizationConfiguration createRecipeCustomizationConfiguration() {
        return new RecipeCustomizationConfiguration()
                .setRecipeDifficultDefinitionPredicates(
                        toMap(
                                EASY, stringPredicatePattern("(?i)easy"),
                                MEDIUM, stringPredicatePattern("(?i)medium"),
                                ADVANCED, stringPredicatePattern("(?i)advanced")))
                .setRecipePriceDefinitionPredicates(toMap(RecipePriceDefinition.LOW, stringPredicatePattern("(?i)low budget")))
                .setInstructionParallelFormatFunction(s -> StringUtils.capitalize(replaceAll(s, PARALLEL_TEXT_PATTERN, "")))
                .setRecipeLocaleLabelFunction(rd -> "en_VI");
    }

    public static RecipeIngredientAnalyzerConfiguration createRecipeIngredientAnalyzerConfiguration() {
        return new RecipeIngredientAnalyzerConfiguration(null) // Would need a way to distinguish of as separator from of as part of ingredient
                .setIngredientPreparationTokens(COOKI_EN_INGREDIENT_PREPARATION_TOKENS)
                //                .setIngredientAlternativeSeparatorTokenSet(" or ") // Careful. Needs spaces!
                .setIngredientOptionalIndicatorTokens(Set.of("(optional)"))
                .setIngredientForbiddenRegexes(Stream.of(COOKI_EN_ACTION_REGEXES).collect(Collectors.toSet()));
    }

    public static RecipeInstructionAnalyzerConfiguration createRecipeInstructionAnalyzerConfiguration() {
        return new RecipeInstructionAnalyzerConfiguration(
                        Stream.of(
                                        toNext(".", ""), // REPLACE ALL with simple regex split implementation "(?<!\\bapprox|\\benv|\\bin|\\bmin|\\bmax)\\."
                                        toNext(" approx.", DO_NOT_SPLIT), // abb
                                        toNext(" in.", DO_NOT_SPLIT),
                                        toNext(" min.", DO_NOT_SPLIT),
                                        toNext(" max.", DO_NOT_SPLIT))
                                .toArray(Replacement[]::new),
                        toMapP(
                                Pair.of(DO_ADD, toRecipeInstructionMatcher("(?i)\\badd\\b")),
                                Pair.of(DO_PLACE, toRecipeInstructionMatcher("(?i)^(?:place)\\b")),
                                //                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatcher("(?i)regrese|retorne")),
                                Pair.of(DO_WEIGH, toRecipeInstructionMatcher("(?i)(?:weigh .*into it|weigh in )")),
                                Pair.of(DO_SERVE, toRecipeInstructionMatcher("(?i)\\b(?:serve|served|serving)\\b")),
                                Pair.of(DO_PREHEAT, toRecipeInstructionMatcher("(?i)\\bpreheat\\b")),
                                Pair.of(FOCUS_MAIN, toRecipeInstructionMatcher("(?i)\\b(?:in|into) mixing bowl")),
                                Pair.of(FOCUS_ON_TOP, toRecipeInstructionMatcher("(?i)onto mixing bowl lid")),
                                Pair.of(FOCUS_EXTERNAL, toRecipeInstructionMatcher("(?i)" + REGEX_FOCUS_EXTERNAL_CONTAINER)),
                                Pair.of(FOCUS_UNDEFINED, toRecipeInstructionMatcher("(?i)set aside")),
                                Pair.of(INGREDIENTS_ALL, toRecipeInstructionMatcher("(?i)all ingredients")),
                                Pair.of(INGREDIENTS_ALL_WITH_EXCEPTIONS, toRecipeInstructionMatcher("(?i)all ingredients.*except")),
                                Pair.of(INGREDIENTS_ALL_REMAINING, toRecipeInstructionMatcher("(?i)(?:all )?remaining.*ingredients")),
                                Pair.of(IS_USE_RESERVED, toRecipeInstructionMatcher("(?i)reserved")),
                                Pair.of(SEQUENCE_PARALLEL, toRecipeInstructionMatcher("(?i)" + REGEX_PARALLEL)),
                                Pair.of(QUALIFIES_PREVIOUS, toRecipeInstructionMatcher("(?i)placing simmering basket instead of measuring")) // Needs context analysis to place before tm step
                                ),
                        compile("[ ]?(?:,|\\band\\b)[ ]?"))
                .setInstructionPreprocessor(createCookiEnInstructionPreprocessor())
                .setRecipeInstructionTransferNexus(compile(REGEX_ITEM_TRANSFER_NEXUS));
    }

    public static RecipeLangAnalyzerConfiguration createRecipeLangAnalyzerConfiguration() {
        return new RecipeLangAnalyzerConfiguration(COOKI_EN_STOPWORDS)
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "'s", "er$", "s$"));

                            @Override
                            public String apply(String s) {
                                if (s.length() <= RecipeLangAnalyzerConfiguration.TOKEN_LENGTH_MIN) {
                                    return s;
                                } else {
                                    return removeAll(s, cleanupPattern);
                                }
                            }
                        });
    }

    public static RecipeMagnitudeAnalyzerConfiguration createRecipeMagnitudeAnalyzerConfiguration() {
        return new RecipeMagnitudeAnalyzerConfiguration(
                toMap(
                        "a", ONE,
                        "an", ONE,
                        "one", ONE,
                        "two", TWO,
                        "three", THREE,
                        "four", FOUR,
                        "five", FIVE,
                        "six", SIX,
                        "seven", SEVEN,
                        "eight", EIGHT,
                        "nine", NINE,
                        "ten", TEN));
    }

    public static RecipeMeasurementAnalyzerConfiguration createRecipeMeasurementAnalyzerConfiguration() {
        return new RecipeMeasurementAnalyzerConfiguration(UNIT_PIECE_EN, UNIT_OZ)
                .setMeasurementUnitDefinitions(
                        UNIT_PIECE_EN,
                        new MeasurementUnitDefinition("bunch", UNIT_PIECE_EN, false),
                        new MeasurementUnitDefinition("can", UNIT_PIECE_EN, false),
                        new MeasurementUnitDefinition("sprig", UNIT_PIECE_EN, false, "sprigs"),
                        new MeasurementUnitDefinition("pinch", UNIT_PINCH, false, "pinches"),
                        new MeasurementUnitDefinition("inch", null, false, "inches", "in."),
                        //
                        UNIT_TSP,
                        UNIT_TBSP,
                        //
                        UNIT_LITER,
                        UNIT_MILILITER,
                        UNIT_GRAM,
                        UNIT_KG,
                        UNIT_OZ,
                        UNIT_POUND);
    }

    public static RecipeToolAnalyzerConfiguration createRecipeToolAnalyzerConfiguration() {
        return new RecipeToolAnalyzerConfiguration(
                RECIPE_TOOL_MATCHERS,
                toMapP(
                        // These 3 will disssapear in favour of explicit tool match
                        //                        Pair.of(TOOL_MAIN, toRecipeInstructionMatcher("(?i)mixing bowl")),
                        //                        Pair.of(TOOL_INTERNAL, toRecipeInstructionMatcher(REGEX_TOOL_INTERNAL)), // varoma dish|varoma tray|varoma|steamer|simmering basket|measuring cup
                        //                        Pair.of(TOOL_EXTERNAL, toRecipeInstructionMatcher(REGEX_TOOL_EXTERNAL)),

                        Pair.of(OVEN_OPERATION, toRecipeInstructionMatcher("(?i)preheat.*oven|bake.*for")),
                        Pair.of(REFRIGERATE, toRecipeInstructionMatcher("(?i)(?:keep|place|store).*(?:refrigerator|fridge|refrigerated)|^refrigerate")),
                        Pair.of(FREEZE, toRecipeInstructionMatcher("(?i)(?:chill.*fridge|freeze.*(?:minutes|hour))")),
                        Pair.of(SIMMERING_BASKET_OTHER, toRecipeInstructionMatcher("(?i)using.*simmering basket.*(drain|strain)")),
                        Pair.of(SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatcher("(?i)(?:insert).*simmering basket")),
                        Pair.of(SIMMERING_BASKET_REMOVE, toRecipeInstructionMatcher("(?i)(?:remove).*simmering basket")),
                        Pair.of(SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatcher("(?i)(?:placing|place|with) simmering basket (instead|in place) of measuring cup")), // Needs context analysis to place before tm step
                        //
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

                        Pair.of(MIXING_BOWL, toRecipeInstructionMatcher("(?i)(?:remove).*mixing.*bowl lid")),
                        Pair.of(MIXING_BOWL_PUT, toRecipeInstructionMatcher("(?i)place mixing bowl.*back into position")),
                        Pair.of(MIXING_BOWL_REMOVE, toRecipeInstructionMatcher("(?i)(?:remove).*(?:mixing bowl(?!.*? lid))")),
                        Pair.of(MIXING_BOWL_EMPTY, toRecipeInstructionMatcher("(?i)(?:empty).*mixing.*bowl")),
                        Pair.of(MIXING_BOWL_CLEAN, toRecipeInstructionMatcher("(?i)(?:clean|rinse).*mixing.*bowl")),
                        Pair.of(TRANSFER_FROM_BOWL, toRecipeInstructionMatcher("(?i)(?:transfer|pour.*over).*" + REGEX_FOCUS_EXTERNAL_CONTAINER)),

                        //                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(MEASURING_CUP_NOTUSE, toRecipeInstructionMatcher("(?i)" + REGEX_WITHOUT_MEASURING_CUP)),
                        Pair.of(MEASURING_CUP_PUT, toRecipeInstructionMatcher("(?i)insert.*measuring cup|with measuring cup in place")),
                        Pair.of(MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatcher("(?i)hold.*measuring cup")),

                        //                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
                        Pair.of(WHISK_PUT, toRecipeInstructionMatcher("(?i)insert.*butterfly")),
                        Pair.of(WHISK_REMOVE, toRecipeInstructionMatcher("(?i)remove.*butterfly")),
                        Pair.of(SPATULA, toRecipeInstructionMatcher("(?i)with aid.*spatula")),
                        Pair.of(SPATULA_MIX, toRecipeInstructionMatcher("(?i)mix with.*spatula")),
                        Pair.of(SPATULA_SCRAP_SIDE, toRecipeInstructionMatcher("(?i)scrape.*bowl.*spatula")),
                        Pair.of(SPATULA_MIX_WELL, toRecipeInstructionMatcher("(?i)mix.*well with.*spatula")),
                        Pair.of(STEAMER_PUT, toRecipeInstructionMatcher("(?i)(?:place|return).*(?:varoma|steamer).*(?:to|in|in).*position")),
                        Pair.of(STEAMER_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(?:varoma|steamer)(?!( tray| dish))")),
                        Pair.of(STEAMER_CLOSE, toRecipeInstructionMatcher("(?i)(?:secure|close).*(?:varoma|steamer)")),
                        Pair.of(STEAMER_TRAY_PUT, toRecipeInstructionMatcher("(?i)insert.*(?:varoma|steamer).*tray")),
                        Pair.of(STEAMER_TRAY_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(?:varoma|steamer).*tray")),
                        Pair.of(STEAMER_DISH_PUT, toRecipeInstructionMatcher("(?i)place.*into (varoma|steamer) dish|set varoma dish into position")),
                        Pair.of(STEAMER_DISH_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(varoma|steamer) dish")),
                        Pair.of(OTHER_OPERATION, toRecipeInstructionMatcher("(?i)insert.*(?:blade cover)"))
                        //                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe()),
                        //                                Pair.of(KITCHEN_EQUIPMENT, toRecipeInstructionMatcher())
                        ));
    }
}
