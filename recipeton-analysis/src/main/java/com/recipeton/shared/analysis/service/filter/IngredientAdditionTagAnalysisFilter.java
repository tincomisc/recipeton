package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.TAGS_UTENSIL_PLACEMENT;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;

public class IngredientAdditionTagAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    public IngredientAdditionTagAnalysisFilter() {
        super("isIngredientAddition?");
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (!recipeInstructionAnalysis.isTagsAnyPresent(TAGS_DO_ADDITION) || !recipeInstructionAnalysis.isTagsAnyPresent(FOCUS_MAIN, FOCUS_ON_TOP)) {
            return recipeInstructionAnalysis;
        }

        // It is place in main or on top...
        if (recipeInstructionAnalysis.isTagsAnyPresent(TAGS_UTENSIL_PLACEMENT)) {
            return recipeInstructionAnalysis;
        }

        // These will start to match more effectively when TAGS_UTENSIL_PLACEMENT is gradually removed in favour of more simple term matching
        if (recipeInstructionAnalysis.isTagPresent(TOOL_MAIN) && recipeInstructionAnalysis.isTagsAnyPresent(TOOL_EXTERNAL, TOOL_INTERNAL)) { // [place] [tool_internal] (on top of|inside) [tool_main]
            return recipeInstructionAnalysis;
        }

        if (recipeInstructionAnalysis.isTagPresent(FOCUS_ON_TOP) && recipeInstructionAnalysis.isTagsAnyPresent(TOOL_EXTERNAL)) {
            return recipeInstructionAnalysis;
        }

        return addTermIfNotPresent(recipeInstructionAnalysis, IS_INGREDIENT_ADDITION);
    }
}
