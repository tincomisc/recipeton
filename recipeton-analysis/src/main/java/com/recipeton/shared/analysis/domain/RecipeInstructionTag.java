package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.ToString;

@ToString(includeFieldNames = false)
public class RecipeInstructionTag implements RecipeTag {

    public static final RecipeInstructionTag DO_PLACE = new RecipeInstructionTag("DO_PLACE");
    public static final RecipeInstructionTag DO_ADD = new RecipeInstructionTag("DO_ADD");
    /** Return something that was before. Not a new ingredient */
    public static final RecipeInstructionTag DO_READD = new RecipeInstructionTag("DO_READD");
    /** Instruction contains specific weight instructions i.e: Weigh in steamer */
    public static final RecipeInstructionTag DO_WEIGH = new RecipeInstructionTag("DO_WEIGH");
    /** Recipe instructions are about serving. Probably subsequent instructions (if present) are too. */
    public static final RecipeInstructionTag DO_SERVE = new RecipeInstructionTag("DO_SERVE");
    /** Specific technical command */
    public static final RecipeInstructionTag DO_COMMAND_MAIN = new RecipeInstructionTag("DO_COMMAND_MAIN");

    public static final RecipeInstructionTag DO_COMMAND_MAIN_PREPARATION_DEFAULT = new RecipeInstructionTag("DO_COMMAND_MAIN_PREPARATION_DEFAULT");

    public static final RecipeInstructionTag DO_REMOVE = new RecipeInstructionTag("DO_REMOVE");
    public static final RecipeInstructionTag DO_CLEAN = new RecipeInstructionTag("DO_CLEAN");

    public static final RecipeInstructionTag DO_PREHEAT = new RecipeInstructionTag("DO_PREHEAT");

    public static final RecipeInstructionTag FOCUS_MAIN = new RecipeInstructionTag("FOCUS_MAIN");
    public static final RecipeInstructionTag FOCUS_ON_TOP = new RecipeInstructionTag("FOCUS_ON_TOP");
    public static final RecipeInstructionTag FOCUS_EXTERNAL = new RecipeInstructionTag("FOCUS_EXTERNAL");
    public static final RecipeInstructionTag FOCUS_UNDEFINED = new RecipeInstructionTag("FOCUS_UNDEFINED");

    public static final RecipeInstructionTag IS_INGREDIENT_ADDITION = new RecipeInstructionTag("IS_INGREDIENT_ADDITION");
    public static final RecipeInstructionTag IS_INGREDIENT_POTENTIAL_MISMATCH = new RecipeInstructionTag("IS_INGREDIENT_POTENTIAL_MISMATCH");

    public static final RecipeInstructionTag IS_ITEM_FROM_TO_TRANSFER = new RecipeInstructionTag("IS_ITEM_FROM_TO_TRANSFER");

    public static final RecipeInstructionTag IS_USE_RESERVED = new RecipeInstructionTag("IS_USE_RESERVED");

    /** Instruction refers to all ingredients instead of naming them one by one */
    public static final RecipeInstructionTag INGREDIENTS_ALL = new RecipeInstructionTag("INGREDIENTS_ALL");

    public static final RecipeInstructionTag INGREDIENTS_ALL_WITH_EXCEPTIONS = new RecipeInstructionTag("INGREDIENTS_ALL_WITH_EXCEPTIONS"); // NOTE: Special case for post process. Should add all non previously matched ingredients
    public static final RecipeInstructionTag INGREDIENTS_ALL_REMAINING = new RecipeInstructionTag("INGREDIENTS_ALL_REMAINING"); // NOTE: Special case for post process. Should add all non previously matched ingredients

    /** Instruction should be carried on in parallel with previous. */
    public static final RecipeInstructionTag SEQUENCE_PARALLEL = new RecipeInstructionTag("SEQUENCE_PARALLEL");

    public static final RecipeInstructionTag QUALIFIES_PREVIOUS = new RecipeInstructionTag("QUALIFIES_PREVIOUS");

    public static final RecipeInstructionTag[] TAGS_DO_ADDITION = {DO_PLACE, DO_ADD, DO_READD};
    public static final RecipeInstructionTag[] TAGS_FOCUS = {FOCUS_MAIN, FOCUS_EXTERNAL, FOCUS_ON_TOP, FOCUS_UNDEFINED};
    public static final RecipeInstructionTag[] TAGS_LOCATION_WEIGH_INTERNAL = {FOCUS_MAIN, FOCUS_ON_TOP};

    private final String value;

    RecipeInstructionTag(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
