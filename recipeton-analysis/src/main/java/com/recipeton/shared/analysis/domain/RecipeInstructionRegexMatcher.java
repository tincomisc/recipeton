package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;
import static java.util.regex.Pattern.compile;

public class RecipeInstructionRegexMatcher implements RecipeInstructionMatcher {

    private final Pattern pattern;

    public RecipeInstructionRegexMatcher(String regex) {
        this(compile(regex));
    }

    public RecipeInstructionRegexMatcher(Pattern pattern) {
        this.pattern = pattern;
    }

    public static RecipeInstructionMatcher toRecipeInstructionMatcher(String regexExpression) {
        return new RecipeInstructionRegexMatcher(regexExpression);
    }

    @Override
    public Optional<RecipeInstructionMatch> findMatch(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        Matcher matcher = pattern.matcher(recipeInstructionAnalysis.getText());
        if (matcher.find()) {
            return Optional.of(new RecipeInstructionMatch(matcher.start(), matcher.group(), SCORE_MAX, "regex?" + pattern));
        }

        return Optional.empty();
    }

    public String getPattern() {
        return pattern.pattern();
    }
}
