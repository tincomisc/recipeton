package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionTag;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;

public class FocusByContextAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    public FocusByContextAnalysisFilter() {
        super("isFocusByContext?");
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) { // NOPMD Complexity BS
        boolean focusTagPresent = recipeInstructionAnalysis.isTagsAnyPresent(TAGS_FOCUS);
        if (!recipeInstructionAnalysis.isTagsAnyPresent(TAGS_DO_ADDITION) || focusTagPresent || recipeInstructionAnalysis.isTagPresent(SEQUENCE_PARALLEL)) {
            return recipeInstructionAnalysis;
        }

        if (recipeInstructionAnalysis.isTagPresent(DO_PLACE) && !focusTagPresent) {
            if (recipeInstructionAnalysis.isTagPresent(IS_ITEM_FROM_TO_TRANSFER)) { // If no focus it should mean that is a transfer to something else that I do not match!
                return addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_UNDEFINED);
            }

            if (recipeInstructionAnalysis.getPrev() != null) {
                // Just check prev in case is a direct qualification
                RecipeInstructionTag focus = recipeInstructionAnalysis.findFirstMatchInPrev(2, TAGS_FOCUS);
                if (focus != null) { // NOPMD BS
                    return addTermIfNotPresent(recipeInstructionAnalysis, focus);
                }
            }

            return addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_UNDEFINED);
        }

        RecipeInstructionTag focus = recipeInstructionAnalysis.findFirstMatchInPrev(Integer.MAX_VALUE, TAGS_FOCUS);
        if (focus == null) {
            if (recipeInstructionAnalysis.isTagPresentInPrev(DO_COMMAND_MAIN)) {
                return addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_MAIN);
            }
        } else {
            if (FOCUS_UNDEFINED.equals(focus) && recipeInstructionAnalysis.isTagPresentInNext(DO_COMMAND_MAIN)) {
                return addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_MAIN);
            } else {
                return addTermIfNotPresent(recipeInstructionAnalysis, focus);
            }
        }

        return recipeInstructionAnalysis;
    }
}
