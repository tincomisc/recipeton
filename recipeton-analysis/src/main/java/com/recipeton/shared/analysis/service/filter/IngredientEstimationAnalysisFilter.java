package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeInstructionAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeLangAnalyzer;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeInstructionMatch.SCORE_MAX;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;

@Slf4j
public class IngredientEstimationAnalysisFilter implements RecipeInstructionAnalysisFilter {

    public static final String QUERY = "isIngredientDeclared?";

    private static final Set<RecipeInstructionTag> TAG_SEGMENTS_REMOVE = Stream.concat(Stream.of(TAGS_DO_ADDITION), Stream.of(TAGS_FOCUS)).collect(Collectors.toSet());

    private final RecipeInstructionAnalyzerConfiguration instructionAnalyzerConfiguration;
    private final RecipeLangAnalyzer recipeLangAnalyzer;

    public IngredientEstimationAnalysisFilter(RecipeInstructionAnalyzerConfiguration instructionAnalyzerConfiguration, RecipeLangAnalyzer recipeLangAnalyzer) {
        this.instructionAnalyzerConfiguration = instructionAnalyzerConfiguration;
        this.recipeLangAnalyzer = recipeLangAnalyzer;
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (!recipeInstructionAnalysis.isTagPresent(IS_INGREDIENT_ADDITION)) {
            return recipeInstructionAnalysis;
        }

        List<RecipeInstructionTerm> termsToTrim =
                recipeInstructionAnalysis
                        .streamRecipeInstructionTerms()
                        .filter(o -> TAG_SEGMENTS_REMOVE.contains(o.getTag()))
                        .filter(t -> !t.isContextualMatch())
                        .sorted(Comparator.comparing(RecipeInstructionTerm::getOffset))
                        .collect(Collectors.toList());

        String text = recipeInstructionAnalysis.getText();
        Matcher matcher = instructionAnalyzerConfiguration.getRecipeInstructionIngredientSeparatorPattern().matcher(text);
        int lastEnd = 0;
        List<RecipeInstructionMatch> recipeInstructionMatches = new ArrayList<>();
        while (matcher.find()) {
            int start = matcher.start();
            if (lastEnd < start) {
                trimMatch(text, lastEnd, start, recipeInstructionMatches, termsToTrim);
            }
            lastEnd = matcher.end();
        }
        if (lastEnd < text.length()) {
            trimMatch(text, lastEnd, text.length(), recipeInstructionMatches, termsToTrim);
        }

        recipeInstructionMatches.forEach(m -> recipeInstructionAnalysis.addTerm(new RecipeDataTerm(m, RecipeDataTerm.RecipeDataTermType.INGREDIENT)));

        return recipeInstructionAnalysis;
    }

    private void trimMatch(String text, int beginIndex, int endIndex, List<RecipeInstructionMatch> matches, List<RecipeInstructionTerm> termsToTrim) { // NOPMD complex
        int offset = beginIndex;
        int endOffset = endIndex;
        for (RecipeInstructionTerm termToTrim : termsToTrim) {
            int termToTrimOffset = termToTrim.getOffset();
            if (termToTrimOffset == offset) { // Begin!
                offset += termToTrim.getLength();
            } else if (termToTrimOffset > offset && termToTrimOffset < endOffset) {
                endOffset = termToTrimOffset;
            }
        }

        String subText = text.substring(offset, endOffset);
        while (subText.length() > 0 && subText.charAt(0) == ' ') {
            offset++;
            subText = text.substring(offset, endOffset);
        }

        boolean changed;
        do {
            Matcher stopWordMatcher = recipeLangAnalyzer.getStartsWithStopWordRegex().matcher(subText);
            changed = stopWordMatcher.find();
            if (changed) {
                int end = stopWordMatcher.end();
                if (end == 0) {
                    log.error("Stopword returning zero. Something wrong in regex!!! text={}", subText);
                    throw new IllegalStateException("Stopword returning zero. Something wrong in regex!!! text=" + subText);
                }
                offset += end;
                subText = text.substring(offset, endOffset);
            }
        } while (changed);

        if (!subText.isBlank()) {
            matches.add(new RecipeInstructionMatch(offset, subText, SCORE_MAX, QUERY));
        }
    }
}
