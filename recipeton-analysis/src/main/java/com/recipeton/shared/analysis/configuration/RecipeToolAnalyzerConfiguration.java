package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionMatcher;
import com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher;
import com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag;
import com.recipeton.shared.analysis.domain.RecipeToolDefinition;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.regex.Pattern.compile;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeToolAnalyzerConfiguration {

    private final Map<RecipeToolDefinition, RecipeInstructionMatcher> recipeToolDefinitionMatchers;
    private final Map<RecipeToolComposedActionTag, RecipeInstructionMatcher> recipeToolComposedActionTagMatchers;
    private final Pattern toolsPattern;

    public RecipeToolAnalyzerConfiguration(Map<RecipeToolDefinition, RecipeInstructionMatcher> recipeToolDefinitionMatchers, Map<RecipeToolComposedActionTag, RecipeInstructionMatcher> recipeToolComposedActionTagMatchers) {
        this.recipeToolDefinitionMatchers = recipeToolDefinitionMatchers;
        this.recipeToolComposedActionTagMatchers = recipeToolComposedActionTagMatchers;

        this.toolsPattern = compile("(?i)" + RecipeToolAnalyzerConfiguration.getCombinedRegex(recipeToolDefinitionMatchers.entrySet().stream()));
    }

    public static String getCombinedRegex(Stream<Map.Entry<RecipeToolDefinition, RecipeInstructionMatcher>> entryStream) {
        return entryStream
                .map(
                        e -> {
                            if (e.getValue() instanceof RecipeInstructionRegexMatcher) {
                                return ((RecipeInstructionRegexMatcher) e.getValue()).getPattern();
                            }
                            return e.getKey().getName();
                        })
                .collect(Collectors.joining("|"));
    }
}
