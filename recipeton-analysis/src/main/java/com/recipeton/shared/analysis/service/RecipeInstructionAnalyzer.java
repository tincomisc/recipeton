package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.filter.RecipeInstructionAnalysisFilter;
import com.recipeton.shared.analysis.service.filter.RecipeInstructionAnalysisFilterFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;

@Slf4j
@RequiredArgsConstructor
public class RecipeInstructionAnalyzer {

    public static final int SINGLE = 1;
    public static final int MAX_ALLOWED_OVERLAP = 2; // to config
    private static final double SCORE_BONUS_MAGNITUDE_OVERRIDE = 10; // To config

    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeMeasurementAnalyzer recipeMeasurementAnalyzer;
    private final RecipeInstructionSplitter recipeInstructionSplitter;
    private final RecipeInstructionAnalysisFilterFactory recipeInstructionAnalysisFilterFactory;
    private final List<RecipeInstructionAnalysisFilter> recipeInstructionAnalysisPreFilters;

    private final RecipeAnalyzerConfiguration configuration;

    public RecipeInstructionAnalyzer(
            RecipeLangAnalyzer recipeLangAnalyzer,
            RecipeMeasurementAnalyzer recipeMeasurementAnalyzer,
            RecipeInstructionSplitter recipeInstructionSplitter,
            RecipeInstructionAnalysisFilterFactory recipeInstructionAnalysisFilterFactory,
            RecipeAnalyzerConfiguration configuration) {
        this.recipeLangAnalyzer = recipeLangAnalyzer;
        this.recipeInstructionSplitter = recipeInstructionSplitter;
        this.recipeMeasurementAnalyzer = recipeMeasurementAnalyzer;
        this.recipeInstructionAnalysisFilterFactory = recipeInstructionAnalysisFilterFactory;

        this.recipeInstructionAnalysisPreFilters = recipeInstructionAnalysisFilterFactory.createRecipeInstructionAnalysisPreFilters();

        this.configuration = configuration;
    }

    public List<RecipeInstructionAnalysis> analyze(List<String> recipeInstructions, List<RecipeIngredientAnalysis> recipeIngredientAnalyses) {
        return analyze(recipeInstructions, recipeIngredientAnalyses, false);
    }

    public List<RecipeInstructionAnalysis> analyze(List<String> recipeInstructions, List<RecipeIngredientAnalysis> recipeIngredientAnalyses, boolean strictAnalysis) { // NOPMD Complex
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = toRecipeInstructionAnalyses(recipeInstructions);

        recipeInstructionAnalysisPreFilters.forEach(f -> recipeInstructionAnalyses.forEach(f::apply));

        // Search
        recipeIngredientAnalyses.stream()
                .filter(i -> StringUtils.isNotBlank(i.getIngredientNotation()))
                .flatMap(i -> getIngredientStandardTokenVariants(i).stream().map(v -> recipeInstructionAnalysisFilterFactory.createRecipeIngredientRecipeInstructionAnalysisFilter(i, v)))
                .forEach(f -> recipeInstructionAnalyses.forEach(f::apply));

        applyScores(recipeInstructionAnalyses);

        discardRecipeIngredientTermsByNonTop(recipeInstructionAnalyses);

        recipeInstructionAnalyses.forEach(
                ria -> {
                    List<RecipeIngredientTerm> ingredientTerms = ria.streamRecipeIngredientTerms().filter(t -> !t.isContextualMatch()).collect(Collectors.toCollection(LinkedList::new));

                    int i = 0;
                    int j = i + 1;
                    while (i < ingredientTerms.size() && j < ingredientTerms.size()) {
                        RecipeIngredientTerm termI = ingredientTerms.get(i);
                        RecipeIngredientTerm termJ = ingredientTerms.get(j);
                        if (isOverlapping(termI, termJ)) {
                            RecipeIngredientTerm termToKeep;
                            RecipeIngredientTerm termToDiscard;
                            if (termI.getTermScore() > termJ.getTermScore()) {
                                termToKeep = termI;
                                termToDiscard = termJ;
                                ingredientTerms.remove(j);
                            } else {
                                termToKeep = termJ;
                                termToDiscard = termI;
                                ingredientTerms.remove(i);
                                i = 0;
                                j = i + 1;
                            }
                            RecipeIngredientTerm alternativeTerm =
                                    ria.streamDiscardedRecipeIngredientTerms()
                                            .filter(t -> t.getIngredient().equals(termToDiscard.getIngredient()))
                                            .filter(t -> t.getTermScore() < termToDiscard.getTermScore())
                                            .filter(t -> !isOverlapping(termToKeep, t))
                                            .max(Comparator.comparing(RecipeIngredientTerm::getTermScore))
                                            .orElse(null);
                            if (alternativeTerm != null) {
                                ria.restoreTerm(alternativeTerm);
                            }
                            ria.discardTerm(termToDiscard);
                        } else {
                            j++;
                        }

                        if (j == ingredientTerms.size()) {
                            i++;
                            j = i + 1;
                        }
                    }
                });

        Set<RecipeIngredientAnalysis> missingRecipeIngredientAnalysis = new HashSet<>(recipeIngredientAnalyses);
        for (RecipeIngredientAnalysis recipeIngredientAnalysis : recipeIngredientAnalyses) {
            List<Pair<RecipeInstructionAnalysis, RecipeIngredientTerm>> recipeIngredientTermByRecipeInstructionsBest = getRecipeIngredientTermByRecipeInstructionsBest(recipeIngredientAnalysis, recipeInstructionAnalyses);

            boolean ingredientFoundInAnyStep = recipeIngredientTermByRecipeInstructionsBest.stream().anyMatch(p -> p.getValue() != null);
            if (!ingredientFoundInAnyStep) {
                // try to recover single best match from discarded
                Pair<RecipeInstructionAnalysis, RecipeIngredientTerm> recoverableBest =
                        recipeInstructionAnalyses.stream()
                                .flatMap(ria -> ria.streamDiscardedRecipeIngredientTerms().filter(t -> t.getIngredient().equals(recipeIngredientAnalysis)).map(t -> Pair.of(ria, t)))
                                .max(Comparator.comparing(p -> p.getValue().getTermScore()))
                                .orElse(null);
                if (recoverableBest != null) {
                    recoverableBest.getLeft().restoreTerm(recoverableBest.getRight());

                    recipeIngredientTermByRecipeInstructionsBest = getRecipeIngredientTermByRecipeInstructionsBest(recipeIngredientAnalysis, recipeInstructionAnalyses);
                    ingredientFoundInAnyStep = true;
                }
            }

            if (!ingredientFoundInAnyStep) { // Not found at all expand criteria and try to match again
                List<String[]> preparationVariants = getIngredientPreparationTokenVariants(recipeIngredientAnalysis);
                if (preparationVariants != null) {
                    preparationVariants.stream().map(v -> recipeInstructionAnalysisFilterFactory.createRecipeIngredientRecipeInstructionAnalysisFilter(recipeIngredientAnalysis, v)).forEach(f -> recipeInstructionAnalyses.forEach(f::apply));

                    applyScores(recipeInstructionAnalyses);
                    discardRecipeIngredientTermsByNonTop(recipeInstructionAnalyses);

                    recipeIngredientTermByRecipeInstructionsBest = getRecipeIngredientTermByRecipeInstructionsBest(recipeIngredientAnalysis, recipeInstructionAnalyses);
                    ingredientFoundInAnyStep = recipeIngredientTermByRecipeInstructionsBest.stream().anyMatch(p -> p.getValue() != null);
                }
            }

            if (!ingredientFoundInAnyStep) {
                log.info("Ingredient not found in any step. Maybe referenced with generic term. ingredient={}", recipeIngredientAnalysis);
                continue;
            }

            // It is somewhere. Maybe usage is not conclusive but that is a different story
            missingRecipeIngredientAnalysis.remove(recipeIngredientAnalysis);

            // Trace!
            boolean conclusiveMatchFound = false;
            boolean weighed = false;
            boolean added = false;
            for (Pair<RecipeInstructionAnalysis, RecipeIngredientTerm> instructionWithTopIngredientMatch : recipeIngredientTermByRecipeInstructionsBest) {
                RecipeIngredientTerm recipeIngredientTerm = instructionWithTopIngredientMatch.getValue();
                if (recipeIngredientTerm == null) {
                    // Not found in step
                    continue;
                }
                RecipeInstructionAnalysis recipeInstructionAnalysis = instructionWithTopIngredientMatch.getKey();

                if (recipeInstructionAnalysis.isTagPresent(DO_WEIGH) && recipeInstructionAnalysis.isTagsAnyPresent(TAGS_LOCATION_WEIGH_INTERNAL)) { // Weigh step
                    if (recipeIngredientTerm.isPartialUse() || !weighed && !added) { // Partial use or not weighed or added before
                        recipeIngredientTerm.setConclusive(true);
                        recipeIngredientTerm.setTarget(RecipeIngredientTerm.Target.PREPROCESS);

                        weighed = true;

                        conclusiveMatchFound = true;
                    }
                    continue;
                }

                if (recipeInstructionAnalysis.isTagPresent(IS_INGREDIENT_ADDITION)) {
                    if (recipeIngredientTerm.isPartialUse() || !added) { // Partial use or not added before
                        recipeIngredientTerm.setConclusive(true);
                        recipeIngredientTerm.setTarget(RecipeIngredientTerm.Target.PROCESS);

                        added = true;

                        conclusiveMatchFound = true;
                    }
                    continue;
                }

                if (recipeInstructionAnalysis.isTagPresent(DO_SERVE)) {
                    if (!weighed && !added) {
                        recipeIngredientTerm.setConclusive(true);
                        recipeIngredientTerm.setTarget(RecipeIngredientTerm.Target.SERVING);

                        conclusiveMatchFound = true;
                    }
                    continue;
                }
            }

            if (!conclusiveMatchFound) {
                if (strictAnalysis) {
                    throw new IllegalStateException("Analyze found ingredient but not in any addition step. Maybe used for external preparation. ingredient=" + recipeIngredientAnalysis);
                }
                log.debug("Analyze found ingredient but not in any addition step. Maybe used for external preparation. ingredient={}", recipeIngredientAnalysis);
            }
        }

        if (!missingRecipeIngredientAnalysis.isEmpty()) {
            List<RecipeInstructionAnalysis> recipeInstructionsOfPlaceMainContainer = recipeInstructionAnalyses.stream().filter(ria -> ria.isTagPresent(IS_INGREDIENT_ADDITION)).collect(Collectors.toList());
            if (recipeInstructionsOfPlaceMainContainer.isEmpty()) {
                log.info("No addition step found. Assuming all ingredients are accessory. Adding to step 0. count={}", recipeInstructionsOfPlaceMainContainer.size());
                // This happens in full accessory sub recipe
                missingRecipeIngredientAnalysis.forEach(ui -> recipeInstructionAnalyses.get(0).addTerm(RecipeIngredientTerm.ofMatchOnAll(ui, "__ALL_GUESSED0__")));
            } else if (recipeInstructionsOfPlaceMainContainer.size() == SINGLE) {
                RecipeInstructionAnalysis singleRecipeInstructionOfAdditionType = recipeInstructionsOfPlaceMainContainer.get(0);
                missingRecipeIngredientAnalysis.forEach(ui -> singleRecipeInstructionOfAdditionType.addTerm(RecipeIngredientTerm.ofMatchOnAll(ui, "__ALL_GUESSED1__")));
            } else {
                RecipeInstructionAnalysis addAllRemainingIngredientsInstruction = recipeInstructionsOfPlaceMainContainer.stream().filter(i -> i.isTagPresent(INGREDIENTS_ALL_REMAINING)).findFirst().orElse(null);
                if (addAllRemainingIngredientsInstruction == null) {
                    log.warn("Could not find any ingredient and multiple addition steps. Will not add any ingredient. count={}", recipeInstructionsOfPlaceMainContainer.size());
                } else {
                    missingRecipeIngredientAnalysis.forEach(ui -> addAllRemainingIngredientsInstruction.addTerm(RecipeIngredientTerm.ofMatchOnAll(ui, "__ALL_GUESSED2__")));
                }
            }
        }

        //        réservé

        recipeInstructionAnalyses.stream()
                .filter(ria -> ria.isTagsAllPresent(FOCUS_MAIN, IS_INGREDIENT_ADDITION))
                .filter(ria -> ria.isTagsNonePresent(INGREDIENTS_ALL))
                .forEach(
                        ria -> {
                            List<RecipeDataTerm> unmatchedIngredientDataTerms =
                                    ria.streamTerms()
                                            .filter(t -> t instanceof RecipeDataTerm)
                                            .map(t -> (RecipeDataTerm) t)
                                            .filter(t -> t.getType().equals(RecipeDataTerm.RecipeDataTermType.INGREDIENT))
                                            .filter(t -> ria.streamRecipeIngredientTerms().noneMatch(rit -> rit.isConclusive() && rit.getOverlap(t) > MAX_ALLOWED_OVERLAP))
                                            .collect(Collectors.toList());

                            unmatchedIngredientDataTerms.forEach(
                                    t -> {
                                        String text = t.getMatch().getText().trim();
                                        RecipeIngredientAnalysis ingredient = new RecipeIngredientAnalysis(text).setText(text).setIngredientNotation(recipeLangAnalyzer.toUid(text), text);
                                        RecipeIngredientTerm recipeTerm = new RecipeIngredientTerm(ingredient, t.getMatch(), true);
                                        recipeTerm.setConclusive(true); // make this optional
                                        ria.addTerm(recipeTerm);
                                    });

                            if (!unmatchedIngredientDataTerms.isEmpty() && ria.isTagsNonePresent(IS_USE_RESERVED)) {
                                ria.addTerm(new RecipeInstructionTerm(IS_INGREDIENT_POTENTIAL_MISMATCH, RecipeInstructionMatch.ofContext("isFoundIngredientsLessThanEstimated?")));
                            }
                        });

        // Move this to filter (post)
        //        recipeInstructionAnalyses.stream()
        //                .filter(ria -> ria.isTagsAllPresent(LOCATION_MAIN, IS_INGREDIENT_ADDITION) && ria.isTagsAnyPresent(TAGS_DO_ADDITION)) // && ria.isTagsAnyPresent(TAGS_DO_ADDITION) probably redundant
        //                .filter(this::isIngredientCountMismatch)
        //                .forEach(ria -> ria.addTerm(new RecipeInstructionTerm(IS_INGREDIENT_POTENTIAL_MISMATCH, RecipeInstructionMatch.ofContext("isFoundIngredientsLessThanEstimated?"))));

        // ADD SANITY WARNING IF  PLACE OR ADD STEPS DO NOT CONTAIN ANY CONCLUSIVE TERMS!!!!

        return recipeInstructionAnalyses;
    }

    public void applyScores(List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        recipeInstructionAnalyses.forEach(ria -> ria.streamRecipeIngredientTerms().filter(t -> t.getTermScore() == null).forEach(t -> t.setTermScore(getScore(t))));
    }

    private void discardRecipeIngredientTermsByNonTop(List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        recipeInstructionAnalyses.forEach(
                ria -> {
                    Map<RecipeIngredientAnalysis, List<RecipeIngredientTerm>> termsByIngredient = ria.streamRecipeIngredientTerms().collect(Collectors.groupingBy(RecipeIngredientTerm::getIngredient));
                    termsByIngredient.forEach(
                            (k, v) -> {
                                RecipeIngredientTerm top = v.stream().max(Comparator.comparing(RecipeIngredientTerm::getTermScore)).get();
                                v.stream().filter(t -> !top.equals(t)).forEach(ria::discardTerm);
                            });
                });
    }

    private String getIngredientBestMatchText(RecipeIngredientAnalysis recipeIngredientAnalysis) {
        int notationIndex = StringUtils.indexOf(recipeIngredientAnalysis.getText(), recipeIngredientAnalysis.getIngredientNotation());
        return recipeIngredientAnalysis.getText().substring(0, notationIndex + recipeIngredientAnalysis.getIngredientNotation().length());
    }

    public List<String[]> getIngredientPreparationTokenVariants(RecipeIngredientAnalysis recipeIngredientAnalysis) {
        String preparationText = recipeIngredientAnalysis.getPreparation();
        if (preparationText == null) {
            return null;
        }

        String[] tokens = recipeLangAnalyzer.getTokens(preparationText, configuration.getRecipeIngredientAnalyzerConfiguration().getIngredientPreparationTokens());
        List<String[]> variants = new ArrayList<>();
        getIngredientTokenVariants(variants, tokens);
        return variants;
    }

    public List<String[]> getIngredientStandardTokenVariants(RecipeIngredientAnalysis recipeIngredientAnalysis) {
        List<String[]> variants = new ArrayList<>();

        String bestMatch = getIngredientBestMatchText(recipeIngredientAnalysis);
        //        En filtro descarta si seccion contiene coma o separadores y

        if (!bestMatch.equals(recipeIngredientAnalysis.getIngredientNotation())) {
            Pattern pattern = configuration.getRecipeInstructionAnalyzerConfiguration().getRecipeInstructionIngredientSeparatorPattern();
            if (pattern == null || !pattern.matcher(bestMatch).find()) {
                String[] superMatchTokens = recipeLangAnalyzer.getTokens(bestMatch);
                variants.add(superMatchTokens);
            }
        }

        String[] ingredientNotationTokens = recipeLangAnalyzer.getTokens(recipeIngredientAnalysis.getIngredientNotation());
        getIngredientTokenVariants(variants, ingredientNotationTokens);

        RecipeIngredientAnalysis alternative = recipeIngredientAnalysis.getAlternative();
        if (alternative != null) {
            String[] tokens = recipeLangAnalyzer.getTokens(alternative.getIngredientNotation());
            getIngredientTokenVariants(variants, tokens);
        }
        return variants;
    }

    private void getIngredientTokenVariants(List<String[]> variants, String... ingredientNotationTokens) {
        int first = 0;
        int last = ingredientNotationTokens.length;
        for (int i = first; i < last; i++) {
            for (int j = last; j > i; j--) {
                variants.add(Arrays.copyOfRange(ingredientNotationTokens, i, j));
            }
        }
    }

    public Optional<RecipeIngredientTerm> getRecipeIngredientTermByRecipeInstructionBest(RecipeInstructionAnalysis recipeInstructionAnalysis, RecipeIngredientAnalysis recipeIngredientAnalysis) {
        return recipeInstructionAnalysis.streamRecipeIngredientTerms().filter(t -> t.getIngredient().equals(recipeIngredientAnalysis)).max(Comparator.comparing(RecipeIngredientTerm::getTermScore));
    }

    private List<Pair<RecipeInstructionAnalysis, RecipeIngredientTerm>> getRecipeIngredientTermByRecipeInstructionsBest(RecipeIngredientAnalysis recipeIngredientAnalysis, List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        return recipeInstructionAnalyses.stream().map(ria -> Pair.of(ria, getRecipeIngredientTermByRecipeInstructionBest(ria, recipeIngredientAnalysis).orElse(null))).collect(Collectors.toList());
    }

    // better use function if need to change
    private double getScore(RecipeIngredientTerm recipeIngredientTerm) {
        String ingredientBestMatch = getIngredientBestMatchText(recipeIngredientTerm.getIngredient());
        String matchText = recipeIngredientTerm.getMatch().getText();
        double balance = (double) matchText.length() / ingredientBestMatch.length();
        double score = recipeIngredientTerm.getMatch().getScore() * balance;
        if (recipeIngredientTerm.isLocalMagnitudeOverride()) { // better match
            score += SCORE_BONUS_MAGNITUDE_OVERRIDE;
        }
        log.trace("score {} ({},{})({})", score, ingredientBestMatch, matchText, recipeIngredientTerm.getMatch().getQuery());
        return score;
    }

    public List<RecipeTerm> getTermsByConclusiveAndSortedByTypeAndOffset(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        // REMOVE ORDER and calculate here based on types, conclusive and match position
        return recipeInstructionAnalysis.getTerms().stream()
                .filter(RecipeTerm::isConclusive)
                .sorted(
                        (o1, o2) -> {
                            if (o1 instanceof RecipeInstructionTerm && !(o2 instanceof RecipeInstructionTerm)) {
                                return -1;
                            }
                            if (o2 instanceof RecipeInstructionTerm && !(o1 instanceof RecipeInstructionTerm)) {
                                return 1;
                            }
                            if (o1 instanceof RecipeToolComposedActionTerm && !(o2 instanceof RecipeToolComposedActionTerm)) {
                                return -1;
                            }
                            if (o2 instanceof RecipeToolComposedActionTerm && !(o1 instanceof RecipeToolComposedActionTerm)) {
                                return 1;
                            }
                            return o1.getOffset() - o2.getOffset();
                        })
                .collect(Collectors.toList());
    }

    public String getTextAsHumanReadable(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        return recipeLangAnalyzer.formatHumanReadable(recipeInstructionAnalysis.getText());
    }

    private boolean isOverlapping(RecipeIngredientTerm term2, RecipeIngredientTerm term1) {
        if (term1.isContextualMatch() || term2.isContextualMatch()) {
            return false;
        }
        int overlap = term1.getOverlap(term2);
        return overlap > MAX_ALLOWED_OVERLAP;
    }

    private List<RecipeInstructionAnalysis> toRecipeInstructionAnalyses(List<String> recipeInstructions) {
        List<RecipeInstructionAnalysis> collect =
                recipeInstructions.stream()
                        .map(recipeLangAnalyzer::normalizeText)
                        .map(recipeMeasurementAnalyzer::formatRecipeMeasurementUnits)
                        .map(s -> configuration.getRecipeInstructionAnalyzerConfiguration().getPreprocessor().apply(s))
                        .flatMap(s -> recipeInstructionSplitter.splitInstruction(s).stream())
                        .filter(StringUtils::isNotBlank)
                        .map(String::trim)
                        .map(recipeLangAnalyzer::getTokenization)
                        .filter(t -> !t.isEmpty()) // Instruction without tokens is just made of stop words
                        .map(RecipeInstructionAnalysis::new)
                        .collect(Collectors.toList());

        for (int i = 0; i < collect.size(); i++) {
            RecipeInstructionAnalysis recipeInstructionAnalysis = collect.get(i);
            if (i > 0) {
                recipeInstructionAnalysis.setPrev(collect.get(i - 1));
            }
            if (i < collect.size() - 1) {
                recipeInstructionAnalysis.setNext(collect.get(i + 1));
            }
        }

        return collect;
    }
}

// Idea??
//    /**
//     * Call like this. Will make everything fit and this is not what I need
//     *             List<RecipeIngredientTerm> ingredientTerms = ria.streamRecipeIngredientTerms()
//     *                     .filter(t -> !t.isContextualMatch())
//     *                     .sorted(Comparator.comparing(t -> t.getMatch().getOffset()))
//     *                     .collect(Collectors.toCollection(LinkedList::new));
//     *
//     *             Collection<RecipeIngredientTerm> bestSolution = best(ingredientTerms, new HashSet<>());
//     * @param ingredientTerms
//     * @param usedBefore
//     * @return
//     */
//    private Collection<RecipeIngredientTerm> best(List<RecipeIngredientTerm> ingredientTerms, Set<RecipeIngredientTerm> usedBefore) {
//        if (ingredientTerms.isEmpty()) {
//            return usedBefore;
//        }
//        RecipeIngredientTerm recipeIngredientTerm = ingredientTerms.get(0);
//        if (usedBefore.stream().anyMatch(t -> t.getIngredient().equals(recipeIngredientTerm.getIngredient()) || isOverlapping(t, recipeIngredientTerm))) {
//            return usedBefore;
//        }
//
//        Collection<RecipeIngredientTerm> bestSolution = Collections.emptyList();
//        double bestScore = 0;
//
//        for (int i = 1; i < ingredientTerms.size(); i++) {
//            List<RecipeIngredientTerm> remainder = ingredientTerms.subList(i, ingredientTerms.size());
//
//            Collection<RecipeIngredientTerm> solutionA = best(remainder, new HashSet<>());
//            double solutionAScore = solutionA.stream().mapToDouble(RecipeIngredientTerm::getTermScore).sum();
//            if (solutionA.size() > bestSolution.size() || (solutionA.size() == bestSolution.size() && solutionAScore > bestScore)) {
//                bestSolution = solutionA;
//                bestScore = solutionAScore;
//            }
//
//            Set<RecipeIngredientTerm> used = new HashSet<>(usedBefore);
//            used.add(recipeIngredientTerm);
//            Collection<RecipeIngredientTerm> solutionB = best(remainder, used);
//            double solutionBScore = solutionB.stream().mapToDouble(RecipeIngredientTerm::getTermScore).sum();
//            if (solutionB.size() > bestSolution.size() || (solutionB.size() == bestSolution.size() && solutionBScore > bestScore)) {
//                bestSolution = solutionB;
//                bestScore = solutionBScore;
//            }
//        }
//        return bestSolution;
//    }
