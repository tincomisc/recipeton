package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeMagnitudeAnalyzerConfiguration {
    public static final Pattern NUMERIC_AND_VULGAR_FRACTION_WITH_SPACE = compile("(\\d+) ([¼-¾⅐-⅞])");
    public static final Pattern NUMERIC_AND_VULGAR_FRACTION = compile("(\\d+)([¼-¾⅐-⅞])");
    public static final Pattern VULGAR_FRACTION = compile("[¼-¾⅐-⅞]");

    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String SEVEN = "7";
    public static final String EIGHT = "8";
    public static final String NINE = "9";
    public static final String TEN = "10";

    public static final String RANGE_SEPARATOR = "-";

    private final Map<String, String> numericStrings;
    private final String rangeSeparator;

    public RecipeMagnitudeAnalyzerConfiguration(Map<String, String> numericStrings, String rangeSeparator) {
        this.numericStrings = numericStrings;
        this.rangeSeparator = rangeSeparator;
    }

    public RecipeMagnitudeAnalyzerConfiguration(Map<String, String> numericStrings) {
        this(numericStrings, RANGE_SEPARATOR);
    }
}
