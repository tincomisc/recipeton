package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@ToString(onlyExplicitlyIncluded = true)
public class MeasurementUnitDefinition {

    public static final MeasurementUnitDefinition UNIT_GRAM = new MeasurementUnitDefinition("g", null, true, true, "gram", "grams", "gramo", "gramos");
    public static final MeasurementUnitDefinition UNIT_KG = new MeasurementUnitDefinition("Kg", UNIT_GRAM, true, true, "kilogram", "kilograms", "kilogramo", "kilogramos");
    public static final MeasurementUnitDefinition UNIT_MGRAM = new MeasurementUnitDefinition("mg", UNIT_GRAM, true, true);

    public static final MeasurementUnitDefinition UNIT_OZ = new MeasurementUnitDefinition("oz", null, true, true, "ounces", "onza", "onzas");
    public static final MeasurementUnitDefinition UNIT_POUND = new MeasurementUnitDefinition("lb", null, true, true, "pound", "pounds");

    public static final MeasurementUnitDefinition UNIT_LITER = new MeasurementUnitDefinition("l", null, false, true, "liter", "liters", "litro", "litros");
    public static final MeasurementUnitDefinition UNIT_MILILITER = new MeasurementUnitDefinition("ml", UNIT_LITER, false, true);

    public static final MeasurementUnitDefinition UNIT_KJ = new MeasurementUnitDefinition("kJ", null, false, true);
    public static final MeasurementUnitDefinition UNIT_KCAL = new MeasurementUnitDefinition("kCal", null, false, true);

    public static final MeasurementUnitDefinition UNIT_PIECE = new MeasurementUnitDefinition("ud", null, false, false);

    public static final MeasurementUnitDefinition UNIT_TSP = new MeasurementUnitDefinition("tsp", null, false, true);
    public static final MeasurementUnitDefinition UNIT_TBSP = new MeasurementUnitDefinition("tbsp", null, false, true);

    public static final MeasurementUnitDefinition UNIT_PINCH = new MeasurementUnitDefinition("pinch", UNIT_PIECE, false);

    @ToString.Include private final String name;
    private final String[] variants;
    private final boolean weighed;
    private final boolean normalizable;
    private final MeasurementUnitDefinition parent;

    private final Set<String> set;
    // think about adding conversion to parent operation

    public MeasurementUnitDefinition(String name, MeasurementUnitDefinition parent, boolean weighed, boolean normalizable, String... variants) {
        this.name = name;
        this.weighed = weighed;
        this.variants = variants;
        this.parent = parent;
        this.normalizable = normalizable;

        this.set = Stream.concat(Stream.of(this.name), Stream.of(this.variants)).collect(Collectors.toSet());
    }

    public MeasurementUnitDefinition(String name, MeasurementUnitDefinition parent, boolean normalizable, String... variants) {
        this(name, parent, false, normalizable, variants);
    }

    public Stream<String> getNotationsStream() {
        return set.stream();
    }

    public boolean isNotation(String text) {
        return set.contains(text);
    }
}
