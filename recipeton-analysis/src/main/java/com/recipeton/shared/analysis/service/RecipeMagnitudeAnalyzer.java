package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration;
import com.recipeton.shared.util.TextUtil;
import com.recipeton.shared.util.VulgarFraction;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.regex.Matcher;

import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.*;

public class RecipeMagnitudeAnalyzer {

    private final RecipeMagnitudeAnalyzerConfiguration configuration;

    public RecipeMagnitudeAnalyzer(RecipeMagnitudeAnalyzerConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * replaceFirstNumericWithVulgarFractionsWithSpaceToSingleToken
     *
     * @param text
     * @return
     */
    public String formatMagnitude(String text) {
        Matcher match = NUMERIC_AND_VULGAR_FRACTION_WITH_SPACE.matcher(text);
        if (match.find()) {
            return match.replaceAll("$1$2");
        }
        return text;
    }

    public Optional<BigDecimal> getMagnitude(String token) {
        if (TextUtil.isNumeric(token)) {
            return Optional.of(new BigDecimal(token));
        }

        Matcher numericAndVulgarFraction = NUMERIC_AND_VULGAR_FRACTION.matcher(token);
        if (numericAndVulgarFraction.find()) {
            return Optional.of(new BigDecimal(numericAndVulgarFraction.group(1)).add(VulgarFraction.by(numericAndVulgarFraction.group(2)).getValue()));
        }

        Matcher vulgarFraction = VULGAR_FRACTION.matcher(token);
        if (vulgarFraction.find()) {
            return Optional.of(VulgarFraction.by(vulgarFraction.group(0)).getValue());
        }

        return Optional.ofNullable(configuration.getNumericStrings().get(token.toLowerCase())).map(BigDecimal::new);
    }

    public boolean isRangeSeparator(String split) {
        return configuration.getRangeSeparator().equals(split);
    }
}
