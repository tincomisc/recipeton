package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeLangAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionTokenization;
import com.recipeton.shared.util.TextUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeLangAnalyzerConfiguration.*;
import static com.recipeton.shared.util.MapUtil.toPairOfParallel;
import static com.recipeton.shared.util.TextUtil.normalizeAscii;
import static com.recipeton.shared.util.TextUtil.removeAll;

@RequiredArgsConstructor
public class RecipeLangAnalyzer {

    private final RecipeLangAnalyzerConfiguration configuration;
    private final Pair<String[], String[]> textNormalizeReplacements;
    private final Pair<String[], String[]> languageNormalizeReplacements;

    private final Pattern startsWithStopWordRegex;

    public RecipeLangAnalyzer(RecipeLangAnalyzerConfiguration configuration) {
        this.configuration = configuration;

        startsWithStopWordRegex = Pattern.compile("^(?i)\\b(?:" + configuration.getStopWords().stream().map(Pattern::quote).collect(Collectors.joining("|")) + ")\\b[ ]?");

        textNormalizeReplacements = toPairOfParallel(configuration.getTextNormalizeReplacements());
        languageNormalizeReplacements = toPairOfParallel(configuration.getLanguageNormalizeReplacements());
    }

    public Pattern getStartsWithStopWordRegex() {
        return startsWithStopWordRegex;
    }

    public String formatHumanReadable(String text) {
        return removeAll(text, DELIMITERS_PATTERN);
    }

    public Collection<String> getStopWords() {
        return configuration.getStopWords();
    }

    public RecipeInstructionTokenization getTokenization(String text, int lengthMin) {
        String textLangCleaned = normalizeLanguage(text);

        String[] splits = textLangCleaned.split(configuration.getTokenSplitRegex());
        if (splits.length <= 0) {
            return new RecipeInstructionTokenization(text, textLangCleaned, Collections.emptyList(), Collections.emptyList());
        }

        List<String> tokens = new ArrayList<>();
        List<RecipeInstructionTokenization.Position> positions = new ArrayList<>();
        int offset = 0;
        int splitIndex = 0;
        do {
            String split = splits[splitIndex];
            String cleaned = removeAll(split, configuration.getTokenCleanPattern()).toLowerCase();
            String trimmed = cleaned.trim();
            if (!configuration.getStopWords().contains(trimmed)) {
                String formatted = configuration.getTokenFormatFunction().apply(trimmed);
                String normalized = normalizeAscii(formatted).trim();
                if (normalized.length() >= lengthMin) {
                    tokens.add(normalized);
                    positions.add(new RecipeInstructionTokenization.Position(offset, split.length())); // NOPMD BS
                }
            }
            offset += split.length() + 1;
            splitIndex++;
        } while (splitIndex < splits.length);

        return new RecipeInstructionTokenization(text, textLangCleaned, tokens, positions);
    }

    public RecipeInstructionTokenization getTokenization(String text) {
        return getTokenization(text, TOKEN_LENGTH_MIN);
    }

    public String[] getTokens(String preparationText, Set<String> excludeTokens) {
        return getTokens(preparationText, excludeTokens, TOKEN_LENGTH_MIN);
    }

    public String[] getTokens(String preparationText, Set<String> excludeTokens, int lengthMin) {
        String cleaned = removeAll(preparationText, configuration.getTokenCleanPattern()).toLowerCase();
        return Stream.of(cleaned.split(configuration.getTokenSplitRegex()))
                .map(String::trim)
                .filter(t -> t.length() > lengthMin)
                .filter(t -> !configuration.getStopWords().contains(t))
                .filter(t -> !excludeTokens.contains(t))
                .map(t -> configuration.getTokenFormatFunction().apply(t))
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(TextUtil::normalizeAscii)
                .toArray(String[]::new);
    }

    public String[] getTokens(String text) {
        return getTokens(text, TOKEN_LENGTH_MIN);
    }

    public String[] getTokens(String text, int lengthMin) {
        String cleaned = removeAll(text, configuration.getTokenCleanPattern()).toLowerCase();
        return Stream.of(cleaned.split(configuration.getTokenSplitRegex()))
                .map(String::trim)
                .filter(t -> !configuration.getStopWords().contains(t))
                .map(configuration.getTokenFormatFunction())
                .filter(StringUtils::isNotBlank)
                .map(TextUtil::normalizeAscii)
                .filter(t -> t.length() >= lengthMin)
                .toArray(String[]::new);
    }

    public String normalizeLanguage(String text) {
        return StringUtils.replaceEach(text, languageNormalizeReplacements.getKey(), languageNormalizeReplacements.getValue()).trim();
    }

    public String normalizeText(String text) {
        if (text == null) {
            return null;
        }

        String cleanReplacementsApplied = StringUtils.replaceEach(text, textNormalizeReplacements.getKey(), textNormalizeReplacements.getValue()).trim();
        return PATTERN_MULTIPLE_SPACES.matcher(configuration.getPunctuationFixPattern().matcher(cleanReplacementsApplied).replaceAll("$0 ")).replaceAll(" ");
    }

    public String toUid(String... tokens) {
        return Stream.of(tokens).sorted(Comparator.reverseOrder()).collect(Collectors.joining(""));
    }

    public String toUid(String text) {
        return toUid(getTokens(text, 1));
    }
}
