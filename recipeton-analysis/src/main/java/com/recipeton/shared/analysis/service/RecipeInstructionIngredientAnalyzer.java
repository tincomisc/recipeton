package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeIngredientAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeInstructionAnalyzerConfiguration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.regex.Pattern.compile;

public class RecipeInstructionIngredientAnalyzer {
    /** Sometimes the fuzzy match may end in comma. so that should be OK What is not acceptable is "," or and in the middle */
    public static final int POSSIBLE_INGREDIENT_SEPARATORS_ACCEPTABLE_MARGIN = 3;

    private final RecipeToolAnalyzer recipeToolAnalyzer;
    private final RecipeInstructionAnalyzerConfiguration instructionAnalyzerConfiguration;

    private final Pattern ingredientForbiddenPattern;

    public RecipeInstructionIngredientAnalyzer(RecipeToolAnalyzer recipeToolAnalyzer, RecipeInstructionAnalyzerConfiguration instructionAnalyzerConfiguration, RecipeIngredientAnalyzerConfiguration ingredientAnalyzerConfiguration) {
        this.instructionAnalyzerConfiguration = instructionAnalyzerConfiguration;
        this.recipeToolAnalyzer = recipeToolAnalyzer;

        String joined = ingredientAnalyzerConfiguration.getIngredientForbiddenRegexes().stream().map(String::toLowerCase).map(String::trim).collect(Collectors.joining("|"));
        ingredientForbiddenPattern = compile("(?i)\\b(?:" + joined + ")\\b");
    }

    public int getRatioMin(int textLength) {
        return textLength <= instructionAnalyzerConfiguration.getIngredientShortSearchSubstringLengthMax()
                ? instructionAnalyzerConfiguration.getIngredientMatchRatioMinSpecific()
                : instructionAnalyzerConfiguration.getIngredientMatchRatioMin();
    }

    public boolean isIngredientForbiddenMatch(String text) {
        return ingredientForbiddenPattern.matcher(text).find();
    }

    // In the future check overlaps with previously detected (non ingredient) terms in RIA
    // STILL I think that is needed to discard commas
    public boolean isPossiblyIngredient(String text) {
        Matcher matcher = instructionAnalyzerConfiguration.getRecipeInstructionIngredientSeparatorPattern().matcher(text);
        if (matcher.find() && (isSeparatorInMiddleOfText(text, matcher.start()) || isSeparatorInMiddleOfText(text, matcher.end()))) {
            return false;
        }
        return !recipeToolAnalyzer.isToolPresent(text) && !isIngredientForbiddenMatch(text);
    }

    private boolean isSeparatorInMiddleOfText(String text, int start) {
        return start > POSSIBLE_INGREDIENT_SEPARATORS_ACCEPTABLE_MARGIN && start < text.length() - POSSIBLE_INGREDIENT_SEPARATORS_ACCEPTABLE_MARGIN;
    }

    public boolean isSimilarLength(int length1, int length2) {
        return Math.abs(length1 - length2) <= instructionAnalyzerConfiguration.getIngredientSearchComparisonMaxLengthDiff();
    }
}
