package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeInstructionIngredientAnalyzer;
import com.recipeton.shared.analysis.service.RecipeMagnitudeAnalyzer;
import com.recipeton.shared.analysis.service.RecipeMeasurementAnalyzer;
import lombok.extern.slf4j.Slf4j;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class IngredientRecipeInstructionAnalysisFilter implements RecipeInstructionAnalysisFilter {
    public static final int UNIT_INGREDIENT_MAX_INTERMEDIATE_STOPWORDS = 2; // To config
    public static final int OFFSET_3 = 3;
    public static final int OFFSET_2 = 2;
    public static final int UNIT_WITH_RANGE_TOKEN_COUNT_MIN = 4;
    // Move somewhere else
    private final RecipeIngredientAnalysis recipeIngredientAnalysis;

    private final String ingredientTextSearch;
    private final int ingredientTokenCount;

    // Eventually make this depend on RecipeInstructionAnalyzer and move all the parsing there
    private final RecipeMeasurementAnalyzer recipeMeasurementAnalyzer;
    private final RecipeMagnitudeAnalyzer recipeMagnitudeAnalyzer;

    private final RecipeInstructionIngredientAnalyzer recipeInstructionIngredientAnalyzer;

    public IngredientRecipeInstructionAnalysisFilter(
            RecipeIngredientAnalysis recipeIngredientAnalysis,
            RecipeMagnitudeAnalyzer recipeMagnitudeAnalyzer,
            RecipeMeasurementAnalyzer recipeMeasurementAnalyzer,
            RecipeInstructionIngredientAnalyzer recipeInstructionIngredientAnalyzer,
            String... ingredientTokens) {
        this.recipeIngredientAnalysis = recipeIngredientAnalysis;

        this.recipeMagnitudeAnalyzer = recipeMagnitudeAnalyzer;
        this.recipeMeasurementAnalyzer = recipeMeasurementAnalyzer;
        this.recipeInstructionIngredientAnalyzer = recipeInstructionIngredientAnalyzer;

        ingredientTextSearch = String.join(" ", ingredientTokens);
        ingredientTokenCount = ingredientTokens.length;
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (recipeInstructionAnalysis.isTagPresent(RecipeInstructionTag.INGREDIENTS_ALL)) {
            recipeInstructionAnalysis.addTerm(RecipeIngredientTerm.ofMatchOnAll(recipeIngredientAnalysis, "__ALL__"));
            return recipeInstructionAnalysis;
        }

        RecipeInstructionTokenization tokenization = recipeInstructionAnalysis.getTokenization();
        int tokenizationLength = tokenization.getLength();
        for (int i = 0; i < tokenizationLength; i++) {
            int fromTokenIndex = i;
            int toTokenIndex = Math.min(i + ingredientTokenCount, tokenizationLength);

            // Should just check for overlaps
            //            if (!recipeInstructionIngredientAnalyzer.isPossiblyIngredient(tokenization, fromTokenIndex, toTokenIndex)) {
            //                continue;
            //            }

            if (!isSimilarLength(tokenization, fromTokenIndex, toTokenIndex)) {
                continue;
            }

            // Should just check for overlaps of commands, actions and tools (when all active) and discard if overlapping
            RecipeInstructionTokenization.Position positionFrom = tokenization.getPositions().get(fromTokenIndex);
            if (toTokenIndex == 0) {
                log.error("");
            }
            RecipeInstructionTokenization.Position positionTo = tokenization.getPositions().get(toTokenIndex - 1);
            String matchText = tokenization.getText().substring(positionFrom.getOffset(), positionTo.getOffsetEnd()).trim();

            if (!recipeInstructionIngredientAnalyzer.isPossiblyIngredient(matchText)) {
                continue;
            }

            String recipeInstructionTokenSubstring = String.join(" ", tokenization.getTokens().subList(fromTokenIndex, toTokenIndex));
            int ratio = FuzzySearch.weightedRatio(ingredientTextSearch, recipeInstructionTokenSubstring);
            if (ratio >= recipeInstructionIngredientAnalyzer.getRatioMin(recipeInstructionTokenSubstring.length())) {
                int offset = positionFrom.getOffset();
                recipeInstructionAnalysis.addTerm(createRecipeIngredientTerm(recipeInstructionAnalysis, ratio, offset, matchText));
            }
        }

        return recipeInstructionAnalysis;
    }

    private RecipeIngredientTerm createRecipeIngredientTerm(RecipeInstructionAnalysis recipeInstructionAnalysis, int ratio, int offset, String matchText) {
        RecipeIngredientTerm recipeIngredientTerm = new RecipeIngredientTerm(recipeIngredientAnalysis, new RecipeInstructionMatch(offset, matchText, ratio, "fuzz?" + ingredientTextSearch), false);
        RecipeMeasurementUnitAnalysis measurementUnitAnalysis = recipeIngredientAnalysis.getUnit();
        if (measurementUnitAnalysis == null || measurementUnitAnalysis.isContextual() && measurementUnitAnalysis.isWeighted()) {
            // There should be nothing before. Weighted should have unit!
            return recipeIngredientTerm;
        }

        if (!recipeIngredientTerm.isLocalMagnitudeOverride()) {
            // Could remove this and just jump forward to unit or to existing magnitude (f contextual) and do back parse from there...
            // Another (much better) idea is to just scan for all magnitudes in a different filter and match by distance at a later stage
            tryEnrichLocalMagnitudeFromInside(matchText, recipeIngredientTerm, measurementUnitAnalysis);
        }
        if (!recipeIngredientTerm.isLocalMagnitudeOverride()) {
            int unitEndOffset = getUnitEndOffset(matchText, recipeIngredientAnalysis.getUnit().getUnitDefinition());
            tryEnrichLocalMagnitudeByBackParse(recipeInstructionAnalysis, unitEndOffset > 0 ? offset + unitEndOffset : offset, recipeIngredientTerm, measurementUnitAnalysis);
        }
        return recipeIngredientTerm;
    }

    private int getUnitEndOffset(String text, MeasurementUnitDefinition unitDefinition) {
        int i = 0;
        while (i < i + text.length()) {
            int nextI = text.indexOf(' ', i);
            if (nextI == -1) {
                break;
            }

            String token = text.substring(i, nextI);
            if (unitDefinition.isNotation(token)) {
                return i + token.length();
            }
            i = nextI + 1;
        }
        return -1;
    }

    private boolean isSimilarLength(RecipeInstructionTokenization tokenization, int fromTokenIndex, int toTokenIndex) {
        int recipeInstructionTokenSubstringLength = 0;
        for (int k = fromTokenIndex; k < toTokenIndex; k++) {
            recipeInstructionTokenSubstringLength += tokenization.getTokens().get(k).length();
        }
        recipeInstructionTokenSubstringLength += (toTokenIndex - fromTokenIndex) - 1; // spaces

        return recipeInstructionIngredientAnalyzer.isSimilarLength(recipeInstructionTokenSubstringLength, ingredientTextSearch.length());
    }

    private void tryEnrichLocalMagnitudeByBackParse(RecipeIngredientTerm recipeIngredientTerm, String previousText) {
        String[] splits = previousText.split(" ");
        if (splits.length == 0) {
            return;
        }

        Optional<BigDecimal> closestMagnitude = recipeMagnitudeAnalyzer.getMagnitude(splits[splits.length - 1]);
        if (!closestMagnitude.isPresent()) {
            return;
        }

        if (splits.length > OFFSET_2) {
            String separatorToken = splits[splits.length - OFFSET_2];
            String furtherMagnitudeToken = splits[splits.length - OFFSET_3];

            Optional<BigDecimal> furtherMagnitude = recipeMagnitudeAnalyzer.getMagnitude(furtherMagnitudeToken);
            if (recipeMagnitudeAnalyzer.isRangeSeparator(separatorToken) && furtherMagnitude.isPresent()) {
                recipeIngredientTerm.setLocalMagnitudeFromValue(furtherMagnitude.get());
                recipeIngredientTerm.setLocalMagnitudeToValue(closestMagnitude.get());
                return;
            }
        }

        recipeIngredientTerm.setLocalMagnitudeFromValue(closestMagnitude.get());
        recipeIngredientTerm.setLocalMagnitudeToValue(null);
    }

    private void tryEnrichLocalMagnitudeByBackParse(RecipeInstructionAnalysis recipeInstructionAnalysis, int offset, RecipeIngredientTerm recipeIngredientTerm, RecipeMeasurementUnitAnalysis measurementUnitAnalysis) {
        String text = recipeInstructionAnalysis.getText();
        String previousText = text.substring(0, offset).trim();
        if (StringUtils.isBlank(previousText)) {
            return;
        }
        // Unit may be before match
        if (measurementUnitAnalysis.isContextual()) {
            // Has to be some unit of something. (1 banana, a banana) just check if previous is a number
            // 1 - 3 banana
            // 1 cucharadita de cafe
            tryEnrichLocalMagnitudeByBackParse(recipeIngredientTerm, previousText);
            return;
        }

        Set<String> intermediateStopWords = recipeMeasurementAnalyzer.getIntermediateStopWordsByUnitDefinition(recipeIngredientAnalysis.getUnit().getUnitDefinition());

        // 1 <unit> of ....
        // 1 - 2 <unit> of ....
        // 1 <unit> of the ....
        // 1 - 2 <unit> of the ....
        for (int i = 0; i < UNIT_INGREDIENT_MAX_INTERMEDIATE_STOPWORDS; i++) {
            String lastWord = StringUtils.substringAfterLast(previousText, " ");
            if (intermediateStopWords.contains(lastWord)) {
                previousText = StringUtils.removeEnd(previousText, lastWord).trim();
            } else {
                break;
            }
        }

        // 1 <unit> ....
        // 1 - 2 <unit> ....
        Optional<String> unit = recipeIngredientAnalysis.getUnit().getUnitDefinition().getNotationsStream().filter(previousText::endsWith).findFirst();
        if (unit.isPresent()) {
            previousText = StringUtils.removeEnd(previousText, unit.get()).trim();
        } else {
            return;
        }

        // 1 <unit> ....
        // 1 - 2 <unit> ....
        tryEnrichLocalMagnitudeByBackParse(recipeIngredientTerm, previousText);
    }

    private void tryEnrichLocalMagnitudeFromInside(String matchText, RecipeIngredientTerm recipeIngredientTerm, RecipeMeasurementUnitAnalysis measurementUnitAnalysis) {
        String[] splits = matchText.split(" ");
        boolean longEnough = splits.length > 1;
        if (!longEnough) {
            return;
        }

        BigDecimal firstMagnitude = recipeMagnitudeAnalyzer.getMagnitude(splits[0]).orElse(null);
        if (firstMagnitude == null) {
            return;
        }

        if (measurementUnitAnalysis.isContextual()) {
            // 1 banana
            recipeIngredientTerm.setLocalMagnitudeFromValue(firstMagnitude);
            return;
        }

        if (recipeIngredientAnalysis.getUnit().getUnitDefinition().isNotation(splits[1])) {
            recipeIngredientTerm.setLocalMagnitudeFromValue(firstMagnitude);
            return;
        }

        if (recipeMagnitudeAnalyzer.isRangeSeparator(splits[1]) && splits.length > UNIT_WITH_RANGE_TOKEN_COUNT_MIN) {
            BigDecimal secondMagnitude = recipeMagnitudeAnalyzer.getMagnitude(splits[OFFSET_2]).orElse(null);
            if (secondMagnitude != null && recipeIngredientAnalysis.getUnit().getUnitDefinition().isNotation(splits[OFFSET_3])) {
                recipeIngredientTerm.setLocalMagnitudeFromValue(firstMagnitude);
                recipeIngredientTerm.setLocalMagnitudeToValue(secondMagnitude);
            }
        }
    }
}
