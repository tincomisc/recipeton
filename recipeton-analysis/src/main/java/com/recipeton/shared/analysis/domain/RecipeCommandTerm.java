package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;

@Data
@Accessors(chain = true)
public class RecipeCommandTerm implements RecipeMatchedTerm, RecipeTagged {
    private static final RecipeInstructionTag RECIPE_ANALYSIS_TAG = RecipeInstructionTag.DO_COMMAND_MAIN;

    @EqualsAndHashCode.Include private final String text;
    @EqualsAndHashCode.Include private final RecipeInstructionMatch match;

    private RecipeCommandRotation rotation;
    private Duration duration;
    private BigDecimal speed;
    private Long temperature;
    private Integer repetitionFrom;
    private Integer repetitionTo;
    private RecipeCommandProgram program;

    @Override
    public int getLength() {
        return text.length();
    }

    @Override
    public Collection<RecipeTag> getTags() {
        return Collections.singleton(RECIPE_ANALYSIS_TAG);
    }

    @Override
    public boolean isConclusive() {
        return isValid();
    }

    @Override
    public boolean isTagPresent(RecipeTag tag) {
        return RECIPE_ANALYSIS_TAG.equals(tag);
    }

    public boolean isValid() {
        return duration != null || program != null || temperature != null || speed != null;
    }
}
