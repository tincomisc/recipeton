package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.regex.Pattern.compile;

public final class TextUtil {

    public static final Pattern BETWEEN_PARENTHESES_PATTERN = compile("\\(.*\\)");
    public static final Pattern SENTENCE_SEPARATOR_PATTERN = compile("[,\\.]");
    public static final Pattern NUMERIC_PATTERN = compile("-?\\d+(\\.\\d+)?");
    public static final Pattern ASCII_PATTERN = compile("[^\\p{ASCII}]");
    public static final Pattern EOL_PATTERN = compile("\n");
    public static final Pattern SPACE_PATTERN = compile(" ");

    public static final Pattern NON_LETTER_OR_DIGIT_PATTERN = compile("[^a-zA-Z0-9]");

    private TextUtil() {}

    public static String capitalizeFirst(String text) {
        return StringUtils.capitalize(text.trim().toLowerCase());
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return NUMERIC_PATTERN.matcher(strNum).matches();
    }

    public static String normalizeAscii(String text) {
        return removeAll(Normalizer.normalize(text, Normalizer.Form.NFD), ASCII_PATTERN);
    }

    public static String normalizeAsciiFirstChar(String text) {
        if (StringUtils.isBlank(text)) {
            return text;
        }
        String first = text.substring(0, 1);
        return normalizeAscii(first) + text.substring(1);
    }

    public static String removeAll(String text, Pattern pattern) {
        return replaceAll(text, pattern, StringUtils.EMPTY);
    }

    public static String replaceAll(String text, Pattern pattern, String replacement) {
        if (text == null) {
            return null;
        }
        return pattern.matcher(text).replaceAll(replacement);
    }

    public static Predicate<String> stringPredicatePattern(String s) {
        return compile(s).asPredicate();
    }

    public static List<Predicate<String>> stringPredicatePatterns(String... s) {
        return Arrays.stream(s).map(TextUtil::stringPredicatePattern).collect(Collectors.toList());
    }
}
