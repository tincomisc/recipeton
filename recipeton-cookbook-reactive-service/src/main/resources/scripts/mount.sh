#!/bin/sh -e

###
# #%L
# recipeton-cookbook-service
# %%
# Copyright (C) 2021 tincomisc
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#
printf "Loading module gmasstorage. file=$1"
/sbin/modprobe -v g_mass_storage file=$1 stall=0 idVendor=0x090c idProduct=0x1000 iManufacturer="General" iProduct="USB Flash Disk" iSerialNumber="0 0;exit 0" bcdDevice=0x1100 cdrom=y ro=y
printf "gmasstorage module loaded"

exit 0
