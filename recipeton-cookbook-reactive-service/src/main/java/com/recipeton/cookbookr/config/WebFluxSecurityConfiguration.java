package com.recipeton.cookbookr.config;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;
import reactor.core.publisher.Mono;

@Slf4j
@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity // This may not work in native mode
public class WebFluxSecurityConfiguration {

    public static final String ANONYMOUS = "ANONYMOUS";
    public static final String GUEST = "guest";

    @Value("${recipeton.server.security.enabled:true}")
    private boolean securityEnabled;

    @Value("${recipeton.server.security.username}")
    private String username;

    @Value("${recipeton.server.security.password}")
    private String password;

    @Value("${recipeton.server.security.role:USER}")
    private String role;

    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        UserDetails user = User.withUsername(username).password(passwordEncoder().encode(password)).roles(role).build();

        return new MapReactiveUserDetailsService(user);
    }

    @Bean
    public SecurityWebFilterChain webSessionSpringSecurityFilterChain(ServerHttpSecurity http) {
        http.authorizeExchange()
                .pathMatchers(HttpMethod.POST, "/api/cookbook/{cookbookId}/publish")
                .hasRole(role)
                //                .pathMatchers("/api/user/**").permitAll()
                //                .pathMatchers("/index.html", "/").permitAll()
                //                .pathMatchers("/login", "/logout").permitAll()
                .pathMatchers("/**")
                .permitAll();
        //                .anyExchange().permitAll();//.authenticated();

        if (!securityEnabled) {
            http.anonymous().principal(GUEST).authorities(role);
        }
        http.httpBasic().securityContextRepository(new WebSessionServerSecurityContextRepository());
        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(
                        (exchange, authentication) -> {
                            log.info("logged out {}", authentication);
                            return Mono.empty();
                        }); // Avoid redirect to login?logout

        http.formLogin().disable();
        http.csrf().disable();
        //        http.csrf().csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse());
        http.exceptionHandling().authenticationEntryPoint((exchange, exception) -> Mono.error(exception)).accessDeniedHandler((exchange, exception) -> Mono.error(exception));
        //        http.cors(); //        CORS Configuration isn't completely supported yet by Reactive Security??

        return http.build();
    }

    // SAMPLE
    //    @Bean
    //    public WebFilter corsFilter() {
    //        return (ServerWebExchange ctx, WebFilterChain chain) -> {
    //            ServerHttpRequest request = ctx.getRequest();
    //            if (CorsUtils.isCorsRequest(request)) {
    //                // Add an header on the response explaining we are allowing other domains
    //                ServerHttpResponse response = ctx.getResponse();
    //                HttpHeaders headers = response.getHeaders();
    //                headers.add("Access-Control-Allow-Origin", "*");
    //                headers.add("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    //                headers.add("Access-Control-Max-Age", "3600");
    //                headers.add("Access-Control-Allow-Headers", "x-requested-with, authorization, Content-Type, Authorization, credential,
    // X-XSRF-TOKEN");
    //                // Specific for preflight request.
    //                if (request.getMethod() == HttpMethod.OPTIONS) {
    //                    response.setStatusCode(HttpStatus.OK);
    //                    return Mono.empty();
    //                }
    //            }
    //            return chain.filter(ctx);
    //        };
    //    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
