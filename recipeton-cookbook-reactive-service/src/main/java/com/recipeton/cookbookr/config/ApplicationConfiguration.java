package com.recipeton.cookbookr.config;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.util.storage.StorageMountParams;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "recipeton")
// Spring-Native issues re-evaluate in the future
// @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
// @EnableGlobalMethodSecurity(prePostEnabled = true)
@Data
public class ApplicationConfiguration {

    private Path dataRootPath;
    private List<String> dataRootPathExclusions = new ArrayList<>();

    private String cookbookImageExtension = "iso";
    private String cookbookContentsExtension = "json";
    private String cookbookContentsListExtension = "lst";

    private StorageMountParams storageMount = new StorageMountParams();
}
