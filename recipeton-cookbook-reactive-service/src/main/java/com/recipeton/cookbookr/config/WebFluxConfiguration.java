package com.recipeton.cookbookr.config;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.Map;
import java.util.Optional;

@Configuration
@EnableWebFlux
public class WebFluxConfiguration implements WebFluxConfigurer {
    private static final int BEAN_ORDER = -2;

    //    @Bean
    //    public MethodValidationPostProcessor methodValidationPostProcessor() {
    //        return new MethodValidationPostProcessor();
    //    }

    // SAMPLE
    @Override
    public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
        //        configurer.defaultCodecs().enableLoggingRequestDetails(true);
    }

    // SAMPLE
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //        registry.addResourceHandler("/resources/**")
        //                .addResourceLocations("/public", "classpath:/static/", "classpath:/public/") ...
        //                .setCacheControl(CacheControl.maxAge(365, TimeUnit.DAYS));
        //        .resourceChain(true)
        //                .addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
    }

    //    @Bean
    //    public NettyContext nettyContext(ApplicationContext context) {
    //        HttpHandler handler = WebHttpHandlerBuilder.applicationContext(context).build();
    //        ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(handler);
    //        HttpServer httpServer = HttpServer.create("localhost", 8080);
    //        return httpServer.newHandler(adapter).block();
    //    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        //        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
        //        registrar.setUseIsoFormat(true);
        //        registrar.registerFormatters(registry);
    }

    @Bean
    @Order(BEAN_ORDER)
    public GlobalErrorWebExceptionHandler globalErrorWebExceptionHandler(ErrorAttributes errorAttributes, ServerCodecConfigurer serverCodecConfigurer, ApplicationContext applicationContext) {
        // Limit depending on profile
        ErrorAttributeOptions errorAttributeOptions = ErrorAttributeOptions.defaults().including(ErrorAttributeOptions.Include.EXCEPTION, ErrorAttributeOptions.Include.STACK_TRACE);
        return new GlobalErrorWebExceptionHandler(errorAttributes, serverCodecConfigurer, applicationContext, errorAttributeOptions);
    }

    public static class GlobalErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {

        private final ErrorAttributeOptions errorAttributeOptions;

        public GlobalErrorWebExceptionHandler(ErrorAttributes errorAttributes, ServerCodecConfigurer serverCodecConfigurer, ApplicationContext applicationContext, ErrorAttributeOptions errorAttributeOptions) {
            super(errorAttributes, new WebProperties.Resources(), applicationContext);
            super.setMessageWriters(serverCodecConfigurer.getWriters());
            super.setMessageReaders(serverCodecConfigurer.getReaders());
            this.errorAttributeOptions = errorAttributeOptions;
        }

        @Override
        protected RouterFunction<ServerResponse> getRoutingFunction(final ErrorAttributes errorAttributes) {
            return RouterFunctions.route(
                    RequestPredicates.all(),
                    r -> {
                        final Map<String, Object> errorPropertiesMap = getErrorAttributes(r, errorAttributeOptions);
                        int status = Optional.ofNullable(errorPropertiesMap.get("status")).filter(v -> v instanceof Integer).map(v -> (Integer) v).orElse(HttpStatus.BAD_REQUEST.value());
                        return ServerResponse.status(status).contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(errorPropertiesMap));
                    });
        }
    }
}
