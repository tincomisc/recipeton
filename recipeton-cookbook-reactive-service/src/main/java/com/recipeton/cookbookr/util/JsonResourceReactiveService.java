package com.recipeton.cookbookr.util;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recipeton.shared.util.ObjectMapperFactory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class JsonResourceReactiveService {

    private final ObjectMapper jsonObjectMapper;

    public JsonResourceReactiveService() {
        jsonObjectMapper = new ObjectMapperFactory().createInstance(false);
    }

    public <T> Mono<List<T>> readCollection(Path path, Class<T> clazz) {
        return Mono.fromCallable(
                        () -> {
                            try (InputStream is = Files.newInputStream(path)) {
                                return jsonObjectMapper.<List<T>>readValue(is, jsonObjectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
                            }
                        })
                .subscribeOn(Schedulers.boundedElastic());
    }
}
