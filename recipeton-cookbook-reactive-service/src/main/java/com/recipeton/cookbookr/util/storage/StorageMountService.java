package com.recipeton.cookbookr.util.storage;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.util.ProcessExecutionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;

@Slf4j
@Service
@AllArgsConstructor
public class StorageMountService {

    private final ProcessExecutionService processExecutionService;

    public int doMount(Path path, StorageMountParams storageMountParams) throws IOException, InterruptedException {
        String mountCommand =
                String.format(
                        "sudo modprobe -v g_mass_storage file=%s stall=0 idVendor=%s idProduct=%s iManufacturer=\"%s\" iProduct=\"%s\" iSerialNumber=\"%s\" bcdDevice=%s cdrom=y ro=y\n",
                        path,
                        storageMountParams.getUsbVendor(),
                        storageMountParams.getUsbProduct(),
                        storageMountParams.getUsbManufacturer(),
                        storageMountParams.getUsbProductString(),
                        storageMountParams.getUsbSerial(),
                        storageMountParams.getUsbBcdDevice());
        return processExecutionService.exec(mountCommand, c -> log.info("exec:" + c));
    }

    public int doUmount() throws IOException, InterruptedException {
        String umountCommand = "sudo rmmod -v g_mass_storage";
        return processExecutionService.exec(umountCommand, c -> log.info("exec:" + c));
    }
}
