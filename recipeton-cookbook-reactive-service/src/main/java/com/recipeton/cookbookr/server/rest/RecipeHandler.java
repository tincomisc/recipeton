package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.service.RecipeReactiveService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Component
@RequiredArgsConstructor
public class RecipeHandler {

    // To config!
    public static final Duration CACHE_DURATION = Duration.ofDays(365);

    private final RecipeReactiveService recipeReactiveService;

    public Mono<ServerResponse> getRecipeImageByCookbookIdAndRecipeId(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");
        String recipeId = serverRequest.pathVariable("recipeId");

        return recipeReactiveService
                .getRecipeImagePathByCookbookIdAndRecipeId(cookbookId, recipeId)
                .map(FileSystemResource::new)
                .flatMap(v -> ServerResponse.ok().contentType(MediaType.IMAGE_JPEG).cacheControl(CacheControl.maxAge(CACHE_DURATION)).bodyValue(v))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getRecipeThumbnailByCookbookIdAndRecipeId(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");
        String recipeId = serverRequest.pathVariable("recipeId");

        return recipeReactiveService
                .getRecipeThumbnailPathByCookbookIdAndRecipeId(cookbookId, recipeId)
                .map(FileSystemResource::new)
                .flatMap(v -> ServerResponse.ok().contentType(MediaType.IMAGE_JPEG).cacheControl(CacheControl.maxAge(CACHE_DURATION)).bodyValue(v))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getRecipeByCookbookIdAndRecipeId(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");
        String recipeId = serverRequest.pathVariable("recipeId");

        return recipeReactiveService
                .getRecipePathByCookbookIdAndRecipeId(cookbookId, recipeId)
                .flatMap(recipeReactiveService::readRecipe)
                .flatMap(v -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(v))
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
