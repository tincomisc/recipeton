package com.recipeton.cookbookr.server.web;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class HomeRouter {

    @Bean
    RouterFunction<ServerResponse> staticResourceRouterFunction() {
        return RouterFunctions.resources("/**", new ClassPathResource("public/"));
    }

    @Bean
    RouterFunction<ServerResponse> homeRouterFunction(HomeHandler homeHandler) {
        return RouterFunctions.route().GET("/recipeton/**", homeHandler::displayHome).GET("/", homeHandler::redirectHome).GET("/index.html", homeHandler::redirectHome).GET("/app/**", homeHandler::redirectHome).build();
    }
}
