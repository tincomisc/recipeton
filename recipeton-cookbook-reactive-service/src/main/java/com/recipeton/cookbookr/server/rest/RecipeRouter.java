package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springdoc.core.fn.builders.parameter.Builder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springdoc.core.fn.builders.apiresponse.Builder.responseBuilder;
import static org.springdoc.webflux.core.fn.SpringdocRouteBuilder.route;

@Configuration
public class RecipeRouter {

    @Bean
    RouterFunction<ServerResponse> recipeRouterFunction(RecipeHandler recipeHandler) {
        return route().GET(
                        "/api/cookbook/{cookbookId}/recipe/{recipeId}/image",
                        recipeHandler::getRecipeImageByCookbookIdAndRecipeId,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getRecipeImageByCookbookIdAndRecipeId")
                                        .summary("Get recipe collection recipe image")
                                        .description("Returns recipe image with matching id")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("recipeId").description("Recipe UID"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation"))
                                        .response(responseBuilder().responseCode("404").description("Cookbook or recipeUID not found"))
                                        .build())
                .GET(
                        "/api/cookbook/{cookbookId}/recipe/{recipeId}/thumbnail",
                        recipeHandler::getRecipeThumbnailByCookbookIdAndRecipeId,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getRecipeThumbnailByCookbookIdAndRecipeId")
                                        .summary("Get recipe collection recipe thumbnail")
                                        .description("Returns recipe thumbnail with matching id")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("recipeId").description("Recipe UID"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation"))
                                        .response(responseBuilder().responseCode("404").description("Cookbook or recipeUID not found"))
                                        .build())
                .GET(
                        "/api/cookbook/{cookbookId}/recipe/{recipeId}",
                        recipeHandler::getRecipeByCookbookIdAndRecipeId,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getRecipeByCookbookIdAndRecipeId")
                                        .summary("Get recipe collection recipe definition")
                                        .description("Returns recipe with matching id")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("recipeId").description("Recipe UID"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation").implementation(RecipeDefinition.class))
                                        .response(responseBuilder().responseCode("404").description("Cookbook or recipeUID not found"))
                                        .build())
                .build();
    }
}
