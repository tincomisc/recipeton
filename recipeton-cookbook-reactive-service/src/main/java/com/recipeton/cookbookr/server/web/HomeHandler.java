package com.recipeton.cookbookr.server.web;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

@Component
public class HomeHandler {

    @Value("classpath:/public/index.html")
    private Resource indexResource;

    public Mono<ServerResponse> redirectHome(ServerRequest serverRequest) {
        return ServerResponse.temporaryRedirect(URI.create("/recipeton")).build();
    }

    public Mono<ServerResponse> displayHome(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.TEXT_HTML).bodyValue(indexResource);
    }
}
