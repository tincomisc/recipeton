package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.server.rest.form.CookbookContentsForm;
import com.recipeton.cookbookr.server.rest.form.CookbookForm;
import com.recipeton.cookbookr.server.rest.form.RecipeSummaryForm;
import com.recipeton.cookbookr.service.CookbookReactiveService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.util.Collections;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CookbookHandler {

    private final CookbookReactiveService cookbookReactiveService;

    public Mono<ServerResponse> getCookbooks(ServerRequest serverRequest) {
        return cookbookReactiveService
                .getCookbookPaths()
                .map(x -> x.stream().map(this::toCookbookForm).collect(Collectors.toList()))
                .defaultIfEmpty(Collections.emptyList())
                .flatMap(l -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(l));
    }

    public Mono<ServerResponse> getCookbookById(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");
        return cookbookReactiveService
                .getCookbookPathByCookbookId(cookbookId)
                .map(this::toCookbookForm)
                .flatMap(p -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(p))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getCookbookContentById(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");
        return cookbookReactiveService
                .getCookbookRecipeSummaries(cookbookId)
                .map(c -> new CookbookContentsForm(c.stream().map(s -> new RecipeSummaryForm(s.getUid(), s.getTitle(), s.getKeywords())).collect(Collectors.toList())))
                .flatMap(v -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(v))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> doCookbookPublish(ServerRequest serverRequest) {
        String cookbookId = serverRequest.pathVariable("cookbookId");

        return cookbookReactiveService.doPublish(cookbookId).flatMap(v -> ServerResponse.ok().build()).switchIfEmpty(ServerResponse.notFound().build());
    }

    private CookbookForm toCookbookForm(Path cookbookPath) {
        String id = cookbookReactiveService.toCookbookId(cookbookPath);
        String name = StringUtils.replace(cookbookReactiveService.getCookbookName(cookbookPath), "_", " ");
        return CookbookForm.builder().id(id).name(name).published(cookbookPath.equals(cookbookReactiveService.getMountedCookbookPath())).build();
    }
}
