package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.server.rest.form.CookbookContentsForm;
import com.recipeton.cookbookr.server.rest.form.CookbookForm;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springdoc.core.fn.builders.parameter.Builder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springdoc.core.fn.builders.apiresponse.Builder.responseBuilder;
import static org.springdoc.webflux.core.fn.SpringdocRouteBuilder.route;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class CookbookRouter {

    @Bean
    RouterFunction<ServerResponse> cookbookRouterFunction(CookbookHandler cookbookHandler) {
        return route().GET(
                        "/api/cookbook",
                        accept(MediaType.APPLICATION_JSON),
                        cookbookHandler::getCookbooks,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getCookbooks")
                                        .summary("Get all recipe collections")
                                        .response(responseBuilder().responseCode("200").description("Successful operation").implementationArray(CookbookForm.class))
                                        .build())
                .GET(
                        "/api/cookbook/{cookbookId}",
                        accept(MediaType.APPLICATION_JSON),
                        cookbookHandler::getCookbookById,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getCookbookById")
                                        .summary("Get recipe collection by id")
                                        .description("Returns recipe collection with matching id")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation").implementation(CookbookForm.class))
                                        .response(responseBuilder().responseCode("404").description("Cookbook not found"))
                                        .response(responseBuilder().responseCode("405").description("Validation exception"))
                                        .build())
                .GET(
                        "/api/cookbook/{cookbookId}/content",
                        accept(MediaType.APPLICATION_JSON),
                        cookbookHandler::getCookbookContentById,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("getCookbookContentById")
                                        .summary("Get recipe collection content by id")
                                        .description("Returns recipe collection content with matching id")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation").implementation(CookbookContentsForm.class))
                                        .response(responseBuilder().responseCode("404").description("Cookbook not found"))
                                        .build())
                .POST(
                        "/api/cookbook/{cookbookId}/publish",
                        cookbookHandler::doCookbookPublish,
                        o ->
                                o.tag("Recipe Collection")
                                        .operationId("doCookbookPublish")
                                        .summary("Loads cookbook")
                                        .parameter(Builder.parameterBuilder().in(ParameterIn.PATH).name("cookbookId").description("Cookbook identifier"))
                                        .response(responseBuilder().responseCode("200").description("Successful operation"))
                                        .response(responseBuilder().responseCode("404").description("Cookbook or recipeUID not found"))
                                        .response(responseBuilder().responseCode("500").description("Error performing operation"))
                                        .build())
                .build();
    }
}
