package com.recipeton.cookbookr.service;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.config.ApplicationConfiguration;
import com.recipeton.cookbookr.util.JsonResourceReactiveService;
import com.recipeton.cookbookr.util.storage.StorageMountService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CookbookReactiveService {

    public static final String SPLITTER = "|";

    private final ApplicationConfiguration applicationConfiguration;
    private final StorageMountService storageMountService;
    private final JsonResourceReactiveService jsonResourceReactiveService;

    private final AtomicBoolean publishing = new AtomicBoolean();

    private Path mountedCookbookPath;

    public Mono<Boolean> doPublish(String cookbookId) throws CookbookPublishException {
        return getCookbookPathByCookbookId(cookbookId)
                .flatMap(
                        p ->
                                Mono.fromCallable(
                                                () -> {
                                                    log.info("doPublish path={}", p);
                                                    if (publishing.compareAndSet(false, true)) {
                                                        if (mountedCookbookPath != null && mountedCookbookPath.equals(p)) {
                                                            log.warn("doPublish same cookbookPath. Ignoring. cookbookPath={}", mountedCookbookPath);
                                                            publishing.set(false);
                                                            return Boolean.FALSE;
                                                        }
                                                        String cookbookName = getCookbookName(p);
                                                        Path cookbookImagePath = p.resolve(cookbookName + FilenameUtils.EXTENSION_SEPARATOR + applicationConfiguration.getCookbookImageExtension());

                                                        try {
                                                            log.debug("umounting path={}", cookbookImagePath);
                                                            int umountCode = storageMountService.doUmount();
                                                            log.debug("umount executed. code={}", umountCode);
                                                            int mountResult = storageMountService.doMount(cookbookImagePath, applicationConfiguration.getStorageMount());
                                                            log.debug("mount executed. code={}", mountResult);

                                                            mountedCookbookPath = p;
                                                        } catch (InterruptedException | IOException e) {
                                                            mountedCookbookPath = null;
                                                            throw new CookbookPublishException("Publish failed for id=" + cookbookImagePath.getFileName() + " , reason=" + e.getMessage(), e);
                                                        } finally {
                                                            publishing.set(false);
                                                        }
                                                    } else {
                                                        log.warn("Already publishing. Ignore. cookbookId={}", cookbookId);
                                                    }
                                                    return Boolean.TRUE;
                                                })
                                        .subscribeOn(Schedulers.boundedElastic()));
    }

    public Mono<Path> getCookbookContentPath(String cookbookId) {
        return getCookbookPathByCookbookId(cookbookId).map(p -> p.resolve(FilenameUtils.getBaseName(p.getFileName().toString()) + FilenameUtils.EXTENSION_SEPARATOR + applicationConfiguration.getCookbookContentsExtension()));
    }

    public String getCookbookName(Path path) {
        return FilenameUtils.getBaseName(path.getFileName().toString());
    }

    public Mono<Path> getCookbookPathByCookbookId(String cookbookId) {
        try {
            String decoded = new String(Base64.getUrlDecoder().decode(cookbookId));
            return Mono.just(applicationConfiguration.getDataRootPath().resolve(StringUtils.split(decoded, SPLITTER)[1])).filter(Files::exists);
        } catch (IllegalArgumentException e) {
            return Mono.error(new CookbookInvalidIdException("Could not decode id=" + cookbookId, e));
        }
    }

    // Experiment. Evaluate!
    //    private Flux<Path> fromPath(Path path) {
    //        return Flux.using(() -> Files.walk(path, FileVisitOption.FOLLOW_LINKS), Flux::fromStream, BaseStream::close)
    //                .doOnDiscard(BaseStream.class, BaseStream::close)
    //                .doOnError(
    //                        err -> {
    //                            throw Exceptions.propagate(new RuntimeException("error walking files", err));
    //                        })
    //                .filter(filePath -> !filePath.toFile().isDirectory())
    //                .filter(filePath -> !filePath.getFileName().toString().startsWith("."));
    //    }

    public Mono<List<Path>> getCookbookPaths() {
        return Mono.just(applicationConfiguration.getDataRootPath())
                .filter(Files::exists)
                .flatMap(
                        r ->
                                Mono.fromCallable(
                                                () ->
                                                        Files.walk(r, 2)
                                                                .filter(p -> FilenameUtils.isExtension(p.getFileName().toString().toLowerCase(), applicationConfiguration.getCookbookImageExtension()))
                                                                .filter(p -> p.getParent().getFileName().toString().equals(FilenameUtils.getBaseName(p.getFileName().toString())))
                                                                .map(Path::getParent)
                                                                .filter(p -> !applicationConfiguration.getDataRootPathExclusions().contains(FilenameUtils.getBaseName(p.getFileName().toString())))
                                                                .sorted()
                                                                .collect(Collectors.toList()))
                                        .subscribeOn(Schedulers.boundedElastic()));
    }

    public Mono<List<RecipeSummary>> getCookbookRecipeSummaries(String cookbookId) {
        return getCookbookContentPath(cookbookId).flatMap(p -> jsonResourceReactiveService.readCollection(p, RecipeSummary.class));
    }

    public Path getMountedCookbookPath() {
        return mountedCookbookPath;
    }

    @PostConstruct
    public void postConstruct() throws IOException {
        Files.createDirectories(applicationConfiguration.getDataRootPath());
    }

    public String toCookbookId(Path path) {
        try {
            String text = Files.getLastModifiedTime(path) + SPLITTER + path.getFileName().toString();
            return Base64.getUrlEncoder().withoutPadding().encodeToString(text.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RecipeSummary {
        private String uid;
        private String title;
        private List<String> keywords;
    }
}
