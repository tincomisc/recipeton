package com.recipeton.cookbookr.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.util.ObjectMapperFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeReactiveService {

    // To config
    public static final String SUBPATH_EXPORT_MATERIAL_PHOTO_150_X_150 = "export/material/photo/150x150";
    public static final String SUBPATH_RECIPE = "recipe";
    public static final String EXTENSION_JPG = ".jpg";
    public static final String EXTENSION_YAML = ".yaml";

    private final ObjectMapper objectMapper;
    private final CookbookReactiveService cookbookReactiveService;

    @Autowired
    public RecipeReactiveService(CookbookReactiveService cookbookReactiveService) {
        this.objectMapper = new ObjectMapperFactory().createInstance(true);
        this.cookbookReactiveService = cookbookReactiveService;
    }

    public Mono<Path> getRecipePathByCookbookIdAndRecipeId(String cookbookId, String recipeId) {
        // Should validate that absolute path hangs from data
        return cookbookReactiveService.getCookbookPathByCookbookId(cookbookId).map(p -> p.resolve(SUBPATH_RECIPE).resolve(recipeId + EXTENSION_YAML)).filter(Files::exists);
    }

    public Mono<Path> getRecipeImagePathByCookbookIdAndRecipeId(String cookbookId, String recipeId) {
        // Should validate that absolute path hangs from data
        return cookbookReactiveService.getCookbookPathByCookbookId(cookbookId).map(p -> p.resolve(SUBPATH_RECIPE).resolve(recipeId + EXTENSION_JPG)).filter(Files::exists);
    }

    public Mono<Path> getRecipeThumbnailPathByCookbookIdAndRecipeId(String cookbookId, String recipeId) {
        // Should validate that absolute path hangs from data
        return cookbookReactiveService.getCookbookPathByCookbookId(cookbookId).map(p -> p.resolve(SUBPATH_EXPORT_MATERIAL_PHOTO_150_X_150).resolve(recipeId + EXTENSION_JPG)).filter(Files::exists);
    }

    public Mono<RecipeDefinition> readRecipe(Path path) {
        return Mono.fromCallable(
                        () -> {
                            try (InputStream is = Files.newInputStream(path)) {
                                return objectMapper.readValue(is, RecipeDefinition.class);
                            }
                        })
                .subscribeOn(Schedulers.boundedElastic());
    }
}
