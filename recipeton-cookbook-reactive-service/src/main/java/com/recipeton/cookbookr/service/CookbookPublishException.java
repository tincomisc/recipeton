package com.recipeton.cookbookr.service;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.RegExUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CookbookPublishException extends RuntimeException {

    public CookbookPublishException(String message, Throwable cause, Object... messageArgs) {
        super(format(message, messageArgs), cause);
    }

    public CookbookPublishException(String message) {
        super(message);
    }

    public CookbookPublishException(String message, Object... messageArgs) {
        super(format(message, messageArgs));
    }

    private static String format(String message, Object... args) {
        if (args == null) {
            return message;
        }
        return String.format(RegExUtils.replaceAll(message, "\\{\\}", "%s"), args);
    }
}
