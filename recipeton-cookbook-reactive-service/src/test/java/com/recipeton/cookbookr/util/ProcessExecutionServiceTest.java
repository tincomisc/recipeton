package com.recipeton.cookbookr.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assumptions.assumeThat;

class ProcessExecutionServiceTest {

    private ProcessExecutionService processExecutionService = new ProcessExecutionService();

    @Test
    public void testExecWithStringConsumerAndTimeoutGreaterThanZeroAndProcessExecutedOutofTimeoutLimitGivenExecutedThenThrowsInterruptedException() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        assertThatExceptionOfType(IllegalThreadStateException.class).isThrownBy(() -> processExecutionService.exec("sleep 1", response::append, 1));
    }

    @Test
    public void testExecWithStringConsumerAndTimeoutGreaterThanZeroAndProcessExecutedWithingTimeoutLimitGivenExecutedThenReturnsProcessOutput() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        processExecutionService.exec("sleep 1", response::append, 1500);

        assertThat(response).isEqualToIgnoringNewLines("");
    }

    // it is flaky. Sometimes response = ""
    @Test
    @Disabled
    public void testExecWithStringConsumerGivenExecutedThenReturnsProcessOutput() throws Exception {
        assumeThat(SystemUtils.IS_OS_LINUX).isTrue();

        StringBuilder response = new StringBuilder();
        processExecutionService.exec("which ls", response::append);

        assertThat(response).endsWith("/ls");
    }
}
