package com.recipeton.cookbookr.test.support;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.recipeton.cookbookr.config.ApplicationConfiguration;
import com.recipeton.cookbookr.server.rest.form.RecipeSummaryForm;
import com.recipeton.cookbookr.service.CookbookReactiveService;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.util.ObjectMapperFactory;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.imageio.ImageIO;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
@EnableConfigurationProperties
@ContextConfiguration(initializers = TestContextInitializer.class)
@Tag("integration")
@ActiveProfiles("test")
public class AbstractIntegrationTest implements CookbookReactiveServiceTestFixtureTrait {

    private final ObjectMapper jsonObjectMapper = new ObjectMapperFactory().createInstance(false);
    private final ObjectMapper yamlObjectMapper = new ObjectMapperFactory().createInstance(true);

    @Autowired protected ApplicationConfiguration applicationConfiguration;

    @Autowired protected CookbookReactiveService cookbookReactiveService;

    @Value("${test.root.path}")
    protected Path testRootPath;

    @AfterEach
    public void afterEach() {
        clearFilesystem();
    }

    @SneakyThrows
    public void clearFilesystem() {
        FileUtils.deleteDirectory(getDataDirectory().toFile());
    }

    private Path getDataDirectory() {
        return testRootPath.resolve("tmdata");
    }

    @SneakyThrows
    public void setUpCookbookRecipeImage(String bookName, int width, int height, Path path) {
        Files.createDirectories(path.getParent());
        ImageIO.write(createBufferedImage(bookName, width, height), "jpg", path.toFile());
    }

    @SneakyThrows
    public Path setUpCookbook(String bookName, Map<String, RecipeDefinition> recipeDefinitions) {
        Path cookBookPath = getDataDirectory().resolve(bookName);
        Files.createDirectories(cookBookPath);
        Path cookbookIsoFilePath = createCookbookIsoPath(cookBookPath);
        Path cookbookJsonFilePath = cookBookPath.resolve(bookName + FilenameUtils.EXTENSION_SEPARATOR + "json");
        Path cookbookLstFilePath = cookBookPath.resolve(bookName + FilenameUtils.EXTENSION_SEPARATOR + "lst");
        Files.write(cookbookIsoFilePath, "dummyIsoContent".getBytes(StandardCharsets.UTF_8));

        Path recipeDirectory = cookBookPath.resolve("recipe");
        Files.createDirectories(recipeDirectory);

        for (Map.Entry<String, RecipeDefinition> stringRecipeDefinitionEntry : recipeDefinitions.entrySet()) {
            String recipeID = stringRecipeDefinitionEntry.getKey();
            Path recipePath = recipeDirectory.resolve(recipeID + FilenameUtils.EXTENSION_SEPARATOR + "yaml");

            Path recipeImagePath = createRecipeImagePath(cookBookPath, recipeID);
            Path recipeThumbnailPath = createRecipeThumbnailPath(cookBookPath, recipeID);

            try (OutputStream os = Files.newOutputStream(recipePath)) {
                yamlObjectMapper.writeValue(os, stringRecipeDefinitionEntry.getValue());
            }
            setUpCookbookRecipeImage(recipeID, 1024, 1024, recipeImagePath);
            setUpCookbookRecipeImage(recipeID, 256, 256, recipeThumbnailPath);
        }

        List<CookbookReactiveService.RecipeSummary> recipeSummaries =
                recipeDefinitions.entrySet().stream().map(e -> new CookbookReactiveService.RecipeSummary(e.getKey(), e.getValue().getName(), Collections.emptyList())).collect(Collectors.toList());
        try (OutputStream os = Files.newOutputStream(cookbookJsonFilePath)) {
            jsonObjectMapper.writeValue(os, recipeSummaries);
        }

        return cookBookPath;
    }
}
