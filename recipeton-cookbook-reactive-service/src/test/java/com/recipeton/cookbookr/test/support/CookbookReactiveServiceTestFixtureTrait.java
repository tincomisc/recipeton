package com.recipeton.cookbookr.test.support;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import org.apache.commons.io.FilenameUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public interface CookbookReactiveServiceTestFixtureTrait {

    default BufferedImage createBufferedImage(String text, int width, int height) {
        Font font = new Font("Serif", Font.PLAIN, 32);
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // Careful! if ARGB jpg does not save!!

        Graphics2D g2d = (Graphics2D) img.getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, width, height);
        g2d.setColor(Color.WHITE);
        g2d.drawLine(0, 0, width, height);
        g2d.drawLine(0, width, height, 0);
        g2d.dispose();
        g2d.drawLine(0, width, height, 0);

        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        g2d.drawString(text, 0, fm.getAscent());
        return img;
    }

    default Map<String, RecipeDefinition> createRecipeDefinitions() {
        return createRecipeDefinitions("r00001", createRecipeDefinition("recipe1"));
    }

    default RecipeDefinition createRecipeDefinition(String name) {
        return new RecipeDefinition().setName(name);
    }

    default Map<String, RecipeDefinition> createRecipeDefinitions(String recipeId, RecipeDefinition recipeDefinition) {
        Map<String, RecipeDefinition> recipes = new HashMap<>();
        recipes.put(recipeId, recipeDefinition);
        return recipes;
    }

    default Map<String, RecipeDefinition> createRecipeDefinitions(String recipeId1, RecipeDefinition recipeDefinition1, String recipeId2, RecipeDefinition recipeDefinition2) {
        Map<String, RecipeDefinition> recipes = new HashMap<>();
        recipes.put(recipeId1, recipeDefinition1);
        recipes.put(recipeId2, recipeDefinition2);
        return recipes;
    }

    default Path createRecipeImagePath(Path cookBookPath, String recipeId) {
        return cookBookPath.resolve("recipe").resolve(recipeId + FilenameUtils.EXTENSION_SEPARATOR + "jpg");
    }

    default Path createRecipeThumbnailPath(Path cookBookPath, String recipeId) {
        return cookBookPath.resolve("export").resolve("material").resolve("photo").resolve("150x150").resolve(recipeId + FilenameUtils.EXTENSION_SEPARATOR + "jpg");
    }

    default Path createCookbookIsoPath(Path cookBookPath) {
        String bookName = cookBookPath.getFileName().toString();
        return cookBookPath.resolve(bookName + FilenameUtils.EXTENSION_SEPARATOR + "iso");
    }
}
