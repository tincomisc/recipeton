package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.server.rest.form.UserForm;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserRestIntegrationTest extends AbstractWebIntegrationTest {

    @Test
    public void testMeGivenNoUserReturnsOk() {
        webTestClient.get().uri("/api/user/me").exchange().expectStatus().isOk();
    }

    @Test
    @WithMockUser(
            username = "testUser",
            password = "somePass",
            roles = {"USER"})
    public void testMeGivenExistingUserReturnsOkAndUserName() {
        webTestClient
                .get()
                .uri("/api/user/me")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(UserForm.class)
                .consumeWith(r -> assertThat(r.getResponseBody()).satisfies(f -> assertThat(f.getName()).isEqualTo("testUser")));
    }

    // SAMPLE: For reference
    //    @Test
    //    public void testMeGivenNoUserReturnsOkAndUserNobody() {
    //        Flux<String> msg$ = webTestClient.get()
    //                .uri("/api/user/me")
    //                .exchange()
    //                .expectStatus().isOk()
    //                .returnResult(String.class).getResponseBody()
    //                .expectBody().json("{\"name\":\"testUser\"}")
    //                .jsonPath().hasJsonPath()."$.name", Matchers.is("textUser") )
    //                .log();
    //
    //        msg$.subscribe(System.out::println);
    //
    //        StepVerifier.create(msg$)
    //                .expectNext("xx")
    //                .verifyComplete();
    //        //.expectComplete(); - do not use
    //    }

}
