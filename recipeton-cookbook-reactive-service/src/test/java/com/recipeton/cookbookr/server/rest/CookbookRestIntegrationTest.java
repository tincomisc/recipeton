package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.server.rest.form.CookbookContentsForm;
import com.recipeton.cookbookr.server.rest.form.CookbookForm;
import com.recipeton.cookbookr.util.storage.StorageMountService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.io.IOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

public class CookbookRestIntegrationTest extends AbstractWebIntegrationTest {
    //                        "/api/cookbook/{cookbookId}/publish",

    @MockBean private StorageMountService mockStorageMountService;

    @Test
    public void testGetCookbooksGivenNoCookbooksThenReturnsEmpty() {
        webTestClient.get().uri("/api/cookbook").exchange().expectStatus().isOk().expectHeader().contentType(MediaType.APPLICATION_JSON).expectBodyList(CookbookForm.class).consumeWith(r -> assertThat(r.getResponseBody()).isEmpty());
    }

    @Test
    public void testGetCookbooksGivenCookbookThenReturnsCookbook() {
        String cookbookName = "BOOK1";
        setUpCookbook(cookbookName, createRecipeDefinitions());

        webTestClient
                .get()
                .uri("/api/cookbook")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(CookbookForm.class)
                .consumeWith(r -> assertThat(r.getResponseBody()).singleElement().satisfies(c -> assertThat(c.getName()).isEqualTo(cookbookName)));
    }

    @Test
    public void testGetCookbooksGivenCookbooksThenReturnsCookbooks() throws Exception {
        String cookbookName1 = "BOOK1";
        Path cookbookPath1 = setUpCookbook(cookbookName1, createRecipeDefinitions());
        String cookbookName2 = "BOOK2";
        Path cookbookPath2 = setUpCookbook(cookbookName2, createRecipeDefinitions());

        webTestClient
                .get()
                .uri("/api/cookbook")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(CookbookForm.class)
                .consumeWith(
                        r ->
                                assertThat(r.getResponseBody())
                                        .hasSize(2)
                                        .anySatisfy(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo(cookbookReactiveService.toCookbookId(cookbookPath1));
                                                    assertThat(c.getName()).isEqualTo(cookbookName1);
                                                    assertThat(c.isPublished()).isFalse();
                                                })
                                        .anySatisfy(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo(cookbookReactiveService.toCookbookId(cookbookPath2));
                                                    assertThat(c.getName()).isEqualTo(cookbookName2);
                                                    assertThat(c.isPublished()).isFalse();
                                                }));
    }

    @Test
    public void testGetCookbookByIdGivenInvalidIdThenReturnsStatus405() throws Exception {
        webTestClient.get().uri("/api/cookbook/12345").exchange().expectStatus().isEqualTo(HttpStatus.NOT_ACCEPTABLE.value());
    }

    @Test
    public void testGetCookbookByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.get().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetCookbookByIdGivenCookbookThenReturnsCookbook() throws Exception {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions());
        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath))
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(CookbookForm.class)
                .consumeWith(
                        r ->
                                assertThat(r.getResponseBody())
                                        .satisfies(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo(cookbookReactiveService.toCookbookId(cookbookPath));
                                                    assertThat(c.getName()).isEqualTo(cookbookName);
                                                    assertThat(c.isPublished()).isFalse();
                                                }));
    }

    @Test
    public void testGetCookbookByIdGivenCookbookAndPublishedThenReturnsCookbookAndPublishedIsTrue() throws Exception {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions());

        setUpPublishSuccessfulExpectations(cookbookPath);
        String cookbookId = cookbookReactiveService.toCookbookId(cookbookPath);
        cookbookReactiveService.doPublish(cookbookId).block();

        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookId)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(CookbookForm.class)
                .consumeWith(
                        r ->
                                assertThat(r.getResponseBody())
                                        .satisfies(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo(cookbookReactiveService.toCookbookId(cookbookPath));
                                                    assertThat(c.getName()).isEqualTo(cookbookName);
                                                    assertThat(c.isPublished()).isTrue();
                                                }));
    }

    @Test
    public void testGetCookbookContentByIdGivenInvalidIdThenReturnsStatus405() throws Exception {
        webTestClient.get().uri("/api/cookbook/12345/content").exchange().expectStatus().isEqualTo(HttpStatus.NOT_ACCEPTABLE.value());
    }

    @Test
    public void testGetCookbookContentByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.get().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz/content").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetCookbookContentByIdGivenCookbookThenReturnsCookbookContents() {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions("recipe1", createRecipeDefinition("name1"), "recipe2", createRecipeDefinition("name2")));
        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/content")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(CookbookContentsForm.class)
                .consumeWith(
                        r ->
                                assertThat(r.getResponseBody().getContents())
                                        .hasSize(2)
                                        .anySatisfy(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo("recipe1");
                                                    assertThat(c.getName()).isEqualTo("name1");
                                                })
                                        .anySatisfy(
                                                c -> {
                                                    assertThat(c.getId()).isEqualTo("recipe2");
                                                    assertThat(c.getName()).isEqualTo("name2");
                                                }));
    }

    private void setUpPublishSuccessfulExpectations(Path cookbookPath) throws IOException, InterruptedException {
        Mockito.when(mockStorageMountService.doUmount()).thenReturn(0);
        Mockito.when(mockStorageMountService.doMount(eq(cookbookPath), any())).thenReturn(0);
    }

    @Test
    @WithMockUser(
            username = "testUser",
            password = "somePass",
            roles = {"USER"})
    public void testPostCookbookPublishByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.post().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz/publish").exchange().expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(
            username = "testUser",
            password = "somePass",
            roles = {"USER"})
    public void testPostCookbookPublishByIdGivenCookbookThenPublishesCookbook() throws Exception {
        Path cookbookPath = setUpCookbook("BOOK" + RandomStringUtils.randomNumeric(5, 5), createRecipeDefinitions());
        setUpPublishSuccessfulExpectations(cookbookPath);
        webTestClient.post().uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/publish").exchange().expectStatus().isOk();

        Mockito.verify(mockStorageMountService, Mockito.times(1)).doUmount();
        Mockito.verify(mockStorageMountService, Mockito.times(1)).doMount(eq(createCookbookIsoPath(cookbookPath)), any());
    }

    @Test
    public void testPostCookbookPublishByIdGivenCookbookAndNotUserThenRetunsUnauthorized() throws Exception {
        Path cookbookPath = setUpCookbook("BOOK" + RandomStringUtils.randomNumeric(5, 5), createRecipeDefinitions());
        webTestClient.post().uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/publish").exchange().expectStatus().isUnauthorized();
    }
}
