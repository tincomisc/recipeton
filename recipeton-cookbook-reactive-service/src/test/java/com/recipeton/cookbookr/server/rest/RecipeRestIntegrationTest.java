package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeRestIntegrationTest extends AbstractWebIntegrationTest {

    @Test
    public void testGetRecipeByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.get().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz/recipe/1234").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeByIdGivenNoRecipeWithMatchingIdThenReturnsNotFound() throws Exception {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions("recipe1", createRecipeDefinition("name1")));
        webTestClient.get().uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + "notExistingUid").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeByIdGivenRecipeWIthMatchingIdThenReturnsRecipeComtent() {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        String recipe1 = "recipe1";
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions(recipe1, createRecipeDefinition("name1"), "recipe2", createRecipeDefinition("name2")));
        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + recipe1)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(RecipeDefinition.class)
                .consumeWith(
                        r ->
                                assertThat(r.getResponseBody())
                                        .satisfies(
                                                c -> {
                                                    assertThat(c.getName()).isEqualTo("name1");
                                                }));
    }

    @Test
    public void testGetRecipeImageByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.get().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz/recipe/1234/image").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeImageByIdGivenNoRecipeWithMatchingIdThenReturnsNotFound() throws Exception {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions("recipe1", createRecipeDefinition("name1")));
        webTestClient.get().uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + "notExistingUid" + "/image").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeImageByIdGivenRecipeThenReturnsRecipeImageStoredWithSameNameAsRecipeId() {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        String recipeId1 = "recipe1";
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions(recipeId1, createRecipeDefinition("name1"), "recipe2", createRecipeDefinition("name2")));
        Path recipeImage = createRecipeImagePath(cookbookPath, recipeId1);
        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + recipeId1 + "/image")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.IMAGE_JPEG)
                .expectBody(byte[].class)
                .consumeWith(r -> assertThat(r.getResponseBody()).satisfies(c -> assertThat(c).isEqualTo(readFileContent(recipeImage))));
    }

    @SneakyThrows
    private byte[] readFileContent(Path recipeImage) {
        return Files.readAllBytes(recipeImage);
    }

    @Test
    public void testGetRecipeThumbnailByIdGivenNoCookbookWithMatchingIdThenReturnsNotFound() throws Exception {
        webTestClient.get().uri("/api/cookbook/MjAyMS0wNi0xMVQxOTo0NzozNS43MjUwODlafEJPT0sz/recipe/1234/thumbnail").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeThumbnailByIdGivenNoRecipeWithMatchingIdThenReturnsNotFound() throws Exception {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions("recipe1", createRecipeDefinition("name1")));
        webTestClient.get().uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + "notExistingUid" + "/thumbnail").exchange().expectStatus().isNotFound();
    }

    @Test
    public void testGetRecipeThumbnailByIdGivenRecipeThenReturnsRecipeThumbnailStoredWithSameNameAsRecipeId() {
        String cookbookName = "BOOK" + RandomStringUtils.randomNumeric(5, 5);
        String recipeId1 = "recipe1";
        Path cookbookPath = setUpCookbook(cookbookName, createRecipeDefinitions(recipeId1, createRecipeDefinition("name1"), "recipe2", createRecipeDefinition("name2")));
        Path recipeThumbnail = createRecipeThumbnailPath(cookbookPath, recipeId1);
        webTestClient
                .get()
                .uri("/api/cookbook/" + cookbookReactiveService.toCookbookId(cookbookPath) + "/recipe/" + recipeId1 + "/thumbnail")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.IMAGE_JPEG)
                .expectBody(byte[].class)
                .consumeWith(r -> assertThat(r.getResponseBody()).satisfies(c -> assertThat(c).isEqualTo(readFileContent(recipeThumbnail))));
    }
}
