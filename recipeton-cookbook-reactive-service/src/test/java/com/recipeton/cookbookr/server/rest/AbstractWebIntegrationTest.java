package com.recipeton.cookbookr.server.rest;

/*-
 * #%L
 * recipeton-cookbook-reactive-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbookr.service.CookbookReactiveService;
import com.recipeton.cookbookr.test.support.AbstractIntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.test.web.reactive.server.WebTestClient;

// @WebFluxTest // Alternative only for controllers with mocks
@AutoConfigureWebTestClient
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

    @Autowired protected WebTestClient webTestClient;

    //    @BeforeEach
    //    public void setup() {
    //        this.webTestClient = WebTestClient
    //                .bindToApplicationContext(this.applicationContext)
    //                .configureClient()
    //                .build();
    //    }
}
