#!/bin/sh
# create and update cookstick images
# place modified losetup in same dir
DIR=~/recipeton/tmdata
KEY=12345678 # replace with real key
for recipe_dir in $DIR/*; do
  name=${recipe_dir##*/}
  new_db_hash=$(md5sum $recipe_dir/export/ext.sdb | awk '{ print $1 }')
  old_db_hash=$(cat $recipe_dir/db.md5)
  if [ "$old_db_hash" = "$new_db_hash" ]; then
    new_photos_hash=$(ls $recipe_dir/export/material/photo/150x150 | md5sum | awk '{ print $1 }')
    old_photos_hash=$(cat $recipe_dir/photos.md5)
    if [ "$old_photos_hash" = "$new_photos_hash" ]; then
      # nothing changed, no need to build the image again
      break
    fi
  fi
  mksquashfs $recipe_dir/export/ $recipe_dir/recipes.squashfs -comp lzo -noappend
  fs_size=$(ls -l $recipe_dir/recipes.squashfs | awk '{ print $5}')
  cp $DIR/shared/resized.iso $recipe_dir/$name.iso
  python resize_encrypted_image.py $recipe_dir/$name.iso $fs_size

  sudo modprobe cryptoloop
  loopdev=$(losetup -f)
  sudo ./losetup $loopdev $recipe_dir/$name.iso $KEY
  sudo dd if=$recipe_dir/recipes.squashfs of=$loopdev bs=1M
  sudo losetup -d $loopdev
  rm -f $recipe_dir/recipes.squashfs

  ls $recipe_dir/export/material/photo/150x150 | md5sum | awk '{ print $1 }' >$recipe_dir/photos.md5
  md5sum $recipe_dir/export/ext.sdb | awk '{ print $1 }' >$recipe_dir/db.md5
done
