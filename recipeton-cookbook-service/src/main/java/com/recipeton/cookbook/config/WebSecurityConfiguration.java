package com.recipeton.cookbook.config;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration {

    @Value("${server.httpsRequired:false}")
    private boolean httpsRequired;

    @Value("${recipeton.server.security.enabled:true}")
    private boolean securityEnabled;

    @Value("${recipeton.server.security.username}")
    private String username;

    @Value("${recipeton.server.security.password}")
    private String password;

    @Value("${recipeton.server.security.role:ROLE_USER}")
    private String role;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.httpBasic();

        if (!securityEnabled) {
            http.anonymous().principal("guest").authorities(role);
        }

        http.authorizeRequests()
                .mvcMatchers(HttpMethod.POST, "/api/cookbook/{cookbookId}/publish")
                .hasRole("USER") // Workaround for native (Cant use annotation)
                .antMatchers("/index.html", "/", "/user", "/images/**", "/js/**", "/webjars/**")
                .permitAll();

        if (httpsRequired) {
            http.requiresChannel().requestMatchers(r -> r.getHeader("X-Forwarded-Proto") != null).requiresSecure();
        }

        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        return new InMemoryUserDetailsManager(
                User.withUsername(username)
                        .password(passwordEncoder().encode(password))
                        //            .roles("USER")
                        .authorities(role)
                        .build());
    }

    /// Not recommended! Using permitAll instead
    //    @Bean
    //    public WebSecurityCustomizer webSecurityCustomizer() {
    //        return web -> web.ignoring().antMatchers("/images/**", "/js/**", "/webjars/**");
    //    }
}
