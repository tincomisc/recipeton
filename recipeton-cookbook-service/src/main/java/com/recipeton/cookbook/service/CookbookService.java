package com.recipeton.cookbook.service;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbook.config.ApplicationConfiguration;
import com.recipeton.cookbook.server.rest.ResourceNotFoundException;
import com.recipeton.cookbook.util.ProcessHelper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CookbookService {

    private final ApplicationConfiguration applicationConfiguration;
    private final JsonResourceService jsonResourceService;
    private final AtomicBoolean publishing = new AtomicBoolean();
    private String mountedCookbookId;

    private int doMassStorageMount(Path path) throws IOException, InterruptedException {
        String usbSerial = applicationConfiguration.getUsbSerial();
        if (applicationConfiguration.isUsbSerialFromIsoSibling()) {
            Path serialPath = path.getParent().resolve(applicationConfiguration.getUsbSerialFromIsoSiblingFileName());
            log.debug("Looking up serial sibling file");
            if (Files.exists(serialPath)) {
                usbSerial = Files.readString(serialPath);
            } else {
                log.warn("doMassStorageMount. Serial sibling file not found. Fallback to configured serial. path={}", serialPath);
            }
        }

        String mountCommand =
                String.format(
                        "sudo modprobe -v g_mass_storage file=%s stall=0 idVendor=%s idProduct=%s iManufacturer=\"%s\" iProduct=\"%s\" iSerialNumber=\"%s\" bcdDevice=%s cdrom=y ro=y\n",
                        path,
                        applicationConfiguration.getUsbVendor(),
                        applicationConfiguration.getUsbProduct(),
                        applicationConfiguration.getUsbManufacturer(),
                        applicationConfiguration.getUsbProductString(),
                        usbSerial,
                        applicationConfiguration.getUsbBcdDevice());
        return ProcessHelper.exec(mountCommand, c -> log.info("exec:" + c));
    }

    private int doMassStorageUmount() throws IOException, InterruptedException {
        String umountCommand = "sudo rmmod -v g_mass_storage";
        return ProcessHelper.exec(umountCommand, c -> log.info("exec:" + c));
    }

    public void doPublish(String cookbookId) throws CookbookPublishException {
        Path cookbookPath = getCookbookPathsById(cookbookId).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));

        log.info("doPublish id={}", cookbookId);
        if (publishing.compareAndSet(false, true)) {
            if (mountedCookbookId != null && mountedCookbookId.equals(cookbookId)) {
                log.warn("doPublish same cookbookPath. Ignoring. cookbookPath={}", mountedCookbookId);
                publishing.set(false);
                return;
            }

            Path cookbookImagePath = cookbookPath.resolve(cookbookId + FilenameUtils.EXTENSION_SEPARATOR + applicationConfiguration.getCookbookImageExtension());
            try {
                int umountCode = doMassStorageUmount();
                log.debug("umount executed. code={}", umountCode);
                int mountResult = doMassStorageMount(cookbookImagePath);
                log.debug("mount executed. code={}", mountResult);

                mountedCookbookId = cookbookId;
            } catch (InterruptedException | IOException e) {
                mountedCookbookId = null;
                throw new CookbookPublishException("Publish failed for id=" + cookbookImagePath.getFileName() + " , reason=" + e.getMessage(), e);
            } finally {
                publishing.set(false);
            }
        } else {
            log.warn("Already publishing. Ignore. cookbookId={}", cookbookId);
        }
    }

    public Optional<Path> getCookbookContentPath(String cookbookId) {
        return getCookbookPathsById(cookbookId).map(p -> p.resolve(FilenameUtils.getBaseName(p.getFileName().toString()) + FilenameUtils.EXTENSION_SEPARATOR + applicationConfiguration.getCookbookContentsExtension()));
    }

    public String getCookbookId(Path path) {
        return FilenameUtils.getBaseName(path.getFileName().toString());
    }

    public List<Path> getCookbookPaths() throws IOException {
        if (Files.exists(applicationConfiguration.getDataRootPath())) {
            return Files.walk(applicationConfiguration.getDataRootPath(), 2)
                    .filter(p -> FilenameUtils.isExtension(p.getFileName().toString().toLowerCase(), applicationConfiguration.getCookbookImageExtension()))
                    .filter(p -> p.getParent().getFileName().toString().equals(FilenameUtils.getBaseName(p.getFileName().toString())))
                    .map(Path::getParent)
                    .filter(p -> !applicationConfiguration.getDataRootPathExclusions().contains(FilenameUtils.getBaseName(p.getFileName().toString())))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public Optional<Path> getCookbookPathsById(String cookbookId) {
        Path fullPath = applicationConfiguration.getDataRootPath().resolve(cookbookId);
        if (Files.exists(fullPath)) {
            return Optional.of(fullPath);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Path> getCookbookRecipeDefinitionPath(String cookbookId, String recipeId) {
        return getCookbookPathsById(cookbookId).map(p -> p.resolve("recipe").resolve(recipeId + ".yaml"));
    }

    public Optional<Path> getCookbookRecipeImagePath(String cookbookId, String recipeId) {
        // Should validate that absolute path hangs from data
        return getCookbookPathsById(cookbookId).map(p -> p.resolve("recipe").resolve(recipeId + ".jpg"));
    }

    public Optional<Path> getCookbookRecipeThumbnailPath(String cookbookId, String recipeId) {
        // Should validate that absolute path hangs from data
        return getCookbookPathsById(cookbookId).map(p -> p.resolve("export/material/photo/150x150").resolve(recipeId + ".jpg"));
    }

    public String getMountedCookbookId() {
        return mountedCookbookId;
    }

    public List<RecipeSummary> getRecipeSummaries(String cookbookId) throws IOException {
        Path path = getCookbookContentPath(cookbookId).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));
        return jsonResourceService.readCollection(path, RecipeSummary.class);
    }

    @PostConstruct
    public void postConstruct() throws IOException {
        Files.createDirectories(applicationConfiguration.getDataRootPath());
    }

    @Data
    public static class RecipeSummary {
        private String uid;
        private String title;
        private List<String> keywords;
    }
}
