package com.recipeton.cookbook.server.rest;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.cookbook.server.rest.form.CookbookContentsForm;
import com.recipeton.cookbook.server.rest.form.CookbookForm;
import com.recipeton.cookbook.server.rest.form.CookbookPublishRequestForm;
import com.recipeton.cookbook.server.rest.form.RecipeSummaryForm;
import com.recipeton.cookbook.service.CookbookPublishException;
import com.recipeton.cookbook.service.CookbookService;
import com.recipeton.shared.domain.RecipeDefinition;
import com.recipeton.shared.service.RecipeDefinitionStorageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/cookbook")
@Tag(name = "cookbook", description = "Recipe collections (cookbooks)")
@RequiredArgsConstructor
public class CookbookController {

    public static final String SPLITTER = "|";
    public static final Duration CACHE_DURATION = Duration.ofDays(365);
    private final CookbookService cookbookService;
    private final RecipeDefinitionStorageService recipeDefinitionStorageService;

    @GetMapping
    public ResponseEntity<List<CookbookForm>> getAll() throws IOException {
        List<CookbookForm> cookbookForms = cookbookService.getCookbookPaths().stream().sorted().map(this::toCookbookForm).collect(Collectors.toList());
        if (cookbookForms.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(cookbookForms, HttpStatus.OK);
    }

    @Operation(
            summary = "Get recipe collection by id",
            description = "Returns recipe collection with matching id",
            tags = {"recipeCollection"})
    @ApiResponses(
            value = {
                @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CookbookForm.class))),
                @ApiResponse(responseCode = "404", description = "recipe collection not found"),
                @ApiResponse(responseCode = "405", description = "Validation exception")
            })
    @GetMapping("/{cookbookId}")
    public CookbookForm getById(@Parameter(description = "identifier") @PathVariable String cookbookId) {
        Path path = cookbookService.getCookbookPathsById(toCookbookId(cookbookId)).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));
        return toCookbookForm(path);
    }

    @GetMapping("/{cookbookId}/content")
    public CookbookContentsForm getContentById(@Parameter(description = "identifier") @PathVariable String cookbookId) throws IOException {
        List<CookbookService.RecipeSummary> contents = cookbookService.getRecipeSummaries(toCookbookId(cookbookId));
        return CookbookContentsForm.builder().contents(contents.stream().map(s -> new RecipeSummaryForm(s.getUid(), s.getTitle(), s.getKeywords())).collect(Collectors.toList())).build();
    }

    @GetMapping(value = "/{cookbookId}/recipe/{recipeId}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> getImageByCookbookAndRecipeId(@PathVariable("cookbookId") String cookbookId, @PathVariable("recipeId") String recipeId) throws IOException {
        Path path = cookbookService.getCookbookRecipeImagePath(toCookbookId(cookbookId), recipeId).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));
        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(CACHE_DURATION)).contentType(MediaType.IMAGE_JPEG).body(new InputStreamResource(Files.newInputStream(path)));
    }

    @GetMapping(value = "/{cookbookId}/recipe/{recipeId}/thumbnail", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> getThumbnailByCookbookAndRecipeId(@PathVariable("cookbookId") String cookbookId, @PathVariable("recipeId") String recipeId) throws IOException {
        Path path = cookbookService.getCookbookRecipeThumbnailPath(toCookbookId(cookbookId), recipeId).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));
        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(CACHE_DURATION)).contentType(MediaType.IMAGE_JPEG).body(new InputStreamResource(Files.newInputStream(path)));
    }

    @GetMapping("/{cookbookId}/recipe/{recipeId}")
    @ResponseBody
    public RecipeDefinition getRecipeDefinitionByCookbookAndRecipeId(@PathVariable("cookbookId") String cookbookId, @PathVariable("recipeId") String recipeId) throws IOException {
        Path path = cookbookService.getCookbookRecipeDefinitionPath(toCookbookId(cookbookId), recipeId).orElseThrow(() -> new ResourceNotFoundException("Could not find resource with id=" + cookbookId));
        return recipeDefinitionStorageService.read(path);
    }

    @Operation(
            summary = "Publish recipe collection",
            tags = {"recipeCollection"})
    @ApiResponses(
            value = {
                @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CookbookForm.class))),
                @ApiResponse(responseCode = "500", description = "error performing operation")
            })
    @PostMapping("/{cookbookId}/publish")
    //    @PreAuthorize("hasRole('ROLE_USER')") // Disabled for native
    //    @RolesAllowed("ROLE_USER")
    public ResponseEntity<?> postPublishRequest(@PathVariable("cookbookId") String cookbookId, @RequestBody CookbookPublishRequestForm cookbookPublishRequestForm) {
        try {
            cookbookService.doPublish(toCookbookId(cookbookId));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (CookbookPublishException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String toCookbookEncodedId(Path path) throws IOException {
        String text = Files.getLastModifiedTime(path) + SPLITTER + path.getFileName().toString();
        return Base64.getUrlEncoder().withoutPadding().encodeToString(text.getBytes(StandardCharsets.UTF_8));
    }

    private CookbookForm toCookbookForm(Path cookbookPath) {
        try {
            String encodedId = toCookbookEncodedId(cookbookPath);
            String name = StringUtils.replace(FilenameUtils.getBaseName(cookbookPath.getFileName().toString()), "_", " ");
            String cookbookId = cookbookService.getCookbookId(cookbookPath);
            return CookbookForm.builder().id(encodedId).name(name).published(cookbookId.equals(cookbookService.getMountedCookbookId())).build();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String toCookbookId(String raw) {
        String decoded = new String(Base64.getUrlDecoder().decode(raw));
        return StringUtils.split(decoded, SPLITTER)[1];
    }
}
